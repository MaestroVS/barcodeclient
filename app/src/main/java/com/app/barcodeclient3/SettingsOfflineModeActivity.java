package com.app.barcodeclient3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import v1.data_model.Callback;
import v1.data_model.DataModelOffline;
import v1.data_model.Response;
import v1.data_model_offline_old.Subdivision;
import v1.data_model.GoodJSON;
import v2.data.firebird.models.Inventory;
import v1.dialogs.DownloadGoodsProgressDialog;

import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.OPT_CONNECT_MODE;
import static v1.data_model.DataModelInterface.ALL;
import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;

import static com.app.barcodeclient3.MainApplication.OPT_UNS_CONNECT_ONLINE;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.MainApplication.unsConnectOnline;
import static com.app.barcodeclient3.MainApplication.writeOption;

public class SettingsOfflineModeActivity extends AppCompatActivity {


    Switch checkOffline;
    TextView mobileGoodsCnt;
    TextView serverGoodsCnt;
    ProgressBar progressBar;
    Button importGoodsBt;
    Button  importInvlistBt;

    Button clearBt;

    Handler h = new Handler();
    int serverGoodsCount = 0;
    int mobileGoodsCount = 0;

    boolean enableOffline = false;

    ArrayList<Inventory> invlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_offline);

        checkOffline = findViewById(R.id.checkOffline);

        mobileGoodsCnt = findViewById(R.id.mobileGoodsCnt);
        serverGoodsCnt = findViewById(R.id.serverGoodsCnt);
        progressBar =  findViewById(R.id.progressBar);
        importGoodsBt = findViewById(R.id.importGoodsBt);
        importInvlistBt = findViewById(R.id.importInvlistBt);

        clearBt = findViewById(R.id.clearBt);

        //----
        getSupportActionBar().setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);

        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        View abarView = getSupportActionBar().getCustomView();


        TextView titleBar = abarView.findViewById(R.id.abarTitle);
        titleBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        titleBar.setText(getString(R.string.settings));

        ImageView statusButton = abarView.findViewById(R.id.statusButton);
        statusButton.setVisibility(View.GONE);

        ImageView settingsButton = abarView.findViewById(R.id.settingsButton);
        settingsButton.setVisibility(View.INVISIBLE);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);


        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);




        checkOffline.setChecked(!unsConnectOnline);
        checkOffline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                Log.d(my, "Check offline ");
                if (checked) {

                  //  connectMode = CONNECT_OFFLINE;

                    return;

                } else {
                    //offlineIcon.setVisibility(View.GONE);
                    // MainApplication.dbHelper.clear_INVENTORIES();
                    //serverLinearDetails.setVisibility(View.VISIBLE);
                    //MainApplication.OFFLINE_MODE = false;
                   // MainApplication.dbHelper.insertOrReplaceOption("OFFLINE_MODE", "0");
                }


            }
        });


        importGoodsBt.setOnClickListener(view -> {
            Log.d("my", "server good count = " + serverGoodsCount);

            progressBar.setProgress(0);

            if (connectMode == CONNECT_ONLINE) {
                Log.d(my, "Load all goods online");
                uploadAndSaveGoodsOnline();
            }
        });

        importInvlistBt.setOnClickListener(view -> {
            Log.d("my", "server good count = " + serverGoodsCount);

            progressBar.setProgress(0);

            if (connectMode == CONNECT_ONLINE) {
                Log.d(my, "Load all invs online");
                updateInventories();

            }
        });

        clearBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsOfflineModeActivity.this);
                builder.setTitle("Удалить?");
                builder.setMessage("Все справочники  будут удалены");
                builder.setCancelable(true);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() { // Кнопка ОК
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EmptyTable emptyTable = new EmptyTable();
                        emptyTable.start();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() { // Кнопка ОК
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss(); // Отпускает диалоговое окно
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });




    }


    @Override
    protected void onResume() {
        super.onResume();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateGoodsCountOnDevice();
                updateGoodsCountOnServerOnline();
            }
        },500);
    }

    class EmptyTable extends Thread {
        @Override
        public void run() {


            MainApplication.dbHelper.clearDic_Goods();
            MainApplication.dbHelper.clearDic_Goods_GRP();

            MainApplication.dbHelper.clear_INVENTORIES();
            h.post(new Runnable() {
                @Override
                public void run() {
                    mobileGoodsCnt.setText("" + mobileGoodsCount);


                    LoadGoodsMobileCount loadGoodsMobileCount = new LoadGoodsMobileCount();
                    loadGoodsMobileCount.start();
                }
            });

        }
    }


    private void updateInventories() {
        invlist.clear();
        Log.d("my", "updateInventories...");
        Log.d(my, "DataModelOffline.getInstance() = " + DataModelOffline.getInstance().getClass().toString());
        MainApplication.getApi().getInventoryList(new Callback<ArrayList<Inventory>>() {
            @Override
            public boolean sendResult(Response<ArrayList<Inventory>> response) {

                invlist = response.body();
                seveInventories();

                return false;
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void seveInventories() {
        //write inventories...
        Log.d("my", "write Inventories...");
        for (Inventory inv : invlist) {
            Log.d("my", "insert to db id inv= " + inv.getId());
            Log.d("my", "insert to db num inv= " + inv.getNum());
            Subdivision subdiv = new Subdivision(0, 0, inv.getSubdivisionName());
            int docType = 0;
            String ans = MainApplication.dbHelper.insertInventoryDoc(Math.toIntExact(inv.getId()), ""+inv.getId(), inv.getDateTime(), subdiv.getName(), docType);
        }
    }



    class LoadGoodsMobileCount extends Thread {
        @Override
        public void run() {

           /* String value = MainApplication.dbHelper.getOption(oldbarcodestartactivity.MainActivity.not_ask_create_good);
            if (value == null) {
                MainApplication.dbHelper.insertOrReplaceOption(oldbarcodestartactivity.MainActivity.not_ask_create_good, "0");
                value = MainApplication.dbHelper.getOption(oldbarcodestartactivity.MainActivity.not_ask_create_good);

            }*/

            int goodsCnt = MainApplication.dbHelper.countOfGoods();
            Log.d("my", "goodsCnt = " + goodsCnt);
            mobileGoodsCount = goodsCnt;
            h.post(new Runnable() {
                @Override
                public void run() {
                    mobileGoodsCnt.setText("" + mobileGoodsCount);


                    ;
                }
            });

        }
    }






    private void updateGoodsCountOnServerOnline(){
        Log.d(my,"updateGoodsCountOnServerOnline");

        int goodsDeviceCnt = MainApplication.dbHelper.countOfGoods();
        mobileGoodsCnt.setText(""+goodsDeviceCnt);
        Log.d(my,"updateGoodsCountOnServerOnline getGoodsCount...");
        MainApplication.getApi().getGoodsCount(new Callback<Integer>() {
            @Override
            public boolean sendResult(Response<Integer> response) {
                Log.d(my,"updateGoodsCountOnServerOnline sendResult...");
                if (response != null) {
                    Log.d(my,"updateGoodsCountOnServerOnline response ok");
                    Log.d("my", "<<<<" + response.body());
                    int count = response.body();
                    serverGoodsCount = count;
                    serverGoodsCnt.setText("" + serverGoodsCount);
                } else {
                    Log.d(my,"updateGoodsCountOnServerOnline response err");
                    Log.d("my", "<<<<null");
                }

                return false;
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void updateGoodsCountOnDevice() {
        Log.d(my, "updateGoodsCountOnDevice");

        int goodsDeviceCnt = MainApplication.dbHelper.countOfGoods();
        mobileGoodsCnt.setText("" + goodsDeviceCnt);
    }

    int progress=0;

    private void uploadAndSaveGoodsOnline(){
        Map<String, String> data = new HashMap<>();

        data.put(ALL, "ALL");
        MainApplication.dbHelper.clear_AC_GOODS_CNT();
        MainApplication.dbHelper.clearDic_Goods();
        MainApplication.dbHelper.clear_INVENTORIES();

        showImportDialog();


        for(Inventory inventoryJSON: invlist){
            MainApplication.dbHelper.insertInventoryDoc(""+inventoryJSON.getId(),inventoryJSON.getDateTime(),inventoryJSON.getSubdivisionName(),inventoryJSON.getDoc_state());
        }

        Log.d("my", "goodsUns .getApi() ");
        MainApplication.getApi().getGoodsListAndSaveToSQLite(data, SettingsOfflineModeActivity.this, new Callback<ArrayList<GoodJSON>>() {
            @Override
            public boolean sendResult(Response<ArrayList<GoodJSON>> response) {
                 ArrayList<GoodJSON> listFromUns = response.body();
                if (listFromUns == null) return false;

                Log.d("my", "goodsUns we get goods list !!!! " + listFromUns.size());
                progressBar.setMax(listFromUns.size());



                Log.d("my", "writed goods to DB !");
                updateGoodsCountOnServerOnline();
                dismissImportDialog();
                //  h.post(readyGoodsHandler);
                return false;
            }

            @Override
            public void onError(String error) {
                Log.d(my,"progress == "+error+"%");
                try{

                    final int progress = Integer.parseInt(error);

                    runOnUiThread(()->{ progressDialog.setProgress(progress);});

                }catch (Exception e){
                    Log.d(my,"progress == err"+e);
                }
            }
        });
    }





    DownloadGoodsProgressDialog progressDialog;



    private void showImportDialog() {

         progressDialog = new DownloadGoodsProgressDialog();
        progressDialog.setCancelable(false);


        progressDialog.show(getFragmentManager(), "Wait");
    }

    private void dismissImportDialog() {
        if (progressDialog != null) progressDialog.dismiss();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.


           /* if(checkOffline.isChecked()) getIntent().putExtra("TADA", "offline");
            if(!checkOffline.isChecked()) getIntent().putExtra("TADA", "");
            setResult(RESULT_OK, getIntent());*/
            finish();
          /*  if(checkOffline.isChecked()){
                connectMode = CONNECT_OFFLINE;
            }
            finish();*/
            saveOptions();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                /*Intent returnIntent = new Intent();

                returnIntent.putExtra("result", 11114);
                setResult(Activity.RESULT_OK, returnIntent);

                if(checkOffline.isChecked()){
                    connectMode = CONNECT_OFFLINE;
                }
                finish();*/
                saveOptions();

               /* if(checkOffline.isChecked()) getIntent().putExtra("TADA", "offline");
                if(!checkOffline.isChecked()) getIntent().putExtra("TADA", "");
                setResult(RESULT_OK, getIntent());*/
                finish();

                return true;
        }


        return super.onOptionsItemSelected(item);

    }


    private void saveOptions()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();

        int online = 1;
        if(checkOffline.isChecked()) online=0;
        else online = 1;

        Log.d("my","save options: mode = "+online);

       // editor.putInt(OPT_UNS_CONNECT_ONLINE,online);
        //editor.commit();

        writeOption(OPT_UNS_CONNECT_ONLINE,online);
        if(checkOffline.isChecked())writeOption(OPT_CONNECT_MODE,CONNECT_OFFLINE);
        else writeOption(OPT_CONNECT_MODE,CONNECT_ONLINE);

    }
}
