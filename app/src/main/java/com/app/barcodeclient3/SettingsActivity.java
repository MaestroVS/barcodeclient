package com.app.barcodeclient3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;
import v2.presentation.activation.ActivationActivity;
import v2.presentation.screens.ModeActivity;
import v2.presentation.screens.ScreenModeOptions;
import v2.presentation.screens.dashboard.settings.TSDScannerActivity;

import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;

import static com.app.barcodeclient3.MainApplication.OPT_CONNECT_MODE;
import static com.app.barcodeclient3.MainApplication.OPT_IP;
import static com.app.barcodeclient3.MainApplication.OPT_PATH;
import static com.app.barcodeclient3.MainApplication.THEME_BLUE_GREY;
import static com.app.barcodeclient3.MainApplication.THEME_INDIGO;
import static com.app.barcodeclient3.MainApplication.THEME_RED;
import static com.app.barcodeclient3.MainApplication.THEME_TEAL;
import static com.app.barcodeclient3.MainApplication.checkLicense;

import static com.app.barcodeclient3.MainApplication.databaseIP;
import static com.app.barcodeclient3.MainApplication.databasePath;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.dbHelper;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.MainApplication.readOption;
import static com.app.barcodeclient3.MainApplication.unsConnectOnline;

public class SettingsActivity extends AppCompatActivity {

    CheckBox offlineCheckBox;

    CheckBox onlineCheckBox;
    LinearLayout connectLayout;
    EditText ipTV;
    EditText pathTV;

    CheckBox barcodeCheckBox;
    LinearLayout connectBarcLayout;
   EditText ipbarcodeTV;

    Button deleteData;

    Button aboutBt;

     Button activationButton;

    static int ACTIVATION_ACTIVITY_REQUEST = 65821;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String themeS = readOption("THEME","0");
        Log.d("theme","themeS th = "+themeS);
        int theme = THEME_RED;
        try{
            theme = Integer.parseInt(themeS);
        }catch (Exception e){
            Log.d("theme","err th = "+e.toString());
        }

        String themeShowS = readOption("THEME_SHOW","1");
        Log.d("theme","themeShow th = "+themeShowS);


        Log.d("theme","theme th = "+theme);

        switch (theme){

            case THEME_RED:
                setTheme(R.style.MainActivityThemeRed);
                break;
            case THEME_TEAL:
                setTheme(R.style.MainActivityThemeTeal);
                break;
            case THEME_INDIGO:
                setTheme(R.style.MainActivityThemeIndigo);
                break;
            case THEME_BLUE_GREY:
                setTheme(R.style.MainActivityThemeBlueGrey);
                break;
        }

        setTheme(R.style.MainActivityThemeTeal); 


        setContentView(R.layout.activity_settings);




        offlineCheckBox =findViewById(R.id.offlineCheckBox);

        onlineCheckBox = findViewById(R.id.onlineCheclBox);
        connectLayout = findViewById(R.id.connectLayout);
        ipTV = findViewById(R.id.ipTV);
        pathTV = findViewById(R.id.pathTV);

        barcodeCheckBox = findViewById(R.id.barcodeCheckBox);
        connectBarcLayout = findViewById(R.id.connectBarcLayout);
        ipbarcodeTV = findViewById(R.id.ipbarcodeTV);

        deleteData = findViewById(R.id.deleteData);
        aboutBt = findViewById(R.id.aboutBt);


        activationButton = findViewById(R.id.activationButtonSt);






        //----
        getSupportActionBar().setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);

        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        View abarView = getSupportActionBar().getCustomView();


        TextView titleBar = abarView.findViewById(R.id.abarTitle);
        titleBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        titleBar.setText(getString(R.string.settings));

        ImageView statusButton = abarView.findViewById(R.id.statusButton);
        statusButton.setVisibility(View.GONE);

        ImageView settingsButton = abarView.findViewById(R.id.settingsButton);
        settingsButton.setVisibility(View.INVISIBLE);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);

        modifyIpButton(ipTV);

        findViewById(R.id.styleRed).setOnClickListener((v)->{ changeTheme(THEME_RED);});
        findViewById(R.id.styleTeal).setOnClickListener((v)->{ changeTheme(THEME_TEAL);});
        findViewById(R.id.styleIndigo).setOnClickListener((v)->{ changeTheme(THEME_INDIGO);});
        findViewById(R.id.styleBlueGrey).setOnClickListener((v)->{ changeTheme(THEME_BLUE_GREY);});


        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);


        offlineCheckBox.setChecked(connectMode==CONNECT_OFFLINE&&unsConnectOnline?true:false);

        onlineCheckBox.setChecked(connectMode==CONNECT_ONLINE||!unsConnectOnline?true:false);
        connectLayout.setVisibility(onlineCheckBox.isChecked()?View.VISIBLE:View.GONE);
        connectBarcLayout.setVisibility(barcodeCheckBox.isChecked()?View.VISIBLE:View.GONE);



        refreshLicenseButtonUI();

        activationButton.setOnClickListener(v -> {
            Log.d("my","activation");
            startActivityForResult(ActivationActivity.Companion.getCallingIntent(SettingsActivity.this),ACTIVATION_ACTIVITY_REQUEST);
        });
        

        offlineCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    connectLayout.setVisibility(View.GONE);
                    connectBarcLayout.setVisibility(View.GONE);
                    barcodeCheckBox.setChecked(false);
                    onlineCheckBox.setChecked(false);
                }

            }
        });


        onlineCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    connectLayout.setVisibility(View.VISIBLE);
                    connectBarcLayout.setVisibility(View.GONE);
                    barcodeCheckBox.setChecked(false);
                    offlineCheckBox.setChecked(false);
                }
                else connectLayout.setVisibility(View.GONE);
            }
        });

        if(databaseIP.length()==0&&databasePath.length()==0){
            ipTV.setTextColor(getResources().getColor(R.color.ash));
            pathTV.setTextColor(getResources().getColor(R.color.ash));
            ipTV.setText("192.168.0.1");
            pathTV.setText("D:/Unisystem/DB/DB.FDB");
        }else {
            ipTV.setText(databaseIP);
            pathTV.setText(databasePath);
        }


        //---

        aboutBt.setOnClickListener((view -> {

            String version ="";
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                 version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }



            Element element = new Element("Build version "+version,R.drawable.apk_64);
            if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.LOLLIPOP){
                element = new Element("Build version "+version,null);
            }

            View aboutPage = new AboutPage(this)
                    .isRTL(false)
                    .setImage(R.drawable.new_icon_112)
                    .addItem(element)
                    .setDescription(getString(R.string.app_description))
                   // .addItem(adsElement)
                    .addGroup(getString(R.string.connect_with_us))
                    .addEmail("scanncode@gmail.com")
                   // .addWebsite("http://medyo.github.io/")
                   // .addFacebook("the.medy")
                   // .addTwitter("medyo80")
                   // .addYoutube("UCdPQtdWIsg7_pi4mrRu46vA")
                    .addPlayStore("com.app.barcodeclient3")
                    //.addGitHub("medyo")
                    //.addInstagram("medyo80")
                    .create();

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

            dialogBuilder.setView(aboutPage);


            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

        }));

        findViewById(R.id.tsdScannerSettings).setOnClickListener(view ->
                startActivity(TSDScannerActivity.Companion.getCallingIntent(SettingsActivity.this)));

        findViewById(R.id.launchScreenOptions).setOnClickListener(view ->
                startActivity(ModeActivity.Companion.getCallingIntent(SettingsActivity.this)));




        ipbarcodeTV.setText(databaseIP);

        barcodeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    connectBarcLayout.setVisibility(View.VISIBLE);
                    connectLayout.setVisibility(View.GONE);
                    onlineCheckBox.setChecked(false);
                    offlineCheckBox.setChecked(false);
                }
                else connectBarcLayout.setVisibility(View.GONE);
            }
        });


        deleteData.setOnClickListener((v)->{

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            dbHelper.clear_AC_GOODS_CNT();
                            dbHelper.clear_INVENTORIES();
                            dbHelper.clearDic_Goods();
                            dbHelper.clearDic_Goods_GRP();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
            builder.setMessage(getString(R.string.clear_dic)+"?  "+getString(R.string.goods)+","+getString(R.string.inventory_list)+"?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        });


    }


    private void refreshLicenseButtonUI(){
        activationButton.setVisibility(View.VISIBLE);
        if(checkLicense()){
            activationButton.setText(getString(R.string.license_check));
            activationButton.setTextColor(ContextCompat.getColor(this,R.color.blue));
        }else{
            activationButton.setText(getString(R.string.activate));
            activationButton.setTextColor(ContextCompat.getColor(this,R.color.orange_settings_err));

        }
    }

    private void saveOptions()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();
        String ip = ipTV.getText().toString();
        if(barcodeCheckBox.isChecked()) ip= ipbarcodeTV.getText().toString();
        String path=pathTV.getText().toString();

        editor.putString(OPT_IP, ip);
        editor.putString(OPT_PATH, path);
        int mode= CONNECT_OFFLINE;
        if(onlineCheckBox.isChecked()) mode=CONNECT_ONLINE;


        Log.d("my","save options: mode = "+mode);
        editor.putInt(OPT_CONNECT_MODE,mode);
        editor.commit();
        editor.apply();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            saveOptions();

            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", 11114);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                saveOptions();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", 11114);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
        }


        return super.onOptionsItemSelected(item);

    }

    private void modifyIpButton(EditText et)
    {


        et.setInputType(InputType.TYPE_CLASS_PHONE);
        et.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, android.text.Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                    if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (int i = 0; i < splits.length; i++) {
                            if (Integer.valueOf(splits[i]) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        }
        });

        et.addTextChangedListener(new TextWatcher() {
            boolean deleting = false;
            int lastCount = 0;

            @Override
            public void afterTextChanged(Editable s) {
                if (!deleting) {
                    String working = s.toString();
                    String[] split = working.split("\\.");
                    String string = split[split.length - 1];
                    if (string.length() == 3 || string.equalsIgnoreCase("0")
                            || (string.length() == 2 && Character.getNumericValue(string.charAt(0)) > 1)) {
                        s.append('.');
                        return;
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (lastCount < count) {
                    deleting = false;
                } else {
                    deleting = true;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing happens here
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ACTIVATION_ACTIVITY_REQUEST){
            refreshLicenseButtonUI();
        }
    }

    private void changeTheme(int theme){
        MainApplication.writeOption("THEME",""+theme);
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

}
