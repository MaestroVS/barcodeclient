package com.app.barcodeclient3;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.multidex.MultiDexApplication;

import android.text.TextUtils;
import android.util.Log;


import org.json.JSONArray;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import v1.data_model.DatabaseHelper;
import v1.data_model.DataModelInterface;
import v1.data_model.DataModelOffline;
import v1.data_model.DataModelOnline;
import v1.utils.AESHelper;
import v2.presentation.activation.utils.LicenseRepository;
import v2.di.components.AppComponent;
import v2.di.components.DaggerAppComponent;
import v2.di.modules.AppModule;


import org.json.JSONObject;

import static v1.excel.AndroidReadExcelActivity.my;

/**
 * Created by userd088 on 28.04.2016.
 */
public class MainApplication extends MultiDexApplication {

    private static Context context;

    public static  String SERVER_IP="SERVER_IP";
    public static  String SERVER_PORT="SERVER_PORT";



    public static final int NORMAL_SCAN = 0;
    public static final int PLUS_ONE_SCAN = 1;
    public static final int PLUS_FACT_SCAN = 2;

    public static final int THEME_RED = 0;
    public static final int THEME_TEAL = 1;
    public static final int THEME_INDIGO = 2;
    public static final int THEME_BLUE_GREY = 3;



    public static int CURRENT_SCAN_MODE=0;

    public static final int INVENTORY = 0;
    public static final int BILL_IN = 1;



    public static boolean firstStart = true;

    public static SQLiteDatabase sdb;
    public static DatabaseHelper dbHelper = null;

    public static String CAMERA_STATE = "CAMERA_STATE";
    public static String CAMERA_ON = "CAMERA_ON";
    public static String CAMERA_OFF = "CAMERA_OFF";
    public static int UPDATE_STATE = 18;
    public static int SETTINGS_STATE = 416;
    public static int OFFLINE_STATE = 419;
    public static int UNS_OFFLINE_STATE = 519;


    public static String databaseIP = "";
    public static String databasePath = "";

    public static final String OPT_IP = "IP";
    public static final String OPT_PATH = "PATH";
    public static final String OPT_CONNECT_MODE = "CONNECT_MODE";
    public static final String OPT_SERVER_OFFLINE = "SERVER_OFFLINE";
    public static final String OPT_LICENSE_HASH = "LICENSE_HASH";
    public static boolean serverOffline = false;
    //public static final String OPT_CONNECT_OFFLINE = "CONNECT_OFFLINE";
    // public static final String OPT_CONNECT_SERVER = "CONNECT_SERVER";
    // public static final String OPT_CONNECT_ONLINE = "CONNECT_ONLINE";
    public static final int CONNECT_OFFLINE = 0;
   // public static final int CONNECT_SERVER = 1;
    public static final int CONNECT_ONLINE = 2;

    public static int connectMode = CONNECT_OFFLINE;

    public static final String OPT_UNS_CONNECT_ONLINE = "UNS_CONNECT_ONLINE";
    public static boolean unsConnectOnline = true; //при подключении к юнс базе данных работаем онлайн или офлайн?


    public static Handler h = new Handler();


    public static String serverIP = "";
    public static String serverPort = "";
    public static String serverName = "BarcodeServer3";
    public static boolean OFFLINE_MODE = false;
    public static String mainURL = ""; //"http://192.168.65.156:8080/BarcodeServer3";//Main !!!!!!!!!!!!!!
    public static String WEIGTH_BARCODE_MASK = "";
    public static String WEIGTH_BARCODE = "";

    public static boolean NOT_CREATE_GOOD = false;

    private static DataModelInterface dataModel;

    public static double sendedCnt = 0;
    public static String sendedGoodsName = "";

    public static final int MAX_FREE_SCANS = 10;
    public static int CURRENT_FREE_SCANS = 0;

    public static boolean isDB_3_0_68_0_plus = false;

    //public static boolean isLicensed=false;

    //  private String rollbarId = "69b56ad85ef34390a56d40727dedf010";



    private static MainApplication app;
    private AppComponent appComponent;

    public static MainApplication app(){
        return app;
    }

    public AppComponent appComponent(){
        return appComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("my", "hello MainApplication!");

        // Rollbar.init(this, rollbarId, "production");



        context = getApplicationContext();

        //dagger
        app = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext())).build();


        Log.d("my", "hello MainApplication! 1");

        refreshOptions();

        Log.d("my", "hello MainApplication! 2");


        {

            refreshOldDBOptions();
        }

        // readyDBHandler = new ReadyDBHandler();
        // LoadDBThread loadDBThread = new LoadDBThread();
        // loadDBThread.start();




    }


    public static Context getAppContext() {

        return MainApplication.context;
    }

    public static void refreshOptions() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());

        Log.d("my", "All prefs = " + prefs.getAll().toString() + " \n METADATA_KEY_PREFERENCES = " + PreferenceManager.METADATA_KEY_PREFERENCES);


        // Log.d(my,"prefs.getBoolean(OPT_UNS_CONNECT_ONLINE,true) = "+prefs.getBoolean(OPT_UNS_CONNECT_ONLINE,true)); //Unable to create application com.app.barcodeclient3.MainApplication: java.lang.ClassCastException: java.lang.Integer cannot be cast to java.lang.Boolean

        try {
            connectMode = prefs.getInt(OPT_CONNECT_MODE, CONNECT_OFFLINE);
        } catch (Exception e) {
            Log.d(my, "error get options connectMode: " + e.toString());
        }

        try {
            databaseIP = prefs.getString(OPT_IP, "");
            serverIP = databaseIP;
        } catch (Exception e) {
            Log.d(my, "error get options databaseIP: " + e.toString());
        }

        try {
            databasePath = prefs.getString(OPT_PATH, "");

        } catch (Exception e) {
            Log.d(my, "error get options databasePath: " + e.toString());
        }

        try {
            serverOffline = prefs.getBoolean(OPT_SERVER_OFFLINE, false);
        } catch (Exception e) {
            Log.d(my, "error get options serverOffline: " + e.toString());
        }

        try {
            unsConnectOnline = true;
            int unsOnline = prefs.getInt(OPT_UNS_CONNECT_ONLINE, 1);
            if (unsOnline == 0) unsConnectOnline = false;
            Log.d(my,"OPT uns ="+unsOnline);
        } catch (Exception e) {
            Log.d(my, "error get options unsConnectOnline: " + e.toString());
        }

        /*for (Map.Entry<String, ?> key : prefs.getAll().entrySet()) {
            Object result = key.getValue();
            if (result instanceof Boolean) {
                //handle boolean
            } else if (result instanceof String) {
                //handle String
            } else if (result instanceof Integer) {
                //handle String
            }
          //  findPreference(key.getKey()).setSummary(result);
        }*/


        NOT_CREATE_GOOD = prefs.getBoolean("NOT_CREATE_GOOD", false);
        CAMERA_STATE = prefs.getString("CAMERA_STATE", "1");
        CURRENT_FREE_SCANS = prefs.getInt("CURRENT_FREE_SCANS", 0);
        Log.d("my", "Main App refresh options we get databaseIP = " + databaseIP);
        Log.d("my", "Main App refresh options we get CONNECT_MODE = " + connectMode);
        Log.d("my", "Main App refresh options we get path" + databasePath);
        Log.d("my", "Main App refresh options we get OPT_SERVER_OFFLINE" + serverOffline);
        Log.d("my", "Main App refresh options we get OPT_UNS_CONNECT_ONLINE" + unsConnectOnline);

        if (connectMode == CONNECT_OFFLINE) OFFLINE_MODE = true;
        else OFFLINE_MODE = false;


        int scanType = prefs.getInt(SettingScanModeActivity.scan_type, 0);
        CURRENT_SCAN_MODE = scanType;

        serverPort = "8080";
        mainURL = "http://" + serverIP + ":" + serverPort + "/" + serverName;
    }

    public static void writeOption(String param, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();
        Log.d("my", "save options: " + param + " = " + value);

        editor.putString(param, value);
        editor.commit();
        editor.apply();
    }

    public static void writeOption(String param, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();
        Log.d("my", "save options: " + param + " = " + value);

        editor.putInt(param, value);
        editor.commit();
        editor.apply();
    }

    public static void writeOption(String param, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();
        Log.d("my", "save options: " + param + " = " + value);

        editor.putBoolean(param, value);
        editor.commit();
        editor.apply();
    }

    public static String readOption(String param, String defValue) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
            return prefs.getString(param, defValue);
        }catch (Exception e){
            return "";
        }
    }

    public static void addFreeScan() {
        CURRENT_FREE_SCANS++;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = prefs.edit();
        Log.d("my", "save options: CURRENT_FREE_SCANS" + " = " + CURRENT_FREE_SCANS);
        editor.putInt("CURRENT_FREE_SCANS", CURRENT_FREE_SCANS);
        editor.commit();
        editor.apply();

    }

    private void refreshOldDBOptions() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();

        dbHelper = new DatabaseHelper(getApplicationContext(), "mydatabase.db", null, 1);
        sdb = dbHelper.getReadableDatabase();
        Log.d("my", "SDB + " + dbHelper.getReadableDatabase().toString());
        ContentValues newValues = new ContentValues();
        newValues.put(dbHelper.PARAMETER, "1");
        newValues.put(dbHelper.VALUE, "1");
        String ip = dbHelper.getOption("IP");//getServerIp();
        if (ip == null) {
            Log.d("my", "ip =null");
           /* serverIP = "";
            dbHelper.insertOrReplaceOption("IP", serverIP);//setServerIp(serverIP);
            startSettings = true;*/

        } else {
            Log.d("my", "ip =" + ip);
            serverIP = ip;
            connectMode = 1;
            OFFLINE_MODE = false;

            editor.putString(OPT_IP, ip);

            editor.commit();
            editor.apply();
            refreshOptions();

        }




        serverPort = "8080";
        mainURL = "http://" + serverIP + ":" + serverPort + "/" + serverName;

    }


    public static DataModelInterface getApi() {
        Log.d("my","getApi data model ="+dataModel+" connect mode = "+connectMode);

        if(connectMode==CONNECT_OFFLINE) return DataModelOffline.getInstance();
        if(connectMode==CONNECT_ONLINE){
            if(unsConnectOnline) return DataModelOnline.getInstance();
            else return DataModelOffline.getInstance();
        }

        /*switch (connectMode) {
            case CONNECT_OFFLINE:
                dataModel = DataModelOffline.getInstance();
                break;
            case CONNECT_SERVER:
                if (serverOffline) dataModel = DataModelOffline.getInstance();
                else dataModel = DataModelBarcodeServerConnect.getInstance();
                break;
            case CONNECT_ONLINE:
                if (serverOffline) dataModel = DataModelOffline.getInstance();
                dataModel = DataModelOnline.getInstance();
                break;
            default:
                break;
        }
        return dataModel;*/

        return DataModelOffline.getInstance();
    }

    private static Connection connection;

    private static Properties paramConnection;

    public static void disconnect() {
        if (connection != null) try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection = null;
    }

    public static Connection initConnect() {
        if (connection == null) connect();
        return connection;
    }

    private static void connect() {


        paramConnection = new Properties();
        paramConnection.setProperty("user", "SYSDBA");
        paramConnection.setProperty("password", "masterkey");
        paramConnection.setProperty("encoding", "UNICODE_FSS");
        String sCon = "jdbc:firebirdsql:" + MainApplication.databaseIP + "/3050:" + MainApplication.databasePath;//E:/Unisystem/DB/DB.FDB";
        try {
            // register Driver
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            // Get connection
            connection = DriverManager.getConnection(sCon, paramConnection);
            //And if you remove the comment in a row
            paramConnection.setProperty("encoding", "WIN1251");
            Log.d("my", "conn ok= " + connection.toString());


        } catch (Exception e) {
            Log.d("my", "conn err= " + e.toString());

        }

    }


    public static boolean isConnect() {
        if (connection == null) return false;
        else return true;
    }

    public static JSONArray executeStatement(String statement) {
        JSONArray jsonArray = null;
        try {
            if (connection == null) {
                String msg = "Error in connection with SQL server";
                Log.d("my", "query statement conn err " + msg);
                return jsonArray;
            }

            Statement st = connection.createStatement();
            Log.d(my, "executeStatement 1");
            ResultSet rs = st.executeQuery(statement);
            Log.d(my, "executeStatement 2");
            ResultSetMetaData rsmd = rs.getMetaData();
            Log.d(my, "executeStatement 3");
            int cols = rsmd.getColumnCount();
            Properties connInfo = new Properties();
            connInfo.put("charSet", "UNICODE_FSS");
            Log.d(my, "executeStatement 4");


            try {
                jsonArray = convertToJSON(rs);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("my", "executeSt jsonArr err = " + e.toString());
            }

            rs.close();
            st.close();
            Log.d(my, "executeStatement 5");


        } catch (SQLException e) {
            Log.d("my", "MainApplication KFDB.There are problems with the query ******");
            Log.d("my", "MainApplication KFDB. " + e.toString());
            e.printStackTrace();
        }

        return jsonArray;
    }


    public static int executeProcedure(String statement) {
        int result = -1;
        Log.d(my, "executeProcedure  " + statement);
        try {
            if (connection == null) {
                String msg = "Error in connection with SQL server";
                Log.d("my", "query statement conn err " + msg);
                return -1;
            }

            Statement st = connection.createStatement();
            Log.d(my, "executeProcedure 1");

            int res = st.executeUpdate(statement);
            result = res;


            st.close();
            Log.d(my, "executeProcedure 5");


        } catch (SQLException e) {
            Log.d("my", "executeProcedure MainApplication KFDB.There are problems with the query ******");
            Log.d("my", "executeProcedure MainApplication KFDB. " + e.toString());
            e.printStackTrace();
            return -1;
        }

        return result;
    }


    public static JSONArray convertToJSON(ResultSet resultSet)
            throws Exception {
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            int total_rows = resultSet.getMetaData().getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1));
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }


    //check license

    private static String dateTo = null;

    public static String getLicenseDateTo() {
        if (dateTo == null) {
            checkLicense();

            if (dateTo == null) return "";
            else return dateTo;
        }
        return dateTo;
    }

    public static boolean checkLicense() {

       return  LicenseRepository.Companion.readLicenseStatus();

       /* String licenseHash = MainApplication.readOption(OPT_LICENSE_HASH, "");
        if (licenseHash == null) return false;
        if (licenseHash.length() == 0) return false;

        //расшифровываем входящий код
        byte[] toDecrypt = Base64.decode(licenseHash,Base64.DEFAULT);

        // Decode the encoded data with AES
        byte[] decodedBytes = AES_new.decrypt("dN_rWLewGLMrdiOa",toDecrypt,"ckZllEn_gLdPaCvk");




        if(decodedBytes == null)   return checkOldLicense(); //false;


        String decryptedString = null;

        decryptedString =  new String(decodedBytes, Charset.forName("UTF-8"));//Base64.encodeToString(decodedBytes, Base64.DEFAULT); //new String(decodedBytes, Charset.forName("UTF-8"));
        if (decryptedString == null) return checkOldLicense(); //false;
        if (decryptedString.length() < 12) return checkOldLicense(); //false;
        if (!decryptedString.contains("&")) return checkOldLicense(); //false;
        if (!isValidLicenseDate(decryptedString.substring(0, 10))) return checkOldLicense(); //false;
        if (!isValidDeviceId(decryptedString.substring(11))) return checkOldLicense(); //false;
        return true;*/
    }


    private static boolean checkOldLicense(){
        try {
            String licenseHash = MainApplication.readOption(OPT_LICENSE_HASH, "");
            if (licenseHash == null) return false;
            if (licenseHash.length() == 0) return false;

            AESHelper helper = new AESHelper();
            String decryptedString = helper.decryption(licenseHash);
            Log.d(my, "normal str =" + decryptedString);
            if (decryptedString == null) return false;
            if (decryptedString.length() < 12) return false;
            if (!decryptedString.contains("&")) return false;
            if (!isValidLicenseDate(decryptedString.substring(0, 10))) return false;
            if (!isValidDeviceId(decryptedString.substring(11))) return false;

            return true;
        }catch (Exception e){
            return false;
        }
    }




    public static String myFormat = "dd.MM.yyyy";

    private static boolean isValidLicenseDate(String str) {
        if (str == null) return false;
        if (str.length() < 10) return false;

        SimpleDateFormat format = new SimpleDateFormat(myFormat);
        try {
            Date licenseDate = format.parse(str);
            Log.d(my, "License date = " + licenseDate);
            dateTo = str;
            Date todayDate = getTodayDate();
            Log.d(my, "todayDate = " + todayDate);
            if (todayDate.after(licenseDate)) {
                Log.d(my, "todatAfterLicense");
                return false;
            } else return true;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d(my, "isValidLicenseDate err = " + e.toString());
            return false;
        }
    }

    private static Date getTodayDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        Date date = new Date();
        return date;
    }

    private static boolean isValidDeviceId(String str) {
        Log.d(my, "License deviceId = " + str);
        Log.d(my, "Current deviceId = " + getDeviceId());
        if (str.trim().contains(getDeviceId())) return true;
        else return false;
    }


    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    public static String getDeviceId() {
        String android_id = Settings.Secure.getString(getAppContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (android_id == null) android_id = getDeviceName();
        if (android_id.length() == 0) android_id = getDeviceName();
        return android_id;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    private static final String PARAM_UNITS = "units";

    public static String lastUnit = "";

    public static ArrayList<String> getUnits() {
        String unitsList = readOption(PARAM_UNITS, getDefUnitsString());
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(unitsList.split(",")));
        Log.d(my, "list size = " + list.size() + " list=" + list.toString());
        return list;

    }

    public static void writeUnit(String unit) {
        String unitsList = readOption(PARAM_UNITS, getDefUnitsString());
        String nstr = unit + ",";
        unitsList += nstr;
        writeOption(PARAM_UNITS, unitsList);

    }

    private static String getDefUnitsString() {
        return getAppContext().getString(R.string.sht) + "," +
                getAppContext().getString(R.string.kg) + "," +
                getAppContext().getString(R.string.but) + ",";

    }

    public static boolean isMainActivityVisible() {
        return mainActivityVisible;
    }

    public static void mainActivityResumed() {
        mainActivityVisible = true;
    }

    public static void mainActivityPaused() {
        mainActivityVisible = false;
    }

    private static boolean mainActivityVisible;


}

