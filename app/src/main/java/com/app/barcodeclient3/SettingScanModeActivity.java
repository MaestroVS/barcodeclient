package com.app.barcodeclient3;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import static com.app.barcodeclient3.MainApplication.CURRENT_SCAN_MODE;
import static com.app.barcodeclient3.MainApplication.checkLicense;


public class SettingScanModeActivity extends AppCompatActivity {

    private androidx.appcompat.app.ActionBar mainActionBar;
    ImageButton closeButton;
    TextView abarTitle;

    RelativeLayout typeCntRelLayout;
    RadioGroup scanModeRadioGroup;
    RadioButton normalScanButton;
    RadioButton plusFactScanButton;
    RadioButton plusOneScanButton;
    RadioButton summButton;
    CheckBox fashSearchButtonMode;



    SharedPreferences prefs = null;

    public static final String scan_type = "scan_type";
    boolean fast_search = false;
    int scanType = 0;

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, SettingScanModeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d("my", "scan settings1");
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_scan_settings);
        Log.d("my", "settings12");


		/*mainActionBar = getActionBar(); //main bar
        mainActionBar.setDisplayShowCustomEnabled(true);
		mainActionBar.setDisplayShowHomeEnabled(false);
		// mainActionBar.hide();
		mainActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.indigo_700)));

		mainActionBar.setCustomView(R.layout.abar_scan_settings);*/


        mainActionBar = getSupportActionBar();
        mainActionBar.setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        mainActionBar.setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        mainActionBar.setTitle(getString(R.string.settings));

        getSupportActionBar().setCustomView(R.layout.action_bar_title);
        // getSupportActionBar().setTitle(getResources().getString(R.string.my_orders));
        View abarView = getSupportActionBar().getCustomView();

        TextView titleBar = (TextView) abarView.findViewById(R.id.abarTitle);

        titleBar.setText(getString(R.string.settings));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);


        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);


        prefs = getSharedPreferences("com.app.barcodeclient3", MODE_PRIVATE);
        scanType = prefs.getInt(scan_type, 0);

        fast_search = prefs.getBoolean("fast_search",false);



        typeCntRelLayout =  findViewById(R.id.typeCntRelLayout);
        scanModeRadioGroup =  findViewById(R.id.scanModeRadioGroup);
        normalScanButton = findViewById(R.id.normalScanButton);
        plusFactScanButton =  findViewById(R.id.plusFactScanButton);
        plusOneScanButton =  findViewById(R.id.plusOneScanButton);
        summButton= findViewById(R.id.summButton);
        scanModeRadioGroup.setOnCheckedChangeListener(checkRadioListener);
        fashSearchButtonMode = findViewById(R.id.fashSearchButtonMode);

        fashSearchButtonMode.setChecked(fast_search);

        if(checkLicense()){
            fashSearchButtonMode.setVisibility(View.VISIBLE);

        }else{
            fashSearchButtonMode.setVisibility(View.GONE);
        }

        Log.d("my","SCAN_TYPE="+scanType);

        switch (scanType) {
            case 0:
                normalScanButton.setChecked(true);
                plusFactScanButton.setChecked(false);
                plusOneScanButton.setChecked(false);
                summButton.setChecked(false);
                break;
            case 1:
                normalScanButton.setChecked(false);
                plusFactScanButton.setChecked(false);
                plusOneScanButton.setChecked(true);
                summButton.setChecked(false);
                break;
            case 2:
                normalScanButton.setChecked(false);
                plusFactScanButton.setChecked(true);
                plusOneScanButton.setChecked(false);
                summButton.setChecked(false);
                break;

            case 3:
                normalScanButton.setChecked(false);
                plusFactScanButton.setChecked(false);
                plusOneScanButton.setChecked(false);
                summButton.setChecked(true);
                break;


        }

        fashSearchButtonMode.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("my","check_fast_search");
            prefs.edit().putBoolean("fast_search", isChecked).apply();
        });

    }

    RadioGroup.OnCheckedChangeListener checkRadioListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // TODO Auto-generated method stub
            switch (checkedId) {
                case -1:
                    //Toast.makeText(getApplicationContext(), "No choice", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.normalScanButton:
                    prefs.edit().putInt(scan_type, 0).apply();
                    CURRENT_SCAN_MODE = 0;
                    //plusTextView.setText("");
                    break;
                case R.id.plusFactScanButton:
                    prefs.edit().putInt(scan_type, 2).apply();
                    CURRENT_SCAN_MODE = 2;
                    //plusTextView.setText("+");
                    break;
                case R.id.plusOneScanButton:
                    prefs.edit().putInt(scan_type, 1).apply();
                    CURRENT_SCAN_MODE = 1;
                    //plusTextView.setText("+");
                    break;
                case R.id.summButton:
                    prefs.edit().putInt(scan_type, 3).apply();
                    CURRENT_SCAN_MODE = 3;
                    //plusTextView.setText("+");
                    break;

                default:
                    prefs.edit().putInt(scan_type, 0).apply();
                    CURRENT_SCAN_MODE = 0;
                    //plusTextView.setText("");
                    break;
            }

			/*if(currentGood!=null) {
				double fcnt = currentGood.getFcnt();
				goodsCntEditText.setSelectAllOnFocus(true);
				setFactCntTextView(fcnt);
			}*/
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


}
