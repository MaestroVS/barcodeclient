package com.app.barcodeclient3;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rollbar.android.Rollbar;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import v1.data_model.Callback;
import v1.data_model.Response;
import v2.presentation.activation.ActivationActivity;
import v2.data.firebird.models.Inventory;
import v1.dialogs.ClickDocumentDialog;
import v1.excel.ExportInventoryToXLS;
import v1.goods_lists.GoodsListActivity;
import v2.helpers.SharedPref;

import static com.app.barcodeclient3.MainApplication.THEME_BLUE_GREY;
import static com.app.barcodeclient3.MainApplication.THEME_INDIGO;
import static com.app.barcodeclient3.MainApplication.THEME_RED;
import static com.app.barcodeclient3.MainApplication.THEME_TEAL;

import static com.app.barcodeclient3.MainApplication.readOption;
import static com.app.barcodeclient3.MainApplication.writeOption;
import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;

import static com.app.barcodeclient3.MainApplication.SETTINGS_STATE;
import static com.app.barcodeclient3.MainApplication.UNS_OFFLINE_STATE;
import static com.app.barcodeclient3.MainApplication.UPDATE_STATE;
import static com.app.barcodeclient3.MainApplication.checkLicense;
import static com.app.barcodeclient3.MainApplication.databaseIP;
import static com.app.barcodeclient3.MainApplication.databasePath;
import static com.app.barcodeclient3.MainApplication.disconnect;
import static com.app.barcodeclient3.MainApplication.getApi;
import static com.app.barcodeclient3.MainApplication.isConnect;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.refreshOptions;
import static com.app.barcodeclient3.MainApplication.unsConnectOnline;


public class MainActivityOld extends AppCompatActivity {


    ListView list;
    ProgressBar progressBar;
    FloatingActionButton fab;
    LinearLayout themeLayout;

    ImageView statusButton;
    ImageView settingsButton;
    TextView titleBar;

    LinearLayout offlineLinear;
    CardView createBt;
    CardView uploadBt;
    CardView goodsBt;

    Button offlineBt;
    ImageView refreshBt;

    SwipeRefreshLayout mSwipeRefreshLayout;

    // findViewById(R.id.serverLinear)
    // LinearLayout serverLinear;
    // findViewById(R.id.serverLinearDetails)
    // LinearLayout serverLinearDetails;


    Button activationButton;
    TextView licenseStatusTextView;

    Handler h = new Handler();
    InventoriesListAdapter adapter = null;
    ArrayList<Inventory> invlist = new ArrayList<>();

    //hi! i snova test!!!
    public static String device_id = "";
    public static String device_name = "";

    public static final int GOOD_ID_CONST = 0; //smeshenie id dlia tovara, sozdannogo na tel
    public static final int GRP_OFFLINE_GOODS = -3; //grp id tovara, sozdannogo na tel
    public static final String not_ask_create_good = "NOT_ASK_CREATE_GOOD";//option ne sprashivat bol'she sozdat novyi tovar


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String themeS = readOption("THEME","0");
        Log.d("theme","themeS th = "+themeS);
        int theme = THEME_RED;
        try{
            theme = Integer.parseInt(themeS);
        }catch (Exception e){
            Log.d("theme","err th = "+e.toString());
        }

        String themeShowS = readOption("THEME_SHOW","1");
        Log.d("theme","themeShow th = "+themeShowS);


        Log.d("theme","theme th = "+theme);

        switch (theme){

            case THEME_RED:
                setTheme(R.style.MainActivityThemeRed);
                break;
            case THEME_TEAL:
                setTheme(R.style.MainActivityThemeTeal);
                break;
            case THEME_INDIGO:
                setTheme(R.style.MainActivityThemeIndigo);
                break;
            case THEME_BLUE_GREY:
                setTheme(R.style.MainActivityThemeBlueGrey);
                break;
        }
        setTheme(R.style.MainActivityThemeTeal);


        setContentView(R.layout.activity_main);

        SharedPref.INSTANCE.init(this);

        //ButterKnife.bind(this);
        Log.d("my", "Hello MainActivity old!");
        Rollbar.init(this, "8bcdfc59d1a7417ca58be6bdba010594", "production2");
        //  Rollbar.reportException(new Exception("Test exception barcode!"));
        Log.d("my", "Hello MainActivity old 2!");

        //----
        getSupportActionBar().setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);

        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        View abarView = getSupportActionBar().getCustomView();


        titleBar = abarView.findViewById(R.id.abarTitle);
        statusButton = abarView.findViewById(R.id.statusButton);

        list = findViewById(R.id.list);
        progressBar = findViewById(R.id.progress);
        fab = findViewById(R.id.fab);
        themeLayout = findViewById(R.id.themeLayout);


        offlineLinear= findViewById(R.id.offlineLinear);
        createBt = findViewById(R.id.createBt);
        uploadBt = findViewById(R.id.uploadBt);
        goodsBt= findViewById(R.id.goodsBt);

        offlineBt= findViewById(R.id.offlineBt);
        refreshBt = findViewById(R.id.refreshBt);

        mSwipeRefreshLayout = findViewById(R.id.activity_main_swipe_refresh_layout);
        activationButton = findViewById(R.id.activationButton);
        licenseStatusTextView = findViewById(R.id.licenseStatusTextView);



        getSha1();




        statusButton.setOnClickListener(v -> {
            runOnUiThread(() -> {
                updateState = true;
                Log.d("my", "connection");
                UpdateStateThread updateStateThread = new UpdateStateThread();
                updateStateThread.start();

            });
        });

        settingsButton = abarView.findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivityOld.this, SettingsActivity.class);
                startActivityForResult(intent, SETTINGS_STATE);
            }
        });

/*        activationButton.setVisibility(View.GONE);

        activationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, LicenseActivity.class);
                startActivityForResult(intent, SETTINGS_STATE);
            }
        });*/
        Log.d("my", "Hello MainActivity old3!");

       // updateOfflineButton();
        Log.d("my", "Hello MainActivity old31!");

        getSupportActionBar().setDisplayHomeAsUpEnabled(false); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setIcon(android.R.color.transparent);
        Log.d("my", "Hello MainActivity old32!");


        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);
        Log.d("my", "Hello MainActivity old4!");
        //--------------

        list.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {  }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                int topRowVerticalPosition = (list == null || list.getChildCount() == 0) ? 0 : list.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        createBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createInvDialog();
            }
        });




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (connectMode) {
                    case CONNECT_OFFLINE:
                        createInvDialog();
                        break;
                    case CONNECT_ONLINE:

                        break;

                }
            }
        });

        refreshBt.setVisibility(View.GONE);
        /*refreshBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildActivityState();
            }
        });*/
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(my,"swipe 0 ");
                if(connectMode==CONNECT_ONLINE&&unsConnectOnline) {
                    Log.d(my,"swipe 1 ");
                    buildActivityState();
                }
                if(connectMode==CONNECT_OFFLINE)
                {
                    Log.d(my,"swipe 2 ");
                    mSwipeRefreshLayout.setRefreshing(false);
                    mSwipeRefreshLayout.setEnabled(false);
                }
            }
        });



        Log.d("my", "Hello MainActivity old5!");
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.red_primary_color,
                R.color.red_primary_light_color,
                R.color.green_light);

       // if(themeShowS.contains("0"))
            themeLayout.setVisibility(View.GONE);
        //else themeLayout.setVisibility(View.VISIBLE);
        findViewById(R.id.closeTheme).setOnClickListener((v)->{
            themeLayout.setVisibility(View.GONE);
            writeOption("THEME_SHOW","0");
        });
        findViewById(R.id.styleRed).setOnClickListener((v)->{ changeTheme(THEME_RED);});
        findViewById(R.id.styleTeal).setOnClickListener((v)->{ changeTheme(THEME_TEAL);});
        findViewById(R.id.styleIndigo).setOnClickListener((v)->{ changeTheme(THEME_INDIGO);});
        findViewById(R.id.styleBlueGrey).setOnClickListener((v)->{ changeTheme(THEME_BLUE_GREY);});


      //  offlineBt.setVisibility(checkLicense() ? View.VISIBLE : View.GONE);


        offlineBt.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivityOld.this, SettingsOfflineModeActivity.class);
            startActivityForResult(intent, UNS_OFFLINE_STATE);
        });

        uploadBt.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivityOld.this,
                    v1.excel.AndroidReadExcelActivity.class);
            startActivity(intent);
        });


        if (list == null) Log.d("my", " List is null");

        goodsBt.setOnClickListener(view -> {
            //  Intent intent = new Intent(MainActivity.this, DicGoods.class);
            // startActivity(intent);
            Intent intent = new Intent(MainActivityOld.this,
                    GoodsListActivity.class);
            startActivity(intent);
        });

        ConnectThread thread = new ConnectThread();
        thread.start();

        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                buildActivityState();
            }
        }, 1000);


    }

    private void buildActivityState() {


        Log.d("my", "::connectMode = " + connectMode);


        if (connectMode == CONNECT_OFFLINE) {
            Log.d(my, "buildActivityState CONNECT_OFFLINE");
            fab.setImageResource(R.drawable.ic_add_white_36dp);
            offlineLinear.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            activationButton.setVisibility(View.GONE);
            licenseStatusTextView.setVisibility(View.GONE);
            offlineBt.setVisibility(View.GONE);
            refreshBt.setVisibility(View.GONE);
            //mSwipeRefreshLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setEnabled(false);
            mSwipeRefreshLayout.setRefreshing(false);


            getDatabaseVersion();
        }

        Log.d("my", "Hello MainActivity old6!");

        if (connectMode == CONNECT_ONLINE || !unsConnectOnline) {
            Log.d(my, "buildActivityState CONNECT_ONLINE");
            fab.setImageResource(R.drawable.ic_refresh_white_36dp);
            offlineLinear.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            licenseStatusTextView.setVisibility(View.VISIBLE);
            refreshBt.setVisibility(View.GONE);
            refreshBt.setEnabled(false);
           // mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setEnabled(true);


            boolean license = checkLicense();
            offlineBt.setVisibility(checkLicense() ? View.VISIBLE : View.GONE);
            Log.d(my, "License is " + license);
            if (license) {
                activationButton.setVisibility(View.GONE);
                licenseStatusTextView.setTextColor(getResources().getColor(R.color.green));
                String txt = getText(R.string.license_active) + " " + MainApplication.getLicenseDateTo();
                licenseStatusTextView.setText(txt);


            } else {
                activationButton.setVisibility(View.VISIBLE);
                licenseStatusTextView.setTextColor(getResources().getColor(R.color.dark_gray));
                licenseStatusTextView.setText(getText(R.string.license_not_active));

                activationButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivityForResult(ActivationActivity.Companion.getCallingIntent(MainActivityOld.this),ACTIVATION_ACTIVITY_REQUEST);
                    }
                });
            }

            disconnect = true;

            if (unsConnectOnline) {
                // ConnectThread thread = new ConnectThread();
                // thread.start();
            } else {
                getDatabaseVersion();
            }
        }

        ///--
    }

    static int ACTIVATION_ACTIVITY_REQUEST = 65821;


    String dbVersion = null;


    class ConnectThread extends Thread {
        @Override
        public void run() {

            Log.d(my, "MainActivity ConnectThread");

            if (disconnect) {
                disconnect = false;
                disconnect();
            }
            Connection connection = MainApplication.initConnect();//!!!!!!!!!!!!!!!!!!!!!!!
            if (connection == null) {
                Log.d("my", "connection is null");
            } else {
                Log.d("my", "connection is ok");


            }
            runOnUiThread(() -> {
                    Log.d("my", "connection is ok2");
                    UpdateStateThread updateStateThread = new UpdateStateThread();
                    updateStateThread.start();

            });

        }
    }

    private boolean updateState = true;


    class UpdateStateThread extends Thread {
        @Override
        public void run() {

            while (updateState) {

                if (connectMode == CONNECT_ONLINE) {
                    if (MainApplication.isMainActivityVisible()) {
                        Log.d(my, "------update activity VISIBLE");


                        MainApplication.initConnect();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getDatabaseVersion();
                            }
                        });



                    } else {
                      //  Log.d(my, "------update activity GONE");
                    }

                }

               // Log.d("my", "----------------tick!!!!!!!!!!!!!!!!!!!!!");
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                updateState = false;
            }
        }
    }



    private void getSha1(){ //HjuE40GXY3pKukuF8v0cDfXMnlo=
        PackageInfo info;
        try {

            info = getPackageManager().getPackageInfo(
                    "com.app.barcodeclient3", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key", something);
                System.out.println("Hash key" + something);
            }

        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }


    private void getDatabaseVersion() {

        Log.d("my", "MainActivity getDatabaseVersion");
        Log.d("my", "MainActivity getDatabaseVersion connectMode = " + connectMode);
        Log.d("my", "MainActivity getDatabaseVersion unsOnline = " + unsConnectOnline);
        Log.d("my", "MainActivity getDatabaseVersion MainApplication.getApi() class=" + MainApplication.getApi().getClass().toString());

        MainApplication.getApi().getDatabaseVersion(new Callback<String>() {
            @Override
            public boolean sendResult(Response<String> response) {

                if (response != null) {
                    if(response.body() != null) {
                        Log.d("my", "<<<< " + response.body());
                        dbVersion = response.body();
                        if (dbVersion != null & dbVersion.length() >= 7) {
                            boolean isNew = isNewDBVersion(dbVersion);
                            Log.d("my", "isNewDB = " + isNew);
                            MainApplication.isDB_3_0_68_0_plus = isNew;
                        }
                    }else{
                      /*  MaterialDialog(this).show {
                            title(text = getString(R.string.error))
                            message(text = throwable.localizedMessage)
                            positiveButton(text = "Close")
                        }*/

                        Toast.makeText(MainActivityOld.this, "Connect DB error", Toast.LENGTH_SHORT).show();
                        // new MaterialDialog(MainActivityOld.this, MaterialDialog.Companion.getDEFAULT_BEHAVIOR()).title(R.string.error_connect,"Connect DB error\nX    Close").cancelable(true)
                         //       .show();
                    }
                } else {
                    Log.d("my", "<<<<null");
                    new MaterialDialog(MainActivityOld.this, MaterialDialog.Companion.getDEFAULT_BEHAVIOR()).title(R.string.error_connect,"Connect DB error\nX    Close").cancelable(true)
                            .show();
                }
                updateStatus();

                updateInventories();

                return false;
            }

            @Override
            public void onError(String error) {

            }
        });
    };

    private boolean isNewDBVersion(String dbVersion){

        int[] pattern = new int[]{ 3,0,68,0 };
        String currentStr = dbVersion;
        int index = 0;
        ArrayList<Integer> currentVer = new ArrayList<>();
        do{
            index = currentStr.indexOf(".");
            Log.d("my","index="+index);

            String nums = "";
                if(index>0){
                    nums=currentStr.substring(0,index);
                }else{
                    nums=currentStr.substring(0);
                }


                try{
                    int num = Integer.parseInt(nums);
                    currentVer.add(num);
                }catch (Exception e){}
                if(index<currentStr.length()-1) {
                    currentStr = currentStr.substring(index + 1);
                }
            //
            Log.d("my","currentStr="+currentStr);
        }while (index>=0);

        Log.d("my","base_nums="+currentVer);
        int to = currentVer.size();
        if(to==0){ return false; }
        if(to>4){to = 4; }
        for (int i=0;i<to;i++){
            int a = pattern[i];
            int b = currentVer.get(i);
            if(b>a){
                return true;
            }
            if(b<a){
                return false;
            }
        }
        return true;
    }


    private void updateInventories() {
        Log.d("my", "updateInventories...");
        // Log.d(my, "DataModelOffline.getInstance() = " + DataModelOffline.getInstance().getClass().toString());
        MainApplication.getApi().getInventoryList(new Callback<ArrayList<Inventory>>() {
            @Override
            public boolean sendResult(Response<ArrayList<Inventory>> response) {

                invlist = response.body();
                if (list != null) Log.d("my", "inv list!=null " + invlist.size());
                else Log.d("my", "list is null");

                if (adapter == null) {
                    Log.d(my, "adapter == null...");
                    adapter = new InventoriesListAdapter(MainActivityOld.this, R.layout.row_inventory_document, invlist);
                    list.setAdapter(adapter);
                    list.setOnItemClickListener(adapterInventoryClickListener);
                } else {
                    Log.d("my", "adapter notifydataChanged");
                    adapter.clear();
                    adapter.addAll(invlist);
                    adapter.setDeleteInventoryListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view.getTag() == null) return;
                            if (!(view.getTag() instanceof Inventory)) return;
                            Inventory inv = (Inventory) view.getTag();
                            int id = Math.toIntExact(inv.getId());
                            deleteInventory(view.getContext(), id);

                        }
                    });


                    mSwipeRefreshLayout.setRefreshing(false);
                    adapter.notifyDataSetChanged();
                }
                return false;
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteInventory(Context _context, int _id) {
        final int idd = _id;
        final Context context = _context;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        MainApplication.dbHelper.clear_INVENTORY(idd);
                        updateInventories();
                        adapter.notifyDataSetChanged();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        if (MainApplication.getAppContext() == null) {
            Log.d("my", "app context is null!!!");
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(context.getString(R.string.delete_inventory_dialog)).setPositiveButton("Ok", dialogClickListener)
                    .setNegativeButton(context.getString(R.string.cancel), dialogClickListener).show();
        }
    }


    Dialog newInventoryDialog;


    private void createInvDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.new_inventory_dialog, null);
        builder.setView(view);
        builder.setTitle(getString(R.string.create_inventory));


        // set the custom newInventoryDialog components - text, image and button
        final EditText idEditText = view.findViewById(R.id.idEditText);
        final EditText nameEditText = view.findViewById(R.id.nameEditText);


        Button cancelButton = view.findViewById(R.id.cancelBt);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newInventoryDialog.dismiss();
            }
        });

        Button createButton = view.findViewById(R.id.createBt);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create inventory...
                //newInventoryDialog.dismiss();

                final String createInvId = idEditText.getText().toString();

                String strnm = nameEditText.getText().toString();
                //strnm += " - offline document";
                final String createInvName = strnm;
                //currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
                dateFormatter.setLenient(false);
                java.util.Date today = new java.util.Date();
                final String currentDateTimeString = dateFormatter.format(today);
                int crInvId = 0;
                try {
                    crInvId = Integer.parseInt(createInvId);

                } catch (Exception e) {
                }


                if (createInvId.length() > 0) {
                    getApi().createInventory(crInvId, createInvId, currentDateTimeString, createInvName, 0);
                }

                updateInventories();
                newInventoryDialog.dismiss();

            }
        });
        newInventoryDialog = builder.create();
        newInventoryDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @androidx.annotation.NonNull final String[] permissions, @androidx.annotation.NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("my", "permissions = " + permissions[0]);


        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //takePicture();

                if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    if(exportInventoryToXLS!=null) exportInventoryToXLS.exportInvToXLSWithPermissions();

            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.mainActivityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.mainActivityPaused();
    }

    private boolean disconnect = false;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.d("my", "onActivityResult");

        if (requestCode == UNS_OFFLINE_STATE) {

            Log.d("my", "onActivityResult UNS_OFFLINE_STATE");
            // String result =  data.getExtras().getString("TADA");
            // Log.d(my,"result = "+result);
          /* if(result.contains("offline")) {
               connectMode = CONNECT_OFFLINE;
           }else connectMode=CONNECT_ONLINE;*/
            refreshOptions();
            updateOfflineButton();
            buildActivityState();
            return;
        }
        if (requestCode == SETTINGS_STATE || requestCode == UPDATE_STATE) {

            Log.d("my", "onActivityResult SETTINGS_STATE");

            if (data != null) {
                Log.d("my", "data=" + data.toString());
                Bundle extras = getIntent().getBundleExtra("result");
                if (extras != null) {
                    Log.d("my", "extras=" + extras.toString());
                    Object newString = extras.get("result");
                    Log.d(my, "result = " + newString);
                } else {
                    Log.d(my, "extras intent is null");
                }
            } else {
                Log.d(my, "result intent is null");
            }

            Log.d("my", "we must update settings!");
            refreshOptions();
            buildActivityState();


        }

        if(requestCode == ACTIVATION_ACTIVITY_REQUEST){
            refreshOptions();
            buildActivityState();
        }
    }


    private void updateStatus() {
        Log.d("my", "update status  connectMode=" + connectMode);

        // titleBar.setTextColor(getResources().getColor(R.color.primary_dark));
        refreshBt.setEnabled(true);
        refreshBt.setVisibility(View.GONE);

        if (connectMode == CONNECT_ONLINE) {

            String textErr = "Can't connect to " + databaseIP + ":" + databasePath + " !";
            titleBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            statusButton.setVisibility(View.VISIBLE);
            if (isConnect()) {
                if (dbVersion == null) dbVersion = "";
                if (dbVersion.length() > 0) {
                    statusButton.setImageDrawable(getResources().getDrawable(R.drawable.tint_status_online));
                    String text = "Connected to " + databaseIP + ":" + databasePath + "\n" + "ver " + dbVersion;

                    titleBar.setText(text);
                } else {
                    String textW = "Connected to " + databaseIP + ":" + databasePath + "\n Сбой получения версии базы !";
                    titleBar.setText(textW);
                    statusButton.setImageDrawable(getResources().getDrawable(R.drawable.tint_status_online_warning));
                }
            } else {
                titleBar.setText(textErr);
                statusButton.setImageDrawable(getResources().getDrawable(R.drawable.tint_status_offline));
            }

        }


        if (connectMode == CONNECT_OFFLINE) {
            titleBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
            titleBar.setText(getString(R.string.inventory));
            statusButton.setVisibility(View.INVISIBLE);
        }
    }

    private void updateOfflineButton() {
        if (!unsConnectOnline) {
            offlineBt.setBackgroundColor(ContextCompat.getColor(this, R.color.gold));
            offlineBt.setText(R.string.uns_online_off);
            offlineBt.setTextColor(getResources().getColor(R.color.white));
        } else {

            offlineBt.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            offlineBt.setText(R.string.uns_online_on);
            offlineBt.setTextColor(getResources().getColor(R.color.dark_gray));
        }
    }

    ExportInventoryToXLS exportInventoryToXLS;


    AdapterView.OnItemClickListener adapterInventoryClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            Log.d("my", "click inv!");

            Inventory inventory = (Inventory) view.getTag();
            if (inventory.getDoc_state() == 0) {

                final int invId = Math.toIntExact(inventory.getId());
                final String date_ = inventory.getDateTime();
                final String subdivision =inventory.getSubdivisionName();


                ClickDocumentDialog dialog = new ClickDocumentDialog();

                Bundle args = new Bundle();
                // String good = barcode;//searchEditText.getText().toString();
                args.putInt("id", invId);
                args.putString("num", inventory.getNum());
                args.putString("subdivision",subdivision );
                args.putString("date",date_);

                dialog.setArguments(args);
                dialog.setCallback(message -> {
                    Log.d(my, "get msg");
                    if (message != null) {
                        Log.d(my, "get msg 1");
                        int action = message.arg1;
                        if(action==3){
                            Log.d(my,"Export to xml...");
                            ExportInventoryToXML xml = new ExportInventoryToXML(invId, MainActivityOld.this);
                            xml.setDate(date_);
                            xml.setInventorySubdivisionName(subdivision);
                            xml.exportInvToXML();
                        }
                        if(action==1){
                            Log.d(my,"Export to xls...");
                            exportInventoryToXLS = new ExportInventoryToXLS(invId, MainActivityOld.this);
                            exportInventoryToXLS.setDate(date_);
                            exportInventoryToXLS.setInventorySubdivisionName(subdivision);
                            exportInventoryToXLS.exportInvToXLS();
                        }

                        if(action==2) {
                            deleteInventory(MainActivityOld.this, invId);
                        }

                    }
                    return false;
                });

                dialog.show(getFragmentManager(), "Hello!");


            }


            //---



        }
    };

    private void changeTheme(int theme){
        MainApplication.writeOption("THEME",""+theme);
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


}
