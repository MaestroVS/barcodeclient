package com.app.barcodeclient3;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.rollbar.android.Rollbar;

import java.io.File;
import java.util.ArrayList;

import v1.data_model_offline_old.Good;
import v1.excel.CreateExportFile;
import v1.excel.FileLoader;

import static v1.excel.AndroidReadExcelActivity.my;

public class ExportInventoryToXML {

    private int invId = -1;
    Context context;

    boolean success = false;

    ProgressDialog progress;

    String date = "_";
    String inventorySubdivisionName = "Market";

    public ExportInventoryToXML(int invId, Context context) {
        this.invId = invId;
        this.context = context;
    }

    static String filename = "";

    public void exportInvToXML() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission((Activity) context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);


            } else { //permission is automatically granted on sdk<23 upon installation
                Log.d("my", "Permission is granted");
                exportInvToXLSWithPermissions();

            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.d("my", "Permission is granted");
            exportInvToXLSWithPermissions();

        }
    }

    public void exportInvToXLSWithPermissions() {
        //Intent intent = new Intent(ScanWorkingActivity.this, ScanSettingsActivity.class);
        //startActivityForResult(intent, 0);
        Rollbar.reportMessage("NewScannerActivity... exportInvToXLS1");

        progress = new ProgressDialog(context);
       /* progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();*/
// To dismiss the dialog


        date = date.replaceAll(":", "_");
        date = date.replaceAll("/", "-");
        int offl = inventorySubdivisionName.indexOf(" - offline");
        if (offl > 0) inventorySubdivisionName = inventorySubdivisionName.substring(0, offl);
        //date=date.replaceAll("offline document","");
        filename = "inv" + "_" + date + "_" + inventorySubdivisionName;
        Log.d("my", "filename = " + filename);


       /* AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Export to file");

// Set up the input
        final EditText input = new EditText(context);
        input.setText(filename);
        input.setSelected(true);
        input.setSelectAllOnFocus(true);

// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("CSV", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filename = input.getText().toString();

                ExportExcelThread exportExcelThread = new ExportExcelThread("csv");
                exportExcelThread.start();
            }
        });


        builder.setNeutralButton("XLS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filename = input.getText().toString();

                ExportExcelThread exportExcelThread = new ExportExcelThread("xls");
                exportExcelThread.start();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();*/

        ExportExcelThread exportExcelThread = new ExportExcelThread("xml");
        exportExcelThread.start();


    }

    File resultFile = null;

    class ExportExcelThread extends Thread {

        String type = "xls";

        ExportExcelThread(String type) {
            this.type = type;
        }

        @Override
        public void run() {
            ArrayList<Good> goodsAccList = MainApplication.dbHelper.getALLGoodsCountListAcc(invId);
            //success = CreateExportFile.exportGoodsXLS(goodsAccList,filename, id);


            if (type.contains("csv"))
                resultFile = CreateExportFile.exportGoodsCSV(goodsAccList, filename + ".csv", invId);
            if (type.contains("xls"))
                resultFile = CreateExportFile.exportGoodsXLS(goodsAccList, filename + ".xls", invId);
            if (type.contains("xml"))
                resultFile = CreateExportFile.exportGoodsXML(goodsAccList, filename + ".xml", invId, date);

            if(resultFile!=null) success=true;
            else success=false;

            impH.post(() -> {
                progress.dismiss();


                String title = "";
                String details = "";
                if (success) {
                    title = "";
                    details = context.getString(R.string.export_success);
                } else {
                    title = context.getString(R.string.error);
                    details = context.getString(R.string.export_error);
                }

                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(title);
                alertDialog.setMessage(details);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                if(resultFile!=null) {
                                    openFile();
                                }else{
                                    Log.d(my,"result file is null" );
                                    openFolder();
                                }

                            }
                        });
                alertDialog.show();
            });

            //


        }
    }

    Handler impH = new Handler();

    public void openFolder() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                + "/" + FileLoader.DIR_NAME);
        intent.setDataAndType(uri, "xls/text/csv");
        context.startActivity(Intent.createChooser(intent, "Open folder"));
    }

    public void openFile() {
       /*// MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
       // String mimeType = ".xlx";//myMime.getMimeTypeFromExtension(fileExt(getFile()).substring(1));
        // newIntent.setDataAndType(Uri.fromFile(resultFile), mimeType);
        newIntent.setDataAndType(Uri.fromFile(resultFile), "xls/text/csv");
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Не удалось открыть файл", Toast.LENGTH_LONG).show();
            openFolder();
        }*/


       Log.d(my,"result xls file = "+resultFile.getAbsolutePath());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(resultFile), "application/vnd.ms-v1.excel");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
           // Toast.makeText(OpenPdf.this, "No Application Available to View Excel",
           //         Toast.LENGTH_SHORT).show();
        }
    }




    public void setDate(String date) {
        this.date = date;
    }



    public void setInventorySubdivisionName(String inventorySubdivisionName) {
        this.inventorySubdivisionName = inventorySubdivisionName;
    }
}
