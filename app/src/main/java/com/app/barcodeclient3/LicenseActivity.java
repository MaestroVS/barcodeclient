package com.app.barcodeclient3;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import v1.utils.AES_new;


import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.OPT_LICENSE_HASH;
import static com.app.barcodeclient3.MainApplication.checkLicense;
import static com.app.barcodeclient3.MainApplication.getDeviceId;
import static com.app.barcodeclient3.MainApplication.writeOption;

/**
 * Created by MaestroVS on 04.02.2018.
 */

public class LicenseActivity extends AppCompatActivity {

    Button copyButton;
    Button shareButton;
    androidx.appcompat.widget.AppCompatEditText vhodyashiiCod;
    androidx.appcompat.widget.AppCompatEditText activationCodeET;
    Button activationButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_license);

        copyButton = findViewById(R.id.copyButton);
        shareButton = findViewById(R.id.shareButton);
        vhodyashiiCod = findViewById(R.id.inputCodeET);
        activationCodeET=findViewById(R.id.activationCodeET);
        activationButton = findViewById(R.id.activationButton);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);

        copyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        copyButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED

                        vhodyashiiCod.setTextColor(getResources().getColor(R.color.blue));
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        vhodyashiiCod.setTextColor(getResources().getColor(R.color.dark_gray));
                        Log.d("my","copy text");
                        String text = vhodyashiiCod.getText().toString();
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("text", text);
                        clipboard.setPrimaryClip(clip);

                        Toast.makeText(LicenseActivity.this,"Copied",Toast.LENGTH_SHORT).show();

                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        String devId = getDeviceId();
        Log.d(my,"DEV id ="+devId);
        //AESHelper helper = new AESHelper();

        byte[] encrypted = AES_new.encrypt("dN_rWLewGLMrdiOa",devId.getBytes(),"ckZllEn_gLdPaCvk");

        if(encrypted==null) return;

                String encryptedString = Base64.encodeToString(encrypted, Base64.DEFAULT);
        Log.d(my, "encoded new  str =" + encryptedString);
        Log.d(my, "encoded new  str length=" + encryptedString.length());


      //
        //String encryptedString = helper.encryption(devId);
        Log.d(my,"encoded str ="+encryptedString);
        Log.d(my,"encoded str length="+encryptedString.length());


        vhodyashiiCod.setText(encryptedString);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareButton();
            }
        });


        activationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = activationCodeET.getText().toString();
                if(code.length()==0)return;
                code=code.trim();
                writeOption(OPT_LICENSE_HASH,code);
                boolean isLicense = checkLicense();

                String resultMsg="";
                if(isLicense){
                    Log.d(my,"АКТИВИРОВАНО!!!!");
                    resultMsg = "АКТИВИРОВАНО!!!!";

                }else{
                    Log.d(my,"ОШИБКА АКТИВАЦИИ");
                    resultMsg = "ОШИБКА АКТИВАЦИИ";
                    Toast.makeText(LicenseActivity.this,"LICENSE ERROR",Toast.LENGTH_SHORT);
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(LicenseActivity.this);
                builder.setMessage(resultMsg)
                        .setCancelable(false)
                        .setPositiveButton("OK", (dialog, id) -> {
                            finish();

                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }

    private void shareButton()
    {


        String sendText = vhodyashiiCod.getText().toString();
        Log.d("my","send text: "+sendText);

        //com.equalibra.app
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sendText);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
        }


        return super.onOptionsItemSelected(item);

    }

}
