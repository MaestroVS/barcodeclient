package com.app.barcodeclient3;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import v2.data.firebird.models.Inventory;

/**
 * Created by MaestroVS on 19.12.2017.
 */

public class InventoriesListAdapter extends ArrayAdapter {

    ArrayList<Inventory> inventories;


    public InventoriesListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Inventory> inventories) {
        super(context, resource, inventories);
        this.inventories=inventories;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_inventory_document, null);


        TextView numTV = v.findViewById(R.id.num);
        TextView subdivisionVT = v.findViewById(R.id.subdivision);
        TextView date =  v.findViewById(R.id.date);
        ImageView lock = v.findViewById(R.id.lock);
        ImageView delete = v.findViewById(R.id.delete);
        ImageView export = v.findViewById(R.id.export);
        //delete.setVisibility(connectMode==CONNECT_OFFLINE&&unsConnectOnline?View.VISIBLE:View.GONE);
        export.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);

        final Inventory inventory = inventories.get(position);
        delete.setTag(inventory);
        export.setTag(inventory);
        if(deleteInventoryListener!=null) delete.setOnClickListener(deleteInventoryListener);

      //  export.setVisibility(View.GONE);
        if(exportInventoryListener!=null){
            export.setVisibility(View.VISIBLE);
            export.setOnClickListener(exportInventoryListener);
        }





        numTV.setText(inventory.getNum());
        subdivisionVT.setText(inventory.getSubdivisionName());
        date.setText(inventory.getDateTime());

        if(inventory.getDoc_state()==1){
            v.setVisibility(View.GONE);
        }
        lock.setVisibility(View.GONE);

        v.setTag(inventory);

        return v;
    }



    private View.OnClickListener deleteInventoryListener=null;

    public void setDeleteInventoryListener(View.OnClickListener deleteInventoryListener) {
        this.deleteInventoryListener = deleteInventoryListener;
    }

    private View.OnClickListener exportInventoryListener=null;

    public void setExportInventoryListener(View.OnClickListener exportInventoryListener) {

        this.exportInventoryListener = exportInventoryListener;
    }

    public View.OnClickListener getDeleteInventoryListener() {
        return deleteInventoryListener;
    }
}
