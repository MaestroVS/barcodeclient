package com.app.barcodeclient3;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
//import android.support.v7.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toolbar;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import v1.data_model.Callback;
import v1.data_model.Response;
import v1.data_model_offline_old.Good;
import v1.data_model_offline_old.Inventory;
import v1.excel.ExportInventoryToXLS;
import v1.data_model.GoodJSON;
import v1.goods_lists.GoodsListActivity;
import v1.dialogs.CreateGoodDialog;
import v1.goods_list_offline_old.TotalGoodsListActivity;

import static com.app.barcodeclient3.MainApplication.THEME_BLUE_GREY;
import static com.app.barcodeclient3.MainApplication.THEME_INDIGO;
import static com.app.barcodeclient3.MainApplication.THEME_RED;
import static com.app.barcodeclient3.MainApplication.THEME_TEAL;

import static com.app.barcodeclient3.MainApplication.readOption;
import static com.app.barcodeclient3.SettingScanModeActivity.scan_type;
import static v1.data_model.DataModelInterface.ARTICLE;
import static v1.data_model.DataModelInterface.BARCODE;
import static v1.data_model.DataModelInterface.INVENTORY_ID;
import static v1.data_model.DataModelInterface.NAME;
import static com.app.barcodeclient3.MainApplication.CURRENT_SCAN_MODE;
import static com.app.barcodeclient3.MainApplication.PLUS_FACT_SCAN;
import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;

import static com.app.barcodeclient3.MainApplication.CURRENT_FREE_SCANS;
import static com.app.barcodeclient3.MainApplication.MAX_FREE_SCANS;
import static com.app.barcodeclient3.MainApplication.NOT_CREATE_GOOD;
import static com.app.barcodeclient3.MainApplication.addFreeScan;
import static com.app.barcodeclient3.MainApplication.checkLicense;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.getApi;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.MainApplication.sendedCnt;
import static com.app.barcodeclient3.MainApplication.sendedGoodsName;


/**
 * Custom Scannner Activity extending from Activity to display a custom layout form scanner view.
 */
public class ScannerActivity extends AppCompatActivity implements
        DecoratedBarcodeView.TorchListener {



    // private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private ImageView switchFlashlightButton;
    private ImageView hideCameraButton;


    //private static DrawerLayout mDrawerLayout;
   //// private ListView mDrawerList;
   // private ActionBarDrawerToggle mDrawerToggle;


    ArrayList<String> data = new ArrayList<>();
    ArrayAdapter<String> adapterDoc;
    GoodsListAdapter<Good> adapter;

    ViewGroup container;
    LayoutInflater inflater;
    View myRootView;
    private String titletxt = "";

    public static int id = -1;
    int docType = 0;
    String date = "";
    String subdiv = "";

    ImageButton closeButton;
    TextView abarTitle;
    ImageView offlineIcon;

    ListView list;


    EditText searchEditText;
    // EditText searchEditTextNew;

    ImageView searchBt;

    RelativeLayout listRelLayout;

    LinearLayout goodDetailsRelLayout;
    TextView goodsName;
    TextView goodsBarcode;
    TextView goodsArticle;
    Button goodsPrice;

    RelativeLayout cntRelLayout;
    EditText goodsCntEditText;
    TextView factCntTextView;
    TextView plusTextView;
    Button plusButton;
    Button minusButton;

    LinearLayout calculatorLinLayout;

    ImageView goodsBt;
    ImageView inputKeyBt;
    ImageView settingsBt;

    Button c0;
    Button c1;
    Button c2;
    Button c3;
    Button c4;
    Button c5;
    Button c6;
    Button c7;
    Button c8;
    Button c9;
    Button cDot;
    Button cClear;
    Button btCancel;
    Button btOk;

    TextView lastGoodName;
    TextView lastGoodCnt;

    ProgressBar exportProgress;
    LinearLayout applyLinLayout;

    LinearLayout subRelLayout;
    ImageButton docListButton1;
    ImageButton keyBoardButton1;

    ImageButton scansettingsButton;

    CheckBox searchArticle;

    NumberPicker numberPicker;


    // private ActionBar mainActionBar;

    ExportInventoryToXLS exportInventoryToXLS;


    Button testButton;

    int enter = 0;
    boolean newScan = true;
    public static ArrayList<Good> goodList = new ArrayList<>();
    ArrayList<Good> originGoodListSaver = new ArrayList<>();
    boolean isEnabledFindButton = true;

    String barcode = "";
    String article = "";
    String name = "";

    Good currentGood = null;
    SharedPreferences prefs = null;
    int scanType = 0;
    boolean fast_search = false;

    public static androidx.appcompat.app.ActionBar mainActionBar;

    int openCameraCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String themeS = readOption("THEME","0");
        Log.d("theme","themeS th = "+themeS);
        int theme = THEME_RED;
        try{
            theme = Integer.parseInt(themeS);
        }catch (Exception e){
            Log.d("theme","err th = "+e.toString());
        }
        Log.d("theme","theme th = "+theme);

        switch (theme){

            case THEME_RED:
                setTheme(R.style.MainActivityThemeRed);
                break;
            case THEME_TEAL:
                setTheme(R.style.MainActivityThemeTeal);
                break;
            case THEME_INDIGO:
                setTheme(R.style.MainActivityThemeIndigo);
                break;
            case THEME_BLUE_GREY:
                setTheme(R.style.MainActivityThemeBlueGrey);
                break;
        }
        setTheme(R.style.MainActivityThemeTeal);

        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_new_scanner);

        Log.d("my", "hello book list");



        goodsBt =findViewById(R.id.goodsBt);
        inputKeyBt =findViewById(R.id.inputKeyBt);
        settingsBt = findViewById(R.id.settingsBt);

        c0 =findViewById(R.id.c0);
        c1 =findViewById(R.id.c1);
        c2=findViewById(R.id.c2);
        c3=findViewById(R.id.c3);
        c4=findViewById(R.id.c4);
        c5=findViewById(R.id.c5);
        c6=findViewById(R.id.c6);
        c7=findViewById(R.id.c7);
        c8=findViewById(R.id.c8);
        c9=findViewById(R.id.c9);
        cDot=findViewById(R.id.cDot);
        cClear=findViewById(R.id.cClear);
        btCancel=findViewById(R.id.btCancel);
        btOk=findViewById(R.id.btOk);

        lastGoodName=findViewById(R.id.lastGoodName);
        lastGoodCnt=findViewById(R.id.lastGoodCnt);





        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        searchArticle = (CheckBox) findViewById(R.id.searchArticle);


        hideCameraButton = findViewById(R.id.cameraBt);
        hideCameraButton.setTag("0");
        // hideCameraButton.setImageResource(R.drawable.ic_close_white_36dp);
        hideCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if((  (String)hideCameraButton.getTag() ).equals("0") ){
                    v.setTag("1");
                    hideCameraButton.setImageResource(R.drawable.ic_camera_alt_white_36dp);
                   // barcodeScannerView.setVisibility(View.GONE);
                  //  switchFlashlightButton.setVisibility(View.INVISIBLE);
                    //capture.onPause();
                    barcodeScannerView.pause();
                    MainApplication.dbHelper.insertOrReplaceOption(MainApplication.CAMERA_STATE,MainApplication.CAMERA_OFF);

                }
                else*/
                openCameraCounter++;
                if (openCameraCounter <= 3) {

                }
                {
                    v.setTag("0");
                    startCamera();
                }
            }
        });







       /* capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();*/


        //setProgressBarIndeterminateVisibility(true);


      /*  mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_menu_white_36dp, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);*///


       // MenuAdapter menuAdapter = new MenuAdapter();

       // mDrawerList.setAdapter(menuAdapter);///////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


       /* mainActionBar = getActionBar(); //main bar
        mainActionBar.setDisplayShowCustomEnabled(true);
        mainActionBar.setDisplayShowHomeEnabled(false);
        mainActionBar.setCustomView(R.layout.abar_scan_settings);*/


        mainActionBar = getSupportActionBar();
        mainActionBar.setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        mainActionBar.setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);

        mainActionBar.setCustomView(R.layout.abar_scan_settings);
      //  mainActionBar.setDisplayHomeAsUpEnabled(true);
        View abarView = mainActionBar.getCustomView();
        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        abarView.findViewById(R.id.back).setOnClickListener((v)->{ finish(); });

        String colorTint = "#c62828";
        switch (theme){

           // case THEME_RED:
              //  colorTint = "#c62828";
              //  break;
            case THEME_TEAL:
                colorTint = "#009688";
                break;
            case THEME_INDIGO:
                colorTint = "#3F51B5";
                break;
            case THEME_BLUE_GREY:
                colorTint = "#616161";
                break;

                default:
                    colorTint = "#616161";
        }

        goodsBt.setColorFilter(Color.parseColor(colorTint));
        inputKeyBt.setColorFilter(Color.parseColor(colorTint));
        settingsBt.setColorFilter(Color.parseColor(colorTint));
        hideCameraButton.setColorFilter(Color.parseColor(colorTint));
        searchArticle.setTextColor(Color.parseColor(colorTint));


        Intent iin = getIntent();

        Bundle bundle = iin.getExtras();

        try {
            docType = bundle.getInt("docType");
            id = bundle.getInt("id");
            date = bundle.getString("date");
            subdiv = bundle.getString("subdiv");
        } catch (Exception e) {
            Log.d("my", " no id =((( ");
        }

        Log.d("my", "myId = " + id);
        if (id == -1) {
            selectOrCreateDocument();
        }


        list = (ListView) findViewById(R.id.list);

        searchEditText = (EditText) mainActionBar.getCustomView().findViewById(R.id.searchEditText);
        searchBt = mainActionBar.getCustomView().findViewById(R.id.searchBt);
        listRelLayout = (RelativeLayout) findViewById(R.id.listRelLayout);
        goodDetailsRelLayout = findViewById(R.id.goodDetailsRelLayout);
        goodsName = (TextView) findViewById(R.id.goodsName);
        goodsBarcode = (TextView) findViewById(R.id.goodsBarcode);
        goodsArticle = (TextView) findViewById(R.id.goodsArticle);
        goodsPrice = findViewById(R.id.goodsPrice);
        cntRelLayout = (RelativeLayout) findViewById(R.id.cntRelLayout);
        goodsCntEditText = (EditText) findViewById(R.id.goodsCnt);
        factCntTextView = (TextView) findViewById(R.id.factCnt);
        plusTextView = (TextView) findViewById(R.id.plusTextView);
        plusButton = (Button) findViewById(R.id.plusButton);
        minusButton = (Button) findViewById(R.id.minusButton);


        calculatorLinLayout = (LinearLayout) findViewById(R.id.calculatorLinLayout);

        c0.setOnClickListener(c0_Click);
        c1.setOnClickListener(c1c9_Click);
        ;
        c2.setOnClickListener(c1c9_Click);
        c3.setOnClickListener(c1c9_Click);
        c4.setOnClickListener(c1c9_Click);
        c5.setOnClickListener(c1c9_Click);
        c6.setOnClickListener(c1c9_Click);
        c7.setOnClickListener(c1c9_Click);
        c8.setOnClickListener(c1c9_Click);
        c9.setOnClickListener(c1c9_Click);
        cDot.setOnClickListener(cDot_Click);
        cClear.setOnClickListener(cClear_Click);
        applyLinLayout = (LinearLayout) findViewById(R.id.applyLinLayout);
        subRelLayout = (LinearLayout) findViewById(R.id.subRelLayout);
        docListButton1 = (ImageButton) findViewById(R.id.docListButton1);
        keyBoardButton1 = (ImageButton) findViewById(R.id.keyBoardButton1);

        scansettingsButton = (ImageButton) findViewById(R.id.scansettingsButton);

        exportProgress = (ProgressBar) findViewById(R.id.exportProgress);


        keyBoardButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showInputMethodPicker();
            }
        });

        goodsPrice.setOnClickListener(updatePriceListener);


        keyBoardButton1.setFocusable(false);

        scansettingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ScannerActivity.this, SettingScanModeActivity.class);
                startActivityForResult(intent, 0);
            }
        });


        testButton = (Button) findViewById(R.id.testButton);
        //testButton.setVisibility(View.VISIBLE);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test();
            }
        });


        searchEditText.setOnKeyListener(findListener);
        searchEditText.setOnClickListener(searchEditTextListener);
        searchEditText.setOnTouchListener(clearTouchListener);
//
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int keyAction, KeyEvent keyEvent) {

                Log.d("myKey","keyAction="+keyAction+" keyEvent="+keyEvent.toString());
                if (
                    //Soft keyboard search
                        keyAction == EditorInfo.IME_ACTION_SEARCH ||
                                //Physical keyboard enter key
                                (keyEvent != null && KeyEvent.KEYCODE_ENTER == keyEvent.getKeyCode()
                                        && keyEvent.getAction() == KeyEvent.ACTION_DOWN)) {
                   // new SearchForEquipmentTask(false, viewHolder.mTextSearch
                      //      .getQuery().toString()).execute();
                    return true;
                }
                return false;
            }
        });

        searchBt.setOnClickListener(searchButtonListener);

        goodsCntEditText.setOnClickListener(goodsCntListener);
        goodsCntEditText.setOnTouchListener(backspaceTouchListener);
        goodsCntEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {


            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                Log.d("my", "CURRENT SCAN MODE = " + CURRENT_SCAN_MODE);
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        // done stuff
                        hideKeyboard();
                        if (CURRENT_SCAN_MODE == 0 || CURRENT_SCAN_MODE == 1) {
                            Log.d("my", "DONE!!!");
                            sendInvDt();
                        }
                        if (CURRENT_SCAN_MODE == 2) {
                            cntPlusFactShow();
                        }
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return true;
            }

            ;
        });
        //InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.hideSoftInputFromWindow(goodsCntEditText.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);

        plusButton.setOnClickListener(plusMinusClickListener);
        minusButton.setOnClickListener(plusMinusClickListener);


		  /*numberPicker =  (NumberPicker) findViewById(R.id.numberPicker);
          numberPicker.setMinValue(0);
		  numberPicker.setMaxValue(10);
		  numberPicker.setWrapSelectorWheel(true);*/


        btCancel.setOnClickListener(clearClickListener);
        btOk.setOnClickListener(commitInvClickListener);

       // docListButton1.setOnClickListener(goodsListListener);


        Log.d("my", "  onCreateView scanwork");

        adapter = new GoodsListAdapter<Good>(ScannerActivity.this,
                R.layout.adapter_good_row, goodList, id);
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        Drawable img = getResources().getDrawable(R.drawable.ic_mode_edit_black_18dp);

        if (connectMode == CONNECT_OFFLINE) {
            // goodsPrice.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        } else goodsPrice.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);


        prefs = getSharedPreferences("com.app.barcodeclient3", MODE_PRIVATE);

        goodsBt.setOnClickListener((v)->{
            if (connectMode == CONNECT_OFFLINE) {
                Intent intent = new Intent(ScannerActivity.this,
                        TotalGoodsListActivity.class);
                Bundle arg = new Bundle();
                arg.putInt("id", id);
                intent.putExtras(arg);
                startActivity(intent);
            }
            if (connectMode == CONNECT_ONLINE)
            {
                Intent intent = new Intent(ScannerActivity.this,
                        GoodsListActivity.class);
                Bundle arg = new Bundle();
                arg.putInt("id", id);
                intent.putExtras(arg);
                startActivity(intent);
            }
        });

        inputKeyBt.setOnClickListener((v)->{
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showInputMethodPicker();
        });

        settingsBt.setOnClickListener((v)->{
            Intent intent = new Intent(ScannerActivity.this, SettingScanModeActivity.class);
            startActivityForResult(intent, 0);
        });


        searchRequestFocus();

      //  ThreadScanSettings threadScanSettings = new ThreadScanSettings();
       // threadScanSettings.start();


    }




    private void searchRequestFocus() {
        searchEditText.setFocusableInTouchMode(true);
        searchEditText.requestFocus();
        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    Dialog dialog;

    com.journeyapps.barcodescanner.DecoratedBarcodeView zxing_barcode_scanner;

    private void showCamera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_camera, null);
        switchFlashlightButton = view.findViewById(R.id.switch_flashlight);
        switchFlashlightButton.setTag("0");
        switchFlashlightButton.setImageResource(R.drawable.ic_highlight_white_36dp);
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        ImageView closeBt = view.findViewById(R.id.closeBt);
        closeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zxing_barcode_scanner.pause();
                dialog.dismiss();
            }
        });

        zxing_barcode_scanner = view.findViewById(R.id.zxing_barcode_scanner);
        zxing_barcode_scanner.resume();

        barcodeScannerView = (DecoratedBarcodeView) view.findViewById(R.id.zxing_barcode_scanner);
        barcodeScannerView.setTorchListener(this);
        barcodeScannerView.decodeContinuous(callback);
        barcodeScannerView.setVisibility(View.VISIBLE);
        barcodeScannerView.resume();
        barcodeScannerView.setStatusText("");

        builder.setView(view);
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                zxing_barcode_scanner.pause();
                dialog.dismiss();

            }
        });

        dialog = builder.create();
        dialog.show();
    }


    private void startCamera() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("my", "get gallery android 6+");


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                Log.d("my", "get gallery no granted");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 0);
            } else {
                Log.d("my", "get gallery granted!");
                //takePicture();
                //startCameraIntegrator();
                showCamera();

            }
        } else {
            Log.d("my", "get gallery android 4,5");
            //takePicture();
            //startCameraIntegrator();
            showCamera();
        }


    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @androidx.annotation.NonNull final String[] permissions, @androidx.annotation.NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("my", "permissions = " + permissions[0]);

      /*  Log.d("my", "permissions2 = ");
        if (requestCode == 0) {
            Log.d("my", "permissions3 = grantResults="+grantResults.length);
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.d("my", "permissions4 = ");
                //takePicture();
                if (permissions[0].equals(Manifest.permission.CAMERA)) {
                    Log.d("my", "permissions5 = ");
                    showCamera(); //startCameraIntegrator();
                }
                if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    if(exportInventoryToXLS!=null) exportInventoryToXLS.exportInvToXLSWithPermissions();

            }
        }*/

        if(hasAllPermissionsGranted(grantResults)){
            // all permissions granted
            Log.d("my", "hasAllPermissionsGranted ");
            if (permissions[0].equals(Manifest.permission.CAMERA)) {
                Log.d("my", "permissions5 = ");
                showCamera(); //startCameraIntegrator();
            }
            if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                if(exportInventoryToXLS!=null) exportInventoryToXLS.exportInvToXLSWithPermissions();


        }else {
            // some permission are denied.
            Log.d("my", "some permission are denied. ");
        }
    }

    public boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }




    /*private void startCameraIntegrator(){

        hideCameraButton.setImageResource(R.drawable.ic_close_white_36dp);
        barcodeScannerView.setVisibility(View.VISIBLE);
        switchFlashlightButton.setVisibility(View.VISIBLE);
        //capture.onResume();
        barcodeScannerView.resume();
        MainApplication.dbHelper.insertOrReplaceOption(MainApplication.CAMERA_STATE,MainApplication.CAMERA_ON);
    }*/


    ArrayList<Good> accGoods = new ArrayList<>();

    class CheckOffineEditDocument extends Thread {
        @Override
        public void run() {
            accGoods.clear();
            ArrayList<Good> alg = MainApplication.dbHelper.getGoodsCountListAcc(id);
            accGoods.addAll(alg);

            if (accGoods.size() > 0) {
                ht.post(new Runnable() {
                    @Override
                    public void run() {
                        exportDataToServerDialog();
                    }
                });
            }

        }
    }

    ;

   /* class ThreadScanSettings extends Thread {
        @Override
        public void run() {
            if (!MainApplication.OFFLINE_MODE) {
                RequestScanSettings requestScanSettings = new RequestScanSettings();
                JSONObject scanJSON = requestScanSettings.getScanSettings();
                Log.d("my", "scan settings = " + scanJSON.toString());
                if (scanJSON.length() > 0) {
                    try {
                        MainApplication.WEIGTH_BARCODE = scanJSON.getString("WEIGTH_BARCODE");
                    } catch (Exception e) {
                    }
                    try {
                        MainApplication.WEIGTH_BARCODE_MASK = scanJSON.getString("WEIGTH_BARCODE_MASK");
                    } catch (Exception e) {
                    }
                }
            }
            ht.post(new Runnable() {
                @Override
                public void run() {
                    CheckOffineEditDocument checkOffineEditDocument = new CheckOffineEditDocument();
                    checkOffineEditDocument.start();
                }
            });
        }
    }*/

    ;

    Handler ht = new Handler();


    private void checkWeigthBarcode(String barcode) {
        if (MainApplication.WEIGTH_BARCODE.length() >= 11 && MainApplication.WEIGTH_BARCODE_MASK.length() >= 2) {

            //String pref1=MainActivity.WEIGTH_BARCODE.substring(2,4);
            //String pref2=MainActivity.WEIGTH_BARCODE.substring(2,4);
            //String pref3=MainActivity.WEIGTH_BARCODE.substring(2,4);
            getScanPrefixs(MainApplication.WEIGTH_BARCODE);

        }
    }

    String[] getScanPrefixs(String sampleString) {
        sampleString = "fgh(101|203|405)dchh";
        String[] stringArray = sampleString.split("|");
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            String numberAsString = stringArray[i];
            intArray[i] = Integer.parseInt(numberAsString);
        }
        System.out.println("Number of integers: " + intArray.length);
        System.out.println("The integers are:");
        for (int number : intArray) {
            System.out.println(number);

        }
        Log.d("my", stringArray.toString());
        return stringArray;
    }


    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_HIDDEN);//!!!!!!!!!!!!!!!!!!!!!!!!
            //to show soft keyboard
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

//to hide it, call the method again
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }


    View.OnClickListener searchButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("myScanner", "search bt1!");
            search();
        }
    };

    private void search()//////!!!!!!!!!!!!!!!!!!!!!!
    {
        Log.d("myScanner", "search bt2!");

        hideKeyboard();

        String barc = searchEditText.getText().toString().trim();

        if (barc.trim().length() > 0) {
            //	clearfindButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.close_32));
            originGoodListSaver.clear();
            originGoodListSaver.addAll(goodList);
            enter = 0;
            newScan = true;
            searchEditText.setFocusable(true);
            //isEnabledFindButton=false;

            setGoodsByBarc(barc, barc, barc);

        }
    }


    View.OnTouchListener clearTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            TextView tv = (TextView) v;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (tv.getRight() - tv.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    Log.d("my", "clear");
                    tv.setText("");
                    return true;
                }
            }
            return false;
        }
    };

    View.OnTouchListener backspaceTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            TextView tv = (TextView) v;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (tv.getRight() - tv.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    Log.d("my", "buckspace");

                    String current_text = tv.getText().toString();
                    if (current_text.length() > 0) {
                        String new_text = current_text.substring(0, current_text.length() - 1);
                        tv.setText(new_text);
                    }

                    return  true;
                }
            }
            return false;
        }
    };


    View.OnKeyListener findListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            //if(!blockScanner){
            Log.d("myScanner", "onKey keyCode="+keyCode+" event="+event.toString());

            if (keyCode == 66) {
                enter++;
                Log.d("my", "key=" + enter);
            }
            if (enter == 2) {

                //clearfindButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.close_32));
                /*originGoodListSaver.clear();
                originGoodListSaver.addAll(goodList);
				enter=0;
				newScan=true;
				searchEditText.setFocusable(true);
				//isEnabledFindButton=false;
				String barc = searchEditText.getText().toString();*/
                //setGoodsByBarc(barc,barc);
                Log.d("myScanner", "search bt1dd scanner!!! ");
                search();
                return false;
            }

            //}
            return  true; //was true;
        }

    };

    class LoadGoodsByBarcThread extends Thread {
        @Override
        public void run() {

            ArrayList<Good> goodsList = new ArrayList<>();
            Log.d("my", "IS checked = " + searchArticle.isChecked());
            if ( connectMode == CONNECT_OFFLINE) {


                    if (!searchArticle.isChecked()) {
                        goodsList = MainApplication.dbHelper.getGoodList(name, article, barcode, id);
                    } else {
                        goodsList = MainApplication.dbHelper.getGoodList("", article, "", id);
                    }


                goodList.clear();
                goodList.addAll(goodsList);
                Log.d("my", "otpravili tovar");
                h.post(readyGoodsHandler);
            }

            if (connectMode == CONNECT_ONLINE) {
                //  goodsList =searchGoodOnline(name,searchArticle.isChecked());
                Map<String, String> data = new HashMap<>();
                if (searchArticle.isChecked()) {
                    data.put(ARTICLE, name);

                } else {
                    data.put(NAME, name);
                    data.put(BARCODE, name);
                }

                data.put(INVENTORY_ID, "" + ScannerActivity.id);


                MainApplication.getApi().getGoodsList(data, new Callback<ArrayList<GoodJSON>>() {
                    @Override
                    public boolean sendResult(Response<ArrayList<GoodJSON>> response) {
                        ArrayList<GoodJSON> list = response.body();
                        if (list == null) return false;

                        Log.d("my", "we get goods list !!!! " + list.size());
                        GoodJSON good = new GoodJSON();

                        goodList.clear();


                        for (GoodJSON goodJSON : list) {
                            goodList.add(goodJSON.toGoodEssence());
                        }
                        Log.d("my", "otpravili tovar");
                        h.post(readyGoodsHandler);
                        return false;
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }


        }
    }


    Handler h = new Handler();

    Handler h3 = new Handler();

    private static boolean firstCreateGood = true;

    Runnable readyGoodsHandler = new Runnable() {


        public void run() {

            Log.d("my", "thread: goodslist sz = " + goodList.size());// buildList();
            Log.d("my", "build rows goods!!!");
            adapter.notifyDataSetChanged();
            hideProgressBar();
            goodDetailsRelLayout.setOnClickListener(null);
            int size = goodList.size();
            switch (size) {
                case 0:
                    clearGoodInfo();
                    searchFocusable(true);
                    if (MainApplication.OFFLINE_MODE) {//dialog. Tovar ne naiden sozdat' ?
                    /*	createGoodDialogBuilder = new AlertDialog.Builder(ScanWorkingActivity.this);
                        createGoodDialogBuilder.setMessage(getString(R.string.good_not_found_create)).setPositiveButton(getString(R.string.create), createGoodDialogClickListener)
								.setNegativeButton(getString(R.string.cancel), createGoodDialogClickListener);
						createGoodDialog = createGoodDialogBuilder.create();
						createGoodDialog.show();*/


                        if (!NOT_CREATE_GOOD) {


                            CreateGoodDialog goodDialog = new CreateGoodDialog();


                            Bundle args = new Bundle();
                            String good = barcode;//searchEditText.getText().toString();
                            args.putString("name", good);
                            goodDialog.setArguments(args);

                            if (firstCreateGood) {
                                firstCreateGood = false;
                            }

                            goodDialog.show(getFragmentManager(), "Hello!");
                        }
                    }

                    break;
                case 1:
                    Good good = goodList.get(0);
                    currentGood = good;
                    showOneGood(currentGood);
                    searchFocusable(false);
                    break;
                default:
                    if (adapter != null) {


                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (Good good1 : goodList) {
                                    //double cnt = MainActivity.dbHelper.getGoodCountAcc(good1.getId(), id);
                                    Log.d("my", "---*** cnt = " + good1.getFcnt());
                                    //good1.setFcnt(cnt);
                                }
                                h3.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("my", "adapter no null");
                                        adapter.notifyDataSetChanged();

                                        Log.d("my", "222");
                                        adapter.setGoodClickListener(goodClickListener);
                                        adapter.notifyDataSetChanged();
                                        list.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();/////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHeck it !!!!
                                        showGoodsList(true);
                                        adapter.notifyDataSetChanged();/////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHeck it !!!!
                                    }
                                });
                            }
                        }).start();


                    }

                    break;
            }
        }
    };


    private void clearGoodInfo() {
        currentGood = null;
        goodsName.setText("");
        goodsArticle.setText("");
        goodsBarcode.setText("");
        goodsPrice.setText("");
        goodsCntEditText.setText("");
        searchEditText.setText("");
        factCntTextView.setTextColor(getResources().getColor(R.color.dark_gray));
        //searchEditText.setFocusable(View.FOCUSABLE);


        ////

        adapter.notifyDataSetChanged();

    }


    private void showOneGood(Good good) {

        hideKeyboard();
        //Good good = goodList.get(0);
        String name = good.getName();
        String article = good.getArticle();
        ArrayList<String> barcodes = good.getBarcodes();
        Log.d("my", "barcodes &&&=" + barcodes);
        String barcodStr = "";
        for (int i = 0; i < barcodes.size(); i++) {
            barcodStr += barcodes.get(i);
            if (i < barcodes.size() - 2) barcodStr += "\n";
        }
        double out_price = good.getOut_price();

        goodsName.setText(name);
        goodsArticle.setText(article);
        goodsBarcode.setText(barcodStr);
        goodsPrice.setText(String.format("%.2f", out_price));

        double fcnt = good.getFcnt();
        setFactCntTextView(fcnt);

    }


    private void showGoodsList(boolean show) {
        if (show) {
            listRelLayout.setVisibility(View.VISIBLE);

            goodDetailsRelLayout.setVisibility(View.GONE);
            cntRelLayout.setVisibility(View.GONE);
            calculatorLinLayout.setVisibility(View.GONE);
            applyLinLayout.setVisibility(View.GONE);
            subRelLayout.setVisibility(View.GONE);
            //typeCntRelLayout.setVisibility(View.GONE);
        } else {

            listRelLayout.setVisibility(View.GONE);

            goodDetailsRelLayout.setVisibility(View.VISIBLE);
            cntRelLayout.setVisibility(View.VISIBLE);
            calculatorLinLayout.setVisibility(View.VISIBLE);
            applyLinLayout.setVisibility(View.VISIBLE);
            subRelLayout.setVisibility(View.VISIBLE);
            //typeCntRelLayout.setVisibility(View.VISIBLE);
        }
    }


    private void cntPlusFactShow() {
        String strFcnt = factCntTextView.getText().toString();
        String strCnt = goodsCntEditText.getText().toString();
        double sum = 0;

        if (strCnt.length() > 0) {

            double factcnt = 0;
            double cnt = 0;

            try {
                factcnt = Double.parseDouble(strFcnt);
                Log.d("my", "01");
            } catch (Exception e) {
                factcnt = 0;
                Log.d("my", "e1");
            }

            try {
                cnt = Double.parseDouble(strCnt);
                sum = cnt + factcnt;
                factCntTextView.setText("");
                goodsCntEditText.setText("" + sum);
                Log.d("my", "02");
            } catch (Exception e) {
                Log.d("my", "e2");

            }

        }

    }

    View.OnClickListener goodClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Object tag = view.getTag();
            if (tag != null) {
                if (tag instanceof Good) {
                    Good good = (Good) tag;
                    String name = good.getName();
                    Log.d("my", "name = " + name);

                    showGoodsList(false);
                    currentGood = good;
                    showOneGood(currentGood);
                    goodDetailsRelLayout.setOnClickListener(goodDetailsRelLayoutListener);
                    searchFocusable(false);
                }
            }

        }
    };


    public void setGoodsByBarc(String barcode, String article, String name) {

        searchEditText.setText("");


        this.barcode = barcode;
        this.article = article;
        this.name = name;

        //showProgressBar(true,getString(R.string.loading_goods)+ " by BARCODE...");
        LoadGoodsByBarcThread loadGoodsThread = new LoadGoodsByBarcThread();
        loadGoodsThread.start();
    }

    View.OnClickListener searchEditTextListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            searchFocusable(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//!!!!!!
            if (imm != null) {
                imm.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    };

    View.OnClickListener goodsCntListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            searchFocusable(false);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(goodsCntEditText.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            //hideKeyboard(); ///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
    };

    private void searchFocusable(boolean searchFocusable) {
        if (searchFocusable) {
            //searchEditText.setFocusable(true);
            searchEditText.requestFocus();
            //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
        } else {
            //goodsCntEditText.setFocusable(true);
            goodsCntEditText.requestFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(goodsCntEditText.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            hideKeyboard();

            //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//!!!!!!!!!!!!!!!!!!!!!!!!
            //imm.showSoftInput(goodsCntEditText, InputMethodManager.SHOW_IMPLICIT);


        }
    }

    View.OnClickListener goodDetailsRelLayoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showGoodsList(true);
        }
    };

   /* public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        Log.d("my","SCANNED!");
        if(dialog!=null) dialog.dismiss();


        if (scanningResult != null) {


            //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();
            //formatTxt.setText("FORMAT: " + scanFormat);
            if (scanContent != null) {

                Log.d("my", "scanContent" + scanContent);
                searchEditText.setText(scanContent);


                if (!scanContent.equals("")) {
                    //	clearfindButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.close_32));
                    originGoodListSaver.clear();
                    originGoodListSaver.addAll(goodList);
                    enter = 0;
                    newScan = true;
                    searchEditText.setFocusable(true);
                    //isEnabledFindButton=false;
                    String barc = searchEditText.getText().toString();
                    setGoodsByBarc(barc, barc, barc);

                }
            }
        } else {
            if (requestCode != 911) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                //toast.show();!!!!!/////////////////////////////////////////
            }
        }

        Log.d("my", "update scan_mode");
        CURRENT_SCAN_MODE = ScannerConstants.CURRENT_SCAN_MODE;
        switch (CURRENT_SCAN_MODE) {
            case 0:
                plusTextView.setText("");
                break;
            case 2:
                plusTextView.setText("+");
                break;
            case 1:
                plusTextView.setText("+");
                break;
            default:
                plusTextView.setText("");
                break;
        }

        Log.d("my", "activity result: requestCode =" + requestCode + " result Code = " + resultCode);

    }*/




    private void setFactCntTextView(double fcnt) {
        double fcntPlusOne = fcnt + 1;
        goodsCntEditText.setSelection(goodsCntEditText.getText().length());
        switch (scanType) { //------
            case 0:
                factCntTextView.setText("");
                goodsCntEditText.setText("" + fcnt);
                goodsCntEditText.setSelectAllOnFocus(true);
                break;
            case 1:
                factCntTextView.setText("" + fcnt);
                goodsCntEditText.setText("" + fcntPlusOne);
                goodsCntEditText.setSelectAllOnFocus(true);
                sendInvDt();
                break;
            case 2:
                factCntTextView.setText("" + fcnt);
                goodsCntEditText.setText("0.00");
                goodsCntEditText.setSelectAllOnFocus(true);
                break;
        }
    }

    View.OnClickListener plusMinusClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof Button) {
                if (currentGood != null) {
                    Button bt = (Button) view;
                    String stD = bt.getText().toString();

                    String goodsCntStr = goodsCntEditText.getText().toString();

                    double cnt = 0;

                    try {
                        cnt = Double.parseDouble(goodsCntStr);
                    } catch (Exception e) {
                        cnt = 0;
                    }


                    if (stD.contains("+")) {
                        cnt += 1;
                        goodsCntEditText.setText("" + cnt);
                    } else {
                        cnt -= 1;
                        double fcnt = currentGood.getFcnt();
                        double anti_fcnt = fcnt * -1;
                        if (cnt < 0) {
                            if (CURRENT_SCAN_MODE == 0) {
                                cnt = 0;
                            } else {
                                if (cnt < anti_fcnt) cnt = 0;
                            }
                        }
                        goodsCntEditText.setText("" + cnt);
                    }
                }
            }

        }
    };


    ProgressDialog dialogGoToBookDetails;

    public void showProgressBar(boolean cancelable, String text) {

        dialogGoToBookDetails = new ProgressDialog(this);
        dialogGoToBookDetails.setMessage(text);
        dialogGoToBookDetails.setIndeterminate(true);
        dialogGoToBookDetails.setCancelable(cancelable);
        dialogGoToBookDetails.show();
    }

    public void hideProgressBar() {
        if (dialogGoToBookDetails != null) dialogGoToBookDetails.hide();
    }

    View.OnClickListener clearClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (fast_search) {
                okBtSearchMode = true;
                //searchEditText.setFocusable(true);
                //goodsCntEditText.setFocusable(false);
                searchEditText.requestFocus();
            }
            clearGoodInfo();
            // barcodeScannerView.resume();
            //barcodeScannerView.setStatusText("");
        }
    };

    int commits = 0;

    boolean okBtSearchMode = true;
    final static int step0 = 0;
    final static int step1 = 1;

    View.OnClickListener commitInvClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {



            commits++;
            if (commits <= 0) {
            }

            if(fast_search){
                Log.d("my","fast_search.."+okBtSearchMode);
                if(okBtSearchMode){
                    Log.d("myScanner", "search bt1!vf");
                    search();
                    okBtSearchMode = false;
                    return;
                }

            }

            // barcodeScannerView.resume();
            // barcodeScannerView.setStatusText("");

            sendInvDt();
        }
    };


    private void sendInvDt() {
        String strcnt = "";
        Log.d("my", "sendInvDt()");

		/*if(CURRENT_SCAN_MODE==ScannerConstants.NORMAL_SCAN||CURRENT_SCAN_MODE==ScannerConstants.PLUS_ONE_SCAN) {
            goodsCntEditText.getText().toString();
		}*/
        if (CURRENT_SCAN_MODE == PLUS_FACT_SCAN) {
            Log.d("my", "CURRENT_SCAN_MODE==ScannerConstants.PLUS_FACT_SCAN");
            cntPlusFactShow();
        }
        strcnt = goodsCntEditText.getText().toString();

        if (strcnt.length() > 0) {
            try {
                double cnt = Double.parseDouble(strcnt);
                SendInvDt sendInvDt = new SendInvDt(id, currentGood.getId(), cnt);
                sendInvDt.start();

            } catch (Exception e) {
            }

        }
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%// KEYBOARD CALC


    private EditText display_currentTextView = null;
    boolean rule = false;

    public View.OnClickListener c1c9_Click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("my", "cifra");
            getCurrentFocusTextView();
            if (display_currentTextView != null) {

                String currentTxt = display_currentTextView.getText().toString();
                Button bt = (Button) view;
                String touchC = bt.getText().toString();
                if (rule) {
                    if (currentTxt.equals("0")) currentTxt = touchC;
                    else currentTxt += touchC;
                } else currentTxt += touchC;
                display_currentTextView.setText(currentTxt);
                display_currentTextView.setSelection(0);


            }
        }
    };

    public View.OnClickListener c0_Click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getCurrentFocusTextView();
            if (display_currentTextView != null) {
                String currentTxt = display_currentTextView.getText().toString();
                Button bt = (Button) view;
                String touchC = bt.getText().toString();
                if (rule) {
                    if (currentTxt.equals("0")) currentTxt = "0";
                    else currentTxt += touchC;
                } else currentTxt += touchC;
                display_currentTextView.setText(currentTxt);
                display_currentTextView.setSelection(0);
            }
        }
    };

    public View.OnClickListener cDot_Click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getCurrentFocusTextView();
            if (display_currentTextView != null) {
                String currentTxt = display_currentTextView.getText().toString();
                Button bt = (Button) view;
                String touchC = bt.getText().toString();
                if (rule) {
                    if (currentTxt.equals("")) currentTxt = "0" + touchC;
                    else {
                        if (!currentTxt.contains(touchC))
                            currentTxt += touchC;
                    }
                } else currentTxt += touchC;
                display_currentTextView.setText(currentTxt);
                display_currentTextView.setSelection(0);
            }
        }
    };

    public View.OnClickListener cClear_Click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getCurrentFocusTextView();
            if (display_currentTextView != null) {

                display_currentTextView.setText("");
                display_currentTextView.setSelection(0);
            }
        }
    };


    private void getCurrentFocusTextView() {
        if (goodsCntEditText.isFocused()) {
            Log.d("my", " good cnt focusable");
            display_currentTextView = goodsCntEditText;
            rule = true;
        }
        if (searchEditText.isFocused()) {
            Log.d("my", "search focusable");
            display_currentTextView = searchEditText;
            rule = false;
        }

        if (display_currentTextView != null) {
            boolean isSelected = display_currentTextView.isSelected();
            String current_text = display_currentTextView.getText().toString();
            int startSelection = display_currentTextView.getSelectionStart();
            int endSelection = display_currentTextView.getSelectionEnd();
            String selectedText = current_text.
                    substring(startSelection, endSelection);
            current_text = current_text.replace(selectedText, "");
            display_currentTextView.setText(current_text);
            Log.d("my", "text selected = " + isSelected);

        }
    }


    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ INVENTORY SEND


    class SendInvDt extends Thread {



        int invId;
        int goodId;
        double cnt;

        public SendInvDt(int invId, int goodId, double cnt) {
            Log.d(my,"SendInvDt");

            this.invId = invId;
            this.goodId = goodId;
            this.cnt = cnt;

            tempcnt = cnt;
            tempGoodId = goodId;
        }

        @Override
        public void run() {
            Log.d(my, "MainApplication.OFFLINE_MODE = " + MainApplication.OFFLINE_MODE);

            sendedCnt = this.cnt;
            sendedGoodsName = goodsName.getText().toString();


            if (MainApplication.OFFLINE_MODE) {
                MainApplication.dbHelper.insertGoodsAcCnt(invId, goodId, cnt);
                tempOfflineAnsCnt = MainApplication.dbHelper.getGoodCountAcc(invId, goodId);
                h2.post(readyOffLineInvWriteCnt);

            }

            if (connectMode == CONNECT_ONLINE) {

                if (!checkLicense()) {
                    if (CURRENT_FREE_SCANS > MAX_FREE_SCANS) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                showLicenseDialog();
                            }
                        });
                        return;
                    }
                }
                getApi().sendEditInventoryDt(invId, goodId, (float) cnt, new Callback() {
                    @Override
                    public boolean sendResult(Response response) {

                        if (response == null) {

                        } else {
                            if (response.body() != null) {
                                int result = (int) response.body();
                                Log.d("my", "we have inv dt result result : " + result);

                                //0  is good!
                                //-1 is fail!
                                searchFocusable(true);
                                clearGoodInfo();

                                if (result == 0) {
                                    //factCntTextView.setText("" + tempOfflineAnsCnt);
                                    //factCntTextView.setTextColor(getResources().getColor(R.color.green));

                                    // Log.d("my", "ans4 ok");


                                    if (fast_search) {
                                        okBtSearchMode = true;
                                      //  searchEditText.setFocusable(true);
                                      //  goodsCntEditText.setFocusable(false);
                                        searchEditText.requestFocus();
                                    }
                                    lastGoodCnt.setText("" + sendedCnt);
                                    lastGoodName.setText(sendedGoodsName);
                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            R.string.labelled, Toast.LENGTH_SHORT);
                                    toast.show();
                                    playDone();
                                    addFreeScan(); //up counter of scan
                                } else {
                                    new AlertDialog.Builder(ScannerActivity.this)
                                            .setTitle("Error")
                                            .setMessage("Error")
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // continue with delete
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                    playError();
                                }


                            }
                        }
                        return false;
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }
        }
    }

    ;

    Handler h2 = new Handler();


    Runnable readyOffLineInvWriteCnt = new Runnable() {
        @Override
        public void run() {

            searchFocusable(true);
            clearGoodInfo();

            if (tempOfflineAnsCnt >= 0) {
                //factCntTextView.setText("" + tempOfflineAnsCnt);
                // factCntTextView.setTextColor(getResources().getColor(R.color.gold));

                lastGoodCnt.setText("" + sendedCnt);
                lastGoodName.setText(sendedGoodsName);
                if (fast_search) {
                    okBtSearchMode = true;
                    //searchEditText.setFocusable(true);
                   // goodsCntEditText.setFocusable(false);
                    searchEditText.requestFocus();
                }
                Log.d("my", "ans4 ok");
                Toast toast = Toast.makeText(getApplicationContext(),
                        R.string.labelled, Toast.LENGTH_SHORT);
                toast.show();
                playDone();
            } else {
                new AlertDialog.Builder(ScannerActivity.this)
                        .setTitle("Error")
                        .setMessage("Ошибка чтения/записи SQLLite")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                playError();
            }

        }
    };


    double tempOfflineAnsCnt = 0;
    JSONObject ansJSON = null;
    double tempcnt = 0;
    int tempGoodId = 0;




    private void showLicenseDialog() {
        new AlertDialog.Builder(ScannerActivity.this)
                .setTitle(getString(R.string.error))
                .setMessage(getString(R.string.license_inactive))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    private void exportDataToServerDialog() {
        String msg = getString(R.string.exist_acc);
        String message = "" + msg + " \n Непереданных товаров: " + accGoods.size();
        int max = 8;
        int mx = max;
        if (max > accGoods.size()) mx = accGoods.size();
        for (int i = 0; i < mx; i++) {

            message += " " + accGoods.get(i).getName();
            if (mx > 2 && i < mx - 1) message += " ,\n ";
        }

        if (max < accGoods.size()) message += "...";

        //exportInvToXLS()

        String expType = "";
        if (!MainApplication.OFFLINE_MODE) {
            expType = getString(R.string.export_server);
        } else {
            expType = getString(R.string.export_file);
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(ScannerActivity.this)
                .setTitle(R.string.message)
                .setMessage(message)

                .setNegativeButton(R.string.clear, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ClearOfflineData clearOfflineData = new ClearOfflineData();
                        clearOfflineData.start();
                    }
                })
                .setNeutralButton(R.string.ignore, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info);
                //
        if(!ScannerActivity.this.isFinishing())
        {
         //  dialog.show();
        }

    }

    ArrayList<String> errList = new ArrayList<>();
    int successSendedGoods = 0;
    int prog = 0;

    boolean export = true;




    Handler hprog = new Handler();


    private boolean sendInvDtOnline(int id, int goodId, double fcnt) {
        while (true) {
            getApi().sendEditInventoryDt(id, goodId, (float) fcnt, new Callback() {
                @Override
                public boolean sendResult(Response response) {
                    if (response == null) {
                        return false;
                    } else {
                        if (response.body() != null) {
                            int result = (int) response.body();
                            Log.d("my", "we have inv dt result result : " + result);
                            //0  is good!
                            //-1 is fail!
                            if (result == 0) {
                                return true;
                            } else {
                                return false;
                            }
                        } else return false;
                    }
                }

                @Override
                public void onError(String error) {

                }
            });
        }
    }



    private class ClearOfflineData extends Thread {
        @Override
        public void run() {
            MainApplication.dbHelper.clearAllForDocID_AC_GOODS_CNT(id);

            h.post(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(ScannerActivity.this)
                            .setTitle(R.string.ready)
                            .setMessage("")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })

                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
            });
        }
    }


    public static void playDone() {


        MediaPlayer mp = MediaPlayer.create(getAppContext(), R.raw.done);
        mp.start();
    }

    public static void playError() {
        MediaPlayer mp = MediaPlayer.create(getAppContext(), R.raw.error_sound);
        mp.start();
    }


    ProgressDialog progress;

    AlertDialog.Builder adb;
    AlertDialog ad;
    Handler deleteH = new Handler();
    int deletingDockId = -1;


    public static void openDriver() {
        new Handler().postDelayed(openDrawerRunnable(), 100);
    }

    private static Runnable openDrawerRunnable() {
        return new Runnable() {


            public void run() {
              //  mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        };
    }

    ;

    public static void closeDriver() {
        new Handler().postDelayed(closeDrawerRunnable(), 100);
    }

    private static Runnable closeDrawerRunnable() {
        return new Runnable() {


            public void run() {
               // mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
        };
    }

    ;


    private ArrayList<Good> testGoodList = new ArrayList<>();

    private void test() {
        testGoodList.clear();
        new Thread() {
            @Override
            public void run() {
                Log.d("my", "loading test goods...");
                testGoodList = MainApplication.dbHelper.getGoodList("", "", "", id);
                Log.d("my", "test: goods list size = " + testGoodList);
                testHandler1.post(new Runnable() {
                    @Override
                    public void run() {

                        new Thread() {
                            @Override
                            public void run() {
                                Log.d("my", "writing cnt...");
                                for (Good testgood : testGoodList) {
                                    //Log.d("my","id="+testgood.getId()+" : "+testgood.getName());
                                    MainApplication.dbHelper.insertGoodsAcCnt(id, testgood.getId(), 33);
                                    tempOfflineAnsCnt = MainApplication.dbHelper.getGoodCountAcc(id, testgood.getId());
                                }
                                Log.d("my", "ready write fcnt 33...");
                            }
                        }.start();
                    }
                });
            }
        }.start();


    }

    Handler testHandler1 = new Handler();

    AlertDialog createGoodDialog;


    AlertDialog.Builder createGoodDialogBuilder;


    DialogInterface.OnClickListener createGoodDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    createGoodDialog.hide();
                    break;
            }
        }
    };

    public void onClickGoodDialog(String val, boolean notAskMore) {
        Log.d("my", "val = " + val);
        //barcodeScannerView.resume();
        // barcodeScannerView.setStatusText("");
        setGoodsByBarc(val, val, val);
        NOT_CREATE_GOOD = notAskMore;
    }

    public void onClickGoodDialogCancel() {

        // barcodeScannerView.resume();
        // barcodeScannerView.setStatusText("");

    }


    ////////CAMERA = @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    @Override
    protected void onResume() {
        super.onResume();
        //capture.onResume();
        // barcodeScannerView.resume();
        Log.d(my,"onResume");
        scanType = prefs.getInt(scan_type, 0);
        fast_search = prefs.getBoolean("fast_search",false);
        if(checkLicense()){
            fast_search = prefs.getBoolean("fast_search",false);

        }else{
            fast_search = false;
        }
        if(fast_search) okBtSearchMode = true;
        else  okBtSearchMode = false;

        Log.d("my","fast_search="+fast_search+ "  okMode="+okBtSearchMode);
        CURRENT_SCAN_MODE=scanType;
        factCntTextView.setText("");

        String state = MainApplication.dbHelper.getOption(MainApplication.CAMERA_STATE);
        if (state != null)
            if (state.equals(MainApplication.CAMERA_OFF)) {
                hideCameraButton.setTag("1");
                // hideCameraButton.setImageResource(R.drawable.ic_camera_alt_white_36dp);
                // barcodeScannerView.setVisibility(View.GONE);
                // switchFlashlightButton.setVisibility(View.INVISIBLE);
                //capture.onPause();
                // barcodeScannerView.pause();
                MainApplication.dbHelper.insertOrReplaceOption(MainApplication.CAMERA_STATE, MainApplication.CAMERA_OFF);
            }
    }

    @Override
    protected void onPause() {
        Log.d("camr", "onPause");
        super.onPause();
        //capture.onPause();
        //   barcodeScannerView.pause();
    }

    @Override
    protected void onDestroy() {
        Log.d("camr", "ScanActivity onDestroy");
        super.onDestroy();
        //capture.onDestroy();
        //  barcodeScannerView.pause();
    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }*/

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("camr","keyDown");

        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }*/


    public void pause(View view) {
        //barcodeScannerView.pause();
    }

    public void resume(View view) {
        //  barcodeScannerView.resume();
    }

    public void triggerScan(View view) {

        //barcodeScannerView.decodeSingle(callback);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //finish();
        return true;//barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    /**
     * Check if the device's camera has a Flashlight.
     *
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (((String) switchFlashlightButton.getTag()).equals("0")) {
            if (barcodeScannerView != null) barcodeScannerView.setTorchOn();
        } else {
            if (barcodeScannerView != null) barcodeScannerView.setTorchOff();
        }
    }

    @Override
    public void onTorchOn() {

        switchFlashlightButton.setTag("1");

        switchFlashlightButton.setImageResource(R.drawable.ic_highlight_off_white_36dp);

    }

    @Override
    public void onTorchOff() {

        switchFlashlightButton.setTag("0");
        switchFlashlightButton.setImageResource(R.drawable.ic_highlight_white_36dp);
    }


    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {

                if (dialog != null) dialog.dismiss();
                if (barcodeScannerView != null) barcodeScannerView.pause();

                //  barcodeScannerView.pause();
                //  barcodeScannerView.setStatusText(result.getText());
                String scanContent = result.getText();
                Log.d("my", scanContent);
                Log.d("my", "finish!!!");

                searchEditText.setText(scanContent);


                if (!scanContent.equals("")) {
                    //	clearfindButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.close_32));
                    originGoodListSaver.clear();
                    originGoodListSaver.addAll(goodList);
                    enter = 0;
                    newScan = true;
                    searchEditText.setFocusable(true);
                    //isEnabledFindButton=false;
                    String barc = searchEditText.getText().toString();
                    setGoodsByBarc(barc, barc, barc);

                }
            }


            //Added preview of scanned barcode
            // ImageView imageView = (ImageView) findViewById(R.id.barcodePreview);
            // imageView.setImageBitmap(result.getBitmapWithResultPoints(Color.YELLOW));
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };


    Dialog creadeInvDialog;
    EditText idEditText;
    EditText nameEditText;

    String createInvId = "";
    int crInvId = 999000;
    String createInvName = "";
    String currentDateTimeString = "";
    Handler createInvH = new Handler();


    private void selectOrCreateDocument() {
        Log.d("my", "selectOrCreateDocument");

        ArrayList<Inventory> invList = MainApplication.dbHelper.getInventoryList();
        if (invList.size() == 1) {

            Inventory inv = invList.get(0);
            id = inv.getId();
            date = inv.getDatetime();
            subdiv = inv.getSubdivision().getName();
            docType = inv.getDocType();

            Toast.makeText(ScannerActivity.this, "inv " + id + " " + subdiv,
                    Toast.LENGTH_SHORT).show();

        } else {


            // custom dialog
            creadeInvDialog = new Dialog(this);
            creadeInvDialog.setContentView(R.layout.new_inventory_dialog);
            creadeInvDialog.setTitle(getString(R.string.create_inventory));

            // set the custom dialog components - text, image and button
            idEditText = (EditText) creadeInvDialog.findViewById(R.id.idEditText);
            nameEditText = (EditText) creadeInvDialog.findViewById(R.id.nameEditText);

            Button cancelButton = (Button) creadeInvDialog.findViewById(R.id.cancelBt);
            Button createButton = (Button) creadeInvDialog.findViewById(R.id.createBt);

            //idEditText.setFocusable(false);


            nameEditText.setFocusable(true);
            nameEditText.setSelectAllOnFocus(true);
            if (invList.size() == 0) {
                idEditText.setText("" + 1);
                nameEditText.setText("Market");
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        creadeInvDialog.dismiss();
                    }
                });
            } else {
                // int maxId = MainApplication.dbHelper.getMaxInventoryId()+1;
                // Log.d("my","maxId = "+maxId);
                // idEditText.setText(""+maxId);

                Inventory inv = invList.get(invList.size() - 1);
                id = inv.getId();
                date = inv.getDatetime();
                subdiv = inv.getSubdivision().getName();
                docType = inv.getDocType();

                idEditText.setText("" + id);
                nameEditText.setText(subdiv);

                createButton.setText("OK");
                cancelButton.setText(R.string.inventory_list);

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                       /* Intent intent = new Intent(NewScannerActivity.this,
                                DockListActivity.class);
                        Bundle b = new Bundle();
                        b.putInt("type", ScannerConstants.INVENTORY);
                        intent.putExtras(b);
                        startActivity(intent);


                        finish();*/
                    }
                });
            }


            createButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //create inventory...
                    //dialog.dismiss();

                    createInvId = idEditText.getText().toString();
                    createInvName = nameEditText.getText().toString();

                    createInvName += " - offline document";
                    //currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                    DateFormat dateFormatter = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
                    dateFormatter.setLenient(false);
                    java.util.Date today = new java.util.Date();
                    currentDateTimeString = dateFormatter.format(today);


                    try {
                        crInvId = Integer.parseInt(createInvId);
                    } catch (Exception e) {
                    }


                    if (createInvId.length() > 0)
                        new Thread() {
                            @Override
                            public void run() {
                                MainApplication.dbHelper.insertInventoryDoc(crInvId, createInvId, currentDateTimeString, createInvName, docType);
                                createInvH.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        id = crInvId;
                                        date = currentDateTimeString;
                                        subdiv = createInvName;

                                        creadeInvDialog.dismiss();

                                        Toast.makeText(ScannerActivity.this, "inv " + id + " " + subdiv,
                                                Toast.LENGTH_SHORT).show();
                                        // loadDocListOffline();
                                    }
                                });
                            }
                        }.start();
                }
            });

            creadeInvDialog.show();
        }
    }

    int updCntCounter = 0;
    View.OnClickListener updatePriceListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updCntCounter++;
            if (updCntCounter < 2) {
            }

            if (connectMode != CONNECT_OFFLINE) return;
            if (currentGood == null) return;
            if (currentGood.getId() < 0) return;


            showUptadePriceDialog();
        }
    };

    private void showUptadePriceDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("");
        builder.setMessage(getString(R.string.dialog_input_new_price));
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        //input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        input.setText(goodsPrice.getText());
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                String str = input.getText().toString();
                try {
                    double prc = Double.parseDouble(str);
                    int id = currentGood.getId();

                    String ans = MainApplication.dbHelper.updateGoodPrice(prc, id);
                    if (ans.length() == 0) {
                        goodsPrice.setText(input.getText());
                    } else {
                        Toast.makeText(ScannerActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                    Log.d(my, "update price result: id=" + id + " prc=" + prc + " result = " + ans);

                    dialog.dismiss();


                } catch (Exception e) {
                    // dialog.dismiss();
                    Toast.makeText(ScannerActivity.this, "Неверное значение", Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    private boolean mReturningWithResult = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mReturningWithResult = true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mReturningWithResult) {
            // Commit your transactions here.
        }
        // Reset the boolean flag back to false for next time.
        mReturningWithResult = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
              /*  Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
