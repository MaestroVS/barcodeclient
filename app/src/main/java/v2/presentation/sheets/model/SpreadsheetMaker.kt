package com.pedrocarrillo.spreadsheetandroid.data.model

import com.google.api.services.sheets.v4.model.*
import v2.presentation.activation.model.License


class SpreadsheetMaker {

    // Will create an Spreadsheet
    fun create(spreadsheetTitle : String,
               sheetOneTitle : String,
               licenses: List<License>) : Spreadsheet {

        val spreadsheet = Spreadsheet()
        val peopleSheetMaker = LicensesSheetMaker()
        val sheets = mutableListOf<Sheet>()
        val spreadsheetProperties = SpreadsheetProperties()
        spreadsheetProperties.title = spreadsheetTitle
        spreadsheet.properties = spreadsheetProperties
        sheets.add(peopleSheetMaker.create(sheetOneTitle, licenses))
        spreadsheet.sheets = sheets
        return spreadsheet
    }

    // Will create a Sheet to be added to the spreadsheet and apply new properties
    private class LicensesSheetMaker {

        fun create(title : String, licenses: List<License>) : Sheet {
            val sheet = Sheet()
            val sheetProperty = SheetProperties()
            val listGridData = mutableListOf<GridData>()
            val listGridDataMaker = GridDataMaker()
            val gridData = listGridDataMaker.create(licenses, 0 , 0)
            sheetProperty.title = title
            sheet.properties = sheetProperty
            listGridData.add(gridData)
            sheet.data = listGridData
            return sheet
        }

    }

    // Will create a grid data with an specific start row, start column and the data you want to display
    private class GridDataMaker {

        fun create(licenses : List<License>, startRow : Int, startColumn : Int) : GridData {
            val gridData = GridData()
            val listRowData = mutableListOf<RowData>()
            val rowDataMaker = RowDataMaker()
            gridData.startRow = startRow
            gridData.startColumn = startColumn
            /*licenses.mapTo(listRowData) {
               rowDataMaker.create(it.name, it.address, it.email, it.phone, it.android_id, it.dateFrom,
                       it.dateTo, it.isActive, it.description
                )
            }*/
            gridData.rowData = listRowData
            return gridData
        }

    }

    // Will create a row with an specific number of cells
    public class RowDataMaker {

        fun create(name : String, address : String, email : String, deviceName : String, deviceId : String,
                   dateFrom: String, dateTo : String,
                   isActive: String, description : String) : RowData {
            val rowData = RowData()
            val listCellData : MutableList<CellData> = mutableListOf()
            val cellDataMaker = CellDataMaker()
            listCellData.add(cellDataMaker.create(name))
            listCellData.add(cellDataMaker.create(address))
            listCellData.add(cellDataMaker.create(email))
            listCellData.add(cellDataMaker.create(deviceName))
            listCellData.add(cellDataMaker.create(deviceId))
            listCellData.add(cellDataMaker.create(dateFrom))
            listCellData.add(cellDataMaker.create(dateTo))
            listCellData.add(cellDataMaker.create(isActive))
            listCellData.add(cellDataMaker.create(description))

            rowData.setValues(listCellData)
            return rowData
        }

    }

    // Will create a cell with its specific data
    private class CellDataMaker {

        fun create(data : String) : CellData {
            val cellData = CellData()
            val extendedValue = ExtendedValue()
            cellData.userEnteredValue = extendedValue.setStringValue(data)
            return cellData
        }

    }


}