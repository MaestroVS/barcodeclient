package com.pedrocarrillo.spreadsheetandroid.data.repository.sheets

import com.google.api.services.sheets.v4.model.Spreadsheet
import v2.presentation.activation.model.License
import com.pedrocarrillo.spreadsheetandroid.data.model.SpreadsheetInfo
import io.reactivex.Observable
import io.reactivex.Single
import java.io.IOException

/**
 * @author Pedro Carrillo.
 */
class SheetsRepository(private val sheetsAPIDataSource: SheetsAPIDataSource) {

    @Throws(IOException::class)
    fun readSpreadSheet(spreadsheetId : String,
                        spreadsheetRange : String): Single<List<License>> {
        return sheetsAPIDataSource.readSpreadSheet(spreadsheetId, spreadsheetRange).onErrorResumeNext( Single.error(Throwable("ERRRRRVsdsdf")))
    }


    fun writeSpreadSheet(spreadsheetId : String,
                        spreadsheetRange : String, license: License): Observable<SpreadsheetInfo> {
        return sheetsAPIDataSource.writeSpreadSheet(spreadsheetId, spreadsheetRange, license)
    }


    fun createSpreadsheet(spreadSheet : Spreadsheet) : Observable<SpreadsheetInfo>  {
        return sheetsAPIDataSource.createSpreadsheet(spreadSheet)
    }

}