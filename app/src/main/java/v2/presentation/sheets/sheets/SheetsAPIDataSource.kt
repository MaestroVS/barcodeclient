package com.pedrocarrillo.spreadsheetandroid.data.repository.sheets

import android.accounts.Account
import android.util.Log
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.*
import com.pedrocarrillo.spreadsheetandroid.data.manager.AuthenticationManager
import v2.presentation.activation.model.License
import com.pedrocarrillo.spreadsheetandroid.data.model.SpreadsheetInfo
import io.reactivex.Observable
import io.reactivex.Single
import java.io.IOException

/**
 * @author Pedro Carrillo.
 */
class SheetsAPIDataSource(private val authManager: AuthenticationManager,
                          private val transport: HttpTransport,
                          private val jsonFactory: JsonFactory) : SheetsDataSource {

    private val sheetsAPI: Sheets
        get() {
            authManager?.googleAccountCredential!!.selectedAccount =   Account("maestrovsmaster@gmail.com","CREATOR")
            //val htp = com.google.api.client.http.HttpRequestInitializer()

            return Sheets.Builder(transport,
                    jsonFactory,
                    authManager.googleAccountCredential)
                    .setApplicationName("test")
                    .build()

            /*return Sheets.Builder(transport,
                    jsonFactory,
                    authManager.googleAccountCredential)
                    .setApplicationName("test")
                    .build()*/
        }

    @Throws(IOException::class)
    override fun readSpreadSheet(spreadsheetId: String,
                                 spreadsheetRange: String): Single<List<License>> {

        return Observable
                .fromCallable {
                    val response = sheetsAPI.spreadsheets().values()
                            .get(spreadsheetId, spreadsheetRange)
                            .execute()
                    response.getValues()
                }

                .flatMapIterable { it -> it }
                .map {

                    License(if (it.size >= 1) {
                        it[0].toString()
                    } else {
                        ""
                    },
                            if (it.size >= 2) {
                                it[1].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 3) {
                                it[2].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 4) {
                                it[3].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 5) {
                                it[4].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 6) {
                                it[5].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 7) {
                                it[6].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 8) {
                                it[7].toString()
                            } else {
                                ""
                            },
                            if (it.size >= 9) {
                                it[8].toString()
                            } else {
                                ""
                            }
                    )


                }
                .toList()
                .doOnError { Log.d("my","THROWABLE = "+it.localizedMessage) }
                //.onErrorResumeNext( Single.error(Throwable("ERRRRRVsdsdf")))


    }

    override fun writeSpreadSheet(spreadsheetId: String, spreadsheetRange: String,
                                  license: License): Observable<SpreadsheetInfo> {

        val licensesList = mutableListOf<License>()
        licensesList.add(license)
        val rows = listOf(
                licenseToStrList(license)
                //licensesList
        )
        val body = ValueRange().setValues(rows)

        authManager?.googleAccountCredential!!.selectedAccount =   Account("maestrovsmaster@gmail.com","CREATOR")
        return Observable
                .fromCallable {
                    val response = sheetsAPI.spreadsheets().values()
                            .append(spreadsheetId, spreadsheetRange, body)
                            .setValueInputOption("RAW")
                            .setInsertDataOption("INSERT_ROWS")
                            .execute()
                    // response.getValues()
                }
                // .flatMapIterable { it -> it }
                .map {
                    SpreadsheetInfo("", "")//it[KEY_ID] as String, it[KEY_URL] as String)

                }
        //.toList()
    }


    fun licenseToStrList(license: License): List<String> {
        return listOf(license.name, license.address, license.email, license.phone, license.android_id,
                license.created_at, license.date_to, license.is_active, license.description)
    }


    override fun createSpreadsheet(spreadSheet: Spreadsheet): Observable<SpreadsheetInfo> {
        return Observable
                .fromCallable {
                    val response =
                            sheetsAPI
                                    .spreadsheets()
                                    .create(spreadSheet)
                                    .execute()
                    response
                }
                .map { SpreadsheetInfo(it[KEY_ID] as String, it[KEY_URL] as String) }
    }

    companion object {
        val KEY_ID = "spreadsheetId"
        val KEY_URL = "spreadsheetUrl"
    }


    fun createValueRange(license: License): com.google.api.services.sheets.v4.model.ValueRange {
        val licensesList = mutableListOf<License>()

        licensesList.add(license)

        val list = mutableListOf<List<License>>()
        list.add(licensesList)
        return com.google.api.services.sheets.v4.model.ValueRange().setValues(list as List<MutableList<Any>>?)

    }

    fun createRow(license: License): RowData {
        val rowData = RowData()
        val listCellData: MutableList<CellData> = mutableListOf()
        val cellDataMaker = CellDataMaker()
        listCellData.add(cellDataMaker.create(license.name))
        listCellData.add(cellDataMaker.create(license.address))
        listCellData.add(cellDataMaker.create(license.email))
        listCellData.add(cellDataMaker.create(license.phone))
        listCellData.add(cellDataMaker.create(license.android_id))
        listCellData.add(cellDataMaker.create(license.created_at))
        listCellData.add(cellDataMaker.create(license.date_to))
        listCellData.add(cellDataMaker.create(license.is_active))
        listCellData.add(cellDataMaker.create(license.description))

        rowData.setValues(listCellData)
        return rowData
    }


    private class CellDataMaker {

        fun create(data: String): CellData {
            val cellData = CellData()
            val extendedValue = ExtendedValue()
            cellData.userEnteredValue = extendedValue.setStringValue(data)
            return cellData
        }

    }
}