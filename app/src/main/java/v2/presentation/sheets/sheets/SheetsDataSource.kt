package com.pedrocarrillo.spreadsheetandroid.data.repository.sheets

import com.google.api.services.sheets.v4.model.Spreadsheet
import v2.presentation.activation.model.License
import com.pedrocarrillo.spreadsheetandroid.data.model.SpreadsheetInfo
import io.reactivex.Observable
import io.reactivex.Single

/**
 * @author Pedro Carrillo.
 */
interface SheetsDataSource {

    fun readSpreadSheet(spreadsheetId : String,
                        spreadsheetRange : String): Single<List<License>>

    fun writeSpreadSheet(spreadsheetId : String,spreadsheetRange : String,
                        license: License): Observable<SpreadsheetInfo>

    fun createSpreadsheet(spreadSheet : Spreadsheet) : Observable<SpreadsheetInfo>
}