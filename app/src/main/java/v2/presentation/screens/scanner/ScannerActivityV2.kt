package v2.presentation.screens.scanner

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.app.barcodeclient3.R
import v2.helpers.filters.DocType


class ScannerActivityV2 : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val DOC_TYPE = "DOC_TYPE"
        const val DOC_ID = "DOC_ID"



        fun getCallingIntent(context: Context, docType: DocType, docId: Long): Intent {
            val intent = Intent(context, ScannerActivityV2::class.java)
            intent.putExtra(DOC_TYPE, docType)
            intent.putExtra(DOC_ID, docId)
            return intent
        }
    }

    var docId: Long? = -1
    lateinit var docType: DocType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        docType = intent.getSerializableExtra(DOC_TYPE) as DocType
        docId = intent.getLongExtra(DOC_ID, -1)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        //val navController = findNavController(R.id.nav_host_fragment)
        val bundle = Bundle()
        Log.d("my","DOC_ID2=$docId")
        bundle.putString("docType",docType.name)
        bundle.putLong("docId",docId!!)
        navController.setGraph(navController.graph,bundle)

        /*next.setOnClickListener {
            nav_host_fragment.findNavController().navigate(R.id.fragment1)
        }*/

        val filter = IntentFilter()
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        filter.addAction(resources.getString(R.string.activity_intent_filter_action))
        registerReceiver(receiver, filter)


    }


    override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }


    val QR_ACTION: String = "android.intent.ACTION_DECODE_DATA"
    val QR_EXTRA: String = "barcode_string"

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("myKey", "Get intent0 ${intent.action}")
            try {
                Log.d("myKey", "Get intent ${intent.action}")
                if (QR_ACTION == intent.action) {
                    if (intent.hasExtra(QR_EXTRA)) {
                        val code = intent.getStringExtra(QR_EXTRA)
                        Log.d("myKey", "New QR code $code")
                        // now you have qr code here
                    }
                }
            } catch (e: Exception) {

            }
        }
    }

   /* override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        // if (event.characters != null && !event.characters.isEmpty()) //Add more code...
        Log.d("myKey", "Get event ${event.scanCode} ${event.toString()}")  //@TODO keyCode=KEYCODE_ENTER SCANNER!!!
        return false
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

}
