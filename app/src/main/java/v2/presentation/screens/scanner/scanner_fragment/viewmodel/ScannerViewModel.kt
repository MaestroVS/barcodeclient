package v2.presentation.screens.scanner.scanner_fragment.viewmodel

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import v2.data.firebird.models.Good
import v2.helpers.filters.DocType
import v2.helpers.filters.GoodSearchFilter
import v2.domain.repositories.GoodsRepository
import v2.domain.repositories.StateLiveData
import v2.domain.repositories.doc_repositories.*
import v2.presentation.screens.scanner.scanner_fragment.utils.FieldType
import v2.presentation.screens.scanner.scanner_fragment.utils.InputField

public class ScannerViewModel : ViewModel() {

    var docType: DocType = DocType.BILL_IN


    var docId: Long = -1

    val mustSearchGoods = MutableLiveData<Boolean>()

    val mustReset = MutableLiveData<Boolean>()

    val tax = 20.0
    val withTax = MutableLiveData<Boolean>()


    val goodCardVisiblity = MutableLiveData<Boolean>()
    val calculatorLinLayoutVisiblity = MutableLiveData<Boolean>()
    val recyclerViewVisiblity = MutableLiveData<Boolean>()
    val noFoundVisiblity = MutableLiveData<Boolean>()
    val progress = MutableLiveData<Boolean>()


    var searchField: MutableLiveData<InputField<String>> = MutableLiveData()
    var countField: MutableLiveData<InputField<String>> = MutableLiveData()

    var priceField: MutableLiveData<InputField<String>> = MutableLiveData()
    var priceTaxField: MutableLiveData<InputField<String>> = MutableLiveData()
    var amountField: MutableLiveData<InputField<String>> = MutableLiveData()
    var amountTaxField: MutableLiveData<InputField<String>> = MutableLiveData()


    val goodName = MutableLiveData<String>()
    val goodArticle = MutableLiveData<String>()
    val goodBarcode = MutableLiveData<String>()
    val goodPrice = MutableLiveData<String>()


    val optionHead = MutableLiveData<String>() // Current amount for inventory or Tax for bill in
    val optionValue = MutableLiveData<String>()

    val showError = MutableLiveData<Throwable>()
    val showSuccess = MutableLiveData<Boolean>()


    fun onSearchFocusChanged(view: View, hasFocus: Boolean) {
        searchField.value = searchField.value!!.copy(isFocused = hasFocus)
    }

    fun onCountFocusChanged(view: View, hasFocus: Boolean) {
        countField.value = countField.value!!.copy(isFocused = hasFocus, clean = true)
    }

    fun onPriceFocusChanged(view: View, hasFocus: Boolean) {
        priceField.value = priceField.value!!.copy(isFocused = hasFocus, clean = true)
    }

    fun onPriceTaxFocusChanged(view: View, hasFocus: Boolean) {
        priceTaxField.value = priceTaxField.value!!.copy(isFocused = hasFocus, clean = true)
    }

    fun onAmountFocusChanged(view: View, hasFocus: Boolean) {
        amountField.value = amountField.value!!.copy(isFocused = hasFocus, clean = true)
    }

    fun onAmountTaxFocusChanged(view: View, hasFocus: Boolean) {
        amountTaxField.value = amountTaxField.value!!.copy(isFocused = hasFocus, clean = true)
    }


    init {

        initFields()

        mustSearchGoods.value = false
        mustReset.value = false

        withTax.value = true

        goodCardVisiblity.value = false
        calculatorLinLayoutVisiblity.value = true
        recyclerViewVisiblity.value = false
        noFoundVisiblity.value = false
        progress.value = false

        goodName.value = ""
        goodArticle.value = ""
        goodBarcode.value = ""
        goodPrice.value = ""
        optionHead.value = ""
        optionValue.value = ""

        showError.value = null
        showSuccess.value = false
    }

    private fun initFields() {
        searchField.value = InputField(FieldType.SEARCH, "", true, false)
        countField.value = InputField(FieldType.COUNT, "", false, false)
        priceField.value = InputField(FieldType.PRICE, "", false, false)
        priceTaxField.value = InputField(FieldType.PRICE_TAX, "", false, false)
        amountField.value = InputField(FieldType.AMOUNT, "", false, false)
        amountTaxField.value = InputField(FieldType.AMOUNT_TAX, "", false, false)
    }


    fun onClickDigit(str: String) {


        if (searchField.value!!.isFocused) {
            searchField.value = searchField.value!!.copy(value = searchField.value!!.value + str)

        } else {

            val focusedField = getCurrentFieldInFocus()
            if (focusedField != null) {

                var currentStr = if (focusedField.value!!.clean) {
                    ""
                } else {
                    focusedField.value!!.value
                }
                var addedStr = str
                if (str == ".") {
                    if (currentStr.contains(str)) return
                    addedStr = if (currentStr.isEmpty()) "0."
                    else str
                } else if (str == "0") {
                    if (currentStr == "0") return
                    else addedStr = str
                } else if (currentStr == "0") {
                    currentStr = ""
                }
                focusedField.value = focusedField.value!!.copy(value = currentStr + addedStr, clean = false)


                calculateFields(focusedField)

            }

        }
    }

    private fun calculateFields(focusedField: MutableLiveData<InputField<String>>) {
        val count = countField.value!!.getDecimalValue()
        var price = priceField.value!!.getDecimalValue()
        var priceTax = priceTaxField.value!!.getDecimalValue()
        var amount = amountField.value!!.getDecimalValue()
        var amountTax = amountTaxField.value!!.getDecimalValue()

        if (!withTax.value!!) {
            price = priceTax
            amount = amountTax
        }

        if (count > 0) {
            when (focusedField.value!!.type) {
                FieldType.SEARCH -> {
                }
                FieldType.COUNT -> {
                    if (withTax.value!!) {
                        amount = count * price
                        amountTax = count * priceTax
                    } else {
                        amountTax = count * priceTax
                        amount = amountTax
                    }
                    priceField.value = priceField.value!!.copy(value = String.format("%.2f", price), clean = false)
                    priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    amountField.value = amountField.value!!.copy(value = String.format("%.2f", amount), clean = false)
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }
                FieldType.PRICE -> {
                    priceTax = price + ((tax * price) / 100)
                    amount = count * price
                    amountTax = count * priceTax
                    priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    amountField.value = amountField.value!!.copy(value = String.format("%.2f", amount), clean = false)
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }
                FieldType.PRICE_TAX -> {
                    price = if (withTax.value!!) {
                        priceTax / (1 + (tax / 100))
                    } else {
                        priceTax
                    }
                    amount = count * price
                    amountTax = count * priceTax
                    priceField.value = priceField.value!!.copy(value = String.format("%.2f", price), clean = false)
                    amountField.value = amountField.value!!.copy(value = String.format("%.2f", amount), clean = false)
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }
                FieldType.AMOUNT -> {
                    price = amount / count
                    priceTax = price + ((tax * price) / 100)
                    amountTax = count * priceTax
                    priceField.value = priceField.value!!.copy(value = String.format("%.2f", price), clean = false)
                    priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }
                FieldType.AMOUNT_TAX -> {
                    priceTax = amountTax / count
                    price = if (withTax.value!!) {
                        priceTax / (1 + (tax / 100))
                    } else {
                        priceTax
                    }
                    amount = count * price
                    priceField.value = priceField.value!!.copy(value = String.format("%.2f", price), clean = false)
                    priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    amountField.value = amountField.value!!.copy(value = String.format("%.2f", amount), clean = false)
                }
            }
        }
    }


    fun refreshCountValue(newValue: String) {
        var currentStr = countField.value!!.value
        if (currentStr != newValue) {
            countField.value = countField.value!!.copy(value = newValue)
        }
        calculateFields(getCurrentFieldInFocus()!!)
    }

    fun refreshPriceValue(newValue: String) {
        var currentStr = priceField.value!!.value
        if (currentStr != newValue) {
            priceField.value = priceField.value!!.copy(value = newValue)
        }
        calculateFields(getCurrentFieldInFocus()!!)
    }

    fun refreshPriceTaxValue(newValue: String) {
        var currentStr = priceTaxField.value!!.value
        if (currentStr != newValue) {
            priceTaxField.value = priceTaxField.value!!.copy(value = newValue)
        }
        calculateFields(getCurrentFieldInFocus()!!)
    }

    fun refreshAmountValue(newValue: String) {
        var currentStr = amountField.value!!.value
        if (currentStr != newValue) {
            amountField.value = amountField.value!!.copy(value = newValue)
        }
        calculateFields(getCurrentFieldInFocus()!!)
    }

    fun refreshAmountTaxValue(newValue: String) {
        var currentStr = amountTaxField.value!!.value
        if (currentStr != newValue) {
            amountTaxField.value = amountTaxField.value!!.copy(value = newValue)
        }
        calculateFields(getCurrentFieldInFocus()!!)
    }


    fun onClickClear() {
        if (searchField.value!!.isFocused) {
            searchField.value = searchField.value!!.copy(value = "")

        } else if (countField.value!!.isFocused) {
            countField.value = countField.value!!.copy(value = "")
        }
    }

    fun onClickReset() {
        initFields()
        hideGoodsRecycler()
        clearGoodCard()
        hideProgress()
        mustReset.value = true

    }

    fun onClickEnter() {
        if (searchField.value!!.isFocused) {


            hideGoodNoFound()
            hideProgress()
            clearGoodCard()
            showProgress()

            mustSearchGoods.postValue(true)
        } else if (countField.value!!.isFocused) {
            when (docType) {
                DocType.INVENTORY -> {
                    insertRowDt()
                }
                DocType.BILL_IN -> {
                    Log.d("my", "onClickEnter1")
                    if (countField.value!!.isFocused) {
                        Log.d("my", "onClickEnter2")
                        focusToPriceWithTax()
                    }
                }
                else -> {
                }
            }
        } else if (priceTaxField.value!!.isFocused) {
            Log.d("my", "onClickEnter3")
            focusToAmountWithTax()
        } else {
            Log.d("my", "onClickEnter4")
            insertRowDt()
        }


    }


    fun searchGoodsList(filter: GoodSearchFilter): StateLiveData<List<Good>> {

        return GoodsRepository.instance.getGoodsListMutableLiveData(filter, null, null, null, null)
    }

    var currentGood: Good? = null

    fun showGoodCard(good: Good) {
        currentGood = good
        goodCardVisiblity.postValue(true)
        calculatorLinLayoutVisiblity.postValue(true)
        recyclerViewVisiblity.postValue(false)
        noFoundVisiblity.postValue(false)

        goodName.postValue(good.name)
        goodArticle.postValue(good.article)
        goodBarcode.postValue(good.barcode)
        goodPrice.postValue(good.price.toString())
        optionValue.postValue(good.cnt_fact.toString())

        searchField.value = searchField.value!!.copy(isFocused = false)
        countField.value = countField.value!!.copy(isFocused = true)
    }

    fun clearGoodCard() {
        currentGood = null
        goodCardVisiblity.postValue(false)
        calculatorLinLayoutVisiblity.postValue(true)
        recyclerViewVisiblity.postValue(false)
        noFoundVisiblity.postValue(false)

        goodName.postValue("")
        goodArticle.postValue("")
        goodBarcode.postValue("")
        goodPrice.postValue("")
        optionValue.postValue("")

    }

    ////
    fun focusToPriceWithTax() {
        Log.d("my", "ssss3")
        countField.value = countField.value!!.copy(isFocused = false)
        priceTaxField.value = priceTaxField.value!!.copy(isFocused = true)
    }


    fun focusToPrice() {

    }

    fun focusToAmountWithTax() {
        priceTaxField.value = priceTaxField.value!!.copy(isFocused = false)
        amountTaxField.value = amountTaxField.value!!.copy(isFocused = true)
    }


    fun focusToAmount() {

    }


    fun showGoodNoFound() {
        noFoundVisiblity.postValue(true)
        goodCardVisiblity.postValue(false)
        recyclerViewVisiblity.postValue(false)
        calculatorLinLayoutVisiblity.postValue(true)
    }

    fun hideGoodNoFound() {
        noFoundVisiblity.postValue(false)
    }


    fun showGoodsRecycler() {
        goodCardVisiblity.postValue(false)
        calculatorLinLayoutVisiblity.postValue(false)
        recyclerViewVisiblity.postValue(true)
        noFoundVisiblity.postValue(false)
    }

    fun hideGoodsRecycler() {
        //goodCardVisiblity.postValue(true)
        calculatorLinLayoutVisiblity.postValue(true)
        recyclerViewVisiblity.postValue(false)
        noFoundVisiblity.postValue(false)
    }


    fun showProgress() {
        progress.postValue(true)
    }

    fun hideProgress() {
        progress.postValue(false)
    }

    fun clearSearchFieldAndSaveFocus() {
        searchField.postValue(InputField(FieldType.SEARCH, "", true, false))
    }


    fun insertRowDt() {
        progress.postValue(true)
        Log.d("my", "insertRowDt")

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }

        val countStr = countField.value!!.value
        if (countStr.isNotEmpty()) {
            try {
                val countFl = countStr.toFloat()
                docId.let {
                    currentGood.let {

                        when (docType) {
                            DocType.INVENTORY -> {
                                sourceModel.insertOrModifyRow(docId, currentGood!!.id, countFl,
                                        { result ->
                                            progress.postValue(false)
                                            Log.d("my", "DT__SUCCESS")
                                            showSuccess.postValue(true)
                                            showSuccess.value = false
                                        },
                                        { throwable ->
                                            progress.postValue(false)
                                            processError(throwable)
                                            Log.d("my", "DT__Error!!!" + throwable.localizedMessage)
                                        })
                            }
                            DocType.BILL_IN -> {
                                Log.d("my", "BILL_IN: unit_id = ${currentGood!!.unit_id}")
                                Log.d("my", "BILL_IN: cnt = ${countField.value!!.getOptionalValue()}")
                                Log.d("my", "BILL_IN: ${amountField.value!!.getOptionalValue()}  ${amountTaxField.value!!.getOptionalValue()}")
                                sourceModel.insertOrModifyRowBill(docId, currentGood!!.id, currentGood!!.unit_id,
                                        countFl, amountField.value!!.getOptionalValue(), amountTaxField.value!!.getOptionalValue(),
                                        priceTaxField.value!!.getOptionalValue(), priceField.value!!.getOptionalValue(),
                                        { result ->
                                            progress.postValue(false)
                                            Log.d("my", "DT__SUCCESS")
                                            showSuccess.postValue(true)
                                            showSuccess.value = false
                                        },
                                        { throwable ->
                                            progress.postValue(false)
                                            processError(throwable)
                                            Log.d("my", "DT__Error!!!" + throwable.localizedMessage)
                                        })
                            }

                            else -> {

                            }
                        }
                    }

                }

            } catch (e: Exception) {
                Log.d("my", "DT__Err!" + e.localizedMessage)
            }

        }
    }

    private fun processError(error: Throwable) {
        showError.postValue(error)
        //showError.postValue(null)
    }

    fun getCurrentFieldInFocus(): MutableLiveData<InputField<String>>? {
        if (searchField.value!!.isFocused) return searchField
        if (countField.value!!.isFocused) return countField
        if (priceField.value!!.isFocused) return priceField
        if (priceTaxField.value!!.isFocused) return priceTaxField
        if (amountField.value!!.isFocused) return amountField
        if (amountTaxField.value!!.isFocused) return amountTaxField
        return null
    }


}