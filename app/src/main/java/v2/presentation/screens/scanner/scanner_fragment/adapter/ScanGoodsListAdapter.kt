package v2.presentation.screens.scanner.scanner_fragment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v2.data.firebird.models.*




class ScanGoodsListAdapter (var items: ArrayList<Good>, val callback: Callback) : RecyclerView.Adapter<ScanGoodsListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_good_scan_search, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }



    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val goodNameTW = itemView.findViewById<TextView>(R.id.goodNameTW)
        private val goodInfoTW = itemView.findViewById<TextView>(R.id.goodInfoTW)



        fun bind(item: Good) {


            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }

            goodNameTW.text = item.name
            goodInfoTW.text =  "арт. ${item.article}  ш/к ${item.barcode}"



        }
    }

    interface Callback {
        fun onItemClicked(item: Good)
    }

    fun setGoodsList(goods: List<Good>) {

        Log.d("GoodsList","GoodsAdapter goods_size = ${goods.size}")

        items.clear();
        items.addAll(goods);
        this.notifyDataSetChanged();
    }


}