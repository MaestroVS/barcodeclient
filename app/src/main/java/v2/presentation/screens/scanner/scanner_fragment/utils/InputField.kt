package v2.presentation.screens.scanner.scanner_fragment.utils

import android.util.Log


data class InputField<T>(val type: FieldType, var value: String, var isFocused: Boolean, var clean: Boolean) {



    fun getDecimalValue(): Double {
        return try {
            val formatVal = value.replace(",",".")
            formatVal.toDouble()
        } catch (e: Exception) {
            0.0
        }
    }

    fun getOptionalValue(): Float? {
        return try {
            val formatVal = value.replace(",",".")
            formatVal.toFloat()
        } catch (e: Exception) {
            Log.d("my","getOptionalValue err = ${e.localizedMessage}")
            null
        }
    }
}