package v2.presentation.screens.scanner.scanner_fragment


import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.FragmentScannerBinding
import kotlinx.android.synthetic.main.fragment_scanner.*
import v2.data.firebird.models.Good
import v2.domain.repositories.StateData
import v2.helpers.SharedPref
import v2.helpers.filters.DocType
import v2.helpers.filters.GoodSearchFilter
import v2.presentation.screens.dashboard.settings.TSDScannerOptions
import v2.presentation.screens.scanner.scanner_fragment.adapter.ScanGoodsListAdapter
import v2.presentation.screens.scanner.scanner_fragment.viewmodel.ScannerViewModel


private const val DOC_ID_EXTRA = "docId"
private const val DOC_TYPE_EXTRA = "docType"


class ScannerFragment : Fragment() {


    private var docId: Long? = null
    private var docType: DocType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            docId = it.getLong(DOC_ID_EXTRA)
            val docTypeStr = it.getString(DOC_TYPE_EXTRA)
            docType = DocType.getDocType(docTypeStr?:"")
        }
    }

    lateinit var goodsAdapter: ScanGoodsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewRoot =
            LayoutInflater.from(requireContext()).inflate(R.layout.fragment_scanner, null)
        binding = FragmentScannerBinding.bind(viewRoot)//inflate()//(inflater, viewRoot!!,  false)
        val view = binding.root

        return view
    }


    companion object {
        /**
         * @param docId id of document.
         * @param docType type of document.
         * @return A new instance of fragment ScannerFragment.
         */
        @JvmStatic
        fun newInstance(docId: Long, docType: String) =
            ScannerFragment().apply {
                arguments = Bundle().apply {
                    putLong(DOC_ID_EXTRA, docId)
                    putString(DOC_TYPE_EXTRA, docType)
                }
            }

    }

    var articleSearchMode = true

    fun readArticleType(): Boolean {
        return SharedPref.readBool("SearchArticle", true)
    }

    fun writeArticleType(article: Boolean) {
        SharedPref.writeBool("SearchArticle", article)
    }

    fun changeInputType() {
        searchEditText.inputType = if (articleSearchMode) {
            InputType.TYPE_CLASS_NUMBER
        } else {
            InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE
        }
    }

    lateinit var binding: FragmentScannerBinding
    lateinit var mainViewModel: ScannerViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        arguments?.let {
            docId = it.getLong(DOC_ID_EXTRA)
            val docTypeStr = it.getString(DOC_TYPE_EXTRA)
            docType = DocType.getDocType(docTypeStr?:"")
        }

        Log.d("my", "DOC_ID=" + docId)

        mainViewModel = ViewModelProviders.of(this).get(ScannerViewModel::class.java)
        mainViewModel.docId = docId!!
        mainViewModel.docType = docType!!

        binding.lifecycleOwner = this
        binding.model = mainViewModel

        articleSearchMode = readArticleType()
        binding.scanType.isChecked = articleSearchMode
        changeInputType()
        binding.searchEditText.showKeyboard()

        binding.scanType.setOnCheckedChangeListener { buttonView, isChecked ->
            articleSearchMode = isChecked
            writeArticleType(articleSearchMode)
            changeInputType()
        }


        backButton.setOnClickListener { requireActivity().finish() }


        mainViewModel.optionHead.postValue(
            when (docType!!) {
                DocType.INVENTORY -> getString(R.string.current_cnt)
                DocType.BILL_IN -> getString(R.string.tax)
                DocType.BILL_OUT -> ""
                DocType.WRITE_OFF -> ""
            }
        )

        billInParams1.visibility = if (docType == DocType.INVENTORY) View.GONE else View.VISIBLE
        billInParams2.visibility = if (docType == DocType.INVENTORY) View.GONE else View.VISIBLE

        goodsAdapter = ScanGoodsListAdapter(arrayListOf(), object : ScanGoodsListAdapter.Callback {
            override fun onItemClicked(item: Good) {
                //startActivityForResult(DocumentDetailsActivity.getCallingIntent(this@GoodsListActivity, docType, item.id), LAUNCH_SECOND_ACTIVITY)
                mainViewModel.showGoodCard(item)
            }
        })

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = goodsAdapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        mainViewModel.mustSearchGoods.observe(viewLifecycleOwner, Observer {
            searchGoods()
        })

        mainViewModel.mustReset.observe(viewLifecycleOwner, Observer {
            if (it) {
                // searchEditText.hideKeyboard()
                mainViewModel.mustReset.postValue(false)
                goodsAdapter.setGoodsList(arrayListOf())
            }
        })


        mainViewModel.searchField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                searchEditText.requestFocus()
            }
        })

        mainViewModel.countField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                goodsCnt.requestFocus()
            }
        })
       ///!!!! goodsCnt.inputType = InputType.TYPE_CLASS_NUMBER

        goodsCnt.doAfterTextChanged {
            goodsCnt.setSelection(goodsCnt.text.length)
            mainViewModel.refreshCountValue(it.toString())
        }



        mainViewModel.showError.observe(viewLifecycleOwner, Observer { it ->
            if (it != null) {
                showError(it)
            }
        })

        mainViewModel.showSuccess.observe(viewLifecycleOwner, Observer { it ->
            if (it) {
                showSuccess()
            }
        })


        /* searchEditText.addTextChangedListener(object : TextWatcher {

             override fun afterTextChanged(s: Editable) {}

             override fun beforeTextChanged(s: CharSequence, start: Int,
                                            count: Int, after: Int) {
             }

             override fun onTextChanged(s: CharSequence, start: Int,
                                        before: Int, count: Int) {
                 mainViewModel.searchField.postValue(InputField(FieldType.SEARCH, s.toString(), true, false))
             }
         })*/

        /* searchEditText.doAfterTextChanged {
             Log.d("myKey","onEdit doAfterTextChanged ${it.toString()}")
             searchEditText.setSelection(searchEditText.text.length)
             searchGoods()//!!!!! TODO Scanner
         }*/



        searchEditText.setOnEditorActionListener(object : OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                Log.d("myKey", "onEdit setOnEditorActionListener event=${event.toString()}")
                Log.d("Counter2", "searchEditText actionId = $actionId")
                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                    //Physical keyboard enter key
                    (event != null && KeyEvent.KEYCODE_ENTER == event.keyCode
                            && event.action == KeyEvent.ACTION_DOWN)
                ) {
                    //searchEditText.hideKeyboard()
                    searchGoods()
                    return true
                }
                return false
            }
        })

        val editListener = OnEditorActionListener { v, actionId, event ->
            //Log.d("Counter2", "actionId = $actionId")
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                mainViewModel.onClickEnter()
                return@OnEditorActionListener true
            }
            false
        }

        goodsCnt.setOnEditorActionListener(editListener)
        priceTaxEt.setOnEditorActionListener(editListener)
        priceET.setOnEditorActionListener(editListener)
        amountTaxEt.setOnEditorActionListener(editListener)
        amountET.setOnEditorActionListener(editListener)



        mainViewModel.priceField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                priceET.requestFocus()
            }
        })

        priceET.doAfterTextChanged {
            priceET.setSelection(priceET.text.length)
            mainViewModel.refreshPriceValue(it.toString())
        }


        mainViewModel.priceTaxField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                priceTaxEt.requestFocus()
            }
        })

        priceTaxEt.doAfterTextChanged {
            priceTaxEt.setSelection(priceTaxEt.text.length)
            mainViewModel.refreshPriceTaxValue(it.toString())
        }


        mainViewModel.amountField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                amountET.requestFocus()
            }
        })

        amountET.doAfterTextChanged {
            amountET.setSelection(amountET.text.length)
            mainViewModel.refreshAmountValue(it.toString())
        }

        mainViewModel.amountTaxField.observe(viewLifecycleOwner, Observer {
            if (it.isFocused) {
                amountTaxEt.requestFocus()
            }
        })

        amountTaxEt.doAfterTextChanged {
            amountTaxEt.setSelection(amountTaxEt.text.length)
            mainViewModel.refreshAmountTaxValue(it.toString())
        }


    }


    private fun searchGoods() {

        val str = searchEditText.text.toString().trim()
        if (str.isEmpty()) return
        goodsAdapter.setGoodsList(arrayListOf())
        mainViewModel.searchGoodsList(GoodSearchFilter(str, str, str, false))
            .observe(viewLifecycleOwner, Observer {


                when (it.status) {
                    StateData.DataStatus.SUCCESS -> {
                        mainViewModel.hideProgress()
                        if (it.data == null || it.data?.size == 0) {
                            mainViewModel.showGoodNoFound()
                            mainViewModel.clearSearchFieldAndSaveFocus()
                        } else {
                            if (it.data!!.size == 1) {
                                binding.goodCard.visibility = View.VISIBLE
                                binding.recyclerView.visibility = View.GONE
                                mainViewModel.showGoodCard(it.data!![0])
                            } else {
                                Log.d("GoodsList", "Goods ok")
                                binding.goodCard.visibility = View.GONE
                                binding.recyclerView.visibility = View.VISIBLE
                                goodsAdapter.setGoodsList(it.data!!)
                                mainViewModel.showGoodsRecycler()

                            }
                        }
                    }
                    StateData.DataStatus.LOADING -> {
                    }
                    StateData.DataStatus.ERROR -> {
                        mainViewModel.hideProgress()
                        showError(it.error!!)
                    }
                    else -> {
                    }
                }

                it.complete()
            })
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun showError(err: Throwable) {
        Log.d("ErroScanner","err= ${err.localizedMessage}")
        if (!err.localizedMessage.capitalize().contains("result")) {
            MaterialDialog(requireContext()).show {
                title(text = getString(R.string.error))
                message(text = err.localizedMessage)
                positiveButton(text = "Close")
            }
            playError()
        }
    }

    val h = Handler()

    private fun showSuccess() {
        playDone()

        val dy = -250
        val t = 300L
        goodCard.animate()
            .translationY((dy).toFloat())
            .alpha(0.0f)
            .setInterpolator(AccelerateInterpolator()).duration = t

        h.postDelayed({
            goodCard.visibility = View.GONE
            goodCard.animate()
                .translationY((0).toFloat())
                .alpha(1.0f)
                .setInterpolator(AccelerateInterpolator()).duration = 1

            mainViewModel.onClickReset()
        }, 2 * t)

    }

    private fun playDone() {
        if (TSDScannerOptions.isPlaySound()) {
            val mp = MediaPlayer.create(requireContext(), R.raw.done)
            mp.start()
        }
    }

    private fun playError() {
        if (TSDScannerOptions.isPlaySound()) {
            val mp = MediaPlayer.create(requireContext(), R.raw.error_sound)
            mp.start()
        }
    }


}
