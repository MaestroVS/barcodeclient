package v2.presentation.screens.goods.list.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.Dispatchers
import v2.data.firebird.models.Good
import v2.domain.repositories_pagination.GoodsListDataSource


class GoodsViewModel : ViewModel() {


    var goodsPaginationSource: GoodsListDataSource? = null

    var goodsLiveData: LiveData<PagedList<Good>>




    val config: PagedList.Config = PagedList.Config.Builder()
            .setPageSize(10)
            .setEnablePlaceholders(false)
            .build()

    /*var circleOptions: CircleOptions? = null
        get() = field
        set(value) {
            field = value
        }*/

    val sortString = MutableLiveData<String>()

    val progress = MutableLiveData<Boolean>()
    val showWaitDialog = MutableLiveData<Boolean>()

    val isRefreshing = MutableLiveData<Boolean>()

    val showNetworkError = MutableLiveData<Boolean>()
    val showUnknownError = MutableLiveData<Boolean>()


    init {

        //circleOptions = CircleOptions(null,null)

        goodsLiveData = initializedPagedListBuilder(config)
                .build()

        //sortString.value = SortOrderTitle.title(SortOrder.DESC)

        progress.value = true
        showWaitDialog.value = false
        isRefreshing.value = false
        showNetworkError.value = false

    }


    fun getGoods(): LiveData<PagedList<Good>> = goodsLiveData

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<String, Good> {
        val dataSourceFactory = object : DataSource.Factory<String, Good>() {
            override fun create(): DataSource<String, Good> {
                goodsPaginationSource = GoodsListDataSource(Dispatchers.IO,
                        object : GoodsListDataSource.Callback {
                            override fun onLoad() {
                                progress.postValue(false)
                                isRefreshing.postValue(false)
                            }

                        })
                return goodsPaginationSource!!
            }
        }
        val builder = LivePagedListBuilder<String, Good>(dataSourceFactory, config)
        return builder
    }


    fun refresh() {
        goodsPaginationSource!!.invalidate()
    }


    /*fun sortByDate() {
        val sortOrder = if (circleOptions!!.sortOrder == SortOrder.DESC)
            SortOrder.ASC else SortOrder.DESC

        sortString.postValue(SortOrderTitle.title(sortOrder))

        circleOptions = CircleOptions(
                if (circleOptions!!.type != null) circleOptions!!.type else null,
                sortOrder)
        circlesPaginationSource!!.invalidate()
        progress.postValue(true)
    }*/




    /*private fun processServerError(error: ErrorResponse?) {

    }

    private fun processNetworkError(error: IOException) {
        showNetworkError.postValue(true)
    }

    private fun processUnknownError(error: Throwable) {
        showUnknownError.postValue(true)
    }*/

}