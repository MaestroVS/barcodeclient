package v2.presentation.screens.goods.list


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.FragmentGoodsListBinding
import kotlinx.android.synthetic.main.content_goods_list.*
import v2.data.firebird.models.Good
import v2.presentation.screens.goods.list.adapter.GoodsAdapter
import v2.presentation.screens.goods.list.viewmodels.GoodsViewModel
import v2.presentation.screens.units.list.UnitsListFragment


class GoodsListFragment : Fragment() {

    companion object {
        const val GRP_ID = "GRP_ID"

        @JvmStatic
        fun newInstance(grpId: Long?) =
                GoodsListFragment().apply {
                    arguments = Bundle().apply {
                        grpId?.let { putLong(GRP_ID, it) }
                    }
                }
    }


   private val goodsAdapter = GoodsAdapter(object : GoodsAdapter.Callback {

        override fun itemSelected(item: Good) {
            //startActivity(FinancialCircleDetailsActivity.getCallingIntent(context!!, item.getId()))
        }
    })


    var grpId: Long? = null

    lateinit var mainViewModel: GoodsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            grpId = it.getLong(GRP_ID)
        }
    }


    lateinit var binding: FragmentGoodsListBinding


    override fun onCreateView(inflater: LayoutInflater,
                              @Nullable container: ViewGroup?,
                              @Nullable savedInstanceState: Bundle?): View? {


        val viewRoot = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_goods_list, null)
        binding = FragmentGoodsListBinding.bind(viewRoot)
        val view = binding.root

        return view
    }


    //var circlesOptions: CircleOptions? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("my","GOODS_LIST_FRAGMENT")


        mainViewModel = ViewModelProviders.of(this).get(GoodsViewModel::class.java)

        //circlesOptions = CircleOptions(circlesType, SortOrder.DESC)
       // mainViewModel.circleOptions = circlesOptions

        binding.lifecycleOwner = this
        binding.model = mainViewModel


        mainViewModel.showWaitDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
              //  Dialogs.showWaitDialog(context!!)
            } else {
               // Dialogs.dismissWaitDialog()
            }
        })

        observeLiveData()
        initializeList()
    }


    private fun observeLiveData() {

        mainViewModel.getGoods().observe(viewLifecycleOwner, Observer {
            goodsAdapter.submitList(it)
        })
    }

    private fun initializeList() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = goodsAdapter
    }


}
