package v2.presentation.screens.goods.utils

import androidx.recyclerview.widget.DiffUtil

import v2.data.firebird.models.Good


class DiffUtilCallBack : DiffUtil.ItemCallback<Good>() {
    override fun areItemsTheSame(oldItem: Good, newItem: Good): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Good, newItem: Good): Boolean {
        return oldItem.id == newItem.id
                && oldItem.name == newItem.name

    }

}