package v2.presentation.screens.goods.list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v2.data.firebird.models.*



class GoodsListAdapter (var items: ArrayList<Good>, val callback: Callback) : RecyclerView.Adapter<GoodsListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_good_simple, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }



    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val name = itemView.findViewById<TextView>(R.id.name)


        fun bind(item: Good) {


            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }

            name.text = item.name



        }
    }

    interface Callback {
        fun onItemClicked(item: Good)
    }

    fun setGoodsList(goods: List<Good>) {

        Log.d("my","ADAPTER_goods_size = ${goods.size}")

        items.clear();
        items.addAll(goods);
        this.notifyDataSetChanged();
    }


}