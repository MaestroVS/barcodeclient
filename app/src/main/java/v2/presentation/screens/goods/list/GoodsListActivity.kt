package v2.presentation.screens.goods.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.content_companies_list.recyclerView
import v2.data.firebird.models.*
import v2.presentation.screens.units.list.UnitsListActivity
import v2.presentation.screens.units.list.UnitsListFragment


class GoodsListActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val GRP_ID = "GRP_ID"

        fun getCallingIntent(context: Context, grpId: Long?): Intent {
            val intent = Intent(context, GoodsListActivity::class.java)
            intent.putExtra(GRP_ID, grpId)
            return intent
        }
    }

    var grpId: Long? = -1



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        grpId = intent.getLongExtra(GRP_ID,-1)

        setContentView(R.layout.activity_goods)

        Log.d("my","GOODS_LIST_ACTIVITY")

       /* setSupportActionBar(toolbar)

        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/


        // create fragment instance
        val fragment : GoodsListFragment = GoodsListFragment.newInstance(grpId)

        // for passing data to fragment
       // val bundle = Bundle()
        //bundle.putString("data_to_be_passed", DATA)
        //fragment.arguments = bundle

        // check is important to prevent activity from attaching the fragment if already its attached
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.frame, fragment)
                    .commit()
        }


        //fetchList()

    }




    /*private fun fetchList() {
        progress.visibility = View.VISIBLE

        GoodsRepository.instance.getList(null, null, null, null,
                { listList ->
                    progress.visibility = View.INVISIBLE
                    refreshUI(listList)

                },
                { throwable ->
                    progress.visibility = View.INVISIBLE
                    MaterialDialog(this).show {
                        title(text = getString(R.string.error))
                        message(text = throwable.localizedMessage)
                        positiveButton(text = "Close")
                    }
                })
    }*/









    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
