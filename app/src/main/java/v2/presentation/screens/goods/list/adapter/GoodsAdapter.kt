package v2.presentation.screens.goods.list.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.row_good_simple.view.*
import v2.data.firebird.models.Good
import v2.presentation.screens.goods.utils.DiffUtilCallBack


class GoodsAdapter(val callback: Callback) : PagedListAdapter<Good, GoodsAdapter.ViewHolder>(DiffUtilCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_good_simple, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bindPost(it, callback) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val root = itemView.root
        val name = itemView.name


        fun bindPost(good: Good, callback: Callback) {

            root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.itemSelected(good)
            }

            name.text = good.name


        }
    }

    public interface Callback {
        fun itemSelected(item: Good)

    }

}