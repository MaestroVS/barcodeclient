package v2.presentation.screens.dashboard

import android.os.Looper
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.InternetObservingSettings
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.strategy.SocketInternetObservingStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.data.database.tables.Connect
import v2.presentation.common.CommonPrefs
import v2.presentation.common.UtilsV2
import v2.presentation.screens.connection.ConnectionModel
import v2.presentation.screens.connection.StatusConnect
import v2.presentation.screens.connection.WorkMode


class DashboardViewModel : ViewModel() {


    var companyId: Long = -1
    var companyName: String = "null"



    val showError = MutableLiveData<Throwable>()

    var status = MutableLiveData<String>()
    var statusFull = MutableLiveData<String>()

    var connectStatus = MutableLiveData<StatusConnect>() //текущее состояние соединения

    var workMode = MutableLiveData<WorkMode>() //глобальный режим работы: соединение с базой или оффлайн

    init {

        connectStatus.value = StatusConnect.waitConnect

        workMode.value = WorkMode.online

    }


    var connect: Connect? = null






     fun getCompanyConnect() {
         connectStatus.postValue(StatusConnect.waitConnect)
        ConnectionModel.instance.getConnectionByCompanyId(companyId,
                { connect ->
                    this.connect = connect
                    setConnection()
                },
                {
                    connectStatus.postValue(StatusConnect.NotConnect)
                    status.postValue(StatusConnect.NotConnect.name)
                    showError.postValue(Throwable("No connect options"))
                })
    }




     fun setConnection() {
        // connection = Connect(ipAddressTV.text.toString(), pathTV.text.toString(), portTV.text.toString(),
        //    userTV.text.toString(), passTV.text.toString(), encodingSpinner.selectedItem.toString(), companyId)
         connectStatus.postValue(StatusConnect.waitConnect)
        ConnectionModel.instance.setFirebirdConnection(connect!!,
                { isSuccess ->
                    if (isSuccess) {
                        try {
                            getDBVersion()
                            startConnectMonitor()
                        } catch (e: Exception) {
                            Log.d("my", "excp=$e")
                        }
                    } else {
                        showError.postValue(Throwable("Connect could not set"))
                    }
                },
                { throwable ->
                    connectStatus.postValue(StatusConnect.NotConnect)
                    status.postValue(StatusConnect.NotConnect.name)
                    showError.postValue(throwable)
                })
    }

    fun startConnectMonitor(){
        val settings = InternetObservingSettings.builder()
                .host("https://github.com/")
                .strategy(SocketInternetObservingStrategy())
                .build()

        val disposable = ReactiveNetwork
                .observeInternetConnectivity(settings)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { isConnectedToHost: Boolean? ->
                    Log.d("my","isConnectedToHost=$isConnectedToHost")
                }
    }



     fun getDBVersion() {

        if(Looper.myLooper() == Looper.getMainLooper()) Log.d("my", " isMainThread")
        else Log.d("my", " is NOT MainThread")

        ConnectionModel.instance.getDBVersion(
                { dbVersion ->

                    connectStatus.postValue(StatusConnect.Connected)
                    status.postValue(StatusConnect.Connected.name)
                    statusFull.postValue(connect.toString() + "[" + dbVersion + "]")
                    CommonPrefs.instance.isDB_3_0_68_0_plus = UtilsV2.instance.isDB_3_0_68_0_plus(dbVersion)

                },
                { throwable ->
                    connectStatus.postValue(StatusConnect.NotConnect)
                    status.postValue(StatusConnect.NotConnect.name)
                    showError.postValue(throwable)
                })
    }



}