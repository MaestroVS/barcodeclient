package v2.presentation.screens.dashboard

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_documents_list.*
import v2.helpers.SharedPref
import v2.presentation.screens.company.list.CompaniesListActivity
import v2.presentation.screens.connection.StatusConnect
import v2.presentation.screens.connection.StatusSync
import v2.presentation.screens.connection.create.CreateConnectionActivity
import v2.domain.SharedCurrentState
import v2.presentation.screens.connection.WorkMode


private const val LAUNCH_SECOND_ACTIVITY_SETTINGS_TAB = 114

class DashboardActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111


    companion object {
        const val COMPANY_ID = "COMPANY_ID"
        const val COMPANY_NAME = "COMPANY_NAME"


        fun getCallingIntent(context: Context, companyId: Long, companyName: String): Intent {
            val intent = Intent(context, DashboardActivity::class.java)
            intent.putExtra(COMPANY_ID, companyId)
            intent.putExtra(COMPANY_NAME, companyName)


            return intent
        }
    }

    lateinit var dashboardViewModel: DashboardViewModel
    lateinit var syncDataViewModel: SyncDataViewModel

    var companyId: Long = 0
    var  companyName: String = ""

    var status = "?"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarTextColor()

        companyId = intent.getLongExtra(COMPANY_ID, 0)
        companyName= intent.getStringExtra(COMPANY_NAME)?:""
        SharedCurrentState.companyId = companyId
       // _activity.setSupportActionBar(toolbar)
        //FragmentHelper.setHomeIconToolbar(_activity, toolbar, profile_button,allowBack)
        SharedPref.init(this)
        SharedPref.writeInt(SharedPref.LAST_COMPANY_ID, companyId.toInt())


        val appBar = supportActionBar


        setContentView(R.layout.activity_dashboard)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_back_office, R.id.navigation_front_office, R.id.navigation_reports, R.id.navigation_settings))
//        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)



        val toolbarTitle = findViewById<View>(R.id.title) as TextView
        toolbarTitle.text = getString(R.string.tab_menu_back)


        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            toolbarTitle.text = destination.label
        }

        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val toolbar: androidx.appcompat.widget.Toolbar = findViewById<View>(R.id.toolbar) as androidx.appcompat.widget.Toolbar
        setSupportActionBar(toolbar)



       // val inflator = this.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
       // val v: View = inflator.inflate(R.layout.dashboard_action_bar, null)
       // supportActionBar?.customView = v

        syncDataViewModel = ViewModelProvider(this).get(SyncDataViewModel::class.java)

        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
        dashboardViewModel.companyId = companyId
        dashboardViewModel.companyName = companyName


        dashboardViewModel.status.observe(this, Observer {
            status = it
        })

        companyNameTV.text = companyName

        settingsBt.setOnClickListener {
            startActivityForResult(CreateConnectionActivity.getCallingIntent(this@DashboardActivity, companyId, "", true), LAUNCH_SECOND_ACTIVITY)
        }

        dashboardViewModel.getCompanyConnect()

        infoBt.setOnClickListener {
            MaterialDialog(this).show {
                message(text = status)
                positiveButton(text = "OK")
            }
        }

        val transitionsContainer: ViewGroup = findViewById(R.id.transitions_container)
        syncDataViewModel.syncStatus.observe(this, Observer {
            Log.d("sync","sync status=$it")
            when (it) {
                StatusSync.syncronization -> updateStatusIcon(DisplaySyncStatus.syncWait)
                StatusSync.ready -> {
                    Log.d("sync","sync status=read=) conn=${dashboardViewModel.connectStatus.value}")
                    when (dashboardViewModel.connectStatus.value) {
                        StatusConnect.Connected -> updateStatusIcon(DisplaySyncStatus.connectSuccess)
                        StatusConnect.NotConnect -> updateStatusIcon(DisplaySyncStatus.connectError)
                        StatusConnect.waitConnect -> updateStatusIcon(DisplaySyncStatus.connectWait)
                        null -> updateStatusIcon(DisplaySyncStatus.connectSuccess)
                    }
                }
                StatusSync.error -> updateStatusIcon(DisplaySyncStatus.syncError)
            }

        })

        dashboardViewModel.connectStatus.observe(this, Observer {
            when (it) {
                StatusConnect.Connected -> updateStatusIcon(DisplaySyncStatus.connectSuccess)
                StatusConnect.NotConnect -> updateStatusIcon(DisplaySyncStatus.connectError)
                StatusConnect.waitConnect -> updateStatusIcon(DisplaySyncStatus.connectWait)
                null -> updateStatusIcon(DisplaySyncStatus.connectSuccess)
            }
        })

        dashboardViewModel.status.observe(this, Observer {
            status = it
        })

        dashboardViewModel.workMode.observe(this, Observer {
            when (it) {
                WorkMode.online -> {

                }
                WorkMode.offline -> {

                }
            }
        })

        syncIcon.setOnClickListener {
            dashboardViewModel.setConnection()
        }

        exitToMain.setOnClickListener {
            SharedPref.writeInt(SharedPref.LAST_COMPANY_ID, -1)
            startActivity(CompaniesListActivity.getCallingIntent(this@DashboardActivity))
            finish()
        }
    }










    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                dashboardViewModel.getCompanyConnect()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if(requestCode == LAUNCH_SECOND_ACTIVITY_SETTINGS_TAB){
            for (fragment in supportFragmentManager.fragments) {
                fragment.onActivityResult(requestCode, resultCode, data)
            }
        }


    }

    fun setStatusBarTextColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val decorView = window.decorView
            val nightModeFlags = resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK
            when (nightModeFlags) {
                Configuration.UI_MODE_NIGHT_YES, Configuration.UI_MODE_NIGHT_UNDEFINED -> decorView.systemUiVisibility = decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                Configuration.UI_MODE_NIGHT_NO -> window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
    }




    private fun updateStatus(status: String){

        statusConnectTV.text = status

        //if(supportActionBar !=null)  supportActionBar!!.title = status
    }

    private fun processError(throwable: Throwable) {
        MaterialDialog(this).show {
            title(text = getString(R.string.error))
            message(text = throwable.localizedMessage)
            positiveButton(text = "Close")
        }
    }



    fun updateStatusIcon(status: DisplaySyncStatus){

        if(objectAnimator != null){
            objectAnimator?.end()
        }
        when(status){
            DisplaySyncStatus.connectSuccess -> {

                syncIcon.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity, R.drawable.ic_phonelink_24px))
                if (dashboardViewModel.workMode.value == WorkMode.offline){
                    swichWorkModeDialog(WorkMode.online)
                }
            }
            DisplaySyncStatus.connectWait -> {

                syncIcon.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity, R.drawable.ic_sync_black_18dp))
                createAnimator(syncIcon)
            }
            DisplaySyncStatus.connectError -> {
                syncIcon.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity, R.drawable.ic_phonelink_off_24px))
                if (dashboardViewModel.workMode.value == WorkMode.online){
                    swichWorkModeDialog(WorkMode.offline)
                }
            }
            DisplaySyncStatus.syncWait -> {
                syncIcon.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity, R.drawable.ic_sync_black_18dp))
                createAnimator(syncIcon)
            }
            DisplaySyncStatus.syncError -> {
                syncIcon.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity, R.drawable.ic_sync_problem_24px))

            }
        }
    }

    fun swichWorkModeDialog(workMode: WorkMode){
        when(workMode){
            WorkMode.online -> {
                /*MaterialDialog(this).show {
                    title(text = getString(R.string.connection_restored))
                    message(text = getString(R.string.enable_online))
                    positiveButton(text = "OK")
                    positiveButton {
                        dashboardViewModel.workMode.postValue(WorkMode.online)
                    }
                    negativeButton(text = getString(R.string.cancel))

                }*/
            }
            /*WorkMode.offline -> {
                MaterialDialog(this).show {
                    title(text = getString(R.string.no_connection))
                    message(text = getString(R.string.enable_offline))
                    positiveButton(text = "OK")
                    positiveButton {
                        dashboardViewModel.workMode.postValue(WorkMode.offline)
                    }
                    negativeButton(text = getString(R.string.cancel))

                }

            }*/
            else -> {}
        }
    }



    var objectAnimator: ObjectAnimator? = null
    private fun createAnimator(img: ImageView){
        objectAnimator = ObjectAnimator.ofFloat(img, View.ROTATION, 0.0f, -360.0f)
        objectAnimator!!.duration = 2000
        objectAnimator!!.repeatCount = Animation.INFINITE
        objectAnimator!!.interpolator = LinearInterpolator()
        objectAnimator!!.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                super.onAnimationCancel(animation)
            }
           /* override fun onAnimationCancel(animation: Animator?) {
                //img.animate().alpha(0.0f).duration = 1000
            }*/
        })
        objectAnimator!!.start()

    }



    enum class DisplaySyncStatus{
        connectSuccess, connectWait, connectError, syncWait, syncError
    }


}
