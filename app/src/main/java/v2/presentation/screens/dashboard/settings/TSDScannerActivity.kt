package v2.presentation.screens.dashboard.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_t_s_d_scanner.*

class TSDScannerActivity : AppCompatActivity() {

    companion object {

        fun getCallingIntent(context: Context): Intent {
            return Intent(context, TSDScannerActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_t_s_d_scanner)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.scan_options)

        scannerEnableSwitch.isChecked = TSDScannerOptions.isScannerEnabled()
        scannerEnableSwitch.setOnCheckedChangeListener { _, isEnable ->
            TSDScannerOptions.setScannerEnabled(isEnable)
        }
        prefET.setText(TSDScannerOptions.getScanPrefix())
        postfixEt.setText(TSDScannerOptions.getScanPostfix())

        saveBt.setOnClickListener {
            TSDScannerOptions.setScanPrefix(prefET.text.toString())
            TSDScannerOptions.setScanPostfix(postfixEt.text.toString())
           /* MaterialDialog(this).show {
                title(text = getString(R.string.success))
                message(text = "Changes saved successfully")
                positiveButton(text = getString(R.string.close))
                positiveButton {
                    finish()
                }
            }*/
            finish()
        }

        playSoundCb.isChecked = TSDScannerOptions.isPlaySound()
        playSoundCb.setOnClickListener { v ->
            val checked = (v as CheckBox).isChecked
            TSDScannerOptions.setPlaySound(checked)
        }

        prefET.clearFocus()
        postfixEt.clearFocus()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}