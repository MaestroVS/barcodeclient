package v2.presentation.screens.dashboard.settings

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.BuildConfig
import com.app.barcodeclient3.R
import com.app.barcodeclient3.SettingScanModeActivity
import kotlinx.android.synthetic.main.fragment_settings.*
import v2.helpers.SharedPref
import v2.presentation.screens.ModeActivity
import v2.presentation.screens.company.list.CompaniesListActivity
import v2.presentation.screens.connection.create.CreateConnectionActivity
import v2.presentation.screens.dashboard.DashboardViewModel
import v2.presentation.screens.dashboard.SyncDataViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private const val LAUNCH_SECOND_ACTIVITY_SETTINGS_TAB = 114


class SettingsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var status = "?"
    var statusFull = "?"

    companion object {
        /**

         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SettingsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SettingsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }



    lateinit var dashboardViewModel: DashboardViewModel
    lateinit var syncDataViewModel: SyncDataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        dashboardViewModel = ViewModelProvider(requireActivity()).get(DashboardViewModel::class.java)
        syncDataViewModel = ViewModelProvider(requireActivity()).get(SyncDataViewModel::class.java)

        dashboardViewModel.status.observe(viewLifecycleOwner, Observer {
            status = it
            status_conn.text = status
        })
        dashboardViewModel.statusFull.observe(viewLifecycleOwner, Observer {
            statusFull = it
        })

        dashboardViewModel.showError.observe(viewLifecycleOwner, Observer {
            statusFull = it.localizedMessage
        })



        status_name.text = dashboardViewModel.companyName

        val versionCode: Int = BuildConfig.VERSION_CODE
        val versionName: String = BuildConfig.VERSION_NAME

        version.text = "$versionName ($versionCode)"

        status_conn.setOnClickListener {
            MaterialDialog(requireContext()).show {
                message(text = statusFull)
                positiveButton(text = "OK")
            }
        }

        settings_connect.setOnClickListener {
            startActivityForResult(CreateConnectionActivity.getCallingIntent(requireContext(),dashboardViewModel.companyId, "", true),LAUNCH_SECOND_ACTIVITY_SETTINGS_TAB)
        }

        settings_terminal.setOnClickListener {
            startActivity(TSDScannerActivity.getCallingIntent(requireContext()))
        }

        settings_scan.setOnClickListener {
            startActivity(SettingScanModeActivity.getCallingIntent(context))
        }

        settings_sync.setOnClickListener {
         startActivity(SyncDataActivity.getCallingIntent(requireContext()))
        }

        settings_launch_mode.setOnClickListener {
            startActivity(ModeActivity.getCallingIntent(requireContext()))
        }

        settings_exit.setOnClickListener {
            SharedPref.writeInt(SharedPref.LAST_COMPANY_ID, -1)
            startActivity(CompaniesListActivity.getCallingIntent(requireContext()))
            requireActivity().finish()
        }

        dashboardViewModel.getCompanyConnect()
    }






    private fun processError(throwable: Throwable) {
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.error))
            message(text = throwable.localizedMessage)
            positiveButton(text = "Close")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("my","SETTINGS_FR_RESULT")
        dashboardViewModel.getCompanyConnect()
    }


}