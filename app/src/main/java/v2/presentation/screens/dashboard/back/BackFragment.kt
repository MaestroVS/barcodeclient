package v2.presentation.screens.dashboard.back

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.fragment_back.*
import v2.helpers.filters.DocType
import v2.presentation.screens.documents.list.DocumentsListActivity
import v2.presentation.screens.goods.list.GoodsListActivity
import v2.presentation.screens.units.list.UnitsListActivity

class BackFragment : Fragment() {

    private lateinit var dashboardViewModel: BackViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(BackViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_back, container, false)
        //val textView: TextView = root.findViewById(R.id.text_dashboard)

        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
          //  textView.text = it
        })

        
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inventoriesBt.setOnClickListener {
            startActivity(DocumentsListActivity.getCallingIntent(requireContext(), DocType.INVENTORY))
        }
        billInBt.setOnClickListener {
            startActivity(DocumentsListActivity.getCallingIntent(requireContext(), DocType.BILL_IN))
        }
        billOutBt.setOnClickListener {
            startActivity(DocumentsListActivity.getCallingIntent(requireContext(), DocType.BILL_OUT))
        }
        writeOffBt.setOnClickListener {
            startActivity(DocumentsListActivity.getCallingIntent(requireContext(), DocType.WRITE_OFF))
        }

        goodsBt.setOnClickListener {
            startActivity(GoodsListActivity.getCallingIntent(requireContext(), 0))
        }

        unitsBt.setOnClickListener {
            startActivity(UnitsListActivity.getCallingIntent(requireContext()))
        }
    }
}