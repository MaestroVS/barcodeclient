package v2.presentation.screens.dashboard

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.barcodeclient3.MainApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.database.dao.ConnectionDao
import v2.data.database.dao.GoodDao
import v2.data.database.dao.GroupDao
import v2.data.database.tables.DicGroup
import v2.domain.repositories.GoodsRepository
import v2.presentation.screens.connection.StatusSync
import javax.inject.Inject
import v2.helpers.addTo


class SyncDataViewModel : ViewModel() {

    @Inject
    lateinit var groupDao: GroupDao

    @Inject
    lateinit var connectionDao: ConnectionDao

    @Inject
    lateinit var goodDao: GoodDao

    private val disposable = CompositeDisposable()


    var companyId: Long = -1
    var companyName: String = "null"


    val syncStatus = MutableLiveData<StatusSync>()
    val progress = MutableLiveData<Boolean>()


    val showError = MutableLiveData<Throwable>()

    var status = MutableLiveData<String>()
    var statusFull = MutableLiveData<String>()

    val dicGroupsList = MutableLiveData<List<DicGroup>>()
    val goodsList = MutableLiveData<List<v2.data.database.tables.DicGood>>()


    init {

        AndroidApplication.app()!!.appComponent()!!.inject(this)
        syncStatus.value = StatusSync.ready
        progress.value = false

        dicGroupsList.value = mutableListOf()
        goodsList.value = mutableListOf()
    }





    fun getGoodsGrpListAndCacheRoom(){
       val disposable  = GoodsRepository.instance.getGoodsGrpListAndCacheRoom(companyId,
                { cnt ->
                    Log.d("my","Cache grp success = $cnt")

                    groupDao.getAll().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map {
                                                            it.map {
                                    Log.d("my","ReadFromDb ${it.name}")
                                }
                                dicGroupsList.postValue(it)
                            }
                            .onErrorReturn { error(it) }
                            .subscribe()




                },
                { throwable ->
                    Log.d("my","Cache grp err = $throwable")
                })



    }

    fun getGoodsListAndCacheRoom(){
        val disposable  = GoodsRepository.instance.getGoodsListAndCacheRoom(companyId,
                { cnt ->
                    Log.d("my","Cache goods success = $cnt")

                    goodDao.getAll().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map {
                                it.map {
                                    Log.d("my","ReadFromDb goods ${it.name}")
                                }
                                goodsList.postValue(it)
                            }
                            .onErrorReturn { error(it) }
                            .subscribe()




                },
                { throwable ->
                    Log.d("my","Cache grp err = $throwable")
                })


    }


}