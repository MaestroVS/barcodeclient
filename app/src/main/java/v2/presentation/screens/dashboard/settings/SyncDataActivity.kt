package v2.presentation.screens.dashboard.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.ActivitySyncDataBinding
import kotlinx.android.synthetic.main.activity_sync_data.*
import v2.presentation.screens.connection.StatusSync
import v2.presentation.screens.dashboard.DashboardActivity
import v2.presentation.screens.dashboard.SyncDataViewModel


class SyncDataActivity :  AppCompatActivity() {

    companion object {


        lateinit var context: Context

        fun getCallingIntent(context: Context): Intent {

            this.context = context
            val intent = Intent(context, SyncDataActivity::class.java)

            return intent
        }
    }

    lateinit var syncDataViewModel: SyncDataViewModel

    lateinit var binding: ActivitySyncDataBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        syncDataViewModel = ViewModelProvider(context as DashboardActivity).get(SyncDataViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sync_data)
        binding.lifecycleOwner = this

        binding.model = syncDataViewModel


        startStop.setOnClickListener {


            startCache()


            syncDataViewModel.getGoodsGrpListAndCacheRoom()

        }



        syncDataViewModel.dicGroupsList.observe(this, Observer {
            if(it.isNotEmpty()){
                syncDataViewModel.getGoodsListAndCacheRoom()
            }
        })

        syncDataViewModel.goodsList.observe(this, Observer {
            if(it.isNotEmpty()){
                finishCache()
            }
        })

    }

    private fun startCache(){
        syncDataViewModel.progress.postValue(true)
        syncDataViewModel.syncStatus.postValue(StatusSync.syncronization)
        startStop.isEnabled = false
    }

    private fun finishCache(){
        syncDataViewModel.progress.postValue(false)
        syncDataViewModel.syncStatus.postValue(StatusSync.ready)
        startStop.isEnabled = true
    }






}