package v2.presentation.screens.dashboard.settings

import android.util.Log
import v2.helpers.SharedPref

private const val SCAN_IS_ENABLE = "SCAN_IS_ENABLE"
private const val SCAN_PREF = "SCAN_PREF"
private const val SCAN_POSTF = "SCAN_POSTF"
private const val PLAY_SOUND = "PLAY_SOUND"

object TSDScannerOptions {

    private val defPref = ";"
    private val defPostf = "%"

    fun isScannerEnabled(): Boolean{
        return SharedPref.readBool(SCAN_IS_ENABLE, false)
    }

    fun setScannerEnabled(isEnabled: Boolean){
        SharedPref.writeBool(SCAN_IS_ENABLE,isEnabled)
    }

    fun getScanPrefix():String{
        val pref =  SharedPref.readString(SCAN_PREF,defPref)
        if(pref == null || pref.isEmpty()){
            SharedPref.writeString(SCAN_PREF, defPref)
        }
        return SharedPref.readString(SCAN_PREF,defPref)?: defPref
    }

    fun setScanPrefix(scanPref: String){
        SharedPref.writeString(SCAN_PREF, scanPref)
    }

    fun getScanPostfix():String{
        val postf =  SharedPref.readString(SCAN_POSTF,defPostf)
        if(postf == null || postf.isEmpty()){
            SharedPref.writeString(SCAN_POSTF, defPostf)
        }
        return SharedPref.readString(SCAN_POSTF,defPostf)?: defPostf
    }

    fun setScanPostfix(scanPref: String){
        SharedPref.writeString(SCAN_POSTF, scanPref)
    }

    fun isPlaySound()  = SharedPref.readBool(PLAY_SOUND,true)

    fun setPlaySound(play: Boolean){
        SharedPref.writeBool(PLAY_SOUND,play)
    }
}