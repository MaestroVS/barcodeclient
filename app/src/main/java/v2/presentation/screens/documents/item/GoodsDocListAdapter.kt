package v2.presentation.screens.documents.item

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v2.data.firebird.models.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class GoodsDocListAdapter(val context: Context, var items: ArrayList<GoodDoc>, val callback: Callback) : RecyclerView.Adapter<GoodsDocListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_good_document, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }


    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val nameTV = itemView.findViewById<TextView>(R.id.nameTV)
        private val countTV = itemView.findViewById<TextView>(R.id.countTV)
        private val descriptionTV = itemView.findViewById<TextView>(R.id.descriptionTV)
        private val priceTV = itemView.findViewById<TextView>(R.id.priceTV)

        fun bind(item: GoodDoc) {

            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }

            nameTV.text = item.goods_name

           // Log.d("my","cnt=${item.cnt}")
            val cntStr = if (isWhole(item.cnt.toDouble())) {
                item.cnt.toString()
            } else {
                String.format("%.3f", item.cnt)
            }
            val unitStr = " " + item.unit_name
            val totalStr = cntStr+unitStr

            val spannable = SpannableString(totalStr)
            //spannable.setSpan(
               //     ForegroundColorSpan(ContextCompat.getColor(context, R.color.body_1_38)),
               //     0, titleStr.length,
                //    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            spannable.setSpan(
                    RelativeSizeSpan(0.6f),
                    cntStr.length, totalStr.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)


            countTV.text = spannable
            descriptionTV.text = item.article
            priceTV.text = NumberFormat.getCurrencyInstance(Locale("ua", "UA"))
                    .format(item.price_with_tax.toDouble())


        }
    }

    interface Callback {
        fun onItemClicked(item: GoodDoc)
    }

    fun setGoodsList(goods: List<GoodDoc>) {

        items.clear();
        items.addAll(goods);
        this.notifyDataSetChanged();
    }

    fun isWhole(value: Double): Boolean {
        return value - value.toInt() == 0.0
    }
}