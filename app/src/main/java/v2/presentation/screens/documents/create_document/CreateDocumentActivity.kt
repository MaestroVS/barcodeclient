package v2.presentation.screens.documents.create_document

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.ActivityCreateDocBinding
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.android.synthetic.main.activity_create_doc.*
import kotlinx.android.synthetic.main.activity_create_doc.subdivisionsSpinner
import kotlinx.android.synthetic.main.activity_doc_details.progress
import kotlinx.android.synthetic.main.content_documents_list.*
import v2.helpers.filters.DocType
import java.util.*


class CreateDocumentActivity : AppCompatActivity() , com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener, com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val DOC_TYPE = "DOC_TYPE"


        fun getCallingIntent(context: Context, docType: DocType): Intent {
            val intent = Intent(context, CreateDocumentActivity::class.java)
            intent.putExtra(DOC_TYPE, docType)
            return intent
        }
    }

    lateinit var docType: DocType

    lateinit var mainViewModel: CreateDocumentViewModel

    lateinit var binding: ActivityCreateDocBinding

   // lateinit var goodsAdapter: GoodsDocListAdapter

    /* private var docSubdivisionId: Long = -1
     private var docSortSelectable = DocsSort.DESC
     private var docStateSelectable = DocState.ALL

     var subdivisions: List<Subdivision> = emptyList()*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        docType = intent.getSerializableExtra(DOC_TYPE) as DocType


        mainViewModel = ViewModelProvider(this).get(CreateDocumentViewModel::class.java)
        mainViewModel.docType = docType

        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_doc)
        binding.lifecycleOwner = this
        binding.model = mainViewModel

        initAbar()

        datePickerTextView.setOnClickListener {
            showDatePickerDialog()
        }


        mainViewModel.organizationsLiveData.observe(this) { list ->
            val subItemsList: MutableList<String> = mutableListOf()
            list.map{
                subItemsList.add(it.name)
            }
            val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subItemsList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            organizationsSpinner.adapter = adapter
        }

        mainViewModel.subdivisionsLiveData.observe(this) { list ->
            val subItemsList: MutableList<String> = mutableListOf()
            list.map{
                subItemsList.add(it.name)
            }
            val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subItemsList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            subdivisionsSpinner.adapter = adapter
        }

        mainViewModel.employyesLiveData.observe(this) { list ->
            val subItemsList: MutableList<String> = mutableListOf()
            list.map{
                subItemsList.add(it.code_name)
            }
            val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subItemsList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            employyesSpinner.adapter = adapter
        }

       /* toolbar.setPadding(0, 0, 0, 0)//for tab otherwise give space in tab
        toolbar.setContentInsetsAbsolute(0, 0)
        setSupportActionBar(toolbar)*/

      //  backButton.setOnClickListener { finish() }

        supportActionBar!!.title = "fsd"



        docIcon.setImageResource(when (docType) {
            DocType.INVENTORY -> R.drawable.ic_inventory
            DocType.BILL_IN -> R.drawable.ic_bill_in
            DocType.BILL_OUT -> R.drawable.ic_bill_out
            DocType.WRITE_OFF -> R.drawable.ic_write_off
        })



        /*goodsAdapter = GoodsDocListAdapter(this@CreateDocumentActivity, arrayListOf(), object : GoodsDocListAdapter.Callback {
            override fun onItemClicked(item: GoodDoc) {
                //startActivityForResult(DocumentDetailsActivity.getCallingIntent(this@GoodsListActivity, docType, item.id), LAUNCH_SECOND_ACTIVITY)

            }
        })*/

       /* val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = goodsAdapter
        recyclerView.addItemDecoration(DividerItemDecoration(this@CreateDocumentActivity, DividerItemDecoration.VERTICAL))
*/

      /*  mainViewModel.showError.observe(this, Observer { it ->
            if (it != null) {
                showError(it)
            }
        })



        mainViewModel.loadedDoc.observe(this, Observer {
            if (it) {
                Log.d("my","Doc was load")
                progress.visibility = View.VISIBLE
                //getGoods()
            }
        })


        mainViewModel.fetchDocument()
*/
        mainViewModel.fetchData()
    }

    fun initAbar(){

        supportActionBar!!.displayOptions = supportActionBar!!.displayOptions or ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM

        supportActionBar!!.setCustomView(R.layout.action_bar_title)
        val abarView = supportActionBar!!.customView
        val titleBar = abarView.findViewById<TextView>(R.id.abarTitle)
        titleBar.text = (when (docType) {
            DocType.INVENTORY -> getString(R.string.doc_title_inventory)
            DocType.BILL_IN -> getString(R.string.doc_title_bill_in)
            DocType.BILL_OUT -> getString(R.string.doc_title_bill_out)
            DocType.WRITE_OFF -> getString(R.string.doc_title_write_off)
        })
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setIcon(android.R.color.transparent)
        val parent = abarView.parent as Toolbar
        parent.setPadding(0, 0, 0, 0)
        parent.setContentInsetsAbsolute(0, 0)
    }


    /*private fun getGoods() {
        mainViewModel.getGoodsList().observe(this, Observer {
            progress.visibility = View.INVISIBLE


            when (it.status) {
                StateData.DataStatus.SUCCESS -> {
                    Log.d("my", "Successdoc!! ${it.data!!.size}")
                    goodsAdapter.setGoodsList(it.data!!)
                }
                StateData.DataStatus.ERROR -> {
                    showError(it.error!!)
                }
                else -> {
                }
            }

            it.complete()
        })
    }*/




    private fun showDatePickerDialog(){
        val now: Calendar = Calendar.getInstance()
        val dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog
                .newInstance(
                this@CreateDocumentActivity,

                now.get(Calendar.YEAR),  // Initial year selection
                now.get(Calendar.MONTH),  // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        )

        dpd.show(supportFragmentManager, "Datepickerdialog")
    }

    override fun onDateSet(view: com.wdullaer.materialdatetimepicker.date.DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        datePickerTextView.text = "day $dayOfMonth"
        val now: Calendar = Calendar.getInstance()
        val dpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog
                .newInstance(
                        this@CreateDocumentActivity,
                        true
                        //now.get(Calendar.YEAR),  // Initial year selection
                        //now.get(Calendar.MONTH),  // Initial month selection
                        //now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                )
        dpd.show(supportFragmentManager, "Datepickerdialog")
    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        datePickerTextView.text = "time $hourOfDay"
    }

    private fun showError(err: Throwable) {
        MaterialDialog(this).show {
            title(text = getString(R.string.error))
            message(text = err.localizedMessage)
            positiveButton(text = "Close")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("my", "DocDetailsRes")

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("my", "DocDetailsRes1")
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("my", "DocDetailsRes2")
                //Write your code if there's no result
            }
        }

        progress.visibility = View.VISIBLE
       // getGoods()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }




}


