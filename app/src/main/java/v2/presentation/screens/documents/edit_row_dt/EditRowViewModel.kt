package v2.presentation.screens.documents.edit_row_dt

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import v2.data.firebird.models.*
import v2.helpers.filters.DocType
import v2.domain.repositories.doc_repositories.*
import v2.presentation.screens.repo.EmployeeRepository
import v2.presentation.screens.repo.OrganizationRepository
import v2.presentation.screens.repo.SubdivisionRepository
import v2.presentation.screens.scanner.scanner_fragment.utils.FieldType
import v2.presentation.screens.scanner.scanner_fragment.utils.InputField
import java.text.NumberFormat
import java.util.*


class EditRowViewModel : ViewModel() {

    var docType: DocType = DocType.BILL_IN
    private var goodDoc: v2.data.firebird.models.parcelize.GoodDoc? = null



    val progress = MutableLiveData<Boolean>()
    val goodName = MutableLiveData<String>()
    val goodArticle = MutableLiveData<String>()
    val goodUnit = MutableLiveData<String>()

    val showSuccess = MutableLiveData<Boolean>()

   // val price = MutableLiveData<String>()
    //val count = MutableLiveData<String>()
    //val summ = MutableLiveData<String>()

    var countField: MutableLiveData<InputField<String>> = MutableLiveData()
    fun onCountFocusChanged(view: View, hasFocus: Boolean) {
        countField.value = countField.value!!.copy(isFocused = hasFocus, clean = true)
    }


    var priceTaxField: MutableLiveData<InputField<String>> = MutableLiveData()
    fun onPriceTaxFocusChanged(view: View, hasFocus: Boolean) {
        priceTaxField.value = priceTaxField.value!!.copy(isFocused = hasFocus, clean = true)
    }

    var amountTaxField: MutableLiveData<InputField<String>> = MutableLiveData()
    fun onAmountTaxFocusChanged(view: View, hasFocus: Boolean) {
        amountTaxField.value = amountTaxField.value!!.copy(isFocused = hasFocus, clean = true)
    }




    val showError = MutableLiveData<Throwable?>()

    val organizationsLiveData = MutableLiveData<List<Org>>()
    val subdivisionsLiveData = MutableLiveData<List<Subdivision>>()
    val employyesLiveData = MutableLiveData<List<Employee>>()


    init {


        initFields()

        goodName.value = ""



        subdivisionsLiveData.value = mutableListOf()



        progress.value = false
        showError.value = null




    }


    fun setGood(good: v2.data.firebird.models.parcelize.GoodDoc) {

        this.goodDoc = good

        goodName.value = goodDoc!!.goods_name
        goodArticle.value = goodDoc!!.article
        goodUnit.value = goodDoc!!.unit_name

        priceTaxField.value =  priceTaxField.value!!  .copy(value = String.format("%.2f", goodDoc!!.price_with_tax), clean = false)
        amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", goodDoc!!.sum_with_tax), clean = false)
        countField.value = countField.value!!.copy(value = String.format("%.1f", goodDoc!!.cnt), clean = false)


        Log.d("asdasdasd","priceTaxField.value=${priceTaxField.value}")
      //  amountTaxField.value = InputField(FieldType.AMOUNT,"33.6", isFocused = false, clean = false)

    }



    private fun initFields() {
        countField.value = InputField(FieldType.COUNT, "", false, clean = true)
        priceTaxField.value = InputField(FieldType.PRICE_TAX, "", isFocused = false, clean = false)
        amountTaxField.value = InputField(FieldType.AMOUNT_TAX, "", false, clean = false)
    }


    fun refreshPriceTaxValue(newValue: String) {
        var currentStr = priceTaxField.value!!.value
        if (currentStr != newValue) {
            priceTaxField.value = priceTaxField.value!!.copy(value = newValue)
        }
        getCurrentFieldInFocus()?.let {
            calculateFields(it)
        }

    }

    fun refreshCountValue(newValue: String) {
        var currentStr = countField.value!!.value
        if (currentStr != newValue) {
            countField.value = countField.value!!.copy(value = newValue)
        }
        getCurrentFieldInFocus()?.let {
            calculateFields(it)
        }
    }

    fun refreshAmountTaxValue(newValue: String) {
        var currentStr = amountTaxField.value!!.value
        if (currentStr != newValue) {
            amountTaxField.value = amountTaxField.value!!.copy(value = newValue)
        }
        getCurrentFieldInFocus()?.let {
            calculateFields(it)
        }
    }



    private fun calculateFields(focusedField: MutableLiveData<InputField<String>>) {
        var count = countField.value!!.getDecimalValue()
        var priceTax = priceTaxField.value!!.getDecimalValue()
        var amountTax = amountTaxField.value!!.getDecimalValue()


        if (count > 0) {
            when (focusedField.value!!.type) {

                FieldType.COUNT -> {
                    amountTax = count * priceTax
                   // priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }

                FieldType.PRICE_TAX -> {
                    amountTax = count * priceTax
                    amountTaxField.value = amountTaxField.value!!.copy(value = String.format("%.2f", amountTax), clean = false)
                }

                FieldType.AMOUNT_TAX -> {
                   // priceTax = amountTax / count
                    count = amountTax / priceTax
                    priceTaxField.value = priceTaxField.value!!.copy(value = String.format("%.2f", priceTax), clean = false)
                    countField.value = countField.value!!.copy(value = String.format("%.3f", count), clean = false)
                }
                else -> {}
            }
        }
    }



    fun getCurrentFieldInFocus(): MutableLiveData<InputField<String>>? {
        if (countField.value?.isFocused == true) return countField
        if (priceTaxField.value?.isFocused == true) return priceTaxField
        if (amountTaxField.value?.isFocused == true) return amountTaxField
        return null
    }



    private fun processError(error: Throwable) {
        showError.postValue(error)
        //showError.postValue(null)
    }



    fun modifyRowDt() {
        progress.postValue(true)
        Log.d("my", "editRowDt")

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }

        val countStr = countField.value!!.value

        val priceStr = priceTaxField.value!!.value

        val amountStr = amountTaxField.value!!.value


        if (countStr.isNotEmpty()) {
            try {
                val countFl = countStr.replace(",",".").toFloat()
                val priceFl = priceStr.replace(",",".").toFloat()
                val amountFl = amountStr.replace(",",".").toFloat()

                goodDoc?.let { goodDoc ->


                        when (docType) {
                            DocType.INVENTORY -> {
                               /* sourceModel.insertOrModifyRow(goodDoc.hd_id, goodDoc.goods_id, countFl,
                                    { result ->
                                        progress.postValue(false)
                                        Log.d("my", "DT__SUCCESS")
                                        showSuccess.postValue(true)
                                        showSuccess.value = false
                                    },
                                    { throwable ->
                                        progress.postValue(false)
                                        processError(throwable)
                                        Log.d("my", "DT__Error!!!" + throwable.localizedMessage)
                                    })*/
                            }
                            DocType.BILL_IN -> {
                               // Log.d("my", "BILL_IN: unit_id = ${currentGood!!.unit_id}")
                               // Log.d("my", "BILL_IN: cnt = ${countField.value!!.getOptionalValue()}")
                                //Log.d("my", "BILL_IN: ${amountField.value!!.getOptionalValue()}  ${amountTaxField.value!!.getOptionalValue()}")
                                sourceModel.updateRowBill(goodDoc.hd_id, goodDoc.goods_id, goodDoc.unit_id,
                                    countFl, amountFl, amountTaxField.value!!.getOptionalValue(),
                                    priceTaxField.value!!.getOptionalValue(), priceFl, goodDoc.id,
                                    { result ->
                                        progress.postValue(false)
                                        Log.d("my", "DT__SUCCESS")
                                        showSuccess.postValue(true)
                                        showSuccess.value = false
                                    },
                                    { throwable ->
                                        progress.postValue(false)
                                        processError(throwable)
                                        Log.d("my", "Update__Error!!!" + throwable.localizedMessage)
                                       // showError.postValue(throwable)
                                    })
                            }

                            else -> {

                            }
                        }


                }

            } catch (e: Exception) {
                Log.d("my", "DT__Err00!" + e.localizedMessage)
                processError(e)
            }

        }
    }





    fun deleteRowDt() {
        progress.postValue(true)
        Log.d("my", "editRowDt")

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }





            try {


                goodDoc?.let { goodDoc ->


                    when (docType) {
                        DocType.INVENTORY -> {

                        }
                        DocType.BILL_IN -> {

                            sourceModel.deleteRow( goodDoc.id,
                                { result ->
                                    progress.postValue(false)
                                    Log.d("my", "DT_DEL__SUCCESS")
                                    showSuccess.postValue(true)
                                    showSuccess.value = false
                                },
                                { throwable ->
                                    progress.postValue(false)
                                    processError(throwable)
                                    Log.d("my", "Update__Error!!!" + throwable.localizedMessage)
                                    // showError.postValue(throwable)
                                })
                        }

                        else -> {

                        }
                    }


                }

            } catch (e: Exception) {
                Log.d("my", "DT__Err00!" + e.localizedMessage)
                processError(e)
            }


    }



}