package v2.presentation.screens.documents.create_document

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import v2.data.firebird.models.*
import v2.helpers.filters.DocType
import v2.domain.repositories.doc_repositories.*
import v2.presentation.screens.repo.EmployeeRepository
import v2.presentation.screens.repo.OrganizationRepository
import v2.presentation.screens.repo.SubdivisionRepository


class CreateDocumentViewModel : ViewModel() {

    var docType: DocType = DocType.BILL_IN
    var docId: Long = -1


    val progress = MutableLiveData<Boolean>()
    val docNum = MutableLiveData<String>()
    val docDate = MutableLiveData<String>()
    val docSubdivision = MutableLiveData<String>()
    val docStatus = MutableLiveData<Boolean>()

    val loadedDoc = MutableLiveData<Boolean>()


    val showError = MutableLiveData<Throwable?>()

    val organizationsLiveData = MutableLiveData<List<Org>>()
    val subdivisionsLiveData = MutableLiveData<List<Subdivision>>()
    val employyesLiveData = MutableLiveData<List<Employee>>()


    init {

        docNum.value = ""
        docDate.value = ""
        docSubdivision.value = ""

        subdivisionsLiveData.value = mutableListOf()

        docStatus.value = false

        progress.value = false
        showError.value = null

        loadedDoc.value = false

    }


    fun fetchDocument() {
        progress.postValue(true)

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }

        docId.let {
            sourceModel.getItem(it,
                    { document ->

                        loadedDoc.postValue(true)

                        progress.postValue(false)
                        docNum.postValue(document.num)
                        docDate.postValue(document.dateTime)
                        docSubdivision.postValue(document.subdivisionName)
                        docStatus.postValue(document.doc_state != 0)

                    },
                    { throwable ->
                        progress.postValue(false)
                        processError(throwable)

                    })
        }
    }

    fun fetchData(){
        fetchOrganizations()
    }






    private fun processError(error: Throwable) {
        showError.postValue(error)
        showError.postValue(null)
    }

    public fun fetchOrganizations() {
        OrganizationRepository.instance.getList(
                { list ->
                    if (list.isNotEmpty()) {
                        organizationsLiveData.postValue(list)
                    }
                    fetchSubdivisions()
                },
                { throwable ->
                    processError(throwable)
                })
    }


    public fun fetchSubdivisions() {
        SubdivisionRepository.instance.getList(
                { list ->
                    if (list.isNotEmpty()) {
                        subdivisionsLiveData.postValue(list)
                    }
                    fetchEmployees()
                },
                { throwable ->
                    processError(throwable)
                })
    }

    public fun fetchEmployees() {
        EmployeeRepository.instance.getList(
                { list ->
                    if (list.isNotEmpty()) {
                        employyesLiveData.postValue(list)
                    }
                },
                { throwable ->
                    processError(throwable)
                })
    }


}