package v2.presentation.screens.documents.item

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.ActivityDocDetailsBinding
import kotlinx.android.synthetic.main.activity_companies_list.toolbar
import kotlinx.android.synthetic.main.activity_doc_details.*
import kotlinx.android.synthetic.main.activity_doc_details.backButton
import kotlinx.android.synthetic.main.activity_doc_details.progress
import kotlinx.android.synthetic.main.activity_doc_details.recyclerView
import kotlinx.android.synthetic.main.activity_doc_details.titleTV
import kotlinx.android.synthetic.main.fragment_scanner.*
import kotlinx.android.synthetic.main.head_doc_info.*
import v2.data.firebird.models.GoodDoc
import v2.data.firebird.models.parcelize.firebirdGoodDocToParcelize
import v2.helpers.filters.DocType
import v2.presentation.screens.scanner.ScannerActivityV2

import v2.domain.repositories.StateData
import v2.domain.repositories.doc_repositories.InventoryRepository
import v2.presentation.screens.documents.edit_row_dt.EditRowActivity


class DocumentDetailsActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val DOC_TYPE = "DOC_TYPE"
        const val DOC_ID = "DOC_ID"

        fun getCallingIntent(context: Context, docType: DocType, docId: Long): Intent {
            val intent = Intent(context, DocumentDetailsActivity::class.java)
            intent.putExtra(DOC_TYPE, docType)
            intent.putExtra(DOC_ID, docId)
            return intent
        }
    }

    var docId: Long? = -1
    lateinit var docType: DocType

    lateinit var mainViewModel: DocumentDetailsViewModel

    lateinit var binding: ActivityDocDetailsBinding

    lateinit var goodsAdapter: GoodsDocListAdapter

    /* private var docSubdivisionId: Long = -1
     private var docSortSelectable = DocsSort.DESC
     private var docStateSelectable = DocState.ALL

     var subdivisions: List<Subdivision> = emptyList()*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        docType = intent.getSerializableExtra(DOC_TYPE) as DocType
        docId = intent.getLongExtra(DOC_ID, -1)


        mainViewModel = ViewModelProvider(this).get(DocumentDetailsViewModel::class.java)
        mainViewModel.docId = docId!!
        mainViewModel.docType = docType

        binding = DataBindingUtil.setContentView(this, R.layout.activity_doc_details)
        binding.lifecycleOwner = this
        binding.model = mainViewModel



        toolbar.setPadding(0, 0, 0, 0)//for tab otherwise give space in tab
        toolbar.setContentInsetsAbsolute(0, 0)
        setSupportActionBar(toolbar)

        backButton.setOnClickListener { finish() }

        titleTV.text = (when (docType) {
            DocType.INVENTORY -> getString(R.string.doc_title_inventory)
            DocType.BILL_IN -> getString(R.string.doc_title_bill_in)
            DocType.BILL_OUT -> getString(R.string.doc_title_bill_out)
            DocType.WRITE_OFF -> getString(R.string.doc_title_write_off)
        })

        docIcon.setImageResource(when (docType) {
            DocType.INVENTORY -> R.drawable.ic_inventory
            DocType.BILL_IN -> R.drawable.ic_bill_in
            DocType.BILL_OUT -> R.drawable.ic_bill_out
            DocType.WRITE_OFF -> R.drawable.ic_write_off
        })



        goodsAdapter = GoodsDocListAdapter(this@DocumentDetailsActivity, arrayListOf(), object : GoodsDocListAdapter.Callback {
            override fun onItemClicked(item: GoodDoc) {
                startActivityForResult(EditRowActivity.getCallingIntent(this@DocumentDetailsActivity, docType, firebirdGoodDocToParcelize(item)), LAUNCH_SECOND_ACTIVITY)



            }
        })

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = goodsAdapter
        recyclerView.addItemDecoration(DividerItemDecoration(this@DocumentDetailsActivity, DividerItemDecoration.VERTICAL))


        mainViewModel.showError.observe(this, Observer { it ->
            if (it != null) {
                showError(it)
            }
        })




        goToScannerBt.setOnClickListener {
            Log.d("my", "DOC_ID1=$docId")
            Log.d("my", "DOC_TYPE=$docType")
            if(docType == DocType.INVENTORY){
                isInventoryReady(docId!!)
            }else {
                startActivityForResult(ScannerActivityV2.getCallingIntent(this@DocumentDetailsActivity, docType, docId!!), LAUNCH_SECOND_ACTIVITY)
            }
        }





        mainViewModel.loadedDoc.observe(this, Observer {
            if (it) {
                Log.d("my","Doc was loas")
                progress.visibility = View.VISIBLE
                getGoods()
            }
        })


        mainViewModel.fetchDocument()




    }


    private fun getGoods() {
        mainViewModel.getGoodsList().observe(this, Observer {
            progress.visibility = View.INVISIBLE


            when (it.status) {
                StateData.DataStatus.SUCCESS -> {
                    Log.d("my", "Successdoc!! ${it.data!!.size}")
                    goodsAdapter.setGoodsList(it.data!!)
                }
                StateData.DataStatus.ERROR -> {
                    showError(it.error!!)
                }
                else -> {
                }
            }

            it.complete()
        })
    }

    private fun isInventoryReady(hdId: Long) {
        InventoryRepository.instance.isInventoryReady(hdId,
                { isReady ->
                    if(isReady){
                        startActivityForResult(ScannerActivityV2.getCallingIntent(this@DocumentDetailsActivity, docType, docId!!), LAUNCH_SECOND_ACTIVITY)
                    }else{
                        MaterialDialog(this).show {
                            title(text = getString(R.string.error))
                            message(text = getString(R.string.inventory_not_ready))
                            positiveButton(text = getString(R.string.close))
                        }
                    }

                },
                { throwable ->

                    MaterialDialog(this).show {
                        title(text = getString(R.string.error))
                        message(text = throwable.localizedMessage)
                        positiveButton(text = getString(R.string.close))
                    }
                })
    }



    private fun showError(err: Throwable) {
        Log.d("ErroScanner","errDoc= ${err.localizedMessage}")
        if (!err.localizedMessage.toString().contains("Result")
            &&!err.localizedMessage.toString().contains("result")
           ) {
            MaterialDialog(this).show {
                title(text = getString(R.string.error))
                message(text = err.localizedMessage)
                positiveButton(text = getString(R.string.close))
            }
        }else{
            Toast.makeText(this,"Waiting connection result..",Toast.LENGTH_SHORT).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("my","DocDetailsRes")

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("my","DocDetailsRes1")
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("my","DocDetailsRes2")
                //Write your code if there's no result
            }
        }

        progress.visibility = View.VISIBLE
        getGoods()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}


