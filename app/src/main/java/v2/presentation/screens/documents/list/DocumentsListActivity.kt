package v2.presentation.screens.documents.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.transition.TransitionManager
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.transitionseverywhere.Rotate
import kotlinx.android.synthetic.main.activity_companies_list.*
import kotlinx.android.synthetic.main.content_companies_list.recyclerView
import kotlinx.android.synthetic.main.content_documents_list.*
import v2.data.firebird.models.*
import v2.helpers.filters.DocState
import v2.helpers.filters.DocType
import v2.helpers.filters.DocsSort
import v2.domain.repositories.doc_repositories.*
import v2.presentation.screens.documents.create_document.CreateDocumentActivity
import v2.presentation.screens.documents.item.DocumentDetailsActivity
import v2.presentation.screens.repo.SubdivisionRepository


class DocumentsListActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val DOC_TYPE = "DOC_TYPE"

        fun getCallingIntent(context: Context, docType: DocType): Intent {
            val intent = Intent(context, DocumentsListActivity::class.java)
            intent.putExtra(DOC_TYPE, docType)
            return intent
        }
    }

    lateinit var docType: DocType

    private var docSubdivisionId: Long = -1
    private var docSortSelectable = DocsSort.DESC
    private var docStateSelectable = DocState.ALL

    var subdivisions: List<Subdivision> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        docType = intent.getSerializableExtra(DOC_TYPE) as DocType

        setContentView(R.layout.activity_documents_list)
        setSupportActionBar(toolbar)

        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = when (docType) {
            DocType.INVENTORY -> getString(R.string.inventory_list)
            DocType.BILL_IN -> getString(R.string.bill_in_list)
            DocType.BILL_OUT -> getString(R.string.bill_out_list)
            DocType.WRITE_OFF -> getString(R.string.write_off_list)
        }

        val transitionsContainer: ViewGroup = findViewById(R.id.transitions_container)


        fab.visibility = when (docType) {
            DocType.INVENTORY -> View.GONE
            DocType.BILL_IN -> View.VISIBLE
            DocType.BILL_OUT -> View.GONE
            DocType.WRITE_OFF -> View.GONE
        }


        fab.setOnClickListener {
           /* registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK) {
                    fetchList()
                }
            }.launch(CreateDocumentActivity.getCallingIntent(this, docType))*/
            startActivityForResult(CreateDocumentActivity.getCallingIntent(this, docType), LAUNCH_SECOND_ACTIVITY)
        }


        sortDocStateIcon.setOnClickListener {

            val state = docStateSelectable

            docStateSelectable = when (state) {
                DocState.ALL -> DocState.OPENED
                DocState.OPENED -> DocState.ALL
                DocState.CLOSED -> DocState.ALL
            }
            val tintColor = if (docStateSelectable == DocState.ALL) ContextCompat.getColor(this@DocumentsListActivity, R.color.ligth_gray)
            else ContextCompat.getColor(this@DocumentsListActivity, R.color.tBlueContrast3)

            sortDocStateIcon.setColorFilter(tintColor, android.graphics.PorterDuff.Mode.SRC_IN)
            fetchList()
        }

        sortAscDescIcon.setOnClickListener {

            val state = docSortSelectable

            docSortSelectable = when (state) {
                DocsSort.DESC -> DocsSort.ASC
                DocsSort.ASC -> DocsSort.DESC
            }
            val tintColor = if (docSortSelectable == DocsSort.DESC){
                ContextCompat.getColor(this@DocumentsListActivity, R.color.ligth_gray)
            }
            else {
                ContextCompat.getColor(this@DocumentsListActivity, R.color.tBlueContrast3)
            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition(transitionsContainer, Rotate())
                sortAscDescIcon.rotation = when (state) {
                    DocsSort.DESC -> 180f
                    DocsSort.ASC -> 0f
                }
            }


            sortAscDescIcon.setColorFilter(tintColor, android.graphics.PorterDuff.Mode.SRC_IN)
            fetchList()
        }

        subdivisionsSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                docSubdivisionId = if (position == 0) {
                    -1
                } else {
                    if (subdivisions.isEmpty()) {
                        -1
                    } else {
                        val positionInSubdivisionsList = position - 1
                        subdivisions[positionInSubdivisionsList].id
                    }
                }
                fetchList()
                return
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                return
            }
        }

        fetchList()

    }


    private fun fetchList() {
        progress.visibility = View.VISIBLE

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }

        sourceModel.getList(docStateSelectable, docSortSelectable, docSubdivisionId,
                { listList ->
                    progress.visibility = View.INVISIBLE
                    refreshUI(listList)
                    if (subdivisions.isEmpty()) {
                        fetchSubdivisions()
                    }
                },
                { throwable ->
                    progress.visibility = View.INVISIBLE
                    MaterialDialog(this).show {
                        title(text = getString(R.string.error))
                        message(text = throwable.localizedMessage)
                        positiveButton(text = getString(R.string.close))
                    }
                })
    }


    private fun fetchSubdivisions() {
        SubdivisionRepository.instance.getList(
                { list ->
                    if (list.isNotEmpty()) {
                        subdivisions = list
                    }
                    refreshSubdivisions()

                },
                { throwable ->
                    MaterialDialog(this).show {
                        title(text = getString(R.string.error))
                        message(text = throwable.localizedMessage)
                        positiveButton(text = getString(R.string.close))
                    }
                })
    }

    private fun refreshSubdivisions() {

        val subItemsList: MutableList<String> = mutableListOf()

        subItemsList.add(getString(R.string.all))

        for (subdivision in subdivisions) {
            subItemsList.add(subdivision.name)
        }

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subItemsList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        subdivisionsSpinner.adapter = adapter
    }


    private fun refreshUI(companiesList: List<Document>) {
        val myAdapter = DocumentsListAdapter(companiesList, object : DocumentsListAdapter.Callback {
            override fun onItemClicked(item: Document) {

                val docType = if (item is Inventory) DocType.INVENTORY else
                    if (item is BillIn) DocType.BILL_IN else
                        if (item is BillOut) DocType.BILL_OUT else
                            if (item is WriteOff) DocType.WRITE_OFF else DocType.INVENTORY

                startActivityForResult(DocumentDetailsActivity.getCallingIntent(this@DocumentsListActivity, docType, item.id), LAUNCH_SECOND_ACTIVITY)
            }
        })

        recyclerView.adapter = myAdapter
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
