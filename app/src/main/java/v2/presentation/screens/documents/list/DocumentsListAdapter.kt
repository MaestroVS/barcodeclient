package v2.presentation.screens.documents.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v1.utils.DateUtils
import v2.data.firebird.models.*

class DocumentsListAdapter (var items: List<Document>, val callback: Callback) : RecyclerView.Adapter<DocumentsListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_document_item, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val numTV = itemView.findViewById<TextView>(R.id.numTV)
        private val descriptionTV = itemView.findViewById<TextView>(R.id.descriptionTV)
        private val docStateIcon = itemView.findViewById<ImageView>(R.id.docStateIcon)
        private val dateTV = itemView.findViewById<TextView>(R.id.dateTV)
        private val docIcon = itemView.findViewById<ImageView>(R.id.docIcon)

        fun bind(item: Document) {
            numTV.text = item.num
            descriptionTV.text = item.subdivisionName

            if(item.doc_state == 1){
                docStateIcon.setImageResource(R.drawable.ic_lock_black_24dp)
            }
            if(item.doc_state == 0){
                docStateIcon.setImageResource(R.drawable.ic_lock_open_black_24dp)
            }

            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }

            dateTV.text = DateUtils.dateFirebirdToString(item.dateTime)

            if(item is Inventory) {
                docIcon.setImageResource(R.drawable.ic_inventory)
            }else if(item is BillIn) {
                docIcon.setImageResource(R.drawable.ic_bill_in)
            } else if(item is BillOut) {
                docIcon.setImageResource(R.drawable.ic_bill_out)
            } else if(item is WriteOff) {
                docIcon.setImageResource(R.drawable.ic_write_off)
            }

        }
    }

    interface Callback {
        fun onItemClicked(item: Document)
    }

}