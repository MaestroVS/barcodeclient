package v2.presentation.screens.documents.edit_row_dt

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.ActivityDocDetailsBinding
import com.app.barcodeclient3.databinding.ActivityEditRowBinding
import kotlinx.android.synthetic.main.activity_companies_list.toolbar
import kotlinx.android.synthetic.main.activity_doc_details.backButton
import kotlinx.android.synthetic.main.activity_doc_details.progress
import kotlinx.android.synthetic.main.activity_doc_details.recyclerView
import kotlinx.android.synthetic.main.activity_doc_details.titleTV
import kotlinx.android.synthetic.main.activity_edit_row.*
import kotlinx.android.synthetic.main.activity_edit_row.priceET
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocType
import v2.presentation.screens.scanner.ScannerActivityV2

import v2.domain.repositories.StateData
import v2.domain.repositories.doc_repositories.InventoryRepository
import v2.presentation.screens.scanner.scanner_fragment.utils.FieldType
import v2.presentation.screens.scanner.scanner_fragment.utils.InputField


class EditRowActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111




    var goodDoc: v2.data.firebird.models.parcelize.GoodDoc? = null
    lateinit var docType: DocType

    lateinit var viewModel: EditRowViewModel

    lateinit var binding: ActivityEditRowBinding



    /* private var docSubdivisionId: Long = -1
     private var docSortSelectable = DocsSort.DESC
     private var docStateSelectable = DocState.ALL

     var subdivisions: List<Subdivision> = emptyList()*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        docType = intent.getSerializableExtra(DOC_TYPE) as DocType
        goodDoc = intent.getParcelableExtra<v2.data.firebird.models.parcelize.GoodDoc>(GOOD_DOC)


        viewModel = ViewModelProvider(this).get(EditRowViewModel::class.java)


        viewModel.docType = docType

        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_row)
        binding.lifecycleOwner = this
        binding.model = viewModel


        setSupportActionBar(toolbar)

        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.edit_good)




        priceET.doAfterTextChanged {
            if(!canObserve) return@doAfterTextChanged
            priceET.setSelection(priceET.text!!.length)
            viewModel.refreshPriceTaxValue(it.toString())
        }


        cntEt.doAfterTextChanged {
            if(!canObserve) return@doAfterTextChanged
            cntEt.setSelection(cntEt.text!!.length)
            viewModel.refreshCountValue(it.toString())
        }

        totalAmount.doAfterTextChanged {
            if(!canObserve) return@doAfterTextChanged
            totalAmount.setSelection(totalAmount.text!!.length)
            viewModel.refreshAmountTaxValue(it.toString())
        }



        viewModel.showError.observe(this, Observer { it ->
            Log.d("my","-----showErro00")
            progress.visibility = View.GONE
            if (it != null) {
                Log.d("my","-----showError")
                showError(it)
            }
        })

        viewModel.showSuccess.observe(this, Observer { it ->
            if (it) {
                finish()
            }
        })

        saveBt.setOnClickListener {

            viewModel.modifyRowDt()
        }

        deteleBt.setOnClickListener {

            MaterialDialog(this).show {
                title(text = getString(R.string.dialog_delete_good))
                //message(text = getString(R.string.dialog_delete_good))
                positiveButton(text = getString(R.string.mdtp_ok))
                negativeButton(text = getString(R.string.cancel))
                positiveButton {
                    viewModel.deleteRowDt()
                }

            }
        }

    }

    var canObserve = false
    val h = Handler()

    override fun onResume() {
        super.onResume()

        viewModel.setGood(goodDoc!!)
        canObserve = true

    }



    private fun showError(err: Throwable) {
        MaterialDialog(this).show {
            title(text = getString(R.string.error))
            message(text = err.localizedMessage)
            positiveButton(text = getString(R.string.close))
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("my","DocDetailsRes")

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("my","DocDetailsRes1")
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("my","DocDetailsRes2")
                //Write your code if there's no result
            }
        }

        progress.visibility = View.VISIBLE

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    companion object {
        const val DOC_TYPE = "DOC_TYPE"
        const val GOOD_DOC = "GOOD_DOC"

        fun getCallingIntent(context: Context, docType: DocType, goodDoc: v2.data.firebird.models.parcelize.GoodDoc): Intent {
            val intent = Intent(context, EditRowActivity::class.java)
            intent.putExtra(DOC_TYPE, docType)
            intent.putExtra(GOOD_DOC, goodDoc)
            return intent
        }
    }
}


