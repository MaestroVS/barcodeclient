package v2.presentation.screens.documents.item

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import v1.utils.DateUtils
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocType
import v2.domain.repositories.doc_repositories.*
import v2.domain.repositories.StateLiveData


class DocumentDetailsViewModel : ViewModel() {

    var docType: DocType = DocType.BILL_IN
    var docId: Long = -1


    val progress = MutableLiveData<Boolean>()
    val docNum = MutableLiveData<String>()
    val docDate = MutableLiveData<String>()
    val docSubdivision = MutableLiveData<String>()
    val docStatus = MutableLiveData<Boolean>()

    val loadedDoc = MutableLiveData<Boolean>()


    val showError = MutableLiveData<Throwable>()


    init {

        docNum.value = ""
        docDate.value = ""
        docSubdivision.value = ""

        docStatus.value = false

        progress.value = true
        showError.value = null

        loadedDoc.value = false

    }


    fun fetchDocument() {
        progress.postValue(true)

        val sourceModel: DocumentsRepository = when (docType) {
            DocType.INVENTORY -> InventoryRepository.instance
            DocType.BILL_IN -> BillInRepository.instance
            DocType.BILL_OUT -> BillOutRepository.instance
            DocType.WRITE_OFF -> WriteOffRepository.instance
        }

        docId.let {
            sourceModel.getItem(it,
                    { document ->

                        loadedDoc.postValue(true)

                        progress.postValue(false)
                        docNum.postValue(document.num)
                        docDate.postValue(DateUtils.dateFirebirdToString(document.dateTime))
                        docSubdivision.postValue(document.subdivisionName)
                        docStatus.postValue(document.doc_state != 0)

                    },
                    { throwable ->
                        progress.postValue(false)
                        processError(throwable)

                    })
        }
    }

    fun getGoodsList(): StateLiveData<List<GoodDoc>> {
        when(docType){
            DocType.INVENTORY -> return  InventoryRepository.instance.listOfGoods(docId)
            DocType.BILL_IN -> return  BillInRepository.instance.listOfGoods(docId)
            DocType.BILL_OUT -> return  BillOutRepository.instance.listOfGoods(docId)
            DocType.WRITE_OFF -> return  WriteOffRepository.instance.listOfGoods(docId)
        }

    }




    private fun processError(error: Throwable) {
        showError.postValue(error)
        showError.postValue(null)
    }




}