package v2.presentation.screens;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.app.barcodeclient3.MainActivityOld;

import v2.helpers.SharedPref;
import v2.presentation.screens.main.MainActivity;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref.INSTANCE.init(this);

        int saveState = ScreenModeOptions.INSTANCE.getScreenMode();
        Log.d("switch","saveState="+saveState);

        /*if(saveState == 1) {
            startActivity(new Intent(SplashActivity.this, MainActivityOld.class));
        }else if(saveState == 2) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }else {
            startActivity(new Intent(SplashActivity.this, ModeActivity.class));
        }*/

        startActivity(new Intent(SplashActivity.this, MainActivity.class));

        finish();
    }
}
