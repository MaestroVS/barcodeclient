package v2.presentation.screens.component.buttons

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.app.barcodeclient3.R
import v2.helpers.Converter


class ButtonTeal : AppCompatButton {

    private var reverse : Boolean = false
    private var customTextSize: Float = 16f

    var active : Boolean = true
        set(value) {
            field = value
            initFont()
            initBackground()
        }

    constructor(context: Context) : super(context) {
        initNonStyle(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initNonStyle(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun initNonStyle(context: Context,  attrs: AttributeSet?) {


        init(context, attrs)
    }



    private fun init(context: Context,  attrs: AttributeSet?) {
        this.minimumHeight = Converter.convertDpToPixel(55f, context)


        if (attrs != null) {

            val ta = context.obtainStyledAttributes(attrs, R.styleable.CustomButtonAttrs, 0, 0)

            if (ta.hasValue(R.styleable.CustomButtonAttrs_reverse)) {
                reverse = ta.getBoolean(R.styleable.CustomButtonAttrs_reverse, false)

            }
            if (ta.hasValue(R.styleable.CustomButtonAttrs_textSize)) {
                customTextSize = ta.getFloat(R.styleable.CustomButtonAttrs_textSize, 16f)
            }
            ta.recycle()
        }

        initFont()
        initBackground()
    }

    private fun initFont(){
        setTextColor(if (!active) ContextCompat.getColor(context, R.color.white) else (if (reverse) ContextCompat.getColor(context, R.color.white) else ContextCompat.getColor(context,R.color.white)))
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, customTextSize)
       // val fontType = FontType.Bold

        //typeface = FontCache.getTypeface(fontType.getFontPath(), context)
    }

    private fun initBackground(){
        setBackgroundResource(if (!reverse && active)  R.drawable.button_main_round_teal else R.drawable.button_main_round_disable)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.stateListAnimator = null
        }

    }
}
