package v2.presentation.screens.component.cards

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.app.barcodeclient3.R


class BaseCard : CardView {



    constructor(context: Context) : super(context) {
        initNonStyle(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initNonStyle(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun initNonStyle(context: Context,  attrs: AttributeSet?) {

        init(context, attrs)
    }



    private fun init(context: Context,  attrs: AttributeSet?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.elevation = 16f
            this.radius = 12f
        }

        this.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
//        this.cardElevation = Converter.convertDpToPixel(13f, context).toFloat()
//        this.useCompatPadding = true
    }


}
