package v2.presentation.screens.company.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_company.*
import v2.data.database.tables.Connect
import v2.presentation.screens.dashboard.DashboardActivity


class CompanyDetailsActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val COMPANY_ID = "COMPANY_ID"
        const val COMPANY_NAME = "COMPANY_NAME"
        const val COMPANY_DESCRIPTION = "COMPANY_DESCRIPTION"
        const val IS_SHOW_NEXT_BUTTON = "IS_SHOW_NEXT_BUTTON"

        fun getCallingIntent(context: Context, companyId: Long, companyName: String, companyDescription: String, isShowNextButton: Boolean? = false): Intent {
            val intent = Intent(context, CompanyDetailsActivity::class.java)
            intent.putExtra(COMPANY_ID, companyId)
            intent.putExtra(COMPANY_NAME, companyName)
            intent.putExtra(COMPANY_DESCRIPTION, companyDescription)
            intent.putExtra(IS_SHOW_NEXT_BUTTON, isShowNextButton ?: false)

            return intent
        }
    }

    var companyId: Long = 0
    lateinit var companyName: String
    lateinit var companyDescription: String
    var isShowNextButton: Boolean = false

    var connect: Connect? = null

    var fragment: CompanyDetailsFragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        companyId = intent.getLongExtra(COMPANY_ID, 0)
        companyName = intent.getStringExtra(COMPANY_NAME)?:""
        companyDescription = intent.getStringExtra(COMPANY_DESCRIPTION)?:""
        isShowNextButton = intent.getBooleanExtra(IS_SHOW_NEXT_BUTTON, false)

        setContentView(R.layout.activity_company)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = companyName


        launchDashboard.setOnClickListener {
            startActivity(DashboardActivity.getCallingIntent(this@CompanyDetailsActivity, companyId, companyName))
        }

        fragment =  CompanyDetailsFragment.newInstance(companyId, companyName, companyDescription)

        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_container, fragment!!)
        fragmentTransaction.commit()



    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
       /* for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }*/

        if(fragment != null){
            fragment!!.onActivityResult(requestCode, resultCode, data);

        }
    }



    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
