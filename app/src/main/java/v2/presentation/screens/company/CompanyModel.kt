package v2.presentation.screens.company

import android.util.Log
import com.app.barcodeclient3.MainApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.database.dao.CompanyDao
import v2.data.database.tables.Company
import javax.inject.Inject
import kotlin.math.absoluteValue

class CompanyModel private constructor() {

    companion object {
        val instance: CompanyModel by lazy { CompanyModel() }
    }

    @Inject
    lateinit var companyDao: CompanyDao

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }

    fun createCompany(company: Company, success: (Long) -> Unit, error: (Throwable) -> Unit) {
        companyDao.insert(company)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { Log.d("my", "company id = ${it.absoluteValue}"); success(it.absoluteValue) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    fun getCompaniesList(success: (List<Company>) -> Unit, error: (Throwable) -> Unit) {
        companyDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { Log.d("my", "companies list = ${it.size}"); success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }


}