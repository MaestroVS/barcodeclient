package v2.presentation.screens.company.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R

import kotlinx.android.synthetic.main.activity_companies_list.*
import kotlinx.android.synthetic.main.content_companies_list.*
import v2.data.database.tables.Company
import v2.presentation.screens.company.CompanyModel
import v2.presentation.screens.company.create.CreateCompanyActivity
import v2.presentation.screens.company.details.CompanyDetailsActivity
import v2.presentation.screens.dashboard.DashboardActivity

class CompaniesListActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {


        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, CompaniesListActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_companies_list)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setTitle(R.string.organizations_list)

        fab.setOnClickListener { view ->
            startActivityForResult(CreateCompanyActivity.getCallingIntent(this@CompaniesListActivity), LAUNCH_SECOND_ACTIVITY)
        }

        fetchList()
    }

    private fun fetchList() {
        CompanyModel.instance.getCompaniesList(
            { companiesList ->
                if (companiesList.isEmpty()) {
                    startActivity(CreateCompanyActivity.getCallingIntent(this@CompaniesListActivity))
                } else {
                    refreshUI(companiesList)
                }
            },
            { throwable ->
                MaterialDialog(this).show {
                    title(text = getString(R.string.error))
                    message(text = throwable.localizedMessage)
                    positiveButton(text = "Close")
                }
            })
    }


    private fun refreshUI(companiesList: List<Company>) {
        val myAdapter = CompanyListAdapter(companiesList, object : CompanyListAdapter.Callback {
            override fun onItemClicked(item: Company) {
                startActivityForResult(DashboardActivity.getCallingIntent(this@CompaniesListActivity, item.id, item.name), LAUNCH_SECOND_ACTIVITY)
            }
        }, object : CompanyListAdapter.CallbackSecond {
            override fun onItemClicked(item: Company) {
                startActivityForResult(CompanyDetailsActivity.getCallingIntent(this@CompaniesListActivity, item.id, item.name, item.description), LAUNCH_SECOND_ACTIVITY)
            }
        })

        recyclerView.adapter = myAdapter
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                fetchList()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
