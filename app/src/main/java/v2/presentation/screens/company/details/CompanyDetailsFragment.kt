package v2.presentation.screens.company.details

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.fragment_company_details.*
import v2.presentation.activation.ActivationActivity
import v2.presentation.activation.utils.LicenseRepository
import v2.data.database.tables.Connect
import v2.helpers.ConnectionStatus
import v2.presentation.screens.connection.ConnectionModel
import v2.presentation.screens.connection.create.CreateConnectionActivity

class CompanyDetailsFragment : Fragment() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {
        const val COMPANY_ID = "COMPANY_ID"
        const val COMPANY_NAME = "COMPANY_NAME"
        const val COMPANY_DESCRIPTION = "COMPANY_DESCRIPTION"
        const val IS_SHOW_NEXT_BUTTON = "IS_SHOW_NEXT_BUTTON"

        @JvmStatic
        fun newInstance(companyId: Long, companyName: String, companyDescription: String, isShowNextButton: Boolean? = false) =
                CompanyDetailsFragment().apply {
                    arguments = Bundle().apply {
                        putLong(COMPANY_ID, companyId)
                        putString(COMPANY_NAME, companyName)
                        putString(COMPANY_DESCRIPTION, companyDescription)
                        if (isShowNextButton != null){
                            putBoolean(IS_SHOW_NEXT_BUTTON, isShowNextButton)
                        }
                    }
                }
    }

    var companyId: Long = 0
    lateinit var companyName: String
    lateinit var companyDescription: String
    var isShowNextButton: Boolean = false

    var connect: Connect? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("my", "onCreate___")

        arguments?.let {

            companyId = it.getLong(CompanyDetailsActivity.COMPANY_ID, 0)
            companyName = it.getString(CompanyDetailsActivity.COMPANY_NAME)?:""
            companyDescription = it.getString(CompanyDetailsActivity.COMPANY_DESCRIPTION)?:""
            isShowNextButton = it.getBoolean(CompanyDetailsActivity.IS_SHOW_NEXT_BUTTON, false)
            
        }
    }

    private lateinit var companyViewModel: CompanyDetailsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        Log.d("my", "onCreateView_____")
        companyViewModel =
                ViewModelProviders.of(this).get(CompanyDetailsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_company_details, container, false)
        //val textView: TextView = root.findViewById(R.id.text_dashboard)

        companyViewModel.text.observe(viewLifecycleOwner, Observer {
          //  textView.text = it
        })

        
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("my", "onViewCreated_____")
        updateConnectStatus()

        companyNameTV.text = companyName
        companyDescriptionTV.text = companyDescription

        connectionCard.setOnClickListener {
            val intent = CreateConnectionActivity.getCallingIntent(requireContext(), companyId, companyName, false)
            startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY)
        }


        editConnect.setOnClickListener {
            val intent = CreateConnectionActivity.getCallingIntent(requireContext(), companyId, companyName, true)
            startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY)
        }



        licenseCard.setOnClickListener {
            startActivityForResult(ActivationActivity.getCallingIntent(requireContext()), LAUNCH_SECOND_ACTIVITY)
        }

        checkLicense()

        getCompanyConnect()

    }

    var licenseIsActive = false

    fun checkLicense(){
        licenseIsActive = LicenseRepository.Companion.readLicenseStatus();
        updateLicenseStatus()
    }

    fun updateLicenseStatus(){
        if(licenseIsActive){
            licenseLogo.setColorFilter(ContextCompat.getColor(requireContext(), R.color.tealDark))
            licenseDetails.setTextColor(ContextCompat.getColor(requireContext(), R.color.tealDark))
            licenseDetails.setText(getText(R.string.license_is_active));
        }else{
            licenseLogo.setColorFilter(ContextCompat.getColor(requireContext(), R.color.primary))
            licenseDetails.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
            val txt = getString(R.string.license_not_active) +". "+getString(R.string.license_check)
            licenseDetails.setText(txt);
        }
    }



    var connectionStatus = ConnectionStatus.none

    private fun getCompanyConnect() {
        Log.d("my","Company id = $companyId")
        ConnectionModel.instance.getConnectionByCompanyId(companyId,
                { connect ->
                    Log.d("my","Company connect=${connect.id}   ip=${connect.ip}")
                    this.connect = connect
                    getDBVersion()
                },
                {
                    connectionStatus = ConnectionStatus.none
                    updateConnectStatus()
                })
    }


    private fun getDBVersion() {

        ConnectionModel.instance.getDBVersion(
                { dbVersion ->
                    connectionStatus = ConnectionStatus.connected
                    updateConnectStatus()
                },
                { throwable ->
                    connectionStatus = ConnectionStatus.not_connected
                    updateConnectStatus()
                })
    }


    private fun updateConnectStatus() {

        when (connectionStatus) {
            ConnectionStatus.none -> {
                connectDetails.text = getString(R.string.сonnection_options)
                firebirdLogo.setColorFilter(ContextCompat.getColor(requireContext(), R.color.primary))
                connectDetails.setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_gray))
            }
            ConnectionStatus.not_connected -> {
                connectDetails.text = getString(R.string.no_connection) + ": " + connect!!.ip
                connectDetails.setTextColor(ContextCompat.getColor(requireContext(), R.color.tRedContrast2))
                firebirdLogo.setColorFilter(ContextCompat.getColor(requireContext(), R.color.primary))


            }
            ConnectionStatus.connected -> {
                connectDetails.text = getString(R.string.connected) + ": " + connect!!.ip
                connectDetails.setTextColor(ContextCompat.getColor(requireContext(), R.color.tealDark))
                firebirdLogo.colorFilter = null


            }
        }
        if (connect == null) {

        } else {


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        Log.d("my", "onActivityResult!!")
        checkLicense()

        getCompanyConnect()
        /*if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                Log.d("my", "onActivityResult")
                checkLicense()

                getCompanyConnect()

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }*/
    }
}