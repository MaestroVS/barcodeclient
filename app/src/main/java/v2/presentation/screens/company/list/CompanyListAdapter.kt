package v2.presentation.screens.company.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v2.data.database.tables.Company

class CompanyListAdapter (var items: List<Company>, val callback: Callback, val callbackSecond: CallbackSecond) : RecyclerView.Adapter<CompanyListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_company_item, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val companyNameTV = itemView.findViewById<TextView>(R.id.companyNameTV)
        private val descriptionTV = itemView.findViewById<TextView>(R.id.descriptionTV)
        private val editView = itemView.findViewById<View>(R.id.editView)
        private val clickView = itemView.findViewById<View>(R.id.clickView)


        fun bind(item: Company) {
            companyNameTV.text = item.name
            descriptionTV.text = item.description
            clickView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }
            editView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callbackSecond.onItemClicked(items[adapterPosition])
            }
        }
    }

    interface Callback {
        fun onItemClicked(item: Company)
    }

    interface CallbackSecond {
        fun onItemClicked(item: Company)
    }

}