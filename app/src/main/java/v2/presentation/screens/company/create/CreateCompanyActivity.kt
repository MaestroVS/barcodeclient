package v2.presentation.screens.company.create

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.MainApplication
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.fragment_wellcome.*
import v2.AndroidApplication
import v2.data.database.tables.Company
import v2.presentation.screens.company.CompanyModel
import v2.presentation.screens.company.details.CompanyDetailsActivity

class CreateCompanyActivity : AppCompatActivity() {

    val LAUNCH_SECOND_ACTIVITY = 111

    companion object {

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, CreateCompanyActivity::class.java)
            return intent
        }
    }



    var companyId: Long = 0
    var companyName: String = ""
    var companyDescription: String = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.fragment_wellcome)

        AndroidApplication.app()!!.appComponent()!!.inject(this)

        progress.visibility = View.GONE

        crateCompany.setOnClickListener {
            if (editText.text.isEmpty()) {
                return@setOnClickListener
            }
            companyName = editText.text.toString()
            companyDescription = if (descrText.text.isNotEmpty()) {
                descrText.text.toString()
            } else {
                ""
            }

            CompanyModel.instance.createCompany(Company(name = companyName, description = companyDescription),
                    { id ->
                        /*MaterialDialog(this).show {
                            title(text = getString(R.string.success))
                            message(text = "Your company success created")
                            positiveButton(text = "OK")
                        }*/
                        companyId = id

                        val intent = CompanyDetailsActivity.getCallingIntent(this@CreateCompanyActivity, companyId, companyName, companyDescription, true)
                        startActivity(intent)

                    },
                    { throwable ->
                        MaterialDialog(this).show {
                            title(text = getString(R.string.error))
                            message(text = throwable.localizedMessage)
                            positiveButton(text = "Close")
                        }
                    })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
