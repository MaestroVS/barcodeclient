package v2.presentation.screens.units.utils

import androidx.recyclerview.widget.DiffUtil

import v2.data.firebird.models.Unit


class DiffUtilCallBack : DiffUtil.ItemCallback<v2.presentation.models.Unit>() {
    override fun areItemsTheSame(oldItem: v2.presentation.models.Unit, newItem: v2.presentation.models.Unit): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: v2.presentation.models.Unit, newItem: v2.presentation.models.Unit): Boolean {
        return oldItem.id == newItem.id
                && oldItem.name == newItem.name

    }

}