package v2.presentation.screens.units.list


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.barcodeclient3.R
import com.app.barcodeclient3.databinding.FragmentUnitsListBinding
import kotlinx.android.synthetic.main.content_goods_list.*
import v2.data.firebird.models.Good
import v2.domain.SharedCurrentState
import v2.presentation.models.Unit
import v2.presentation.screens.units.list.adapter.UnitsAdapter
import v2.presentation.screens.units.list.viewmodels.UnitsViewModel


class UnitsListFragment : Fragment() {

    companion object {


        @JvmStatic
        fun newInstance() =
                UnitsListFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }






    lateinit var mainViewModel: UnitsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }


    lateinit var binding: FragmentUnitsListBinding


    override fun onCreateView(inflater: LayoutInflater,
                              @Nullable container: ViewGroup?,
                              @Nullable savedInstanceState: Bundle?): View? {


        val viewRoot = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_units_list, null)
        binding = FragmentUnitsListBinding.bind(viewRoot)
        val view = binding.root

        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        mainViewModel = ViewModelProviders.of(this).get(UnitsViewModel::class.java)
        mainViewModel.companyId = SharedCurrentState.companyId

        binding.lifecycleOwner = this
        binding.model = mainViewModel


        mainViewModel.showWaitDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
              //  Dialogs.showWaitDialog(context!!)
            } else {
               // Dialogs.dismissWaitDialog()
            }
        })

        observeLiveData()

        mainViewModel.getUnits()
    }


    private fun observeLiveData() {

        mainViewModel.unitsLiveData.observe(viewLifecycleOwner, Observer {
            Log.d("my","Units ...")
            if(it.isNotEmpty()) {
                Log.d("my","Units ${it.size}")
                recyclerView.layoutManager = LinearLayoutManager(requireContext())
                recyclerView.adapter = UnitsAdapter(it, object: UnitsAdapter.Callback{
                    override fun onItemClicked(item: Unit) {
                        Log.d("my","Select ${item.name}")
                    }

                })
            }
        })
    }




}
