package v2.presentation.screens.units.list.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.barcodeclient3.R
import v2.presentation.models.Unit

class UnitsAdapter(var items: List<Unit>, val callback: Callback) : RecyclerView.Adapter<UnitsAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_unit, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class MainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val name = itemView.findViewById<TextView>(R.id.name)
        private val root = itemView.findViewById<LinearLayout>(R.id.root)


        fun bind(item: Unit) {
            name.text =  if(item.fullName.isNullOrEmpty() ) {
                item.name + " (" + item.fullName + ")"
            } else{
                item.name
            }


            root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(items[adapterPosition])
            }


        }
    }

    interface Callback {
        fun onItemClicked(item: Unit)
    }

}