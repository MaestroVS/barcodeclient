package v2.presentation.screens.units.list.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.Dispatchers
import v2.data.firebird.models.Good
import v2.domain.repositories.GoodsRepository
import v2.domain.repositories_pagination.GoodsListDataSource
import v2.presentation.models.Unit


class UnitsViewModel : ViewModel() {




    var unitsLiveData = MutableLiveData<List<Unit>>()


    var companyId: Long = 0


    val config: PagedList.Config = PagedList.Config.Builder()
            .setPageSize(10)
            .setEnablePlaceholders(false)
            .build()





    val progress = MutableLiveData<Boolean>()
    val showWaitDialog = MutableLiveData<Boolean>()

    val isRefreshing = MutableLiveData<Boolean>()

    val showNetworkError = MutableLiveData<Boolean>()
    val showUnknownError = MutableLiveData<Boolean>()


    init {


        unitsLiveData.value = mutableListOf<Unit>()



        progress.value = true
        showWaitDialog.value = false
        isRefreshing.value = false
        showNetworkError.value = false

    }




    fun getUnits(){
        progress.postValue(true)
           GoodsRepository.instance.getUnitsList(companyId, {
               progress.postValue(false)
               Log.d("my","UnitsViewModel success ${it.size}")
               unitsLiveData.postValue(it)
           },{
               progress.postValue(false)
               Log.d("my","UnitsViewModel err ${it.localizedMessage}")
           });
    }


    fun refresh() {

    }




}