package v2.presentation.screens.connection.create

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.MainApplication
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.fragment_create_connection.*
import v2.AndroidApplication
import v2.data.database.tables.Connect
import v2.data.firebird.FirebirdConnectionProvider
import v2.presentation.screens.connection.ConnectionModel
import javax.inject.Inject


class CreateConnectionActivity : AppCompatActivity() {

    @Inject
    lateinit var jdbcConnection: FirebirdConnectionProvider

    //@Inject
    //lateinit var firebirdDataSource: FirebirdDataSource

    companion object {
        const val COMPANY_ID = "COMPANY_ID"
        const val COMPANY_NAME = "COMPANY_NAME"

        const val IS_EDIT_CONNECT = "IS_EDIT_CONNECT"


        fun getCallingIntent(context: Context, companyId: Long, companyName: String, isEditConnect: Boolean): Intent {
            val intent = Intent(context, CreateConnectionActivity::class.java)
            intent.putExtra(COMPANY_ID, companyId)
            intent.putExtra(COMPANY_NAME, companyName)
            intent.putExtra(IS_EDIT_CONNECT, isEditConnect)
            return intent
        }
    }

    var companyId: Long = 0
    lateinit var companyName: String
    var isEditConnect = false


    var encodeList = arrayOf("UNICODE_FSS", "WIN1251")


    var connection: Connect? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidApplication.app()!!.appComponent()!!.inject(this)

        companyId = intent.getLongExtra(COMPANY_ID, 0)
        companyName = intent.getStringExtra(COMPANY_NAME)?:""

        val bundle = intent.extras
        isEditConnect = true //bundle.getBoolean(IS_EDIT_CONNECT) //intent.getBooleanExtra(IS_EDIT_CONNECT, false)

        setContentView(R.layout.fragment_create_connection)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Firebird connection"

        companyNameTV.text = companyName

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, encodeList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        encodingSpinner.adapter = adapter


        testConnection.setOnClickListener {
            initConn()
            setConnection()
        }

        saveConnection.setOnClickListener {
            initConn()
            saveConnection()
        }

        back.setOnClickListener { finish() }

        if (isEditConnect) {
            getCompanyConnect()
        }

    }

    private fun getCompanyConnect() {
        Log.d("my","CompanyUUUid = $companyId")
        ConnectionModel.instance.getConnectionByCompanyId(companyId,
                { connect ->
                    Log.d("my","CompanyUUUconnect=${connect.id}   ip=${connect.ip}")
                    this.connection = connect
                    updateConnectStatus()
                },
                {
                    updateConnectStatus()
                })
    }

    private fun updateConnectStatus() {
        if (connection == null) {
            return
        } else {
            ipAddressTV.setText(connection!!.ip)
            pathTV.setText(connection!!.path)
            portTV.setText(connection!!.port)
            userTV.setText(connection!!.user)
            passTV.setText(connection!!.password)

            val index = if (connection!!.encoding.equals(encodeList[0])) 0 else 1
            encodingSpinner.setSelection(index)
        }
    }


    private fun initConn(){
        if (ipAddressTV.text.isEmpty() || pathTV.text.isEmpty() || portTV.text.isEmpty()) return
        connection = Connect(ipAddressTV.text.toString(), pathTV.text.toString(), portTV.text.toString(),
                userTV.text.toString(), passTV.text.toString(), encodingSpinner.selectedItem.toString(), companyId)
    }



    private fun setConnection() {

        if(connection == null) return

        testProgress.visibility = View.VISIBLE
        testConnection.visibility = View.INVISIBLE


        ConnectionModel.instance.setFirebirdConnection(connection!!,
                { isSuccess ->
                    testProgress.visibility = View.INVISIBLE
                    testConnection.visibility = View.VISIBLE
                    if (isSuccess){ getDBVersion() }
                    else{ processError(Throwable("Connect could not set")) }
                },
                { throwable ->
                    testProgress.visibility = View.INVISIBLE
                    testConnection.visibility = View.VISIBLE
                    processError(throwable)
                })
    }

    private fun getDBVersion() {
        ConnectionModel.instance.getDBVersion(
                { dbVersion ->
                    MaterialDialog(this).show {
                        title(text = getString(R.string.success))
                        message(text = "Connection is success! \n DB version $dbVersion")
                        positiveButton(text = "OK")
                    }
                },
                { throwable ->
                    processError(throwable)
                })
    }

    private fun saveConnection() {
        if (connection == null) return
        ConnectionModel.instance.saveConnectionToDB(connection!!,
                { connectionId ->
                    val intent = intent
                    intent.putExtra("id", connectionId)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                },
                { throwable ->
                    processError(throwable)
                })
    }


    private fun processError(throwable: Throwable) {
        MaterialDialog(this).show {
            title(text = getString(R.string.error))
            message(text = throwable.localizedMessage)
            positiveButton(text = "Close")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
