package v2.presentation.screens.connection

import android.os.Handler
import android.util.Log
import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.database.dao.ConnectionDao
import v2.data.database.tables.Connect
import v2.data.firebird.FirebirdConnectionProvider
import v2.data.firebird.FirebirdManager
import v2.data.firebird.datasources.CommonSource
import javax.inject.Inject
import kotlin.math.absoluteValue

class ConnectionModel private constructor() {

    companion object {
        val instance: ConnectionModel by lazy { ConnectionModel() }
    }

    @Inject
    lateinit var firebirdManager: FirebirdManager

    @Inject
    lateinit var connectionDao: ConnectionDao

    @Inject
    lateinit var commonSource: CommonSource

    @Inject
    lateinit var jdbcConnection: FirebirdConnectionProvider

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }

    fun saveConnectionToDB(connect: Connect, success: (Long) -> Unit, error: (Throwable) -> Unit) {
        connectionDao.insert(connect)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it.absoluteValue) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    fun getConnectionByCompanyId(companyId: Long, success: (Connect) -> Unit, error: (Throwable) -> Unit) {
        connectionDao.getConnectByCompanyId(companyId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    fun getDBVersion(success: (String) -> Unit, error: (Throwable) -> Unit) {

        Observable.fromCallable(commonSource.getDBVersion())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    val h = Handler()

    fun setFirebirdConnection(connect: Connect, success: (Boolean) -> Unit, error: (Throwable) -> Unit) {

        Observable.fromCallable(jdbcConnection.createConnection(connect))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    Log.d("my","setFirebirdConnection succ")
                    success(true) }
                .onErrorReturn {
                    Log.d("my","setFirebirdConnection err")
                    error(it) }
                .subscribe()



    }


}