package v2.presentation.screens.connection

enum class StatusSync {
     syncronization, ready, error
}