package v2.presentation.screens.main

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.app.barcodeclient3.MainApplication
import com.app.barcodeclient3.R
import v2.AndroidApplication
import v2.data.database.dao.CompanyDao
import v2.data.database.tables.Company
import javax.inject.Inject

class SecondActivity : AppCompatActivity() {

    @Inject
    lateinit var companyDao: CompanyDao


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        Log.d("my", "Welcome second")

        AndroidApplication.app()!!.appComponent()!!.inject(this)


         Thread(){
             run(){
                 val id = companyDao.insert(Company("Second)",""))
                 Log.d("my", "company id = $id")
                // val company = companyDao.getById(id)
                 //Log.d("my", "company name = ${company.name}")

                 h.post {
                     //startActivity(Intent(this@SecondActivity, WelcomeActivity::class.java))
                     //finish()
                 }
             }
         }.start()


    }

    val h = Handler()

}