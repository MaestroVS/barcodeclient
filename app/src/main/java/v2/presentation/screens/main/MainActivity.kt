package v2.presentation.screens.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.android.gms.drive.Drive
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.sheets.v4.SheetsScopes
import com.pedrocarrillo.spreadsheetandroid.data.manager.AuthenticationManager
import v2.helpers.SharedPref
import v2.presentation.screens.company.CompanyModel
import v2.presentation.screens.company.create.CreateCompanyActivity
import v2.presentation.screens.company.list.CompaniesListActivity
import v2.presentation.screens.dashboard.DashboardActivity
import java.util.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        Log.d("my", "Welcome main")
        SharedPref.init(this)

        //initDependencies()
    }

    override fun onResume() {
        super.onResume()
        //SharedPref.init(this)
        fetchList()





    }

    companion object {
        const val TAG = "ReadSpreadsheetActivity"
        const val RQ_GOOGLE_SIGN_IN = 999
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_GOOGLE_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {

                Log.d("my", "sign in ok")
               // initDependencies2()


            } else {
                Log.d("my", "sign in err")
            }
        }
    }

   // var sheetsApiDataSource :
   //         SheetsAPIDataSource? = null

    var authManager:
            AuthenticationManager? = null

     fun initDependencies() {
        val signInOptions : GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Scope(SheetsScopes.SPREADSHEETS_READONLY))
                        .requestScopes(Scope(SheetsScopes.SPREADSHEETS))
                        .requestScopes(Drive.SCOPE_FILE)
                        .requestEmail()
                        .build()
        val googleSignInClient = GoogleSignIn.getClient(this, signInOptions)
        val googleAccountCredential = GoogleAccountCredential
                .usingOAuth2(this, Arrays.asList(*AuthenticationManager.SCOPES))
                .setBackOff(ExponentialBackOff())
         authManager =
                AuthenticationManager(
                        lazyOf(this),
                        googleSignInClient,
                        googleAccountCredential)



         startActivityForResult(googleSignInClient.signInIntent, RQ_GOOGLE_SIGN_IN)

    }


        val spreadsheetId = "1VpKeVAf7fmwAA8n4OzefDopyRt3rs6tnhjmto2zgyvw"
        val range = "Data!A1:C"   //"Data!=IF(\"testS\")"//"Log!A2:C"






    private fun fetchList() {
        CompanyModel.instance.getCompaniesList(
                { companiesList ->
                    if (companiesList.isEmpty()) {
                        startActivity(CreateCompanyActivity.getCallingIntent(this@MainActivity))
                    } else {

                        if (companiesList.size == 1) {
                            startActivity(DashboardActivity.getCallingIntent(this@MainActivity, companiesList[0].id, companiesList[0].name))
                        } else {
                            val lastCompanyId = SharedPref.readInt(SharedPref.LAST_COMPANY_ID, -1)
                            if (lastCompanyId >= 0) {
                                var hasId = false
                                var title = ""
                                companiesList.map {
                                    if (it.id.toInt() == lastCompanyId) {
                                        hasId = true
                                        title = it.name
                                    }
                                }
                                if (hasId) {
                                    startActivity(DashboardActivity.getCallingIntent(this@MainActivity, lastCompanyId.toLong(), title))
                                } else {
                                    startActivity(CompaniesListActivity.getCallingIntent(this@MainActivity))
                                }
                            } else {
                                startActivity(CompaniesListActivity.getCallingIntent(this@MainActivity))
                            }
                        }
                    }
                },
                { throwable ->
                    MaterialDialog(this).show {
                        title(text = getString(R.string.error))
                        message(text = throwable.localizedMessage)
                        positiveButton(text = "Close")
                    }
                })
    }




}