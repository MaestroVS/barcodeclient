package v2.presentation.screens

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.app.barcodeclient3.MainActivityOld
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_mode.*
import v2.presentation.screens.dashboard.settings.TSDScannerActivity
import v2.presentation.screens.main.MainActivity

class ModeActivity : AppCompatActivity() {

    companion object {

        fun getCallingIntent(context: Context): Intent {
            return Intent(context, ModeActivity::class.java)
        }
    }

    var launchScreen:LaunchScreen = LaunchScreen.OLD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mode)

        radio_old.isChecked = true

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
             when(i) {
                 R.id.radio_old -> launchScreen = LaunchScreen.OLD
                 R.id.radio_new -> launchScreen = LaunchScreen.NEW
                 else -> launchScreen = LaunchScreen.OLD
             }
        }

        continueBt.setOnClickListener {

            saveChoose()

            when(launchScreen){
                LaunchScreen.OLD -> startActivity(Intent(this, MainActivityOld::class.java))
                LaunchScreen.NEW -> startActivity(Intent(this, MainActivity::class.java))
            }
            finish()
        }
    }

    fun saveChoose(){
        val saveState = if(saveCheckBox.isChecked){
            if(launchScreen == LaunchScreen.OLD ){
                1
            }else{
                2
            }
        }else{
            -1
        }
        Log.d("switch", "saveState**=$saveState")
        ScreenModeOptions.setScreenMode(saveState)

    }



    enum class LaunchScreen{
         OLD, NEW
    }
}