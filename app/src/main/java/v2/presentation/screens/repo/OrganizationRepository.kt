package v2.presentation.screens.repo

import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.firebird.datasources.OrganizationSource
import v2.data.firebird.models.Org
import javax.inject.Inject

class OrganizationRepository private constructor() {

    companion object {
        val instance: OrganizationRepository by lazy { OrganizationRepository() }
    }


    @Inject
    lateinit var organizationSource: OrganizationSource

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }


     fun getList(success: (List<Org>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(organizationSource.list())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }


}