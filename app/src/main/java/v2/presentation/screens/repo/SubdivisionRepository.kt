package v2.presentation.screens.repo

import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.firebird.datasources.SubdivisionSource
import v2.data.firebird.models.Subdivision
import javax.inject.Inject

class SubdivisionRepository private constructor() {

    companion object {
        val instance: SubdivisionRepository by lazy { SubdivisionRepository() }
    }


    @Inject
    lateinit var subdivisionSource: SubdivisionSource

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }


     fun getList(success: (List<Subdivision>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(subdivisionSource.list())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }


}