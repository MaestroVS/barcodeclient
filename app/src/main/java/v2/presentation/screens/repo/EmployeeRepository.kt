package v2.presentation.screens.repo

import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.firebird.datasources.EmployeeSource
import v2.data.firebird.models.Employee
import javax.inject.Inject

class EmployeeRepository private constructor() {

    companion object {
        val instance: EmployeeRepository by lazy { EmployeeRepository() }
    }


    @Inject
    lateinit var employeeSource: EmployeeSource

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }


     fun getList(success: (List<Employee>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(employeeSource.list())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }


}