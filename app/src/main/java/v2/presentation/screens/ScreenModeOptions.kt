package v2.presentation.screens

import v2.helpers.SharedPref
import v2.presentation.screens.dashboard.settings.TSDScannerOptions

private const val SCREEN_MODE = "SCREEN_MODE"

object ScreenModeOptions {

    fun getScreenMode():Int{

        return SharedPref.readInt(SCREEN_MODE,-1)
    }

    fun setScreenMode(mode: Int){
        SharedPref.writeInt(SCREEN_MODE,mode)
    }
}