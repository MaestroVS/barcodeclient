package v2.presentation.common

class CommonPrefs {
    private var instance: CommonPrefs? = null

    companion object {
        val instance = CommonPrefs()
    }

    var isDB_3_0_68_0_plus = true
}