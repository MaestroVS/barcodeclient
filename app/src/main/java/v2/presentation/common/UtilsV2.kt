package v2.presentation.common

import java.util.*

class UtilsV2 {

    private var instance: UtilsV2? = null

    companion object {
        val instance = UtilsV2()
    }

    public fun isDB_3_0_68_0_plus(dbVersion: String): Boolean {
        val pattern = intArrayOf(3, 0, 68, 0)
        var currentStr = dbVersion
        var index = 0
        val currentVer = ArrayList<Int>()
        do {
            index = currentStr.indexOf(".")
            var nums = ""
            nums = if (index > 0) {
                currentStr.substring(0, index)
            } else {
                currentStr.substring(0)
            }
            try {
                val num = nums.toInt()
                currentVer.add(num)
            } catch (e: Exception) {
            }
            if (index < currentStr.length - 1) {
                currentStr = currentStr.substring(index + 1)
            }
            //
            //Log.d("my", "currentStr=$currentStr")
        } while (index >= 0)
        //Log.d("my", "base_nums=$currentVer")
        var to = currentVer.size
        if (to == 0) {
            return false
        }
        if (to > 4) {
            to = 4
        }
        for (i in 0 until to) {
            val a = pattern[i]
            val b = currentVer[i]
            if (b > a) {
                return true
            }
            if (b < a) {
                return false
            }
        }
        return true
    }
}