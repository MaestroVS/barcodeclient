package v2.presentation.activation.model

//import com.squareup.moshi.Json
//import com.squareup.moshi.JsonClass

/**
 * Created by theapache64 : Aug 29 Sat,2020 @ 10:13
 */
//@JsonClass(generateAdapter = true)
data class AddLicenseRequest(

        //@Json(name = "name")
        val name: String,

       // @Json(name = "address")
        val address: String,

        //@Json(name = "email")
        val email: String,

       // @Json(name = "phone")
        val phone: String,

       // @Json(name = "android_id")
        val android_id: String,

       // @Json(name = "date_to")
        val date_to: String,

       // @Json(name = "is_active")
        val is_active: String,

       // @Json(name = "description")
        val description: String
)