package v2.presentation.activation.repository

//import com.theapache64.retrosheet.core.Write
//import com.theapache64.retrosheet.core.Read
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
//import androidx.room.Query
import com.github.theapache64.retrosheet.annotations.*

import v2.presentation.activation.model.AddLicenseRequest
import v2.presentation.activation.model.License


interface LicensesApi {

    @Read("SELECT *")
    @GET("notes") // sheet name
    suspend fun getNotes(): License

    @Read("SELECT * WHERE android_id = :android_id")
    @GET("notes") // sheet name
    suspend fun getNotes(@Query("android_id") android_id: String): License

    @Write
    @POST(ADD_NOTE_ENDPOINT) // form name
    suspend fun addNote(@Body addNoteRequest: AddLicenseRequest): AddLicenseRequest


}