package v2.presentation.activation.repository

//import com.squareup.moshi.Moshi
//import com.theapache64.retrosheet.RetrosheetInterceptor
import com.github.theapache64.retrosheet.RetrosheetInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//import retrofit2.converter.moshi.MoshiConverterFactory

const val ADD_NOTE_ENDPOINT = "add_note"

class RetrofitProvider private constructor(){

    private val useMyUrl = true

    private val exampleSheetUrl = "https://docs.google.com/spreadsheets/d/1YTWKe7_mzuwl7AO1Es1aCtj5S9buh3vKauKCMjx1j_M/"
    private val exampleFormtUrl = "https://docs.google.com/forms/d/e/1FAIpQLSdmavg6P4eZTmIu-0M7xF_z-qDCHdpGebX8MGL43HSGAXcd3w/viewform?usp=sf_link"


    private val mySheetUrl = "https://docs.google.com/spreadsheets/d/1-1eiKucj51k7kpJsHqCYUa0ytOJskqg3lyLQgVLR2ik/"//"https://docs.google.com/spreadsheets/d/1PVqqj-EyHhwNqX6O3JZVt3BalLV0H1qJhZk_uqKKp-M/"
    private val myFormUrl =  "https://docs.google.com/forms/d/e/1FAIpQLSd1MyKvQoHYGSglyoL_eaeNPhsYUHnypP-9UUCke3yyPV4EeQ/viewform?usp=sf_link"//"https://docs.google.com/forms/d/e/1FAIpQLSfkUzYK0ew9Vh9zNjAdR8oKGdNdPCUg8anY9luht-LXE88xQQ/viewform?usp=sf_link"

   // https://docs.google.com/forms/d/e/1FAIpQLSd1MyKvQoHYGSglyoL_eaeNPhsYUHnypP-9UUCke3yyPV4EeQ/viewform?usp=sf_link

    lateinit var instance: RetrofitProvider

    lateinit var retrofit: Retrofit

    companion object {
     val instance: RetrofitProvider by lazy { RetrofitProvider() }
    }

    init {
        var sheetUrl = exampleSheetUrl
        var formUrl = exampleFormtUrl

        if(useMyUrl){
            sheetUrl = mySheetUrl
            formUrl = myFormUrl
        }


        val retrosheetInterceptor = RetrosheetInterceptor.Builder()
                .setLogging(false)
                // To Read
                .addSheet(
                        "notes", // sheet name
                        "created_at", "name", "address" , "email", "phone","android_id","date_to","is_active","description"
                )
                // To write

                .addForm(
                        ADD_NOTE_ENDPOINT,
                        formUrl
                )
                .build()

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(retrosheetInterceptor)
                .build()

       // val moshi = Moshi.Builder().build()


         retrofit = Retrofit.Builder()
                .baseUrl(sheetUrl) // Sheet's public URL
                .client(okHttpClient)
                //.addConverterFactory(MoshiConverterFactory.create(moshi))
                 .addConverterFactory(GsonConverterFactory.create())
                .build()
    }



}