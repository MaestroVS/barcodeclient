package v2.presentation.activation.deprecate_sheets

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_web_create_license.*


class CreateLicenseWebActivity : AppCompatActivity(), LicenseFormWebClient.CompleteHandler {

    companion object {

        const val EMAIL = "EMAIL"
        const val ANDROID_ID = "ANDROID_ID"

        fun getCallingIntent(context: Context, email: String, androidId: String): Intent {
            val intent = Intent(context, CreateLicenseWebActivity::class.java)
            intent.putExtra(EMAIL, email)
            intent.putExtra(ANDROID_ID, androidId)
            return intent
        }
    }

    lateinit var email: String
    lateinit var androidId: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        email = intent.getStringExtra(EMAIL)?:""
        androidId = intent.getStringExtra(ANDROID_ID)?:""

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.license_request)

        setContentView(R.layout.activity_web_create_license)


        myEmailTV.text = email
        myAndroidIDTV.text = androidId

        copyEmail.setOnClickListener {
            copyTextToClipboard(myEmailTV.text.toString())
        }
        copyAndroidId.setOnClickListener {
            copyTextToClipboard(myAndroidIDTV.text.toString())
        }

        initWebForm()
    }

    fun initWebForm(){


        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.settings.setSupportMultipleWindows(true)


        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.getSettings().setSupportMultipleWindows(true);


        webView.webViewClient = LicenseFormWebClient(this)
        webView.loadUrl("https://docs.google.com/forms/d/e/1FAIpQLSfZoneiz6XxhnzJOh02lCxc7EYpYGn6nRVjrFDePpt36xYbbg/viewform")
    }

    private fun copyTextToClipboard(textToCopy: String) {
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", textToCopy)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(this, "Text copied to clipboard", Toast.LENGTH_LONG).show()
    }

    val h = Handler()
    var completes = 0
    override fun complete() {
       Log.d("my", "COMPLETE!!!")
        completes++
        if(completes>=5) {
            h.postDelayed({
            finish()
        },600)
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}