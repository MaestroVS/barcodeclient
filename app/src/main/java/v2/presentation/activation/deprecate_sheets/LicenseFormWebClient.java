package v2.presentation.activation.deprecate_sheets;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LicenseFormWebClient extends WebViewClient {


    CompleteHandler completeHandler = null;

    public LicenseFormWebClient(CompleteHandler completeHandler) {
        this.completeHandler = completeHandler;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        Log.d("web","shouldOverrideUrlLoading request="+request.getUrl());


        String url = request.getUrl().toString();

        String completeUrl = "https://ssl.gstatic.com/docs/spreadsheets/forms/favicon_qp2.png";

        if(url.contains(completeUrl)) {
            if (completeHandler != null) {
                completeHandler.complete();
            }
        }


        return null;
    }

    public interface CompleteHandler{

        public  void complete();
    }
}
