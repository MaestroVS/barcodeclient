package v2.presentation.activation.deprecate_sheets

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.app.barcodeclient3.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.android.gms.drive.Drive
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.sheets.v4.SheetsScopes
import com.pedrocarrillo.spreadsheetandroid.data.manager.AuthenticationManager
import v2.presentation.activation.model.License
import com.pedrocarrillo.spreadsheetandroid.data.repository.sheets.SheetsAPIDataSource
import com.pedrocarrillo.spreadsheetandroid.data.repository.sheets.SheetsRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_activation.*
import v1.utils.DateUtils
import v1.utils.InternetUtil
import v2.presentation.activation.utils.ActivationUtils
import v2.presentation.activation.utils.LicenseRepository
import v2.presentation.activation.utils.LicenseStatus
import java.util.*


class ActivationActivityDeprecateSheet : AppCompatActivity() {

    val WARNING_DAYS = 14
    val DEMO_DAYS = 7

    companion object {

        const val TAG = "ReadSpreadsheetActivity"
        const val RQ_GOOGLE_SIGN_IN = 999

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, ActivationActivityDeprecateSheet::class.java)
            return intent
        }
    }

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.license_check)


        activateButton.isEnabled = false

        if( InternetUtil.verifyAvailableNetwork(this) ){
            initGoogleAuthorization()
        }else{
            showOffline()
        }

        // initWebForm()
    }


    var licenseStatus: LicenseStatus = LicenseStatus.NOT_FOUND

    var isHasMyEmail = false



    fun showOffline(){
        licenseStatusTV.visibility = View.VISIBLE
        licenseStatusTV.text = getString(R.string.error_connect)
        progress.visibility = View.INVISIBLE
        checkImageView.visibility = View.VISIBLE
        checkImageView.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.ic_no_internet))

    }

    var authManager:
            AuthenticationManager? = null

    fun initGoogleAuthorization() {
        val signInOptions: GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Scope(SheetsScopes.SPREADSHEETS_READONLY))
                        .requestScopes(Scope(SheetsScopes.SPREADSHEETS))
                        .requestScopes(Drive.SCOPE_FILE)
                        .requestEmail()
                        .build()
        val googleSignInClient = GoogleSignIn.getClient(this, signInOptions)
        val googleAccountCredential = GoogleAccountCredential
                .usingOAuth2(this, Arrays.asList(*AuthenticationManager.SCOPES))
                .setBackOff(ExponentialBackOff())
        authManager =
                AuthenticationManager(
                        lazyOf(this),
                        googleSignInClient,
                        googleAccountCredential)



        startActivityForResult(googleSignInClient.signInIntent, RQ_GOOGLE_SIGN_IN)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_GOOGLE_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {

                Log.d("my", "sign in ok")
                //  initDependencies2()

                readLicenses()

            } else {
                Log.d("my", "sign in err")
            }
        } else if(requestCode == CREATE_LICENSE_REQUEST){

            readLicenses()
        }
    }

    //https://docs.google.com/spreadsheets/d/1xUevTBd8WiRAwtL0WTEW0w0F2zl7qXERE5ihDN4Shgc/edit?usp=sharing
    val spreadsheetId = "1xUevTBd8WiRAwtL0WTEW0w0F2zl7qXERE5ihDN4Shgc"
    val range = "Відповіді форми (1)!A1:I"//"Data!A1:I"   //"Data!=IF(\"testS\")"//"Log!A2:C"


    var sheetsRepository: SheetsRepository? = null

    var myEmail: String? = null
    var myAndroidId: String? = null
    var myDeviceName: String? = null


    fun readLicenses() {

        progress.visibility = View.VISIBLE
        checkImageView.visibility = View.GONE
        closeButton.visibility = View.GONE

        authManager!!.setUpGoogleAccountCredential()


        if (authManager!!.getLastSignedAccount() != null) {
            myEmail = authManager!!.getLastSignedAccount()!!.email

            Log.d("my", "last account email = $myEmail")
        }

        myAndroidId = ActivationUtils.getDeviceId(this)
        myDeviceName = ActivationUtils.getDeviceName()

        val sheetsApiDataSource =
                SheetsAPIDataSource(authManager!!,
                        AndroidHttp.newCompatibleTransport(),
                        JacksonFactory.getDefaultInstance())

        sheetsRepository = SheetsRepository(sheetsApiDataSource)

        try {
            disposable = sheetsRepository!!.readSpreadSheet(spreadsheetId, range)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                          Log.d("my", "sheets err=${it.localizedMessage}")
                    }
                    .onErrorResumeNext( Single.error(Throwable("ERRRRRVsdsdf>>>>>>>>")))
                    .subscribe(Consumer {
                        Log.d("my", "sheets success=$it")
                         licenseStatus = searchMyDataInLicansesScope(it) ?: LicenseStatus.NOT_FOUND
                    Log.d("my", "isLicensed=$licenseStatus")
                    refreshUI()
                    })



        }catch (e: Exception){
            Log.d("my", "sheets eerr22")
        }
    }

    var _androidId = ""

    fun refreshUI() {

        _androidId = ""
        if (androidId != null) {
            _androidId = myAndroidId!!
            if (myAndroidId!!.length > 3) {
                _androidId = "Id = ***" + myAndroidId!!.substring(2)
            }
        }

        //androidId.text = _androidId

        //activateButton.isEnabled = true

        progress.visibility = View.GONE



        myEmailTV.text = myEmail

        val licenseStatusText = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> getString(R.string.license_not_exists)
            LicenseStatus.FOUND_WRONG -> getString(R.string.licence_data_incorrect)
            LicenseStatus.ACTIVE -> {
                val diffDays = DateUtils.daysToDate(licenseDateTo!!)
                if (diffDays > WARNING_DAYS) {
                    getString(R.string.license_active)+" \n"+DateUtils.dateMM_ToMMMDateString(licenseDateTo!!)
                } else {
                    getString(R.string.license_expires_in) + " " + resources.getQuantityString(R.plurals.plurals_day, diffDays.toInt(), diffDays)
                }
            }
            LicenseStatus.DEMO -> {
                getString(R.string.licence_demo_text1) + resources.getQuantityString(R.plurals.plurals_day, DEMO_DAYS, DEMO_DAYS)+
                        getString(R.string.licence_demo_text2)
            }
            LicenseStatus.EXPIRED -> getString(R.string.license_expired)
            LicenseStatus.INACTIVE -> getString(R.string.license_inactive)
            LicenseStatus.DUPLICATE -> getString(R.string.license_duplicated)

        }

        val licenseStatusColor = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> ContextCompat.getColor(this, R.color.dark_gray_a70)
            LicenseStatus.ACTIVE -> {

                if (DateUtils.daysToDate(licenseDateTo!!) > WARNING_DAYS) {
                    ContextCompat.getColor(this, R.color.teal)
                } else {
                    ContextCompat.getColor(this, R.color.tYellowContrast2)
                }
            }
            LicenseStatus.DEMO -> ContextCompat.getColor(this, R.color.tYellowContrast2)

            else -> ContextCompat.getColor(this, R.color.tRedContrast1)
        }

        checkImageView.visibility = View.VISIBLE

        checkImageView.setImageDrawable(ContextCompat.getDrawable(this,
                when (licenseStatus) {
                    LicenseStatus.ACTIVE, LicenseStatus.DEMO -> R.drawable.animated_check
                    LicenseStatus.NOT_FOUND -> R.drawable.ic_ans_mark
                        else -> R.drawable.ic_cancel_mark
                } ))

        when (licenseStatus) {
            LicenseStatus.ACTIVE, LicenseStatus.DEMO -> {

                (checkImageView.drawable as Animatable).start()

                continueButton.visibility = View.VISIBLE
                closeButton.visibility = View.GONE
                activateButton.visibility = View.GONE
                continueButton.setOnClickListener { finish() }
            }
            LicenseStatus.NOT_FOUND -> {
                continueButton.visibility = View.GONE
                closeButton.visibility = View.GONE
                activateButton.visibility = View.VISIBLE
                activateButton.active = true
                activateButton.isEnabled = true
                activateButton.setOnClickListener {
                    if (myEmail != null && myAndroidId != null)
                        startActivityForResult(CreateLicenseWebActivity.getCallingIntent(ActivationActivity@ this, myEmail!!, myAndroidId!!), CREATE_LICENSE_REQUEST)
                }
            }

            else -> {
                continueButton.visibility = View.GONE
                closeButton.visibility = View.VISIBLE
                activateButton.visibility = View.GONE
                closeButton.setOnClickListener { finish() }
            }
        }

        licenseStatusTV.setTextColor(licenseStatusColor)
        licenseStatusTV.text = licenseStatusText

        activateButton.visibility = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> View.VISIBLE
            else -> View.INVISIBLE
        }



        LicenseRepository.writeLicenseStatus(licenseStatus)

    }

    val CREATE_LICENSE_REQUEST = 6541

    var licenseDateTo: Date? = null

    fun searchMyDataInLicansesScope(scope: List<License>): LicenseStatus? {
        if (myEmail == null || myAndroidId == null || myDeviceName == null) return null
        var containsEmail = false
        var containsId = false
        var containsEmailAndID = false
        var dateFrom = ""
        var dateTo = ""
        var demo = ""

        var equalLicensesCount = 0

        scope.map {
            Log.d("my", "email = ${it.email}  android_id=${it.android_id}  dateTo=${it.date_to}")
            if (it.email == myEmail) containsEmail = true
            if (it.android_id == myAndroidId) containsId = true
            if (it.android_id == myAndroidId && it.email == myEmail) {
                equalLicensesCount += 1
                containsEmailAndID = true
                dateFrom = it.created_at
                dateTo = it.date_to
                demo = it.is_active
            }
        }

        if (containsEmailAndID) {
            Log.d("my", "containsEmailAndID")

            if (equalLicensesCount > 1) {
                return LicenseStatus.DUPLICATE
            } else {

                if (dateTo.isEmpty()) {

                    if(demo != null && demo.isNotEmpty() && demo.contains("1")){
                        Log.d("my","dateFrom = $dateFrom  DEMO_DAYS = $DEMO_DAYS")
                        val _dateFrom = DateUtils.stringToDate(dateFrom)
                        Log.d("my","dateFrom = $_dateFrom ")
                        val dateOfDemoEnd = DateUtils.addDaysToDate(_dateFrom, DEMO_DAYS)
                        val daysToDemoEnd = DateUtils.daysToDate(dateOfDemoEnd)
                        Log.d("my","dateOfDemoEnd = $dateOfDemoEnd  daysToDemoEnd = $daysToDemoEnd")

                        if(daysToDemoEnd > 0){
                            return LicenseStatus.DEMO
                        }else{
                            return LicenseStatus.INACTIVE
                        }

                    } else {
                        return LicenseStatus.INACTIVE
                    }
                } else {

                    try {
                        val _dateTo = DateUtils.stringToDate(dateTo)

                        val isDateExpired = DateUtils.isDateExpired(_dateTo)

                        Log.d("my", "isDateExpired=$isDateExpired")
                        if (isDateExpired) {
                            return LicenseStatus.EXPIRED
                        } else {
                            licenseDateTo = _dateTo
                            return LicenseStatus.ACTIVE
                        }

                    } catch (e: Exception) {
                        return LicenseStatus.INACTIVE
                    }

                }
            }

        } else {

            Log.d("my", " not containsEmailAndID")

            if (!containsId && !containsEmail) {
                return LicenseStatus.NOT_FOUND
            } else {
                return LicenseStatus.FOUND_WRONG
            }
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    /*fun writeLicension() {


        val name = nameET.text.toString()
        val address = addressET.text.toString()
        val email = myEmail ?: ""
        val deviceName = ActivationUtils.getDeviceName() ?: ""
        val androidId = ActivationUtils.getDeviceId() ?: ""
        val currentDate = ActivationUtils.getCurrentDateString()
        val dateTo = ActivationUtils.getDaysToString(LicenseTime.DEMO.days)
        val isActive = LicenseStatus.ACTIVE.name
        val note = noteET.text.toString()

        val licensesList = mutableListOf<License>()
        //val license = License(name, address, email, deviceName, androidId, currentDate, dateTo, isActive, note)
        Rollbar.reportException(Exception("New registration: name=$name address=$address address2=${addressET2.text} email=$email deviceName=$deviceName androidId=$androidId  currentDate=$currentDate" +
                "dateTo=$dateTo isActive=$isActive note=$note"))
        ///val email2 = "sf@sdf.ukr"
        val _email = email.replace("(^[^@]{3}|(?!^)\\G)[^@]".toRegex(), "$1*")


        var _name =  "***"
        if(name.length > 3){
            _name = name.replace("(^[^ ]{3}|(?!^)\\G)".toRegex(), "$1*")
        }

        val license = License(_name, address, _email, "--", _androidId, currentDate, dateTo, isActive, "--")
        licensesList.add(license)

        if (myEmail != null && name.isNotEmpty()) {

            disposable = sheetsRepository!!.writeSpreadSheet(spreadsheetId, range, license)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        Log.d("my", "write license err${it.localizedMessage}")
                    }
                    .subscribe(Consumer {
                        Log.d("my", "write license success")
                    })

        }

    }*/


    /*

    CREATE NEW SPREADSHEET

    val spreadsheetMaker = SpreadsheetMaker()
            val spreadsheet: Spreadsheet =
                    spreadsheetMaker
                            .create("Example of creating spreadsheet",
                                    "Data",
                                    licensesList)

            disposable = sheetsRepository!!
                    .createSpreadsheet(spreadsheet)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        //  view.showError(it.message!!)
                        Log.d("my", "write license err${it.localizedMessage}")
                    }
                    .subscribe {
                        // view.showResult(it.id, it.url)

                        Log.d("my", "write license success")
                    }*/

}