package v2.presentation.activation

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_request_license.*
import kotlinx.coroutines.runBlocking
import retrofit2.Retrofit
import v1.utils.DateUtils
import v2.presentation.activation.model.AddLicenseRequest
import v2.presentation.activation.repository.LicensesApi
import v2.presentation.activation.repository.RetrofitProvider
import v2.presentation.activation.utils.ActivationUtils
import java.net.URLEncoder

class RequestLicenseActivity : AppCompatActivity() {

    //val WARNING_DAYS = 14
    val DEMO_DAYS = 7

    var retrofit: Retrofit? = null
    var licenseApi: LicensesApi? = null

    var myEmail: String? = null
    var myAndroidId: String? = null
    var myDeviceName: String? = null


    companion object {

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, RequestLicenseActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_license)


         myEmail= ActivationUtils.getAccountEmail(this)
         myAndroidId = ActivationUtils.getDeviceId(this)
         myDeviceName = ActivationUtils.getDeviceName()


        initUI()
        retrofit = RetrofitProvider.instance.retrofit
        licenseApi = retrofit!!.create(LicensesApi::class.java)



    }

    fun initUI(){
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.license_request)


        emailTV.setText(myEmail?:"")
        noteTV.setText(myDeviceName?:"")


        requestBt.setOnClickListener {
            runBlocking {
                requestBt.isEnabled = false
                requestLicense()
            }
        }
    }




    suspend fun requestLicense() {

        val dateTo = DateUtils.getDaysToString(DEMO_DAYS)

        val emptyName = companyNameTV.text.isNullOrEmpty()
        val emptyAddress = companyAddressTV.text.isNullOrEmpty()
        val emptyEmail = emailTV.text.isNullOrEmpty()

        if (emptyName || emptyEmail || emptyAddress) {
            requestBt.isEnabled = true
            showEmptyfieldDialog(emptyName,emptyAddress,emptyEmail)
        } else  if(!ActivationUtils.isValidEmail(emailTV.text.toString())){
            requestBt.isEnabled = true
            showIncorrectEmailDialog()
        } else{

            val name = encUTF8(companyNameTV.text.toString())
            val address = encUTF8(companyAddressTV.text.toString())
            val email = emailTV.text.toString()
            val phone = phoneTV.text.toString()
            val description = encUTF8(noteTV.text.toString())

            val licenseRequest =  AddLicenseRequest(name,address,email,phone,myAndroidId?:"?",dateTo,"1",description)

           // val licenseRequest =  AddLicenseRequest("name","address","emailler@gmail.com","phone","myAndroidId","dateTo","1","description")

            val addLicense = licenseApi!!.addNote(licenseRequest)

            Log.d("my","addLicense=$addLicense")

            finish()

        }

    }

    fun showEmptyfieldDialog(name: Boolean, address: Boolean,  email: Boolean ){
        var msg = ""
        if(name){
            msg += getString(R.string.license_company_name)
            msg += "\n"
        }
        if(address){
            msg += getString(R.string.address)
            msg += "\n"
        }
        if(email){
            msg += getString(R.string.email)
            msg += "\n"
        }
        MaterialDialog(this).show {
            title(text = getString(R.string.empty_field))
            message(text = msg)
            positiveButton(text = getString(R.string.close))
        }
    }

    fun showIncorrectEmailDialog(){
        var msg =getString(R.string.email_incorrect)

        MaterialDialog(this).show {
            title(text = getString(R.string.error))
            message(text = msg)
            positiveButton(text = getString(R.string.close))
        }
    }


    fun encUTF8(str: String): String {
        return URLEncoder.encode(str, "UTF-8")?:""
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}