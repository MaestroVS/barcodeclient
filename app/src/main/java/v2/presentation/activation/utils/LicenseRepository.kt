package v2.presentation.activation.utils

import android.util.Log
import v1.utils.DateUtils
import v2.helpers.SharedPref

class LicenseRepository {

    companion object{

        fun writeLicenseStatus(licenseStatus: LicenseStatus){



            val currentDate = DateUtils.getCurrentDateString()

            Log.d("license","writeLicenseStatus licenseStatus=$licenseStatus")
            Log.d("license","writeLicenseStatus currentDate=$currentDate")

            SharedPref.writeString(SharedPref.LICENSE_CHECK_DATE,currentDate)
            val storedData = SharedPref.readString(SharedPref.LICENSE_CHECK_DATE,"*")
            Log.d("license","writeLicenseStatus storedData=$storedData")
            when(licenseStatus){
                LicenseStatus.ACTIVE, LicenseStatus.DEMO -> {
                    SharedPref.writeBool(SharedPref.LICENSE_IS_ACTIVE, true)
                }
                else ->{
                    SharedPref.writeBool(SharedPref.LICENSE_IS_ACTIVE, false)
                }
            }
        }

        fun readLicenseStatus(): Boolean{



            val currentDate = DateUtils.getCurrentDateString()

            val storedData = SharedPref.readString(SharedPref.LICENSE_CHECK_DATE,"+")

            val isActive = SharedPref.readBool(SharedPref.LICENSE_IS_ACTIVE,false)

            Log.d("license","readLicenseStatus currentDate=$currentDate")
            Log.d("license","readLicenseStatus storedData=$storedData")
            Log.d("license","readLicenseStatus isActive=$isActive")


            if(currentDate != storedData ){
                return false
            }else{
                return  isActive
            }
        }

    }
}