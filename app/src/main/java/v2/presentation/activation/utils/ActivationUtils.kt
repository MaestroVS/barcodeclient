package v2.presentation.activation.utils

import android.accounts.AccountManager
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Patterns
import com.app.barcodeclient3.MainApplication
import java.util.regex.Pattern


class ActivationUtils {

    companion object {



        fun getDeviceName(): String? {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
                capitalize(model)
            } else {
                capitalize(manufacturer) + " " + model
            }
        }

        fun getDeviceId(context: Context): String? {
            var android_id = Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ANDROID_ID)
            if (android_id == null) android_id = MainApplication.getDeviceName()
            if (android_id!!.length == 0) android_id = MainApplication.getDeviceName()
            return android_id
        }


        private fun capitalize(s: String?): String {
            if (s == null || s.length == 0) {
                return ""
            }
            val first = s[0]
            return if (Character.isUpperCase(first)) {
                s
            } else {
                Character.toUpperCase(first).toString() + s.substring(1)
            }
        }

        fun getAccountEmail(context: Context): String?{
            var gmail: String? = null

            val gmailPattern: Pattern = Patterns.EMAIL_ADDRESS // API level 8+

            val accounts = AccountManager.get(context).accounts
            for (account in accounts) {
                if (gmailPattern.matcher(account.name).matches()) {
                    gmail = account.name
                }
            }

            return gmail
        }

        fun isValidEmail(target: CharSequence?): Boolean {
            return if (TextUtils.isEmpty(target)) {
                false
            } else {
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }

    }




}