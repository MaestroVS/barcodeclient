package v2.presentation.activation.utils

enum class LicenseStatus {

    NOT_FOUND, FOUND_WRONG, ACTIVE , EXPIRED , INACTIVE, DUPLICATE, DEMO
}