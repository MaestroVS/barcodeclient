package v2.presentation.activation.utils

enum class LicenseTime(val days:Int) {

    DEMO(3),
    TRIAL(14),
    YEAR(365)
}