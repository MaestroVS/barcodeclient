package v2.presentation.activation

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.app.barcodeclient3.R
import kotlinx.android.synthetic.main.activity_activation.*
import kotlinx.coroutines.runBlocking
import retrofit2.Retrofit
import v1.utils.DateUtils
import v1.utils.InternetUtil
import v2.presentation.activation.model.License
import v2.presentation.activation.repository.LicensesApi
import v2.presentation.activation.repository.RetrofitProvider
import v2.presentation.activation.utils.ActivationUtils
import v2.presentation.activation.utils.LicenseRepository
import v2.presentation.activation.utils.LicenseStatus
import java.util.*

class ActivationActivity : AppCompatActivity() {


    var retrofit: Retrofit? = null
    var licenseApi: LicensesApi? = null

    var licenseStatus: LicenseStatus = LicenseStatus.NOT_FOUND

    companion object {

        const val TAG = "ReadSpreadsheetActivity"

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, ActivationActivity::class.java)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation)

        retrofit = RetrofitProvider.instance.retrofit
        licenseApi = retrofit!!.create(LicensesApi::class.java)

        initUI()
    }

    fun initUI() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.license_check)


        activateButton.isEnabled = false

        if (InternetUtil.verifyAvailableNetwork(this)) {
            runBlocking {
                checkLicense()
            }
        } else {
            showOffline()
        }
    }


    var myEmail: String? = null
    var myAndroidId: String? = null
    var myDeviceName: String? = null

    val CREATE_LICENSE_REQUEST = 6541
    var licenseDateTo: Date? = null


    var license: License? = null

    suspend fun checkLicense() {

        progress.visibility = View.VISIBLE
        checkImageView.visibility = View.GONE
        closeButton.visibility = View.GONE

        myEmail = ActivationUtils.getAccountEmail(this)
        myAndroidId = ActivationUtils.getDeviceId(this)
        myDeviceName = ActivationUtils.getDeviceName()
        Log.d("my", "getNote myAndroidId = " + myAndroidId)
        try {
             license = licenseApi!!.getNotes(myAndroidId ?: "")
            Log.d("my", "getNote responseAGAAA = " + license)

            licenseStatus = searchMyDataInLicansesScope(license)

        } catch (e: Exception) {
            Log.d("my", "getNote error = " + e.localizedMessage)
            licenseStatus = LicenseStatus.NOT_FOUND
        }

        refreshStatusUI()
    }

    val WARNING_DAYS = 14
    val DEMO_DAYS = 7

    fun searchMyDataInLicansesScope(license: License?): LicenseStatus {
        if(license == null) return LicenseStatus.NOT_FOUND
        if (myEmail == null && myAndroidId == null && myDeviceName == null) return LicenseStatus.NOT_FOUND
        var containsEmail = false
        var containsId = false
        var containsEmailAndID = false
        var dateFrom = license.created_at
        var dateTo = license.date_to
        var demo = ""


        Log.d("my", "email = ${license.email}  android_id=${license.android_id}  dateTo=${license.date_to}")
        if (license.email == myEmail) containsEmail = true
        if (license.android_id == myAndroidId) containsId = true
        if (license.android_id == myAndroidId && license.email == myEmail) {

            containsEmailAndID = true

            demo = license.is_active
        }


        if (containsId) {
            Log.d("my", "containsId")

            if (dateTo.isEmpty()) {
                Log.d("my", "containsId 1")
                return LicenseStatus.INACTIVE

            } else {
                Log.d("my", "containsId 2")
                try {
                    Log.d("my", "containsId 21")
                    val _dateTo = DateUtils.stringToDate(dateTo)

                    val isDateExpired = DateUtils.isDateExpired(_dateTo)

                    Log.d("my", "isDateExpired=$isDateExpired")
                    if (isDateExpired) {
                        return LicenseStatus.EXPIRED
                    } else {
                        licenseDateTo = _dateTo
                        return LicenseStatus.ACTIVE
                    }

                } catch (e: Exception) {
                    Log.d("my", "containsId 3")
                    return LicenseStatus.INACTIVE
                }
            }

        } else {

            Log.d("my", " not containsEmailAndID")

            if (!containsId && !containsEmail) {
                return LicenseStatus.NOT_FOUND
            } else {
                return LicenseStatus.FOUND_WRONG
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CREATE_LICENSE_REQUEST) {
            runBlocking {
                checkLicense()
            }
        }
    }

    var _androidId = ""

    fun refreshStatusUI() {

        _androidId = ""
        if (_androidId != null) {
            _androidId = myAndroidId!!
            if (myAndroidId!!.length > 3) {
                _androidId = "Id = ***" + myAndroidId!!.substring(2)
            }
        }

        //androidId.text = _androidId

        //activateButton.isEnabled = true

        progress.visibility = View.GONE



        myEmailTV.text = myEmail

        val licenseStatusText = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> getString(R.string.license_not_exists)
            LicenseStatus.FOUND_WRONG -> getString(R.string.licence_data_incorrect)
            LicenseStatus.ACTIVE -> {
                val diffDays = DateUtils.daysToDate(licenseDateTo!!)
                if (diffDays > WARNING_DAYS) {
                    getString(R.string.license_active) + " \n" + DateUtils.dateMM_ToMMMDateString(licenseDateTo!!)
                } else {
                    getString(R.string.license_expires_in) + " " + resources.getQuantityString(R.plurals.plurals_day, diffDays.toInt(), diffDays)
                }
            }
            LicenseStatus.DEMO -> {
                getString(R.string.licence_demo_text1) + resources.getQuantityString(R.plurals.plurals_day, DEMO_DAYS, DEMO_DAYS) +
                        getString(R.string.licence_demo_text2)
            }
            LicenseStatus.EXPIRED -> getString(R.string.license_expired)
            LicenseStatus.INACTIVE -> getString(R.string.license_inactive)
            LicenseStatus.DUPLICATE -> getString(R.string.license_duplicated)

        }

        val licenseStatusColor = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> ContextCompat.getColor(this, R.color.dark_gray_a70)
            LicenseStatus.ACTIVE -> {

                if (DateUtils.daysToDate(licenseDateTo!!) > WARNING_DAYS) {
                    ContextCompat.getColor(this, R.color.teal)
                } else {
                    ContextCompat.getColor(this, R.color.tYellowContrast2)
                }
            }
            LicenseStatus.DEMO -> ContextCompat.getColor(this, R.color.tYellowContrast2)

            else -> ContextCompat.getColor(this, R.color.tRedContrast1)
        }

        checkImageView.visibility = View.VISIBLE

        checkImageView.setImageDrawable(ContextCompat.getDrawable(this,
                when (licenseStatus) {
                    LicenseStatus.ACTIVE, LicenseStatus.DEMO -> R.drawable.animated_check
                    LicenseStatus.NOT_FOUND -> R.drawable.ic_ans_mark
                    else -> R.drawable.ic_cancel_mark
                }))

        when (licenseStatus) {
            LicenseStatus.ACTIVE, LicenseStatus.DEMO -> {

                (checkImageView.drawable as Animatable).start()

                continueButton.visibility = View.VISIBLE
                closeButton.visibility = View.GONE
                activateButton.visibility = View.GONE
                continueButton.setOnClickListener { finish() }
            }
            LicenseStatus.NOT_FOUND -> {
                continueButton.visibility = View.GONE
                closeButton.visibility = View.GONE
                activateButton.visibility = View.VISIBLE
                activateButton.active = true
                activateButton.isEnabled = true
                activateButton.setOnClickListener {
                    Log.d("my","activate myEmail=$myEmail  myAndroidId=$myAndroidId")

                        Log.d("my","activate")
                        startActivityForResult(RequestLicenseActivity.getCallingIntent(ActivationActivity@ this), CREATE_LICENSE_REQUEST)

                }
            }

            else -> {
                continueButton.visibility = View.GONE
                closeButton.visibility = View.VISIBLE
                activateButton.visibility = View.GONE
                closeButton.setOnClickListener { finish() }
            }
        }

        if ((licenseStatus == LicenseStatus.ACTIVE && DateUtils.daysToDate(licenseDateTo!!) > WARNING_DAYS) || licenseStatus == LicenseStatus.NOT_FOUND) {
            emailFab.visibility = View.INVISIBLE
        } else {
            emailFab.visibility = View.VISIBLE
            emailFab.setOnClickListener {
                sendEmail(license)
            }
        }

        licenseStatusTV.setTextColor(licenseStatusColor)
        licenseStatusTV.text = licenseStatusText

        /*activateButton.visibility = when (licenseStatus) {
            LicenseStatus.NOT_FOUND -> View.VISIBLE
            else -> View.INVISIBLE
        }*/



        LicenseRepository.writeLicenseStatus(licenseStatus)

    }

    fun sendEmail(license: License?){

        val name = license?.name ?: ""
        val address = license?.address ?: ""
        val phone = license?.phone ?: ""
        val androidId = license?.android_id ?: ActivationUtils.getDeviceId(this)

        val mailTo = "horecasoft.ua@gmail.com"
        val subject = "Запит поновлення ліцензії"
        val body = "Назва закладу:  $name \nАдреса:  $address \n Телефон:  $phone \nID ліцензії:  $androidId \n\n .."
        val intentTitle = getString(R.string.license_request)

       /* val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", mailTo, null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, body)
        startActivity(Intent.createChooser(emailIntent, "Send email..."))

        */


        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "message/rfc822"
        sendIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(mailTo))
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        sendIntent.putExtra(Intent.EXTRA_TEXT, body)
        startActivity(sendIntent)
    }


    fun showOffline() {
        licenseStatusTV.visibility = View.VISIBLE
        licenseStatusTV.text = getString(R.string.error_connect)
        progress.visibility = View.INVISIBLE
        checkImageView.visibility = View.VISIBLE
        checkImageView.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.ic_no_internet))

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}