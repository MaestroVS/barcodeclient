package v2.presentation.models

data class Unit(val id: Long, val name: String, val fullName: String?)
