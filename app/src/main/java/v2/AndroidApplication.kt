package v2

import android.app.Application
import com.app.barcodeclient3.MainApplication
import v2.di.components.AppComponent
import v2.di.components.DaggerAppComponent
import v2.di.modules.AppModule


class AndroidApplication : Application() {

    //public static boolean isLicensed=false;
    //  private String rollbarId = "69b56ad85ef34390a56d40727dedf010";

    private var appComponent: AppComponent? = null

    companion object {
        private var app: AndroidApplication? = null
        fun app(): AndroidApplication? {
            return app
        }
    }



    fun appComponent(): AppComponent? {
        return appComponent
    }

   /* val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }*/

    override fun onCreate() {
        super.onCreate()


        // Rollbar.init(this, rollbarId, "production");
       // context = applicationContext

        //dagger

        //dagger
        app = this
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext)).build()

        //FirebaseFirestore.getInstance().addSnapshotsInSyncListener()

        //FirebaseFirestore.getInstance().addSnapshotsInSyncListener()

    }


}