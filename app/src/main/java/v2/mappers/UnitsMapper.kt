package v2.mappers

import v2.data.database.tables.DicUnit
import v2.presentation.models.Unit

object UnitsMapper {

    fun fromRoomDB(unit: DicUnit) : Unit{
        return Unit(unit.id, unit.name, unit.full_name)
    }

    fun fromRoomDBAll(units: List<DicUnit>) : List<Unit>{
        val list = mutableListOf<Unit>()
        units.map {
            list.add(Unit(it.id, it.name, it.full_name))
        }
        return list
    }

    fun fromFirebirdDB(unit: v2.data.firebird.models.Unit) : Unit{
        return Unit(unit.id, unit.name, unit.full_name)
    }

    fun fromFirebirdDBAll(units: List<v2.data.firebird.models.Unit>) : List<Unit>{
        val list = mutableListOf<Unit>()
        units.map {
            list.add(Unit(it.id, it.name, it.full_name))
        }
        return list
    }
}