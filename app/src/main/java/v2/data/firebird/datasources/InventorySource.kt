package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.StatementsDic
import v2.data.firebird.models.GoodDoc
import v2.data.firebird.models.Inventory
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import v2.presentation.common.CommonPrefs
import java.io.IOException
import java.util.*
import java.util.concurrent.Callable

class InventorySource(private val firebirdManager: FirebirdManager) {


    fun list(docState: DocState, docsSort: DocsSort, subdivisionId: Long): Callable<List<Inventory>> {
        return Callable {
            val inventories = ArrayList<Inventory>()

            val subdivisionFilter = if (subdivisionId < 0) " where  JB.subdivision_id is not null " else " where  JB.subdivision_id = $subdivisionId "

            val docStateFilter = when (docState) {
                DocState.ALL -> ""
                DocState.OPENED -> " and JB.doc_state = 0 "
                DocState.CLOSED -> " and JB.doc_state = 1 "
            }

            val docSortFilter = when (docsSort) {
                DocsSort.DESC -> " DESC "
                DocsSort.ASC -> " ASC "
            }

            var statement = ("select JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_inventory_act JB ")
            statement += subdivisionFilter
            statement += docStateFilter
            statement += " order by JB.DATE_TIME "
            statement += docSortFilter


            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val inventory = gson.fromJson(obj.toString(), Inventory::class.java)
                            if (inventory != null) {
                                inventories.add(inventory)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
            inventories
        }
    }


    fun item(id: Long): Callable<Inventory?> {
        return Callable {
            val list = ArrayList<Inventory>()

            val statement = ("select first 1 JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_inventory_act JB where JB.ID = " + id)

            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv get stm =$statement")
            if (jsonArray != null) {
                Log.d("my", "inv list not null size = " + jsonArray.length())
                Log.d("my", "inv list = $jsonArray")

                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val inventory = gson.fromJson(obj.toString(), Inventory::class.java)
                            if (inventory != null) {
                                list.add(inventory)
                            } else
                                Log.d("my", "inventory =---err")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Log.d("my", "inventory =---errtttt$e")
                        }
                    }
                }
            } else {
                Log.d("my", "inv get is jsonArr null5")
            }

            if (list.isNotEmpty()) {
                list[0]
            } else {
                null
            }
        }
    }

    @Throws(IOException::class)
    fun insertOrModifyRow(inventoryId: Long, goodId: Long, count: Float): Callable<Int?> {
        return Callable {
            firebirdManager.executeProcedure(StatementsDic.request_sys_connect)
            Log.d("my", " isDB_3_0_68_0_plus=" + CommonPrefs.instance.isDB_3_0_68_0_plus)

            val jsonArray = firebirdManager.executeProcedure(if (CommonPrefs.instance.isDB_3_0_68_0_plus) {
                "execute procedure Z\$INVENTORY_EDITOR_U( $inventoryId,$goodId,$count,0, null,null, 1)"
            } else {
                "execute procedure Z\$INVENTORY_EDITOR_U( $inventoryId,$goodId,$count,0, null,null)"
            })
            Log.d("my", "insertOrModifyRow = $jsonArray")

            (jsonArray.toString()).toInt() //0 success


        }
    }

    fun listOfGoods(id: Long): Callable<List<GoodDoc>> {
        return Callable {
            val list = ArrayList<GoodDoc>()

            val statement = "select * from Z\$JOR_INVENTORY_ACT_DT_S($id)"
            Log.d("my", "inv_state=$statement")

            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv_state=${jsonArray.toString()}")
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), GoodDoc::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }

    /**
     * Чи заповнена інвентаризація
     */
    fun isInventoryReady(id: Long): Callable<Boolean> {
        return Callable {
            var isReady = false

            val statement = "select count(AD.HD_ID)\n" +
                    " from JOR_INVENTORY_ACT_DT AD\n" +
                    " where AD.HD_ID = $id"
            Log.d("my", "inv_state=$statement")

            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv_state=${jsonArray.toString()}")
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    try {
                        val obj = jsonArray.getJSONObject(0)
                        val count = obj.getInt("count")
                        isReady = count > 0
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            isReady
        }
    }


}