package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.StatementsDic
import v2.data.firebird.models.BillIn
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import java.io.IOException
import java.util.ArrayList
import java.util.concurrent.Callable

class BillInSource(private val firebirdManager: FirebirdManager) {


    fun list(docState: DocState, docsSort: DocsSort, subdivisionId: Long): Callable<List<BillIn>> {
        return Callable {
            val inventories = ArrayList<BillIn>()

            val subdivisionFilter = if (subdivisionId < 0) " where  JB.subdivision_id is not null " else " where  JB.subdivision_id = $subdivisionId "

            val docStateFilter = when (docState) {
                DocState.ALL -> ""
                DocState.OPENED -> " and JB.doc_state = 0 "
                DocState.CLOSED -> " and JB.doc_state = 1 "
            }

            val docSortFilter = when (docsSort) {
                DocsSort.DESC -> " DESC "
                DocsSort.ASC -> " ASC "
            }

            var statement = ("select JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_bill_in JB ")
            statement += subdivisionFilter
            statement += docStateFilter
            statement += " order by JB.DATE_TIME "
            statement += docSortFilter

            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val document = gson.fromJson(obj.toString(), BillIn::class.java)
                            if (document != null) {
                                inventories.add(document)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()

                        }

                    }

                }
            }

            inventories
        }
    }


    fun item(id: Long): Callable<BillIn?> {
        return Callable {
            val list = ArrayList<BillIn>()

            val statement = ("select first 1 JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_bill_in JB where JB.ID = " + id)

            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), BillIn::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            if (list.isNotEmpty()) {
                list[0]
            } else {
                null
            }
        }
    }


    fun listOfGoods(id: Long): Callable<List<GoodDoc>> {
        return Callable {
            val list = ArrayList<GoodDoc>()

            val statement = "select * from Z\$JOR_BILL_IN_DT_S($id)"

            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    Log.d("my","jsonArray= $jsonArray")
                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), GoodDoc::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }

    @Throws(IOException::class)
    fun insertOrModifyRow(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?, rowSumWithTax: Float?, priceWithTax: Float?, price: Float?): Callable<Int?> {
        return Callable {
            firebirdManager.executeProcedure(StatementsDic.request_sys_connect)
            val jsonArray = firebirdManager.executeProcedure(
                    "execute procedure Z\$JOR_BILL_IN_DT_I( null, $billInId, $goodId, $unitId , $count, $rowSum, $rowSumWithTax,null, null, null,  $priceWithTax,$price, null)")

            Log.d("my", "insertOrModifyRow = $jsonArray")

            (jsonArray.toString()).toInt() //0 success


        }
    }

    @Throws(IOException::class)
    fun updateRow(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?, rowSumWithTax: Float?, priceWithTax: Float?, price: Float?, id: Long): Callable<Int?> {
        return Callable {
            //firebirdManager.executeProcedure(StatementsDic.request_sys_connect)

            val request = "update JOR_BILL_IN_DT " +
                    "  set HD_ID = $billInId, " +
                    "      GOODS_ID = $goodId, " +
                    "      UNIT_ID = $unitId, " +
                    "      CNT = $count, " +
                    "      ROW_SUM = $rowSum, " +
                    "      SUM_WITH_TAX = $rowSumWithTax, " +
                    "      PRICE_BASE =  $price,\n" +
                    "      OUT_PRICE = 0, " +
                    "      MAIN_VALUE = 0, " +
                    "      PRICE = $price, " +
                    "      PRICE_WITHOUT_TAX = $priceWithTax, " +
                    "      SUPPLIER_REQUEST_DT_ID = null " +
                    "  where ID = $id;"

            Log.d("my", "request = $request")
            try{
                val jsonArray = firebirdManager.executeStatement(request)
            }catch (e: Exception){

            }


            0


        }
    }


    @Throws(IOException::class)
    fun deleteRow( id: Long): Callable<Int?> {
        return Callable {
            //firebirdManager.executeProcedure(StatementsDic.request_sys_connect)

            val request = "delete from jor_bill_in_dt dt where dt.id = $id"

            Log.d("my", "request = $request")
            try{
                val jsonArray = firebirdManager.executeStatement(request)
            }catch (e: Exception){

            }


            0


        }
    }

}