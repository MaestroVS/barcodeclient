package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.models.Good
import v2.data.firebird.models.Group
import v2.helpers.filters.DocFilter
import v2.helpers.filters.EntitySort
import v2.helpers.filters.EntitySort.*
import v2.helpers.filters.GoodSearchFilter
import v2.helpers.filters.PageFilter
import java.util.*
import java.util.concurrent.Callable

class GoodsSource(private val firebirdManager: FirebirdManager) {


    fun grpList(): Callable<List<Group>> {
        return Callable {
            val list = ArrayList<Group>()

            val statement = "select   dg.id,  dg.parent_id , dg.name from dic_goods_grp dg  order by dg.name"
            Log.d("my", "get_goods_grp_list_statement =$statement")
            val jsonArray = firebirdManager.executeStatement(statement)

            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), Group::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }


    fun goodsList(goodSearchFilter: GoodSearchFilter?, grpId: Long?, goodSort: EntitySort?,
                  docFilter: DocFilter?, pageFilter: PageFilter?): Callable<List<Good>> {
        return Callable {
            val list = ArrayList<Good>()


            var conditionsFilterStr = "  where DG.is_service = 0 "

            if (goodSearchFilter != null) {
                conditionsFilterStr += " and "
                conditionsFilterStr += goodSearchFilter.getConditionString()
            }
            if (grpId != null) {
                conditionsFilterStr += " and DG.GRP_ID = $grpId "
            }


            val sortFilter = if (goodSort != null)
                when (goodSort) {
                    NAME -> " order by dg.name "
                    ASC -> " order by ASC "
                    DESC -> " order by DESC "
                }
            else " order by dg.name "

            val statement = """
                select 
                    ${pageFilter?.getPageString() ?: " "}
                    dg.id, 
                    dg.grp_id, 
                    dg.article, 
                    dg.name,
                    dg.unit_id, 
                    (select DU.NAME from DIC_UNIT DU where DU.ID = DG.UNIT_ID) as unit, 
                    (select first 1 db.barcode from dic_goods_barcodes db where db.goods_id = dg.id) as barcode, 
                    (select first 1 DPLD.PRICE 
                        from DIC_PRICE_LIST DPL, DIC_PRICE_LIST_DT DPLD 
                        where DPLD.HD_ID = DPL.ID and 
                        DPL.GOODS_ID = DG.ID and 
                        DPLD.DATE_FROM <  current_timestamp 
                    order by DPLD.DATE_FROM desc) as price 
                    ${docFilter?.getFilterConditionPref() ?: " "}
                    from DIC_GOODS DG 
                    $conditionsFilterStr
                    $sortFilter
            """
            Log.d("my", "get_goods_list_statement =$statement")
            val jsonArray = firebirdManager.executeStatement(statement)

            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), Good::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }


    fun unitsList(): Callable<List<v2.data.firebird.models.Unit>> {
        return Callable {
            val list = ArrayList<v2.data.firebird.models.Unit>()

            val statement = "select   dg.id,  dg.name , dg.full_name from dic_unit dg  order by dg.name"
            val jsonArray = firebirdManager.executeStatement(statement)

            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            //Log.d("my","JSON_unit = $obj")
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), v2.data.firebird.models.Unit::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }



}