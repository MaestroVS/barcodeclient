package v2.data.firebird.datasources

import android.util.Log
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import java.util.concurrent.Callable

class CommonSource(private val firebirdManager: FirebirdManager) {

    fun getDBVersion(): Callable<String?> {
        return Callable {
            var dbver: String? = null
            val statement = "execute procedure sp_get_version"
            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                //[{"RESULT":"2.1.186.0"}]
                if (jsonArray.length() > 0) {
                    try {
                        val obj = jsonArray.getJSONObject(0)
                        dbver = obj.getString("result")
                        Log.d("my", "DBVER = " + dbver!!)
                    } catch (e: JSONException) {
                        e.printStackTrace()

                    }

                }
            }
            dbver
        }
    }

}