package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.models.BillOut
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import java.util.ArrayList
import java.util.concurrent.Callable

class BillOutSource(private val firebirdManager: FirebirdManager) {


    fun list(docState: DocState, docsSort: DocsSort, subdivisionId: Long): Callable<List<BillOut>> {
        return Callable {
            val inventories = ArrayList<BillOut>()

            val subdivisionFilter = if (subdivisionId < 0) " where  JB.subdivision_id is not null " else " where  JB.subdivision_id = $subdivisionId "

            val docStateFilter = when (docState) {
                DocState.ALL -> ""
                DocState.OPENED -> " and JB.doc_state = 0 "
                DocState.CLOSED -> " and JB.doc_state = 1 "
            }

            val docSortFilter = when (docsSort) {
                DocsSort.DESC -> " DESC "
                DocsSort.ASC -> " ASC "
            }

            var statement = ("select JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_bill_out JB ")
            statement += subdivisionFilter
            statement += docStateFilter
            statement += " order by JB.DATE_TIME "
            statement += docSortFilter

            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val document = gson.fromJson(obj.toString(), BillOut::class.java)
                            if (document != null) {
                                inventories.add(document)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()

                        }

                    }

                }
            }

            inventories
        }
    }

    fun item(id: Long): Callable<BillOut?> {
        return Callable {
            val list = ArrayList<BillOut>()

            val statement = ("select first 1 JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                    + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                    + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                    + " from jor_bill_out JB where JB.ID = " + id)

            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv get stm =$statement")
            if (jsonArray != null) {
                Log.d("my", "inv list not null size = " + jsonArray.length())
                Log.d("my", "inv list = $jsonArray")

                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val inventory = gson.fromJson(obj.toString(), BillOut::class.java)
                            if (inventory != null) {
                                list.add(inventory)
                            } else
                                Log.d("my", "inventory =---err")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Log.d("my", "inventory =---errtttt$e")
                        }
                    }
                }
            } else {
                Log.d("my", "inv get is jsonArr null3")
            }

            if (list.isNotEmpty()) {
                list[0]
            } else {
                null
            }
        }
    }

    fun listOfGoods(id: Long): Callable<List<GoodDoc>> {
        return Callable {
            val list = ArrayList<GoodDoc>()

            val statement = "select * from Z\$JOR_BILL_OUT_DT_S($id)"

            val jsonArray = firebirdManager.executeStatement(statement)
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), GoodDoc::class.java)
                            if (item != null) {
                                list.add(item)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            list
        }
    }

}