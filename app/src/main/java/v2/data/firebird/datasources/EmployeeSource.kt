package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.models.Employee
import java.util.ArrayList
import java.util.concurrent.Callable

class EmployeeSource(private val firebirdManager: FirebirdManager) {


    fun list(): Callable<List<Employee>> {
        return Callable {
            val list = ArrayList<Employee>()
            Log.d("my", "EmployeeSource")

            val statement = ("select DG.ID, DG.GRP_ID, DG.CODE_NAME, DG.SURNAME, DG.NAME, " +
                    " DG.SEC_NAME, DG.PIN, DG.JOB_TITLE_ID, " +
                    " (select DT.NAME from DIC_JOB_TITLE DT where DT.ID = DG.JOB_TITLE_ID) as JOB_TITLE, " +
                    " DG.USER_ID FROM DIC_EMPLOYEE DG ")
            Log.d("my", "EmployeeSource $statement")
            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv get stm =$statement")
            if (jsonArray != null) {
                Log.d("my", " list not null size = " + jsonArray.length())
                Log.d("my", " list = $jsonArray")

                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), Employee::class.java)
                            if (item != null) {
                                list.add(item)
                            } else
                                Log.d("my", " =---err")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Log.d("my", " =---errtttt$e")

                        }

                    }

                }
            } else {
                Log.d("my", "inv get is jsonArr null4")
            }

            list
        }
    }

}