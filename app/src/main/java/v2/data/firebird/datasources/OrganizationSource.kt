package v2.data.firebird.datasources

import android.util.Log
import com.google.gson.Gson
import org.json.JSONException
import v2.data.firebird.FirebirdManager
import v2.data.firebird.models.Org
import java.util.ArrayList
import java.util.concurrent.Callable

class OrganizationSource(private val firebirdManager: FirebirdManager) {


    fun list(): Callable<List<Org>> {
        return Callable {
            val list = ArrayList<Org>()


            var statement = ("select DG.ID, DG.GRP_ID, DG.NAME FROM DIC_ORG DG ")

            val jsonArray = firebirdManager.executeStatement(statement)
            Log.d("my", "inv get stm =$statement")
            if (jsonArray != null) {
                Log.d("my", " list not null size = " + jsonArray.length())
                Log.d("my", " list = $jsonArray")

                if (jsonArray.length() > 0) {

                    for (i in 0 until jsonArray.length()) {
                        try {
                            val obj = jsonArray.getJSONObject(i)
                            val gson = Gson()
                            val item = gson.fromJson(obj.toString(), Org::class.java)
                            if (item != null) {
                                list.add(item)
                            } else
                                Log.d("my", " =---err")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Log.d("my", " =---errtttt$e")

                        }

                    }

                }
            } else {
                Log.d("my", "inv get is jsonArr null7")
            }

            list
        }
    }

}