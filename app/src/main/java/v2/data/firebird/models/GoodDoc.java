package v2.data.firebird.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoodDoc  {

    @SerializedName("id")
    @Expose
    Long id;

    @SerializedName("hd_id")
    @Expose
    Long grp_id;

    @SerializedName("goods_id")
    @Expose
    Long goods_id;

    @SerializedName("article")
    @Expose
    private String article;


    @SerializedName("goods_name")
    @Expose
    private String goods_name;

    @SerializedName("unit_id")
    @Expose
    Long unit_id;

    @SerializedName("unit_name")
    @Expose
    private String unit_name;

    @SerializedName("cnt")
    @Expose
    private float cnt;

    @SerializedName("cnt_fact")
    @Expose
    private float cnt_fact;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("price_with_tax")
    @Expose
    private float price_with_tax;

    @SerializedName("price_base")
    @Expose
    private float price_base;

    @SerializedName("tax")
    @Expose
    private float tax;

    @SerializedName("row_sum")
    @Expose
    private float row_sum;

    @SerializedName("sum_tax")
    @Expose
    private float sum_tax;

    @SerializedName("sum_with_tax")
    @Expose
    private float sum_with_tax;


    @SerializedName("sum_base")
    @Expose
    private float sum_base;

    @SerializedName("sum_disc")
    @Expose
    private float sum_disc;


    @SerializedName("out_price")
    @Expose
    private float out_price;

    @SerializedName("sum_sum")
    @Expose
    private float sum_sum;


   /* @Override
    public Long getId() {
        return id;
    }*/

    /*@Override
    public void setId(Long id) {
        this.id = id;
    }*/

   /* @Override
    public Long getGrp_id() {
        return grp_id;
    }

    @Override
    public void setGrp_id(Long grp_id) {
        this.grp_id = grp_id;
    }*/

    public Long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Long goods_id) {
        this.goods_id = goods_id;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Long getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(Long unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public float getCnt() {
        return cnt;
    }

    public void setCnt(float cnt) {
        this.cnt = cnt;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice_with_tax() {
        return price_with_tax;
    }

    public void setPrice_with_tax(float price_with_tax) {
        this.price_with_tax = price_with_tax;
    }

    public float getPrice_base() {
        return price_base;
    }

    public void setPrice_base(float price_base) {
        this.price_base = price_base;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getRow_sum() {
        return row_sum;
    }

    public void setRow_sum(float row_sum) {
        this.row_sum = row_sum;
    }

    public float getSum_tax() {
        return sum_tax;
    }

    public void setSum_tax(float sum_tax) {
        this.sum_tax = sum_tax;
    }

    public float getSum_with_tax() {
        return sum_with_tax;
    }

    public void setSum_with_tax(float sum_with_tax) {
        this.sum_with_tax = sum_with_tax;
    }

    public float getSum_base() {
        return sum_base;
    }

    public void setSum_base(float sum_base) {
        this.sum_base = sum_base;
    }

    public float getSum_disc() {
        return sum_disc;
    }

    public void setSum_disc(float sum_disc) {
        this.sum_disc = sum_disc;
    }

    public float getOut_price() {
        return out_price;
    }

    public void setOut_price(float out_price) {
        this.out_price = out_price;
    }

    public float getSum_sum() {
        return sum_sum;
    }

    public void setSum_sum(float sum_sum) {
        this.sum_sum = sum_sum;
    }

    public float getCnt_fact() {
        return cnt_fact;
    }

    public void setCnt_fact(float cnt_fact) {
        this.cnt_fact = cnt_fact;
    }

    public Long getId() {
        return id;
    }

    public Long getGrp_id() {
        return grp_id;
    }
}
