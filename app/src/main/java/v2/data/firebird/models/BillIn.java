package v2.data.firebird.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MaestroVS on 28.05.2020.
 */

public class BillIn extends Document {



    @SerializedName("is_tax_in")
    @Expose
    int is_tax_in=0;


    public BillIn() {

    }

    /**
     * @param id
     * @param num
     * @param dateTime
     * @param subdivisionId
     * @param subdivisionName
     * @param doc_state
     */
    public BillIn(Long id, String num, String dateTime, Long subdivisionId, String subdivisionName, int doc_state) {
        this.id = id;
        this.num = num;
        this.dateTime = dateTime;
        this.subdivisionId = subdivisionId;
        this.subdivisionName = subdivisionName;
        this.doc_state = doc_state;
    }


}


