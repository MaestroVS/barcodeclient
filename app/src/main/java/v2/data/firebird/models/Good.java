package v2.data.firebird.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Good extends Entity {



    @SerializedName("article")
    @Expose
    private String article;

    @SerializedName("unit_id")
    @Expose
    Long unit_id;

    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("barcode")
    @Expose
    private String barcode;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("cnt_fact")
    @Expose
    private float cnt_fact;



    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getName() {
        return name;
    }


    public Long getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(Long unit_id) {
        this.unit_id = unit_id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getUnit() {
        return unit;
    }

    public float getPrice() {
        return price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getCnt_fact() {
        return cnt_fact;
    }

    public void setCnt_fact(float cnt_fact) {
        this.cnt_fact = cnt_fact;
    }


}
