package v2.data.firebird.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class Entity {

    @SerializedName("id")
    @Expose
    Long id;

    @SerializedName("grp_id")
    @Expose
    Long grp_id;


    @SerializedName("name")
    @Expose
    String name;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGrp_id() {
        return grp_id;
    }

    public void setGrp_id(Long grp_id) {
        this.grp_id = grp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
