package v2.data.firebird.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class Document {

    @SerializedName("id")
    @Expose
    Long id;
    @SerializedName("num")
    @Expose
    String num;
    @SerializedName("date_time")
    @Expose
    String dateTime;
    @SerializedName("subdivision_id")
    @Expose
    Long subdivisionId;
    @SerializedName("subdivision_name")
    @Expose
    String subdivisionName;
    @SerializedName("doc_state")
    @Expose
    int doc_state=0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Long getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Long subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    public String getSubdivisionName() {
        return subdivisionName;
    }

    public void setSubdivisionName(String subdivisionName) {
        this.subdivisionName = subdivisionName;
    }

    public int getDoc_state() {
        return doc_state;
    }

    public void setDoc_state(int doc_state) {
        this.doc_state = doc_state;
    }


    @NonNull
    @Override
    public String toString() {
        return "[ id="+id+" num="+num+" subdivision_id= "+subdivisionId+" ]";
    }
}
