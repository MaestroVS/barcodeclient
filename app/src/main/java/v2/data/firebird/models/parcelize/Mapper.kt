package v2.data.firebird.models.parcelize

import v2.data.firebird.models.GoodDoc

fun firebirdGoodDocToParcelize(good: GoodDoc) =
    v2.data.firebird.models.parcelize.GoodDoc(
        good.id, good.grp_id, good.goods_id,
        good.article, good.goods_name,
        good.unit_id, good.unit_name,
        good.cnt, good.cnt_fact,
        good.price, good.price_with_tax,
        good.price_base, good.tax,
        good.row_sum, good.sum_tax,
        good.sum_with_tax, good.sum_base,
        good.sum_disc, good.out_price,
        good.sum_sum
    )