package v2.data.firebird.models;

/**
 * Created by MaestroVS on 28.05.2020.
 */

public class WriteOff extends Document {


    public WriteOff() {

    }

    /**
     * @param id
     * @param num
     * @param dateTime
     * @param subdivisionId
     * @param subdivisionName
     * @param doc_state
     */
    public WriteOff(Long id, String num, String dateTime, Long subdivisionId, String subdivisionName, int doc_state) {
        this.id = id;
        this.num = num;
        this.dateTime = dateTime;
        this.subdivisionId = subdivisionId;
        this.subdivisionName = subdivisionName;
        this.doc_state = doc_state;
    }


}


