package v2.data.firebird.models.parcelize

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoodDoc(val id: Long, val hd_id: Long, val goods_id: Long,
                   val article: String, val goods_name: String,
                   val unit_id: Long, val unit_name: String,
                   val cnt: Float, val cnt_fact: Float,
                   val price: Float, val price_with_tax: Float,
                   val price_base: Float, val tax: Float,
                   val row_sum: Float, val sum_tax: Float,
                   val sum_with_tax: Float, val sum_base: Float,
                   val sum_disc: Float, val out_price: Float,
                   val sum_sum: Float): Parcelable
