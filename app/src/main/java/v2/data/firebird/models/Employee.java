package v2.data.firebird.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Employee extends Entity {

    @SerializedName("code_name")
    @Expose
    String code_name;

    @SerializedName("surname")
    @Expose
    String surname;

    @SerializedName("sec_name")
    @Expose
    String sec_name;

    @SerializedName("pin")
    @Expose
    String pin;

    @SerializedName("user_id")
    @Expose
    Long user_id;

    @SerializedName("job_title_id")
    @Expose
    Long job_title_id;

    @SerializedName("job_title")
    @Expose
    String job_title;

    public String getCode_name() {
        return code_name;
    }

    public void setCode_name(String code_name) {
        this.code_name = code_name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSec_name() {
        return sec_name;
    }

    public void setSec_name(String sec_name) {
        this.sec_name = sec_name;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getJob_title_id() {
        return job_title_id;
    }

    public void setJob_title_id(Long job_title_id) {
        this.job_title_id = job_title_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }
}
