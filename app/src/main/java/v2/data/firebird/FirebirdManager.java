package v2.data.firebird;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;

import static v1.excel.AndroidReadExcelActivity.my;

public class FirebirdManager {

    private FirebirdConnectionProvider fbConn;

    public FirebirdManager(FirebirdConnectionProvider jdbcConnection) {
        this.fbConn = jdbcConnection;
    }

    public JSONArray executeStatement(String statement) throws Exception {
        JSONArray jsonArray = null;

        if (fbConn.get() == null) {
            String msg = "Error in connection with SQL server";
            Log.d("my", "query statement conn err " + msg);
            return jsonArray;
        }


        Statement st = fbConn.get().createStatement();
        ResultSet rs = st.executeQuery(statement);
        ResultSetMetaData rsmd = rs.getMetaData();
        int cols = rsmd.getColumnCount();
        Properties connInfo = new Properties();
        connInfo.put("charSet", "UNICODE_FSS");


        jsonArray = convertToJSON(rs);


        rs.close();
        st.close();


        return jsonArray;
    }


    public int executeProcedure(String statement) throws Exception {
        int result = -1;
        Log.d(my, "executeProcedure  " + statement);

        if (fbConn.get() == null) {
            String msg = "Error in connection with SQL server";
            Log.d("my", "query statement conn err " + msg);
            return -1;
        }

        Statement st = fbConn.get().createStatement();

        result = st.executeUpdate(statement);


        st.close();

        return result;
    }


    private JSONArray convertToJSON(ResultSet resultSet)
            throws Exception {
        JSONArray jsonArray = new JSONArray();

        Log.d("resultSet","resultSet.isClosed() = "+resultSet.isClosed());

        if(!resultSet.isClosed()) {
            Log.d("resultSet","resultSet.isClosed() ++++ ");
            while (resultSet.next()) {
                int total_rows = resultSet.getMetaData().getColumnCount();
                JSONObject obj = new JSONObject();
                for (int i = 0; i < total_rows; i++) {
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1));
                }
                jsonArray.put(obj);
            }
        }
        return jsonArray;
    }
}
