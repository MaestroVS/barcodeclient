package v2.data.firebird

import v2.data.database.tables.Connect
import java.io.IOException
import java.sql.Connection
import java.sql.DriverManager
import java.util.*
import java.util.concurrent.Callable

class FirebirdConnectionProvider {

    private var fbConnection: Connection? = null


    @Throws(IOException::class)
    fun createConnection(connectionProps: Connect) : Callable<Connection?> {
        return Callable {
            val paramConnection = Properties()
            paramConnection.setProperty("user", connectionProps.user)
            paramConnection.setProperty("password", connectionProps.password)
            paramConnection.setProperty("encoding", connectionProps.encoding)

            val sCon = ("jdbc:firebirdsql:" + connectionProps.ip +
                    "/" + connectionProps.port + ":"
                    + connectionProps.path)//E:/Unisystem/DB/DB.FDB";

                // register Driver
                Class.forName("org.firebirdsql.jdbc.FBDriver")
                // Get connection
                fbConnection = DriverManager.getConnection(sCon, paramConnection)


            fbConnection

        }
    }

    public fun get(): Connection? {
        return fbConnection
    }
}