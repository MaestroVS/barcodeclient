package v2.data.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import v2.data.database.dao.*
import v2.data.database.tables.*


@Database(entities = [Company::class, Connect::class, Subdivision::class, User::class, DicGroup::class,
    DicGood::class, DicUnit::class],
        version = 3, exportSchema = false)  // Перечисляем в entities, какие классы будут использоваться для создания таблиц.
abstract class InvDatabase : RoomDatabase() {

    abstract fun companyDAO(): CompanyDao

    abstract fun connectionDAO(): ConnectionDao

    abstract fun userDAO(): UsersModelDao           // Описываем абстрактные методы для получения объектов интерфейса BorrowModelDao, которые вам понадобятся

    abstract fun groupDAO(): GroupDao

    abstract fun goodDAO(): GoodDao

    abstract fun unitDAO(): UnitDao


    // Сопутствующий объект для получения базы данных (фактически синглтон). Можно не использовать.
    companion object {

        private var INSTANCE: InvDatabase? = null

        fun getDatabase(context: Context): InvDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder<InvDatabase>(context, InvDatabase::class.java, "inventory_db")
                        //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                        .build()
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        //http://dolbodub.blogspot.com/2018/03/4-room.html
        // Апдейт базы с 1 на 2 версию, добавление поля
       /* val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ReadoutTable ADD COLUMN comment TEXT NOT NULL DEFAULT ''")
            }
        }

        // Апдейт базы с 2 на 3 версию, переделка всех полей из NOT NULL в NULL
        val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ReadoutTable RENAME TO ReadoutTable_old")
                database.execSQL("CREATE TABLE ReadoutTable (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT DEFAULT 0, addr TEXT NULL, hvs TEXT NULL, gvs TEXT NULL, t1 TEXT NULL, t2 TEXT NULL, t3 TEXT NULL, comment TEXT NULL, curDate INTEGER)")
                database.execSQL("INSERT INTO ReadoutTable(id, addr, hvs, gvs, t1, t2, t3, comment, curDate) SELECT id, addr, hvs, gvs, t1, t2, t3, comment, curDate FROM ReadoutTable_old")
                database.execSQL("DROP TABLE ReadoutTable_old")
            }
        }*/
    }
}