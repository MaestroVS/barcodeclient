package v2.data.database.dao

import androidx.room.*
import v2.data.database.tables.Subdivision

@Dao
interface SubdivisionsModelDao {


    @Query("select * from Subdivision where id = :id")
    fun getSubdivisionById(id: Long): Subdivision

    @Query("select * from Subdivision where name = :name")
    fun getSubdivisionByName(name: String): List<Subdivision>


    @Query("select * from Subdivision where company_id = :companyId")
    fun getSubdivisionsByCompanyId(companyId: Long): List<Subdivision>




}