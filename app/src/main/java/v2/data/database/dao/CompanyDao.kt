package v2.data.database.dao

import androidx.room.*
import io.reactivex.Maybe
import v2.data.database.tables.Company

@Dao
interface CompanyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: Company): Maybe<Long>

    @Query("select * from Company ")
    fun getAll(): Maybe<List<Company>>


    @Query("select * from Company where id = :id")
    fun getById(id: Long): Company

    @Query("select * from Company where name = :name")
    fun getByName(name: String): Company

    @Delete
    fun delete(obj: Company)


}