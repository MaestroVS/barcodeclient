package v2.data.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

interface BaseDao<T> {


    @Insert
    fun insert(obj: T)

    @Update
    fun update(userModel: T)

    @Delete
    fun delete(userModel: T)


}

