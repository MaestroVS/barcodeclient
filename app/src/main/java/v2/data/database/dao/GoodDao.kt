package v2.data.database.dao

import androidx.room.*
import io.reactivex.Maybe
import v2.data.database.tables.DicGood


@Dao
interface GoodDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: DicGood): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(order: List<DicGood>): Maybe<List<Long>>

    @Query("select * from DicGood ")
    fun getAll(): Maybe<List<DicGood>>


    @Query("select * from DicGood where id = :id")
    fun getById(id: Long): DicGood

    @Query("select * from DicGood where name = :name")
    fun getByName(name: String): DicGood

   // @Query("select * from DicGroup where parent_id = :grpId")
    //fun getByParentId(grpId: String): DicGroup

    @Delete
    fun delete(obj: DicGood)


}