package v2.data.database.dao

import androidx.room.*
import io.reactivex.Maybe
import v2.data.database.tables.DicGroup


@Dao
interface GroupDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: DicGroup): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(order: List<DicGroup>): Maybe<List<Long>>

    @Query("select * from DicGroup ")
    fun getAll(): Maybe<List<DicGroup>>


    @Query("select * from DicGroup where id = :id")
    fun getById(id: Long): DicGroup

    @Query("select * from DicGroup where name = :name")
    fun getByName(name: String): DicGroup

    @Query("select * from DicGroup where parent_id = :grpId")
    fun getByParentId(grpId: String): DicGroup

    @Delete
    fun delete(obj: DicGroup)


}