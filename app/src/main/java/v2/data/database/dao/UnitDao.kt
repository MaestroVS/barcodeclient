package v2.data.database.dao

import androidx.room.*
import io.reactivex.Maybe
import v2.data.database.tables.DicUnit


@Dao
interface UnitDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: DicUnit): Maybe<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(order: List<DicUnit>): Maybe<List<Long>>

    @Query("select * from DicUnit where company_id = :companyId")
    fun getAll(companyId: Long): Maybe<List<DicUnit>>


    @Query("select * from DicUnit where id = :id and company_id = :companyId")
    fun getById(id: Long, companyId: Long): DicUnit

    @Query("select * from DicUnit where name = :name and company_id = :companyId")
    fun getByName(name: String, companyId: Long): DicUnit

    @Delete
    fun delete(obj: DicUnit)


}