package v2.data.database.dao

import androidx.room.*
import io.reactivex.Maybe
import v2.data.database.tables.Connect

@Dao
interface ConnectionDao  {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: Connect): Maybe<Long>

    @Query("select * from Connect ")
    fun getAll(): List<Connect>


    @Query("select * from Connect where company_id = :companyId")
    fun getConnectByCompanyId(companyId: Long): Maybe<Connect>

   // @Query("select * from Connect where name = :name")
   // fun getByName(name: String): Connect

    @Delete
    fun delete(obj: Connect)








}