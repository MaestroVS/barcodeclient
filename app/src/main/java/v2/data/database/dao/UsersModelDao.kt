package v2.data.database.dao

import androidx.room.*
import v2.data.database.tables.User

@Dao
interface UsersModelDao {



    @Query("select * from User where id = :id")
    fun getUserById(id: Long): User

    @Query("select * from User where name = :name")
    fun getUserByName(name: String): User


    @Query("select * from User where company_id = :companyId")
    fun getUsersByCompanyId(companyId: Long): List<User>




}