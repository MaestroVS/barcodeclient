package v2.data.database.tables

import androidx.room.*

@Entity(
        foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = ForeignKey.CASCADE)])
class JorInventory (

    @PrimaryKey(autoGenerate = false) var id: Long = 0,

    @ColumnInfo
    var grp_id: Long = 0,

    @ColumnInfo
    var date_time: String,

    @ColumnInfo
    var num: String,

    @ColumnInfo
    var doc_state: Int = 0,

    @ColumnInfo
    var subdivision_id: Long = 0,

    @ColumnInfo
    var employee_id: Long = 0,

    @ColumnInfo
    var descr: String?,

    @ColumnInfo
    var company_id: Long

){


}