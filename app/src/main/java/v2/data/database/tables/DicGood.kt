package v2.data.database.tables

import androidx.room.*

@Entity(
        /*foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = ForeignKey.CASCADE)]*/)
class DicGood (

    @PrimaryKey(autoGenerate = false) var id: Long = 0,

    @ColumnInfo
    var grp_id: Long = 0,

    @ColumnInfo
    var name: String,

    @ColumnInfo
    var article: String,

    @ColumnInfo
    var unit_id: Long = 0,

    @ColumnInfo
    var unit: String,

    @ColumnInfo
    var barcode: String?,

    @ColumnInfo
    var price: Float = 0f,

    @ColumnInfo
    var company_id: Long

){


}