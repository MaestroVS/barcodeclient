package v2.data.database.tables

import androidx.room.*
import androidx.room.ForeignKey.Companion.CASCADE


@Entity(indices = arrayOf(Index(value = ["company_id"], unique = true)),
        foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = CASCADE)])
class Connect(


        @ColumnInfo
        var ip: String,

        @ColumnInfo
        var path: String,

        @ColumnInfo
        var port: String = "3050",

        @ColumnInfo
        var user: String = "SYSDBA",

        @ColumnInfo
        var password: String = "password",

        @ColumnInfo
        var encoding: String = "encoding",

        @ColumnInfo
        var company_id: Long
) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0 // Long - без вопросительного знака, значит поле NOT NULL

    override fun toString(): String {
        val _ip = if (ip.isEmpty()) "" else ip
        val _path = if (path.isEmpty()) "" else path
        return _ip + ":" + _path
    }
}