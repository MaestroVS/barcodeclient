package v2.data.database.tables

import androidx.room.*


@Entity(
        foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = ForeignKey.CASCADE)])
class User (
    @ColumnInfo
    var name: String,

    @ColumnInfo
    var avatar: String,

    @ColumnInfo
    var pincode: String?,

    @ColumnInfo
    var role: Int = 0, // 0 - admin , 1 - kassir

    @ColumnInfo(name = "company_id", index = true)
    var company_id: Long,

    @ColumnInfo
    var is_active: Boolean
){

    @PrimaryKey(autoGenerate = true) var id: Long = 0 // Long - без вопросительного знака, значит поле NOT NULL
}