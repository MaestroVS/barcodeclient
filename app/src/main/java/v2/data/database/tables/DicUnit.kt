package v2.data.database.tables

import androidx.room.*

@Entity(
        foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = ForeignKey.CASCADE)])
class DicUnit (

    @PrimaryKey(autoGenerate = false,) var id: Long = 0,

    @ColumnInfo
    var grp_id: Long = 0,

    @ColumnInfo
    var name: String,

    @ColumnInfo
    var full_name: String,

    @ColumnInfo(name = "company_id", index = true)
    var company_id: Long


){


}