package v2.data.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.Companion.CASCADE
import androidx.room.PrimaryKey



@Entity(
        foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = CASCADE)])
class Subdivision(


        @ColumnInfo
        var name: String,

        @ColumnInfo(name = "company_id", index = true)
        var company_id: Long
) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0 // Long - без вопросительного знака, значит поле NOT NULL
}