package v2.data.database.tables

import androidx.room.*

@Entity(
       /* foreignKeys = [
            ForeignKey(entity = Company::class,
                    parentColumns = ["id"],
                    childColumns = ["company_id"],
                    onDelete = ForeignKey.CASCADE)]*/)
class DicGroup (

    @PrimaryKey(autoGenerate = false) var id: Long = 0,

    @ColumnInfo
    var parent_id: Long = 0,

    @ColumnInfo
    var name: String,

    @ColumnInfo
    var company_id: Long

){


}