package v2.domain

enum class RepositorySource {
     firebird_db , local_db
}