package v2.domain.repositories.doc_repositories

import android.util.Log
import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.firebird.datasources.InventorySource
import v2.data.firebird.models.Document
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import v2.domain.repositories.StateLiveData
import javax.inject.Inject

class InventoryRepository private constructor() : DocumentsRepository {

    companion object {
        val instance: InventoryRepository by lazy { InventoryRepository() }
    }


    @Inject
    lateinit var source: InventorySource


    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }


    override fun getList(docState: DocState, docsSort: DocsSort, subdivisionId: Long, success: (List<Document>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.list(docState, docsSort, subdivisionId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }

    override fun getItem(id: Long, success: (Document) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.item(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    override fun insertOrModifyRow(inventoryId: Long, goodId: Long, count: Float, success: (Int) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.insertOrModifyRow(inventoryId, goodId, count))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if(it == 0) {
                        success(it)
                    }else{
                        error(it)
                    }}
                .onErrorReturn { error(it) }
                .subscribe()
    }

    override fun insertOrModifyRowBill(billInId: Long, goodId: Long, unitId: Long,count: Float, rowSum: Float?, rowSumWithTax: Float?, priceWithTax: Float?, price: Float?, success: (Int) -> Unit, error: (Throwable) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun listOfGoods(hdId: Long): StateLiveData<List<GoodDoc>> {
        val mutableLiveData = StateLiveData<List<GoodDoc>>()
        Observable.fromCallable(source.listOfGoods(hdId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    Log.d("my", "MAP POST SUCCESS ${it.size}")
                    mutableLiveData.postSuccess(it)
                }
                .onErrorReturn {
                    mutableLiveData.postError(it)
                }
                .subscribe()
        return mutableLiveData
    }

    /**
     * Перевірка, чи заповнений пероблік
     */
    fun isInventoryReady(hdId: Long, success: (Boolean) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.isInventoryReady(hdId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }
}


