package v2.domain.repositories.doc_repositories

import android.util.Log
import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.firebird.datasources.BillInSource
import v2.data.firebird.models.Document
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import v2.domain.repositories.StateLiveData
import javax.inject.Inject

class BillInRepository private constructor() : DocumentsRepository {


    companion object {
        val instance: BillInRepository by lazy { BillInRepository() }
    }


    @Inject
    lateinit var source: BillInSource


    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }


    override fun getList(docState: DocState, docsSort: DocsSort, subdivisionId: Long, success: (List<Document>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.list(docState, docsSort, subdivisionId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }

    override fun getItem(id: Long, success: (Document) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.item(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    override fun insertOrModifyRow(inventoryId: Long, goodId: Long, count: Float, success: (Int) -> Unit, error: (Throwable) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun insertOrModifyRowBill(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?,
                                       rowSumWithTax: Float?, priceWithTax: Float?, price: Float?,
                                       success: (Int) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.insertOrModifyRow(billInId, goodId, unitId, count, rowSum, rowSumWithTax, priceWithTax, price))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()
    }

    override fun listOfGoods(hdId: Long): StateLiveData<List<GoodDoc>> {
        val mutableLiveData = StateLiveData<List<GoodDoc>>()
        Observable.fromCallable(source.listOfGoods(hdId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    Log.d("my", "MAP POST SUCCESS ${it.size}")
                    mutableLiveData.postSuccess(it)
                }
                .onErrorReturn {
                    mutableLiveData.postError(it)
                }
                .subscribe()
        return mutableLiveData
    }

    override fun updateRowBill(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?,
                                       rowSumWithTax: Float?, priceWithTax: Float?, price: Float?, id: Long,
                                       success: (Int) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.updateRow(billInId, goodId, unitId, count, rowSum, rowSumWithTax, priceWithTax, price, id))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { success(it) }
            .onErrorReturn { error(it) }
            .subscribe()
    }

    override fun deleteRow(dtId: Long, success: (Int) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.deleteRow(dtId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { success(it) }
            .onErrorReturn { error(it) }
            .subscribe()
    }


}