package v2.domain.repositories.doc_repositories

import v2.data.firebird.models.Document
import v2.data.firebird.models.GoodDoc
import v2.helpers.filters.DocState
import v2.helpers.filters.DocsSort
import v2.domain.repositories.StateLiveData

interface DocumentsRepository {

    fun getList(docState: DocState, docsSort: DocsSort, subdivisionId: Long, success: (List<Document>) -> Unit, error: (Throwable) -> Unit)

    fun getItem(id: Long, success: (Document) -> Unit, error: (Throwable) -> Unit)

    fun insertOrModifyRow(inventoryId: Long, goodId: Long, count: Float, success: (Int) -> Unit, error: (Throwable) -> Unit)

    fun insertOrModifyRowBill(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?,
                              rowSumWithTax: Float?, priceWithTax: Float?, price: Float?,
                              success: (Int) -> Unit, error: (Throwable) -> Unit)

    fun listOfGoods(hdId: Long): StateLiveData<List<GoodDoc>>

    //fun insertDocumentHD(id: Long, success: (Document) -> Unit, error: (Throwable) -> Unit)

    fun updateRowBill(billInId: Long, goodId: Long, unitId: Long, count: Float, rowSum: Float?,
                              rowSumWithTax: Float?, priceWithTax: Float?, price: Float?, id: Long,
                              success: (Int) -> Unit, error: (Throwable) -> Unit){

    }

    fun deleteRow(dtId: Long,  success: (Int) -> Unit, error: (Throwable) -> Unit){

    }

}