package v2.domain.repositories

import android.util.Log
import com.app.barcodeclient3.MainApplication
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import v2.AndroidApplication
import v2.data.database.dao.GoodDao
import v2.data.firebird.datasources.GoodsSource
import v2.data.firebird.models.Good
import v2.helpers.filters.EntitySort
import v2.helpers.filters.GoodSearchFilter
import v2.helpers.filters.PageFilter
import javax.inject.Inject
import v2.data.database.dao.GroupDao
import v2.data.database.dao.UnitDao
import v2.data.database.tables.DicGroup
import v2.domain.RepositorySource
import v2.domain.SharedCurrentState
import v2.helpers.filters.DocFilter
import v2.mappers.UnitsMapper


class GoodsRepository private constructor() {

    companion object {
        val instance: GoodsRepository by lazy { GoodsRepository() }
    }


    @Inject
    lateinit var source: GoodsSource

    @Inject
    lateinit var dicEntityDAO: GroupDao

    @Inject
    lateinit var goodDao: GoodDao

    @Inject
    lateinit var unitDao: UnitDao

    init {
        AndroidApplication.app()!!.appComponent()!!.inject(this)
    }

    /**
     * return items count
     */
    fun getGoodsListAndCacheRoom(companyId: Long, success: (Int) -> Unit, errorResult: (Throwable) -> Unit) {
        Observable.fromCallable(source.goodsList(null, null, null, null, null))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { goods ->
                    Log.d("my", "Goods success from database count= ${goods.size}")
                    val totalCnt: Int = goods.size

                    val goodsList = mutableListOf<v2.data.database.tables.DicGood>()
                    goods.map {
                        goodsList.add(v2.data.database.tables.DicGood(it.id, it.grp_id, it.name, it.article, it.unit_id, it.unit, it.barcode, it.price,
                                companyId))
                    }

                    goodDao.insertAll(goodsList)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map {
                                Log.d("my", "ReadFromDb $it")
                                success(totalCnt)
                            }
                            .onErrorReturn {
                                Log.d("my", "it err: ${it.localizedMessage}")
                                error(it)
                            }
                            .subscribe()


                }
                .onErrorReturn {
                    Log.d("my", "Good error from database count= ${it.localizedMessage}")
                    error(it)
                }
                .subscribe()

    }


    /**
     * return items count
     */
    fun getGoodsGrpListAndCacheRoom(companyId: Long, success: (Int) -> Unit, errorResult: (Throwable) -> Unit) {
        Observable.fromCallable(source.grpList())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    Log.d("my", "Grp succes from database count= ${it.size}")
                    val totalCnt: Int = it.size

                    val dicGroupList = mutableListOf<DicGroup>()
                    it.map {
                        dicGroupList.add(DicGroup(it.id, it.parent_id, it.name, companyId))
                    }

                    dicGroupList.sortBy { it.id }



                    dicEntityDAO.insertAll(dicGroupList)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map {
                                Log.d("my", "ReadFromDb $it")
                                success(totalCnt)
                            }
                            .onErrorReturn {
                                Log.d("my", "it err: ${it.localizedMessage}")
                                error(it)
                            }
                            .subscribe()


                }
                .onErrorReturn {
                    Log.d("my", "Grp error from database count= ${it.localizedMessage}")
                    error(it)
                }
                .subscribe()

    }


    fun getGoodsList(goodSearchFilter: GoodSearchFilter?, grpId: Long?, goodSort: EntitySort?, docFilter: DocFilter?,
                     pageFilter: PageFilter?, success: (List<Good>) -> Unit, error: (Throwable) -> Unit) {
        Observable.fromCallable(source.goodsList(goodSearchFilter, grpId, goodSort, docFilter, pageFilter))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { success(it) }
                .onErrorReturn { error(it) }
                .subscribe()

    }


    private val mutableLiveData = StateLiveData<List<Good>>()

    fun getGoodsListMutableLiveData(goodSearchFilter: GoodSearchFilter?, grpId: Long?, goodSort: EntitySort?, docFilter: DocFilter?,
                                    pageFilter: PageFilter?): StateLiveData<List<Good>> {
        Observable.fromCallable(source.goodsList(goodSearchFilter, grpId, goodSort, docFilter, pageFilter))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    Log.d("my", "MAP POST SUCCESS ${it.size}")
                    mutableLiveData.postSuccess(it)
                }
                .onErrorReturn {
                    mutableLiveData.postError(it)
                }
                .subscribe()
        return mutableLiveData
    }


    fun getUnitsList(companyId: Long,success: (List<v2.presentation.models.Unit>) -> Unit, error: (Throwable) -> Unit) {
        when (SharedCurrentState.repositorySource) {
            RepositorySource.firebird_db -> {
                Observable.fromCallable(source.unitsList())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map {
                            success(UnitsMapper.fromFirebirdDBAll(it))
                        }
                        .onErrorReturn { error(it) }
                        .subscribe()
            }

            RepositorySource.local_db -> {
                unitDao.getAll(companyId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map {
                            success(UnitsMapper.fromRoomDBAll(it))
                        }
                        .onErrorReturn { error(it) }
                        .subscribe()
            }
        }


    }


}