package v2.domain

import v2.presentation.screens.connection.StatusConnect

object SharedCurrentState {

    var companyId: Long = 0

    var repositorySource: RepositorySource = RepositorySource.firebird_db
}