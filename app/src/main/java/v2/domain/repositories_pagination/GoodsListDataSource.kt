package v2.domain.repositories_pagination

import android.util.Log
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import v2.data.firebird.models.Good
import v2.helpers.filters.PageFilter
import v2.domain.repositories.GoodsRepository
import kotlin.coroutines.CoroutineContext

class GoodsListDataSource(coroutineContext: CoroutineContext, val loadCallback: GoodsListDataSource.Callback?) :
        PageKeyedDataSource<String, Good>() {

    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)


    var offset = 1
    var limit = 40
    var currentPage = 1

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, Good>) {
        scope.launch {


            Log.d("my", "loadInitial")
            GoodsRepository.instance.getGoodsList(null, null, null, null, PageFilter(offset, limit),
                    { list ->
                        Log.d("my", "loadInitial ok ${list.size}")
                        offset += if (list.size >= limit) {
                            limit
                        } else {
                            list.size
                        }
                        if (list.isNotEmpty()) currentPage++

                        callback.onResult(list, (currentPage - 1).toString(), (currentPage + 1).toString())
                        launch(Dispatchers.Main) { loadCallback?.onLoad() }

                    },
                    { throwable ->
                        Log.d("my", "loadInitial throwable $throwable")
                    })
        }

    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, Good>) {
        scope.launch {
            GoodsRepository.instance.getGoodsList(null, null, null, null, PageFilter(offset, limit),
                    { list ->
                        offset += if (list.size >= limit) {
                            limit
                        } else {
                            list.size
                        }
                        if (list.isNotEmpty()) currentPage++
                        callback.onResult(list
                                ?: listOf(), (currentPage + 1).toString())
                        //launch(Dispatchers.Main) { Log.d("my","loadAfter 3")
                        //  loadCallback?.onLoad() }
                        loadCallback?.onLoad()

                    },
                    { throwable ->

                    })

        }

    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, Good>) {
        scope.launch {
            GoodsRepository.instance.getGoodsList(null, null, null, null, PageFilter(offset, limit),
                    { list ->

                    },
                    { throwable ->

                    })
        }

    }


    override fun invalidate() {
        super.invalidate()
        job.cancel()
    }

    interface Callback {
        fun onLoad()
    }
}