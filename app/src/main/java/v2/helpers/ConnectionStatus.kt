package v2.helpers

enum class ConnectionStatus {

    none, not_connected , connected
}