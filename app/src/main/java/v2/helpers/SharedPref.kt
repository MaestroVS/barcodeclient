package v2.helpers

import android.content.Context
import android.content.SharedPreferences
import android.app.Activity
import android.util.Log


object SharedPref {
    private var mSharedPref: SharedPreferences? = null

    val RESTORE_EMAIL = "RESTORE_EMAIL"
    val OPTION_SEND_CRASH_REPORT = "SEND_CRASH_REPORT"
    val OPTION_NEED_APP_UPDATE = "OPTION_NEED_APP_UPDATE"
    val LAST_COMPANY_ID = "LAST_COMPANY_ID"

    val LICENSE_CHECK_DATE = "LICENSE_CHECK_DATE"
    val LICENSE_IS_ACTIVE = "LICENSE_IS_ACTIVE"

    val LAUNCH_MODE_SCREEN = "LAUNCH_MODE_SCREEN"



    public fun init(context: Context) {
        if (mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.packageName, Activity.MODE_PRIVATE)
    }

    fun readString(key: String, defValue: String): String? {
        return try {
            mSharedPref!!.getString(key, defValue)
        }catch (e: Exception){
            defValue
        }
    }

    fun writeString(key: String, value: String) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putString(key, value)
        prefsEditor.commit()
    }

    fun readBool(key: String, defValue: Boolean): Boolean {
        return mSharedPref?.getBoolean(key, defValue)?:false
    }

    fun writeBool(key: String, value: Boolean) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putBoolean(key, value)
        prefsEditor.commit()
    }

    fun readInt(key: String, defValue: Int): Int {
        return mSharedPref!!.getInt(key, defValue)
    }

    fun writeInt(key: String, value: Int?) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putInt(key, value!!).commit()
    }


}



