package v2.helpers.filters

enum class DocState {

    ALL, OPENED, CLOSED
}