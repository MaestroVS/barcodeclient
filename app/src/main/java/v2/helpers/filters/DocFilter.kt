package v2.helpers.filters

class DocFilter constructor(val docType: DocType, val docId: Long){

    fun getFilterConditionPref():String{
        var condition =""
        var docIdStr = docId.toString()
        var docTable =
        when(docType){
            DocType.INVENTORY -> "jor_inventory_act_dt"
            DocType.BILL_IN -> "jor_bill_in_dt"
            DocType.BILL_OUT -> "jor_bill_out_dt"
            DocType.WRITE_OFF -> "jor_write_off_dt"
        }

        condition += ",   (select sum(jad.cnt_fact) from "+docTable+" jad "+
        " where jad.hd_id = ${docIdStr.trim { it <= ' ' }} and jad.goods_id = DG.ID  order by JAD.id desc) as CNT_FACT "

        return condition
    }

}