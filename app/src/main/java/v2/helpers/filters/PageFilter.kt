package v2.helpers.filters

class PageFilter constructor(val offset: Int, val limit: Int){

    fun getPageString(): String {

        return " first $limit skip $offset "
    }
}