package v2.helpers.filters

enum class EntitySort {

    NAME, ASC, DESC
}