package v2.helpers.filters

enum class DocType {

     INVENTORY,BILL_IN, BILL_OUT, WRITE_OFF;

     companion object {
          fun getDocType(type: String) : DocType {
               return when(type){
                    INVENTORY.name -> INVENTORY
                    BILL_IN.name -> BILL_IN
                    BILL_OUT.name -> BILL_OUT
                    WRITE_OFF.name -> WRITE_OFF
                    else -> BILL_IN
               }

          }
     }


}