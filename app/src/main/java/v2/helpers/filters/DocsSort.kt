package v2.helpers.filters

enum class DocsSort {

    ASC, DESC
}