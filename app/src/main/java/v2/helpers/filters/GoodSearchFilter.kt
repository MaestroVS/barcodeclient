package v2.helpers.filters

class GoodSearchFilter constructor(val name: String?, val article: String?, val barcode: String?, private val strongFilter: Boolean = false) {

    fun getConditionString(): String {

        var res = " "

        if (name == null && article == null && barcode == null) return res

        val containing = if (strongFilter) " = " else " LIKE "

        var or = false

        if (name != null) {
            res += " LOWER(dg.name) $containing LOWER('%${name.trim().replace("'", "''")}%') "
            or = true
        }
        if (article != null) {
            if (or) res += " or "
            res += " dg.article = '${article.trim().replace("'", "''")}' "
            or = true
        }
        if (barcode != null) {
            if (or) res += " or "
            res += " dg.id = ( select dgb.goods_id  from dic_goods_barcodes dgb where dgb.barcode = '${barcode.trim().replace("'", "''")}' ) "
        }

        return res
    }

}