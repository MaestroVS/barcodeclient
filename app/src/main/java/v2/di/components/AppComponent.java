package v2.di.components;

import javax.inject.Singleton;

import dagger.Component;
import v2.di.modules.AppModule;
import v2.di.modules.DatabaseModule;
import v2.di.modules.FirebirdModule;
import v2.presentation.screens.company.CompanyModel;
import v2.presentation.screens.company.create.CreateCompanyActivity;
import v2.presentation.screens.connection.ConnectionModel;
import v2.presentation.screens.connection.create.CreateConnectionActivity;
import v2.domain.repositories.doc_repositories.BillInRepository;
import v2.domain.repositories.doc_repositories.BillOutRepository;
import v2.domain.repositories.doc_repositories.InventoryRepository;
import v2.domain.repositories.doc_repositories.WriteOffRepository;
import v2.domain.repositories.GoodsRepository;
import v2.presentation.screens.dashboard.SyncDataViewModel;
import v2.presentation.screens.main.MainActivity;
import v2.presentation.screens.main.SecondActivity;
import v2.presentation.screens.repo.EmployeeRepository;
import v2.presentation.screens.repo.OrganizationRepository;
import v2.presentation.screens.repo.SubdivisionRepository;

@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, FirebirdModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void inject(SecondActivity secondActivity);

    void inject(CreateConnectionActivity createConnectionActivity);

    void inject(CreateCompanyActivity createCompanyActivity);

    void inject(CompanyModel companyModel);

    void inject(ConnectionModel companyModel);

    void inject(InventoryRepository inventoryRepository);

    void inject(BillInRepository billInModel);

    void inject(BillOutRepository billOutRepository);

    void inject(WriteOffRepository writeOffRepository);

    void inject(OrganizationRepository organizationRepository);

    void inject(EmployeeRepository employeeRepository);

    void inject(SubdivisionRepository subdivisionRepository);

    void inject(GoodsRepository goodsRepository);

    void inject(SyncDataViewModel syncDataViewModel);





}