package v2.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import v2.data.firebird.FirebirdConnectionProvider;
import v2.data.firebird.FirebirdManager;
import v2.data.firebird.datasources.BillInSource;
import v2.data.firebird.datasources.BillOutSource;
import v2.data.firebird.datasources.CommonSource;
import v2.data.firebird.datasources.EmployeeSource;
import v2.data.firebird.datasources.GoodsSource;
import v2.data.firebird.datasources.InventorySource;
import v2.data.firebird.datasources.OrganizationSource;
import v2.data.firebird.datasources.SubdivisionSource;
import v2.data.firebird.datasources.WriteOffSource;

@Module(includes = AppModule.class)
public class FirebirdModule {

    @Singleton
    @Provides
    FirebirdConnectionProvider provideJDBCConnection(){
        return new FirebirdConnectionProvider();
    }

    @Singleton
    @Provides
    FirebirdManager provideFirebirdManager(FirebirdConnectionProvider jdbcConnect){
        return new FirebirdManager(jdbcConnect);
    }


    @Singleton
    @Provides
    CommonSource provideCommonSource(FirebirdManager firebirdManager)
    {
        return new CommonSource(firebirdManager);
    }


    @Singleton
    @Provides
    InventorySource provideInventorySource(FirebirdManager firebirdManager)
    {
        return new InventorySource(firebirdManager);
    }

    @Singleton
    @Provides
    BillInSource provideIBillInSource(FirebirdManager firebirdManager)
    {
        return new BillInSource(firebirdManager);
    }

    @Singleton
    @Provides
    BillOutSource provideIBillOutSource(FirebirdManager firebirdManager)
    {
        return new BillOutSource(firebirdManager);
    }

    @Singleton
    @Provides
    WriteOffSource provideWriteOffSource(FirebirdManager firebirdManager)
    {
        return new WriteOffSource(firebirdManager);
    }

    @Singleton
    @Provides
    SubdivisionSource provideSubdivisionSource(FirebirdManager firebirdManager)
    {
        return new SubdivisionSource(firebirdManager);
    }

    @Singleton
    @Provides
    EmployeeSource provideEmployeeSource(FirebirdManager firebirdManager)
    {
        return new EmployeeSource(firebirdManager);
    }

    @Singleton
    @Provides
    OrganizationSource provideOrganizationSource(FirebirdManager firebirdManager)
    {
        return new OrganizationSource(firebirdManager);
    }

    @Singleton
    @Provides
    GoodsSource provideGoodsSource(FirebirdManager firebirdManager)
    {
        return new GoodsSource(firebirdManager);
    }



}
