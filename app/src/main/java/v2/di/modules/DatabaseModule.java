package v2.di.modules;

import android.content.Context;

import androidx.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import v2.data.database.dao.CompanyDao;
import v2.data.database.InvDatabase;
import v2.data.database.dao.ConnectionDao;
import v2.data.database.dao.GoodDao;
import v2.data.database.dao.GroupDao;
import v2.data.database.dao.UnitDao;

@Module(includes = AppModule.class)
public class DatabaseModule {

    @Singleton
    @Provides
    InvDatabase provideDatabase(Context context){
        return Room.databaseBuilder(context, InvDatabase.class, "inventory")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton @Provides
    CompanyDao provideCompanyDao(InvDatabase db){
        return db.companyDAO();
    }

    @Singleton @Provides
    ConnectionDao provideConnectionDao(InvDatabase db){
        return db.connectionDAO();
    }

    @Singleton @Provides
    GroupDao provideGroupDao(InvDatabase db){
        return db.groupDAO();
    }

    @Singleton @Provides
    GoodDao provideGoodDao(InvDatabase db){
        return db.goodDAO();
    }

    @Singleton @Provides
    UnitDao provideUnitDao(InvDatabase db){
        return db.unitDAO();
    }
}
