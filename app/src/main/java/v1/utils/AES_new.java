package v1.utils;

import android.util.Log;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES_new {

    public static byte[]encrypt(String key, byte[] data, String iv) {
        final byte[]keyBytes=key.getBytes();
        final byte[] ivBytes=iv.getBytes();


        try {
            byte[] result;
            SecretKeySpec sks=new SecretKeySpec(keyBytes,"AES");
            Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(ivBytes));
            result=c.doFinal(data);
            return result;

        }
        catch (Exception e){
            System.out.println("Ошибка шифрования! - "+ e);
            Log.d("my","Ошибка шифрования! - "+ e);
        }
        return null;
    }
    public static byte[] decrypt (String key, byte[]data, String iv) {
        byte[] result;
        final byte[]keyBytes=key.getBytes();
        final byte[]ivBytes=iv.getBytes();

        try {
            SecretKeySpec sks=new SecretKeySpec(keyBytes, "AES");
            Cipher c=Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE,sks, new IvParameterSpec(ivBytes));
            result=c.doFinal(data);
            return result;
        }
        catch (Exception e) {
            System.out.println("Ошибка дешифровки! - "+e);
            Log.d("my","Ошибка дешифровки! - "+e);
        }
        return null;
    }
}
