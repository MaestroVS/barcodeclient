package v1.utils;

import android.os.Build;
import android.util.Base64;
import android.util.Log;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESHelper {

    public static String encrypt(String seed, String cleartext) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = encrypt(rawKey, cleartext.getBytes());
        return toHex(result);
    }

    public static String decrypt(String seed, String encrypted) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(rawKey, enc);
        return new String(result);
    }

    private static byte[] getRawKey(byte[] seed) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
      // if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){

             SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
            sr.setSeed(seed);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();
            return raw;
        /*} else{
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG","Crypto");
            // SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
            sr.setSeed(seed);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();
            return raw;
        }*/

    }


    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {


        SecretKey secretKey;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            String stringKey = "YourSecKey";
            byte[] encodedKey     = Base64.decode(stringKey, Base64.DEFAULT);
            secretKey= new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");


            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encrypted = cipher.doFinal(clear);
            return encrypted;
        } else {
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(clear);
            return encrypted;
        }
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static String toHex(String txt) {
        return toHex(txt.getBytes());
    }
    public static String fromHex(String hex) {
        return new String(toByte(hex));
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length()/2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2*buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }
    private final static String HEX = "0123456789ABCDEF";
    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b>>4)&0x0f)).append(HEX.charAt(b&0x0f));
    }


    public String encryption(String strNormalText){

       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           // SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
        }else */



            String seedValue = "YourSecKey";
            String normalTextEnc = "";
            try {
                normalTextEnc = AESHelper.encrypt(seedValue, strNormalText);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("my", "enctypt exeptioncc: " + e.toString());
            }
            return normalTextEnc;

    }
    public String decryption(String strEncryptedText){
        String seedValue = "YourSecKey";
        String strDecryptedText="";
        try {
            strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDecryptedText;
    }
}