package v1.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateUtils {

    companion object {

        val formatterString = "dd.MM.yyyy"


        fun daysToDate(dateTo: Date): Long{
            val diff: Long = dateTo.time - Date().time
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        }

        fun daysBetweenDates(date1: Date, date2: Date): Long{
            val diff: Long = date2.time - date1.time
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        }


        fun isDateExpired(date: Date): Boolean{
             return (Date().after(date))
        }


        fun stringToDate(dateString: String): Date {
            val format = SimpleDateFormat(formatterString)
            return format.parse(dateString)
        }

        fun dateMM_ToMMMDateString(date: Date): String {
            val targetFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
            return targetFormat.format(date)
        }

        /**
         * date from yyyy-MM-dd HH:mm:ss.SSS
         */
        fun dateFirebirdToString(date: String): String {

            val NEW_FORMAT = "dd MMM yyyy  HH:mm"
            val newDateString: String

            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
            val d = sdf.parse(date)
            sdf.applyPattern(NEW_FORMAT)
            newDateString = sdf.format(d)

            return newDateString
        }


        fun getCurrentDateString(): String {
            val sdf = SimpleDateFormat(formatterString)
            return sdf.format(Date())
        }

        fun getDaysTo(daysTo: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, +daysTo)

            return calendar.time
        }

        fun getFromDaysTo(dateFrom: Date, daysTo: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.time = dateFrom
            calendar.add(Calendar.DAY_OF_YEAR, +daysTo)

            return calendar.time
        }

        fun addDaysToDate(date: Date, days: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.DAY_OF_YEAR, +days)

            return calendar.time
        }

        fun getDaysToString(daysTo: Int): String {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, +daysTo)

            return toSimpleString(calendar.time)
        }

        fun toSimpleString(date: Date): String {
            val format = SimpleDateFormat(formatterString)
            return format.format(date)
        }




    }


}