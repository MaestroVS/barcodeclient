package v1.goods_lists;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import v1.data_model.Callback;
import com.app.barcodeclient3.MainApplication;
import com.app.barcodeclient3.ScannerActivity;
import com.app.barcodeclient3.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import v1.data_model.GoodGrpJSON;
import v1.data_model.GoodIntf;
import v1.data_model.GoodJSON;
import v1.data_model.Response;
import v1.dialogs.CreateGoodDialog;
import v1.dialogs.ModifyCountOfGoodDialog;
import v1.dialogs.ModifyGoodDialog;

import static v1.data_model.DataModelInterface.ALL;
import static v1.data_model.DataModelInterface.GRP_ID;
import static v1.data_model.DataModelInterface.ID;
import static v1.data_model.DataModelInterface.INVENTORY_ID;
import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;

import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.dbHelper;

/**
 * Created by MaestroVS on 05.02.2018.
 */

public class GoodsListActivity extends AppCompatActivity {


    ListView list;

    ProgressBar progress;

    RelativeLayout headerLayout;

    GoodsListAdapter adapter;
    View headerView = null;
    TextView headerTitle = null;

    ArrayList<GoodJSON> goodList = new ArrayList<>();
    ArrayList<GoodGrpJSON> allgoodGrpList = new ArrayList<>();


    FloatingActionButton fab;

    ProgressDialog waitDialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_list_3);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);

        Log.d(my, "Hello " + this.getClass().toString());


        headerLayout = findViewById(R.id.headerLayout);

        list = findViewById(R.id.list);
        fab = findViewById(R.id.fab);
        progress =  findViewById(R.id.progress);

        list.setOnItemClickListener(listClickListener);

        LayoutInflater inflater = getLayoutInflater();
        headerView = inflater.inflate(R.layout.header_good_3, null);
        headerTitle = headerView.findViewById(R.id.name);
        headerTitle.setText(". . .");
        headerLayout.setVisibility(View.GONE);

        // list.addHeaderView(headerView);
        // list.setHeaderDividersEnabled(true);

        headerLayout.addView(headerView);
        headerLayout.setOnClickListener(headerClickListener);

        progress.setVisibility(View.VISIBLE);
        //loadContent();

        fab.setVisibility(connectMode == CONNECT_OFFLINE ? View.VISIBLE : View.GONE);
        fab.setOnClickListener(createGoodListener);

        if (connectMode == CONNECT_ONLINE) {
            loadGoodsGrp();
        }
        if (connectMode == CONNECT_OFFLINE) {
            loadLocalGoods();
        }
    }

    private void loadLocalGoods() {
        ArrayList<GoodIntf> contentList = new ArrayList<>();
        initAdapterContent(contentList);
        loadContentGoods(null);
    }


   /* private void loadGoodsOffline(){

        ArrayList<GoodIntf> contentList = new ArrayList<>();
        contentList.addAll(currentFoldersList);
        initAdapterContent(contentList);
    }*/


    private void loadGoodsGrp() {
        MainApplication.getApi().getGoodsGrpList(new Callback<ArrayList<GoodGrpJSON>>() {
            @Override
            public boolean sendResult(Response<ArrayList<GoodGrpJSON>> response) {

                progress.setVisibility(View.GONE);

                ArrayList<GoodGrpJSON> list = response.body();
                if (list == null) return false;
                if (list.size() == 0) return false;

                Log.d("my", "we get goods list !!!! " + list.size());


                allgoodGrpList.clear();
                allgoodGrpList.addAll(list);

                processGrpList();

                currentFoldersList = rootFolders;


                ArrayList<GoodIntf> contentList = new ArrayList<>();
                contentList.addAll(currentFoldersList);
                initAdapterContent(contentList);

                return false;
            }

            @Override
            public void onError(String error) {

            }
        });
    }


    GoodGrpJSON rootRootFolder = null;
    GoodGrpJSON superRootFolder = null;
    ArrayList<GoodGrpJSON> rootFolders = new ArrayList<>();
    ArrayList<GoodGrpJSON> currentFoldersList = new ArrayList<>();

    private boolean processGrpList() {
        rootRootFolder = searchRootRootFolder();
        if (rootRootFolder == null) return false;

        superRootFolder = rootRootFolder;
        searchRootFolders(superRootFolder);
        Log.d(my, "RootFoldersList size = " + rootFolders.size());
        searchChildrens();
        return true;
    }

    private GoodGrpJSON searchRootRootFolder() {
        for (GoodGrpJSON fol : allgoodGrpList) {
            if (fol.getId() == 0) return fol;
        }
        return null;
    }

    /**
     * Ищем первые папки, которых больше одной, имеющие общего родителя
     *
     * @param rootF
     */
    private void searchRootFolders(GoodGrpJSON rootF) {
        ArrayList<GoodGrpJSON> tempRootFolders = new ArrayList<>();
        tempRootFolders.clear();
        for (GoodGrpJSON fol : allgoodGrpList) {
            if (fol.getParentId() == rootF.getId()) {
                tempRootFolders.add(fol);
            }
        }
        switch (tempRootFolders.size()) {
            case 0:
                rootFolders.add(rootF);
                break;
            case 1:
                searchRootFolders(tempRootFolders.get(0));
            default:
                rootFolders.addAll(tempRootFolders);
                rootFolders.remove(rootRootFolder);
                break;
        }
    }

    private void searchChildrens() {
        Log.d(my, "search children....");
        for (GoodGrpJSON folder : allgoodGrpList) {
            Log.d(my, "\n   ===current folder: " + folder.getName() + "  ID=" + folder.getId());
            for (GoodGrpJSON fol2 : allgoodGrpList) {
                if (fol2.getParentId() == folder.getId()) {
                    //  Log.d(my,"\n             -- child folder: "+fol2.getName()+" id="+fol2.getId()+" grpId="+fol2.getParentId());
                    //  folder.addChildAndMeetWithBrothers(fol2);
                    if (!(fol2.getName().contains("rootrootroot"))) {

                        folder.addChildren(fol2);
                        fol2.setParent(folder);
                    }
                    // fol2.setParent(folder);
                }
            }


        }
        for (GoodGrpJSON folder : allgoodGrpList) {
            for (GoodGrpJSON fol2 : allgoodGrpList) {

                if (fol2.getParentId() == folder.getParentId()) {
                    // Log.d(my, "\n             -- child folder: " + fol2.getName() + " id=" + fol2.getId() + " grpId=" + fol2.getParentId());
                    //  folder.addChildAndMeetWithBrothers(fol2);
                    if (!(fol2.getName().contains("rootrootroot"))) {
                        folder.addBrother(fol2);
                    }
                }
            }
        }

        Log.d(my, "\n============================================================================================");
        Log.d(my, " ============================================================================================");
        Log.d(my, " ============================================================================================");

        for (GoodGrpJSON folder : allgoodGrpList) {
            Log.d(my, "+-+-+-folder: " + folder.getName() + "  ID=" + folder.getId() + " parent="/*+/*folder.getParent().getName()/*+" parentId="+folder.getParentId()*/);
            Log.d(my, "         ---brothers: " + folder.brothersToString());
            //  Log.d(my,"         ---children: "+folder.childrenToString());
            // Log.d(my,"+-+-+-");
        }
    }


    private void loadContentGoods(GoodGrpJSON folder) {

        final GoodGrpJSON folder1 = folder;

        //  goodsList =searchGoodOnline(name,searchArticle.isChecked());
        Map<String, String> data = new HashMap<>();

        data.put(ALL, "ALL");
        if (folder1 != null) data.put(GRP_ID, Integer.toString(folder1.getId()));
        data.put(INVENTORY_ID, "" + ScannerActivity.id);


        MainApplication.getApi().getGoodsList(data, new Callback<ArrayList<GoodJSON>>() {
            @Override
            public boolean sendResult(Response<ArrayList<GoodJSON>> response) {

                progress.setVisibility(View.GONE);
                if (waitDialog != null) waitDialog.dismiss();

                ArrayList<GoodJSON> list = response.body();
                if (list == null) return false;

                Log.d("my", "we get goods list !!!! " + list.size());


                goodList.clear();
                goodList.addAll(list);

                for (GoodJSON goodJSON : goodList) {
                    if (folder1 != null) goodJSON.setParent(folder1);
                }

                ArrayList<GoodIntf> contentList = new ArrayList<>();
                contentList.addAll(goodList);
                updatedData(contentList);

                return false;
            }

            @Override
            public void onError(String error) {

            }
        });

    }


    private void initAdapterContent(ArrayList<GoodIntf> contentList) {

        boolean showCnt = true;
        if (ScannerActivity.id < 0) showCnt = false;
        adapter = new GoodsListAdapter(GoodsListActivity.this, R.layout.row_good_3, contentList, showCnt);
        list.setAdapter(adapter);
    }


    AdapterView.OnItemClickListener listClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            final View currentItemView = view;
            Object obj = adapterView.getAdapter().getItem(i);
            Log.d(my, "click obj = " + obj.getClass().toString());

            headerLayout.setVisibility(connectMode == CONNECT_ONLINE ? View.VISIBLE : View.GONE);


            if (obj instanceof GoodGrpJSON) {


                GoodGrpJSON folder = (GoodGrpJSON) obj;
                if (folder.isParentDirectory()) {

                    Log.d(my, "click folder: " + folder.getName() + " brothers = " + folder.getBrothersList().size());


                    ArrayList<GoodGrpJSON> foldersList = folder.getChildGrpList();
                    ArrayList<GoodIntf> contentList = new ArrayList<>();
                    contentList.addAll(foldersList);

                    headerLayout.setTag(folder);
                    headerView.setTag(folder);
                    headerTitle.setText(folder.getName());

                    //   waitDialog = ProgressDialog.show(GoodsListActivity.this, "",
                    //          "Loading...", true);
                    // waitDialog.show();

                    updatedData(contentList);

                    // if(waitDialog!=null) waitDialog.dismiss();
                } else {

                    headerLayout.setTag(folder);
                    headerView.setTag(folder);
                    headerTitle.setText(folder.getName());

                    waitDialog = ProgressDialog.show(GoodsListActivity.this, "",
                            "Loading...", true);
                    // waitDialog.show();


                    loadContentGoods(folder);

                    //  if(waitDialog!=null) waitDialog.dismiss();

                }
            }
            if (obj instanceof GoodJSON) {

                GoodJSON good = (GoodJSON) obj;

                int id = good.getId();
                String name = good.getName();
                String barcode = good.getBarcode();
                float price = good.getPrice();
                String unit = good.getUnit();
                String article = good.getArticle();
                //String count = good.get;

                if (connectMode == CONNECT_ONLINE) {
                    Log.d(my, "connectOnline");
                    Map<String, String> data = new HashMap<>();
                    if (ScannerActivity.id > -1)
                        data.put(INVENTORY_ID, "" + ScannerActivity.id);
                    data.put(ID, "" + id);
                    MainApplication.getApi().getGoodsList(data, new Callback<ArrayList<GoodJSON>>() {
                        @Override
                        public boolean sendResult(Response<ArrayList<GoodJSON>> response) {
                            ArrayList<GoodJSON> list = response.body();
                            if (list == null) {
                                Log.d("my", "*-we get goods list null !!!! ");
                                return false;
                            }
                            Log.d("my", "*-we get goods list !!!! " + list.size());
                            if (list.size() != 1) return false;

                            GoodJSON good = list.get(0);
                            int good_id = good.getId();
                            String name = good.getName();
                            String barcode = good.getBarcode();
                            Log.d(my, "**/**barcode=" + barcode);
                            float price = good.getPrice();
                            String unit = good.getUnit();
                            float count = good.getCnt_fact();
                            String article = good.getArticle();

                            ModifyCountOfGoodDialog goodDialog = new ModifyCountOfGoodDialog();


                            Bundle args = new Bundle();
                            // String good = barcode;//searchEditText.getText().toString();
                            args.putInt("good_id", good_id);
                            args.putString("name", name);
                            args.putString("barcode", barcode);
                            args.putString("unit", unit);
                            args.putString("article", article);

                            DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                            args.putString("price", decimalFormat.format(price));

                            DecimalFormat decimalFormat2 = new DecimalFormat("#00.000");
                            String cnt = "";
                            try {
                                cnt = Float.toString(count);
                            } catch (Exception e) {
                            }

                            boolean isShtuka = false;
                            if (unit.contains("шт") || unit.contains("бут") || unit.contains("пач"))
                                isShtuka = true;


                            if (count == 0) {
                                if (isShtuka) {
                                    decimalFormat2 = new DecimalFormat("#0");
                                } else decimalFormat2 = new DecimalFormat("#0.000");


                            } else {
                                float ostatok = count - (int) count;
                                if (ostatok == 0) {
                                    if (isShtuka) {
                                        decimalFormat2 = new DecimalFormat("#0");
                                    } else decimalFormat2 = new DecimalFormat("#0.000");
                                } else {
                                    decimalFormat2 = new DecimalFormat("#0.000");
                                }
                            }

                            cnt = decimalFormat2.format(count);

                            args.putString("count", cnt);

                            args.putInt("hd_id", ScannerActivity.id);


                            goodDialog.setArguments(args);
                            goodDialog.setCallback(new Handler.Callback() {
                                @Override
                                public boolean handleMessage(Message message) {
                                    Log.d(my, "get msg");
                                    if (message != null) {
                                        Log.d(my, "get msg 1");
                                        int good_id = message.arg1;
                                        float fcnt = (float) message.obj;

                                        TextView cnt = currentItemView.findViewById(R.id.cnt);
                                        if (cnt != null) {
                                            Log.d(my, "get msg 2 " + fcnt);
                                            DecimalFormat decimalFormat2 = new DecimalFormat("#00.000");
                                            String strcnt = decimalFormat2.format(fcnt);
                                            cnt.setText(strcnt);
                                            currentItemView.setBackgroundColor(getResources().getColor(R.color.blue));
                                            adapter.updateGoodFactCnt(good_id, fcnt);
                                            adapter.notifyDataSetChanged();

                                        }


                                    }
                                    return false;
                                }
                            });

                            goodDialog.show(getFragmentManager(), "Hello!");

                            return false;
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });


                }
                if (connectMode == CONNECT_OFFLINE) {
                    Log.d(my, "connect offline"); //we ll modify good


                    ModifyGoodDialog goodDialog = new ModifyGoodDialog();


                    Bundle args = new Bundle();
                    // String good = barcode;//searchEditText.getText().toString();
                    args.putInt("good_id", id);
                    args.putString("name", name);
                    args.putString("barcode", barcode);
                    args.putString("unit", unit);
                    args.putString("article", article);

                    DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                    args.putString("price", decimalFormat.format(price));


                    goodDialog.setArguments(args);
                    goodDialog.setCallback(new Handler.Callback() {
                        @Override
                        public boolean handleMessage(Message message) {
                            Log.d(my, "get msg");
                            if (message != null) {
                                Log.d(my, "get msg 1");
                                final int good_id = message.arg1;

                                if (good_id > 0) { //значит редактировали товар
                                    final String newName = (String) message.obj;

                                    TextView nameTV = currentItemView.findViewById(R.id.name);
                                    if (nameTV != null) {
                                        Log.d(my, "get msg 2 " + newName);
                                        // DecimalFormat decimalFormat2 = new DecimalFormat("#00.000");
                                        // String strcnt =decimalFormat2.format(newName);
                                        nameTV.setText(newName);
                                        currentItemView.setBackgroundColor(getResources().getColor(R.color.blue));
                                        adapter.updateGoodName(good_id, newName);
                                        adapter.notifyDataSetChanged();

                                    }
                                } else {//значит удалили товар
                                    final String newName = (String) message.obj;
                                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    dbHelper.clearGood(-1*good_id); //потому что ид товара отрицательно по модулю как признак удаления
                                                    loadLocalGoods();
                                                    break;

                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    //No button clicked
                                                    break;
                                            }
                                        }
                                    };

                                    AlertDialog.Builder builder = new AlertDialog.Builder(GoodsListActivity.this);
                                    String msg = getString(R.string.dialog_delete_good)+" <b>"+newName+"</b> ?";
                                    builder.setMessage(Html.fromHtml(msg)).setPositiveButton("Ok", dialogClickListener)
                                            .setNegativeButton(getString(R.string.cancel), dialogClickListener).show();


                                }


                            }
                            return false;
                        }
                    });

                    goodDialog.show(getFragmentManager(), "Hello!");


                }
            }
        }
    };

    View.OnClickListener headerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Log.d(my, "header click");

            Object objTag = view.getTag();
            if (objTag == null) {
                Log.d(my, "header click obj tag = null");
                return;
            }
            if (objTag instanceof GoodGrpJSON) {
                GoodGrpJSON parent = (GoodGrpJSON) objTag;

                if (parent == null) return;
                String title = ". . .";
                if (parent.getParent() != null) {
                    if (parent.getParent().getName() != null) {
                        title = parent.getParent().getName();
                    }
                }


                Log.d(my, "header click current parent = " + parent.getName());
                Log.d(my, "older parent = " + title);


                ArrayList<GoodGrpJSON> foldersList = parent.getBrothersList();
                Log.d(my, "brothersList size = " + foldersList.size());
                ArrayList<GoodIntf> contentList = new ArrayList<>();
                contentList.addAll(foldersList);
                updatedData(contentList);

                String text = title;
                if (text.contains("rootroot")) text = ". . .";
                headerTitle.setText(text);
                if (text.equals(". . .")) headerLayout.setVisibility(View.GONE);
                else headerLayout.setVisibility(View.VISIBLE);
                headerTitle.setTag(parent.getParent());
                headerLayout.setTag(parent.getParent());
                headerView.setTag(parent.getParent());

               /* parent = (GoodGrpJSON) headerTitle.getTag();

                Log.d(my, "after update header click current parent = "+parent.getName());
                Log.d(my,"after update older parent = "+parent.getParent().getName());*/
            } else {
                Log.d(my, "header click objtag no instanceof((");
            }


        }
    };

    View.OnClickListener createGoodListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            CreateGoodDialog goodDialog = new CreateGoodDialog();
            goodDialog.setCallback(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message message) {
                    loadLocalGoods();
                    return false;
                }
            });
            goodDialog.show(getFragmentManager(), "Hello!");
        }
    };

    public void updatedData(ArrayList<GoodIntf> contentList) {

        adapter.clear();


        if (contentList != null) {

            for (Object object : contentList) {

                adapter.insert(object, adapter.getCount());
            }
        }

        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
