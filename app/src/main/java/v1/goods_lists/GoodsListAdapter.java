package v1.goods_lists;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.barcodeclient3.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import v1.data_model.GoodGrpJSON;
import v1.data_model.GoodIntf;
import v1.data_model.GoodJSON;

/**
 * Created by MaestroVS on 05.02.2018.
 */

public class GoodsListAdapter extends ArrayAdapter {

    Context context;

    ArrayList<GoodIntf> goodslist;

    public GoodsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<GoodIntf> list) {
        super(context, resource, list);
        this.goodslist=list;
        this.context=context;
    }

    private boolean showCnt = true;

    public GoodsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<GoodIntf> list, boolean showCnt) {
        super(context, resource, list);
        this.goodslist=list;
        this.context=context;
        this.showCnt=showCnt;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_good_3, null);

        final TextView nameTV = v.findViewById(R.id.name);
        final ImageView image = v.findViewById(R.id.image);
        final TextView cnt = v.findViewById(R.id.cnt);



        if(goodslist==null) return v;
        if(goodslist.size()==0) return v;
        if(position>=goodslist.size()) return v;

        GoodIntf good = goodslist.get(position);

        if(good==null) return v;

       // Log.d(my,"showCnt = "+showCnt);

        image.setVisibility((good instanceof GoodGrpJSON)?View.VISIBLE:View.GONE);
        cnt.setVisibility(((good instanceof GoodGrpJSON) )?View.GONE:View.VISIBLE);
        cnt.setVisibility(showCnt?View.VISIBLE:View.GONE);

        String name = good.getName();

        if(good instanceof GoodJSON){
            GoodJSON goodJSON = (GoodJSON) good;
            float fcnt = goodJSON.getCnt_fact();
            DecimalFormat decimalFormat2 = new DecimalFormat("#0.000");
            String strcnt =decimalFormat2.format(fcnt);
            cnt.setText(strcnt);
        }

        if(name==null) name="";

        nameTV.setText(name);


        return v;
    }

    public void updateGoodFactCnt(int goodId, float fcnt){
        for(GoodIntf goodJSON:goodslist){
            if(goodJSON instanceof GoodJSON){
                GoodJSON good = (GoodJSON) goodJSON;
                int currentGoodId = good.getId();
                if(currentGoodId==goodId){
                    good.setCnt_fact(fcnt);
                    return;
                }
            }
        }
    }


    public void updateGoodName(int goodId, String name){
        for(GoodIntf goodJSON:goodslist){
            if(goodJSON instanceof GoodJSON){
                GoodJSON good = (GoodJSON) goodJSON;
                int currentGoodId = good.getId();
                if(currentGoodId==goodId){
                    good.setName(name);
                    return;
                }
            }
        }
    }

    /*public void updatedData(List itemsArrayList) {

        goodslist.clear();

        if (itemsArrayList != null){

            for (Object object : itemsArrayList) {

                goodslist.insert(object, mAdapter.getCount());
            }
        }

        notifyDataSetChanged();

    }*/
}
