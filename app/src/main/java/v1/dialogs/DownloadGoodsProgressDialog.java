package v1.dialogs;


import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.barcodeclient3.R;

import static v1.excel.AndroidReadExcelActivity.my;

/**
 * Created by userd088 on 20.07.2016.
 */
public class DownloadGoodsProgressDialog extends DialogFragment {


    //ProgressBar progressBar;
    TextView statusText;
    Activity activity;

    com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar progress2;

    int maxProgress = 100;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.warning));


        View v = inflater.inflate(R.layout.dialog_view_download_goods, null);

       // progressBar = v.findViewById(R.id.progressBar);
        statusText = v.findViewById(R.id.progressText);

        progress2 = v.findViewById(R.id.progress2);

       // progressBar.setMax(maxProgress);
        progress2.setMax(maxProgress);


        return v;
    }

    public void setMax(int max) {
        maxProgress = max;
       // progressBar.setMax(maxProgress);
    }

    public void setProgress(int progress) {
        Log.d(my,"dialog set progress = "+progress);
       // progressBar.setProgress(progress);
        progress2.setProgress(progress);
        statusText.setText("" + progress + "%");
    }


}