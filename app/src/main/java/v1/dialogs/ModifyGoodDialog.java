package v1.dialogs;


import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.barcodeclient3.R;

import v1.data_model_offline_old.Good;
import com.app.barcodeclient3.MainApplication;

import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.dbHelper;

/**
 * Created by MaestroVS on 15.09.2018.
 */
public class ModifyGoodDialog extends DialogFragment {

    final String LOG_TAG = "myLogs";

    EditText nameTV;
    EditText barcodeTV;
    EditText priceTv;
    EditText countET;
    EditText unitEt;
    Button okBt;
    TextView articleTV;
   // CheckBox notAskMore;

   // List<String> unitsList = new ArrayList<>();

    Handler.Callback callback=null;

    int hd_id=-1;
    int good_id=-1;
    String name = "";
    String barcode = "";
    String price = "";
    String unit = "";
    String count = "";
    String article="";

    Handler h = new Handler();

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.modify_count_of_good));

        good_id = getArguments().getInt("good_id");

        name = getArguments().getString("name");
        barcode = getArguments().getString("barcode");
        price = getArguments().getString("price");
        unit = getArguments().getString("unit");
        count = getArguments().getString("count");
        hd_id = getArguments().getInt("hd_id");

        article = getArguments().getString("article");

        Good good = dbHelper.getGood(good_id);
        if(good!=null ){
            Log.d(my,"OUR GOOD is GOOD =)");
            name = good.getName();
            if(name==null) name="";
            if(good.getBarcodes()!=null){
                if(good.getBarcodes().size()>0)  barcode= good.getBarcodes().get(0);
            }
            article = good.getArticle();
            if(article==null) article="";
            unit = good.getUnit();
            if(unit==null) unit = "";
            price = ""+good.getOut_price();


        }

        else{
            Log.d(my,"OUR GOOD is NULL =(");
            dismiss();
        }


        if(good_id<0) dismiss();
        if(hd_id<0) dismiss();


        View v = inflater.inflate(R.layout.dialog_modify_good, null);
        unitEt = v.findViewById(R.id.unitEt);
        barcodeTV =  v.findViewById(R.id.barcodeTV);
        nameTV =  v.findViewById(R.id.nameTV);
        priceTv =  v.findViewById(R.id.priceTv);
        countET =  v.findViewById(R.id.countET);
        articleTV =   v.findViewById(R.id.articleTV);


        ImageView removeButton = v.findViewById(R.id.removeButton);
        removeButton.setOnClickListener(removeGoodListener);



        Log.d("my", "GOOD = " + name);

        if(name==null) name = "";
        if(barcode==null) barcode = "";
        if(unit==null) unit = "";
        if(price==null) price = "";
        if(count==null) count = "0";
        if(article==null) article="";


        nameTV.setText(name);
        barcodeTV.setText(barcode);
        unitEt.setText(unit);
        priceTv.setText(price);
        articleTV.setText(article);

        float fcount=-1;
        try{
            fcount = Float.parseFloat(count);
        }catch (Exception e){}

        Log.d(my,"fcouuunt = "+fcount);
        if(fcount==0){
            countET.setText("");
            countET.setHint("0");
        }else {
            countET.setText(count);
        }





        Button cancelGood =  v.findViewById(R.id.cancelGood);
        cancelGood.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               /* NewScannerActivity sw = (NewScannerActivity) getActivity(); ///***!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                sw.onClickGoodDialogCancel();*/
                hideSoftInputPanel();
                dismiss();
            }
        });

        //

        okBt = (Button) v.findViewById(R.id.createGood);
        okBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nameTV.getText().toString().trim().length()>0 && unitEt.getText().toString().trim().length()>0) {
                    Log.d(my,"editGood...");
                    editGood();
                }else {
                    Log.d(my,"editGood((");
                }
            }
        });

       /* countET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                Log.d(my,"keyEvent = "+event.toString());
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    modyfyCount();
                    return true;
                }
                return false;
            }
        });*/

        InputMethodManager imm = (InputMethodManager)   MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);



        return v;
    }




    public void setCallback(Handler.Callback callback){
        this.callback=callback;
    }




    private void editGood()
    {
         double newprice = 0;

        Log.d(my,"editGood");
        String newPriceStr = priceTv.getText().toString().trim();
        Log.d(my,"newPriceStr ="+newPriceStr);


        try{
            newprice = Double.parseDouble(newPriceStr);
            Log.d(my,"newprice="+newprice);
        }catch (Exception e){
            newprice=0;
            Log.d(my,"editGood e="+e.toString());
        }



        final String newName = nameTV.getText().toString().trim();
        String newUnit = unitEt.getText().toString().trim();
        String newBarcode = barcodeTV.getText().toString().trim();

        if(!name.equals(newName)||!unit.equals(newUnit)||!barcode.equals(newBarcode)||!price.equals(newPriceStr)) {
            if(newName.length()>0&&newUnit.length()>0) {
                dbHelper.editGood(good_id, newName, newUnit, newBarcode, newprice);
            }
        }


        if(callback!=null) {
            Message message = new Message();
            message.arg1 = good_id;
            message.obj = newName;
            callback.handleMessage(message);
        }

        hideSoftInputPanel();
        dismiss();

    }

    OnClickListener removeGoodListener = new OnClickListener() {
        @Override
        public void onClick(View view) {



            if (callback != null) {
                Message message = new Message();
                message.arg1 = -1 * good_id;
                message.obj = name;
                callback.handleMessage(message);

                hideSoftInputPanel();
                dismiss();

            }
        }
    };




   // private static int currentUnitItem=0;



    private void hideSoftInputPanel() {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(countET.getWindowToken(), 0);
    }

    private void showSoftInputPanel(View view) {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }





    }
}