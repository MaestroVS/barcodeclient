package v1.dialogs;


import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import v1.data_model.Callback;
import com.app.barcodeclient3.R;
import v1.data_model.Response;

import com.app.barcodeclient3.MainApplication;

import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;
import static com.app.barcodeclient3.MainApplication.CURRENT_FREE_SCANS;
import static com.app.barcodeclient3.MainApplication.MAX_FREE_SCANS;
import static com.app.barcodeclient3.MainApplication.addFreeScan;
import static com.app.barcodeclient3.MainApplication.checkLicense;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.getApi;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.ScannerActivity.playDone;
import static com.app.barcodeclient3.ScannerActivity.playError;

/**
 * Created by MaestroVS on 15.09.2018.
 */
public class ModifyCountOfGoodDialog extends DialogFragment {

    final String LOG_TAG = "myLogs";

    TextView nameTV;
    TextView barcodeTV;
    TextView priceTv;
    EditText countET;
    TextView unitTv;
    Button okBt;
    TextView articleTV;
   // CheckBox notAskMore;

   // List<String> unitsList = new ArrayList<>();

    int hd_id=-1;
    int good_id=-1;
    String name = "";
    String barcode = "";
    String price = "";
    String unit = "";
    String count = "";
    String article="";

    Handler h = new Handler();

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.modify_count_of_good));

        name = getArguments().getString("name");
        barcode = getArguments().getString("barcode");
        price = getArguments().getString("price");
        unit = getArguments().getString("unit");
        count = getArguments().getString("count");
        hd_id = getArguments().getInt("hd_id");
        good_id = getArguments().getInt("good_id");
        article = getArguments().getString("article");


        if(good_id<0) dismiss();
        if(hd_id<0) dismiss();


        View v = inflater.inflate(R.layout.dialog_modify_count_of_good, null);
        unitTv = v.findViewById(R.id.unitTv);
        barcodeTV =  v.findViewById(R.id.barcodeTV);
        nameTV =  v.findViewById(R.id.nameTV);
        priceTv =  v.findViewById(R.id.priceTv);
        countET =  v.findViewById(R.id.countET);
        articleTV =   v.findViewById(R.id.articleTV);



        Log.d("my", "GOOD = " + name);

        if(name==null) name = "";
        if(barcode==null) barcode = "";
        if(unit==null) unit = "";
        if(price==null) price = "";
        if(count==null) count = "0";
        if(article==null) article="";


        nameTV.setText(name);
        barcodeTV.setText(barcode);
        unitTv.setText(unit);
        priceTv.setText(price);
        articleTV.setText(article);

        float fcount=-1;
        try{
            fcount = Float.parseFloat(count);
        }catch (Exception e){}

        Log.d(my,"fcouuunt = "+fcount);
        if(fcount==0){
            countET.setText("");
            countET.setHint("0");
        }else {
            countET.setText(count);
        }





        Button cancelGood =  v.findViewById(R.id.cancelGood);
        cancelGood.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               /* NewScannerActivity sw = (NewScannerActivity) getActivity(); ///***!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                sw.onClickGoodDialogCancel();*/
                hideSoftInputPanel();
                dismiss();
            }
        });

        //

        okBt = (Button) v.findViewById(R.id.createGood);
        okBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                modyfyCount();
            }
        });

        countET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                Log.d(my,"keyEvent = "+event.toString());
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    modyfyCount();
                    return true;
                }
                return false;
            }
        });

        InputMethodManager imm = (InputMethodManager)   MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);



        return v;
    }


    Handler.Callback callback=null;

    public void setCallback(Handler.Callback callback){
        this.callback=callback;
    }




    private void modyfyCount()
    {
        String newcnt = countET.getText().toString().trim();
        if(newcnt.length()==0) return;

        float fcnt = 0;
        try{
            fcnt = Float.parseFloat(newcnt);
        }catch (Exception e){
            return;
        }

        final float factCount = fcnt;



        if (connectMode == CONNECT_ONLINE) {

            if(!checkLicense()) {
                if (CURRENT_FREE_SCANS > MAX_FREE_SCANS) {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            //  showLicenseDialog();
                        }
                    });
                    return;
                }
            }
            getApi().sendEditInventoryDt(hd_id, good_id, factCount, new Callback() {
                @Override
                public boolean sendResult(Response response) {

                    if (response == null) {

                    } else {
                        if (response.body() != null) {
                            int result = (int) response.body();
                            Log.d("my", "we have inv dt result result : " + result);



                            if (result == 0) {
                                //factCntTextView.setText("" + tempOfflineAnsCnt);
                                //factCntTextView.setTextColor(getResources().getColor(R.color.green));

                                // Log.d("my", "ans4 ok");


                                Toast toast = Toast.makeText(getAppContext(),
                                        R.string.labelled, Toast.LENGTH_SHORT);
                                toast.show();
                                playDone();
                                addFreeScan(); //up counter of scan
                                if(callback!=null) {
                                    Message message = new Message();
                                    message.arg1 = good_id;
                                    message.obj = factCount;
                                    callback.handleMessage(message);

                                }
                            } else {

                                playError();
                            }


                        }
                    }
                    return false;
                }

                @Override
                public void onError(String error) {

                }
            });
        }

        hideSoftInputPanel();
        dismiss();

    }




   // private static int currentUnitItem=0;



    private void hideSoftInputPanel() {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(countET.getWindowToken(), 0);
    }

    private void showSoftInputPanel(View view) {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }





    }
}