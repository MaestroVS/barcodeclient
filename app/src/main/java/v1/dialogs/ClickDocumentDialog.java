package v1.dialogs;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.barcodeclient3.MainApplication;
import com.app.barcodeclient3.R;
import com.app.barcodeclient3.ScannerActivity;

import java.util.ArrayList;

import v1.data_model.Callback;
import v1.data_model.Response;
import v1.data_model_offline_old.Good;

import static com.app.barcodeclient3.MainApplication.CONNECT_OFFLINE;
import static com.app.barcodeclient3.MainApplication.CONNECT_ONLINE;
import static com.app.barcodeclient3.MainApplication.UPDATE_STATE;
import static com.app.barcodeclient3.MainApplication.connectMode;
import static com.app.barcodeclient3.MainApplication.getApi;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.MainApplication.unsConnectOnline;
import static v1.excel.AndroidReadExcelActivity.my;

/**
 * Created by userd088 on 20.07.2016.
 */
public class ClickDocumentDialog extends DialogFragment {


    RelativeLayout headLayout;
    Button exportToUnsBt;
    Button exportInventoryXLS;
    Button deleteInventory;
    ProgressBar progress;

    TextView statusText;


    int id = -1;
    String num = "";
    String subdivision = "";
    String date_ = "";

    Handler h = new Handler();

    Handler.Callback callback = null;

    Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.inventory));

        if (getArguments() != null) {
            id = getArguments().getInt("id");
            num = getArguments().getString("num");
            subdivision = getArguments().getString("subdivision");
            date_ = getArguments().getString("date");
        }


        View v = inflater.inflate(R.layout.dialog_click_document, null);
        exportToUnsBt = v.findViewById(R.id.exportToUnsBt);
        exportToUnsBt.setOnClickListener(exportOnlineClickListener);
        if (connectMode == CONNECT_ONLINE && unsConnectOnline)
            exportToUnsBt.setVisibility(View.VISIBLE);
        if (connectMode == CONNECT_OFFLINE || !unsConnectOnline)
            exportToUnsBt.setVisibility(View.GONE);
        progress = v.findViewById(R.id.progress);

        exportInventoryXLS = v.findViewById(R.id.exportInventory);
        exportInventoryXLS.setOnClickListener(view -> {
            dismiss();
            if (callback != null) {
                Message message = new Message();
                message.arg1 = 1;
                callback.handleMessage(message);
            }
        });

        View xmlBt = v.findViewById(R.id.exportInventoryXML);

        xmlBt.setOnClickListener(v1 -> {

            dismiss();
            if (callback != null) {
                Message message = new Message();
                message.arg1 = 3;
                callback.handleMessage(message);
            }

        });

        deleteInventory = v.findViewById(R.id.deleteInventory);
        deleteInventory.setOnClickListener(view -> {
            dismiss();
            if (callback != null) {
                Message message = new Message();
                message.arg1 = 2;
                callback.handleMessage(message);
            }
        });

        //exportToUnsBt.setVisibility(View.GONE);

        statusText = v.findViewById(R.id.statusText);


        TextView numTV = v.findViewById(R.id.num);
        TextView subdivisionVT = v.findViewById(R.id.subdivision);
        TextView date = v.findViewById(R.id.date);
        ImageView closeBt = v.findViewById(R.id.closeBt);

        closeBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        date.setText(date_);
        numTV.setText(num);
        subdivisionVT.setText(subdivision);


        headLayout = v.findViewById(R.id.headLayout);
        headLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ScannerActivity.class);

                Bundle arg = new Bundle();
                arg.putInt("id", id);
                arg.putString("date", date_);
                arg.putString("subdiv", subdivision);
                arg.putInt("docType", 0);

                intent.putExtras(arg);
                //startActivity(intent);
                startActivityForResult(intent, UPDATE_STATE);
                dismiss();
            }
        });


        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.GONE);
                accGoods.clear();
                Log.d(my, "click inv id =" + id);
                Log.d(my, "click inv num =" + num);
                // if(connectMode==CONNECT_ONLINE&&unsConnectOnline)

                ArrayList<Good> alg = MainApplication.dbHelper.getGoodsCountListAccByNum(Integer.parseInt(num));
                goodsCnt = alg.size();

                Log.d(my, "acc Goods list = " + alg.size());
                accGoods.addAll(alg);
                statusText.setTextColor(ContextCompat.getColor(getAppContext(), R.color.dark_gray));
                if (goodsCnt > 0) {
                    if (connectMode == CONNECT_ONLINE && unsConnectOnline)
                        exportToUnsBt.setVisibility(View.VISIBLE);
                    if (connectMode == CONNECT_OFFLINE || !unsConnectOnline)
                        exportToUnsBt.setVisibility(View.GONE);
                    statusText.setText("Просканированных товаров: " + goodsCnt);
                } else {
                    statusText.setText("Позиций по данному переучету не найдено");
                }
            }
        }, 300);


        return v;
    }


    public void setCallback(Handler.Callback callback) {
        this.callback = callback;
    }


    View.OnClickListener exportOnlineClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            progress.setVisibility(View.VISIBLE);
            CheckOffineEditDocument checkGoods = new CheckOffineEditDocument(id);
            checkGoods.start();
        }
    };

    Handler ht = new Handler();

    ArrayList<Good> accGoods = new ArrayList<>();

    int goodsCnt = 0;

    class CheckOffineEditDocument extends Thread {

        int id;

        public CheckOffineEditDocument(int id) {
            this.id = id;
        }

        @Override
        public void run() {


            if (accGoods.size() > 0) {
                ht.post(() -> {
                    Log.d(my, "acc good size = " + accGoods.size());
                    if (accGoods.size() == 0) progress.setVisibility(View.GONE);
                    exportDataToOnline(num);
                });
            } else {
                ht.post(() -> {
                progress.setVisibility(View.GONE);
                });
            }

        }
    }

    private void exportDataToOnline(String num) { //для онлайна id переучета в базе юнс соответствует параметру num
        // в локальной базе, поскольку id локальной базы зарезервирован под праймари кей

       /* export = true;

        prog = 0;
        int i = 0;
        int max = accGoods.size();*/


        getApi().sendEditInventoriesList(Integer.parseInt(num), accGoods, new Callback() {
            @Override
            public boolean sendResult(Response response) {
                if (response == null) {
                    return false;
                } else {
                    if (response.body() != null) {
                        int result = (int) response.body();
                        Log.d("my", "we have inv dt result result : " + result);
                        //0  is good!
                        //-1 is fail!
                        if (result == 0) {
                            // return true;


                            statusText.setText("Отправлено товаров: " + goodsCnt);
                            statusText.setTextColor(ContextCompat.getColor(getAppContext(), R.color.green));

                        }
                    }
                }
                return true;
            }

            @Override
            public void onError(String error) {
                statusText.setTextColor(ContextCompat.getColor(getAppContext(), R.color.red_primary_dark_color));
                statusText.setText("Ошика экспорта");
            }
        });

        ht.post(new Runnable() {
            @Override
            public void run() {
                String msg = "";
                progress.setVisibility(View.GONE);
                // Toast.makeText(activity, getString(R.string.success_send), Toast.LENGTH_SHORT).show();


            }
        });
    }

}