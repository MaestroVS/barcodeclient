package v1.dialogs;


import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.app.barcodeclient3.MainActivityOld;
import com.app.barcodeclient3.R;
import com.reginald.editspinner.EditSpinner;

import java.util.ArrayList;

import com.app.barcodeclient3.MainApplication;
import com.app.barcodeclient3.ScannerActivity;

import static com.app.barcodeclient3.MainApplication.getAppContext;
import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.getUnits;
import static com.app.barcodeclient3.MainApplication.lastUnit;

/**
 * Created by userd088 on 20.07.2016.
 */
public class CreateGoodDialog extends DialogFragment {

    final String LOG_TAG = "myLogs";

    com.wrbug.editspinner.EditSpinner editSpinner1;

    EditSpinner editSpinner;
    EditText editUnit;
    Button okUnitBt;
   // CustomizedSpinnerAdapter adapter1;
    EditText barcodeET;
    EditText nameEt;
    Button createGoodBt;
    CheckBox notAskMore;
    TextView unitV;

    ArrayList<String> unitsList = new ArrayList<>();

    String good = "";

    Handler h = new Handler();

    Handler.Callback callback=null;

    String[] arrayUnits;

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.create_new_good));

        if(getArguments()!=null)
        good = getArguments().getString("name");


        View v = inflater.inflate(R.layout.dialog_create_good, null);
        editSpinner = v.findViewById(R.id.edit_spinner);


        editUnit = (EditText) v.findViewById(R.id.editUnit);
        okUnitBt = (Button) v.findViewById(R.id.okUnitBt);

        unitV = v.findViewById(R.id.unitV);
        unitV.setVisibility(( Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)?View.VISIBLE:View.GONE);

        okUnitBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditUnit(false);

                String unit = editUnit.getText().toString();
                unit.trim();
                if (unit.length() > 0) {
                    Log.d("my", "unit = " + unit);

                    MainApplication.dbHelper.getOrInsertUnitFromDB(unit);

                    // ArrayList<String> unitsList = MainActivity.dbHelper.getUnits();
                    //  Log.d("my","units list = "+unitsList.toString());
                }



                unitsList = MainApplication.dbHelper.getUnits();
                String[] data = new String[unitsList.size()];
                data = unitsList.toArray(data);
               /* adapter1 = new CustomizedSpinnerAdapter(
                        CreateGoodDialog.this.getActivity(), R.layout.unit_spinner_item,
                        data);*/




               arrayUnits = unitsList.toArray(new String[0]);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                        CreateGoodDialog.this.getActivity(),R.layout.unit_spinner_item,arrayUnits
                );
                spinnerArrayAdapter.setDropDownViewResource(R.layout.unit_spinner_item);

                editSpinner.setAdapter(spinnerArrayAdapter);
                int position = unitsList.size() - 1;
                if (position >= 0) editSpinner.setSelection(position);
                editSpinner.refreshDrawableState();
                editSpinner.setTextColor(ContextCompat.getColor(getAppContext(),R.color.dark_gray));


                //int position = unitsList.size() - 1;
               // EditSpinnerSimpleAdapter editSpinnerSimpleAdapter = new EditSpinnerSimpleAdapter(unitsList,MainApplication.getAppContext());
              //  if (position >= 0) editSpinner1.s
               // editSpinner1.setAdapter(editSpinnerSimpleAdapter);

            }
        });

        barcodeET = (EditText) v.findViewById(R.id.barcodeET);
        nameEt = (EditText) v.findViewById(R.id.nameET);

        Log.d("my", "GOOD = " + good);


        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Double.parseDouble(good);
                    barcodeET.setText(good);
                    // barcodeTV.setFocusable(false);
                    nameEt.setFocusable(true);
                } catch (Exception e) {
                    nameEt.setText(good);
                    barcodeET.setFocusable(true);
                    //nameEt.setFocusable(false);
                }
            }
        }, 300);


        //Сделать ед измерения по умолчанию


       /* ArrayList<String> data0 = new ArrayList<>();
        data0.add("kg");
        data0.add("litr");
        data0.add("st");*/
       /* unitsList =MainApplication.dbHelper.getUnits();
        if(unitsList.size()==0){


            MainApplication.dbHelper.getOrInsertUnitFromDB(getString(R.string.sht));
            MainApplication.dbHelper.getOrInsertUnitFromDB(getString(R.string.kg));
            MainApplication.dbHelper.getOrInsertUnitFromDB(getString(R.string.but));
            unitsList = MainApplication.dbHelper.getUnits();
        }*/


        editSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(my, "onItemClick() position = " + position);
                if (position == arrayUnits.length - 1) {
                    Log.d(my,"Click unit!!!!!!!!!!");
                    showSoftInputPanel(editSpinner);
                    editSpinner.setEditable(true);
                    editSpinner.setText("");
                    editSpinner.setTextColor(ContextCompat.getColor(MainApplication.getAppContext(), R.color.dark_gray));
                } else editSpinner.setEditable(false);

                unitV.setText(editSpinner.getText());
            }
        });

        editSpinner.setOnShowListener(new EditSpinner.OnShowListener() {
            @Override
            public void onShow() {
                hideSoftInputPanel();
            }
        });

        // it converts the item in the list to a string shown in EditText.
        editSpinner.setItemConverter(new EditSpinner.ItemConverter() {
            @Override
            public String convertItemToString(Object selectedItem) {
                Log.d(my, "setItemConverter = " + selectedItem.toString());
                return selectedItem.toString();
            }
        });

        // select the first item initially

        editSpinner.setEditable(false);
        editSpinner.setSingleLine();
        editSpinner.setDropDownDrawableSpacing(50);
        editSpinner.setLineSpacing(40,40);

        addSpinnerAdapter();
        editSpinner.selectItem(currentUnitItem);
       // if (editSpinner.getText().length() == 0) editSpinner.setText(data[0]);

        editSpinner.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Log.d(my, "Entered unit is =" + editSpinner.getText());
                    String newUnit = editSpinner.getText().toString();
                    MainApplication.writeUnit(newUnit);

                    addSpinnerAdapter();
                    editSpinner.setEditable(false);
                    unitV.setText(editSpinner.getText());
                    return true;
                }
                return false;
            }
        });

        editSpinner.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d(my,"Long ");
                return false;
            }
        });


        Button cancelGood = (Button) v.findViewById(R.id.cancelGood);
        cancelGood.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });

        //

        createGoodBt = (Button) v.findViewById(R.id.createGood);
        createGoodBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String barc = barcodeET.getText().toString().trim();
                String name = nameEt.getText().toString();
                String unit = editSpinner.getText().toString();
                lastUnit=unit;
                Log.d(my, "Current unit = " + unit);

                int maxId = MainApplication.dbHelper.getMaxGoodId();
                Log.d("my", "maxGoodId = " + maxId);
                int id = MainActivityOld.GOOD_ID_CONST + maxId + 1;
                int grpId = MainActivityOld.GRP_OFFLINE_GOODS;

                if (name.length() == 0) name = "__";

                MainApplication.dbHelper.writeGood(id, grpId, name, barc, unit, barc, 0);

                if(getActivity() instanceof ScannerActivity) {
                    ScannerActivity sw = (ScannerActivity) getActivity();
                    boolean notAsk = notAskMore.isChecked();

                    String ns = "0";
                    if (notAsk) ns = "1";
                    MainApplication.dbHelper.insertOrReplaceOption(MainActivityOld.not_ask_create_good, ns);

                    sw.onClickGoodDialog(name, notAsk);
                }

                if(callback!=null) {
                    Message message = new Message();
                  /*  message.arg1 = good_id;
                    message.obj = newName;*/
                    callback.handleMessage(message);
                }

                dismiss();
            }
        });

        notAskMore = (CheckBox) v.findViewById(R.id.notAskMore);


        return v;
    }



    private static int currentUnitItem=0;

    private void addSpinnerAdapter() {
        unitsList = getUnits();
        unitsList.add("+ "+getString(R.string.unit_custom));

        currentUnitItem=0;
        if(lastUnit==null) lastUnit="";
        if(lastUnit.length()>0)
        {
            currentUnitItem = unitsList.indexOf(lastUnit);
        }
        String[] data = new String[unitsList.size()];

        data = unitsList.toArray(data);
      /*  adapter1 = new CustomizedSpinnerAdapter(
                CreateGoodDialog.this.getActivity(), android.R.layout.simple_spinner_item,
                data);*/

        arrayUnits = unitsList.toArray(new String[0]);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                CreateGoodDialog.this.getActivity(),R.layout.unit_spinner_item,arrayUnits
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.unit_spinner_item);

        editSpinner.setAdapter(spinnerArrayAdapter);

    }

    private void showEditUnit(boolean edit) {
        if (edit) {
            editSpinner.setVisibility(View.GONE);
            editUnit.setVisibility(View.VISIBLE);
            okUnitBt.setVisibility(View.VISIBLE);
            //adapter1.setOnItemClickListener(adapterClickListener);
        } else {
            editSpinner.setVisibility(View.VISIBLE);
            editUnit.setVisibility(View.GONE);
            okUnitBt.setVisibility(View.GONE);
            //adapter1.setOnItemClickListener(null);
        }
    }

    View.OnClickListener adapterClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView tv = (TextView) v;
            String str = tv.getText().toString();
            editUnit.setText(str);
        }
    };


   /* public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }*/


    private void hideSoftInputPanel() {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editSpinner.getWindowToken(), 0);
        }
    }

    private void showSoftInputPanel(View view) {
        InputMethodManager imm = (InputMethodManager) MainApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }
    }

    public void setCallback(Handler.Callback callback){
        this.callback=callback;
    }
}