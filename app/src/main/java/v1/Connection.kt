package v1

import android.util.Log
import com.app.barcodeclient3.MainApplication
import org.json.JSONArray
import org.json.JSONObject
import v1.excel.AndroidReadExcelActivity
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*

/**
 * Created by Vasyl Horodetskyi by 30.04.2021
 */
object Connection {

   /* private var connection: Connection? = null

    private var paramConnection: Properties? = null


    fun disconnect() {
        if (connection != null) try {
            connection!!.close()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        connection = null
    }

    fun initConnect(): Connection? {
        if (connection == null) connect()
        return connection
    }

    private fun connect() {
        paramConnection = Properties()
        paramConnection.setProperty("user", "SYSDBA")
        paramConnection.setProperty("password", "masterkey")
        paramConnection.setProperty("encoding", "UNICODE_FSS")
        val sCon = "jdbc:firebirdsql:" + MainApplication.databaseIP + "/3050:" + MainApplication.databasePath //E:/Unisystem/DB/DB.FDB";
        try {
            // register Driver
            Class.forName("org.firebirdsql.jdbc.FBDriver")
            // Get connection
            connection = DriverManager.getConnection(sCon, paramConnection)
            //And if you remove the comment in a row
            paramConnection.setProperty("encoding", "WIN1251")
            Log.d("my", "conn ok= " + connection.toString())
        } catch (e: Exception) {
            Log.d("my", "conn err= $e")
        }
    }


    fun isConnect(): Boolean {
        return if (connection == null) false else true
    }

    fun executeStatement(statement: String?): JSONArray? {
        var jsonArray: JSONArray? = null
        try {
            if (connection == null) {
                val msg = "Error in connection with SQL server"
                Log.d("my", "query statement conn err $msg")
                return jsonArray
            }
            val st = connection.createStatement()
            Log.d(AndroidReadExcelActivity.my, "executeStatement 1")
            val rs = st.executeQuery(statement)
            Log.d(AndroidReadExcelActivity.my, "executeStatement 2")
            val rsmd = rs.metaData
            Log.d(AndroidReadExcelActivity.my, "executeStatement 3")
            val cols = rsmd.columnCount
            val connInfo = Properties()
            connInfo["charSet"] = "UNICODE_FSS"
            Log.d(AndroidReadExcelActivity.my, "executeStatement 4")
            try {
                jsonArray = convertToJSON(rs)
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("my", "executeSt jsonArr err = $e")
            }
            rs.close()
            st.close()
            Log.d(AndroidReadExcelActivity.my, "executeStatement 5")
        } catch (e: SQLException) {
            Log.d("my", "MainApplication KFDB.There are problems with the query ******")
            Log.d("my", "MainApplication KFDB. $e")
            e.printStackTrace()
        }
        return jsonArray
    }


    fun executeProcedure(statement: String): Int {
        var result = -1
        Log.d(AndroidReadExcelActivity.my, "executeProcedure  $statement")
        try {
            if (connection == null) {
                val msg = "Error in connection with SQL server"
                Log.d("my", "query statement conn err $msg")
                return -1
            }
            val st = connection.createStatement()
            Log.d(AndroidReadExcelActivity.my, "executeProcedure 1")
            val res = st.executeUpdate(statement)
            result = res
            st.close()
            Log.d(AndroidReadExcelActivity.my, "executeProcedure 5")
        } catch (e: SQLException) {
            Log.d("my", "executeProcedure MainApplication KFDB.There are problems with the query ******")
            Log.d("my", "executeProcedure MainApplication KFDB. $e")
            e.printStackTrace()
            return -1
        }
        return result
    }


    @Throws(Exception::class)
    fun convertToJSON(resultSet: ResultSet): JSONArray? {
        val jsonArray = JSONArray()
        while (resultSet.next()) {
            val total_rows = resultSet.metaData.columnCount
            val obj = JSONObject()
            for (i in 0 until total_rows) {
                obj.put(resultSet.metaData.getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1))
            }
            jsonArray.put(obj)
        }
        return jsonArray
    }*/
}