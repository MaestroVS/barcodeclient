package v1.excel;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;





import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.barcodeclient3.R;
import com.opencsv.CSVReader;

import v1.data_model_offline_old.Good;
import ru.bartwell.exfilepicker.ExFilePicker;
import ru.bartwell.exfilepicker.data.ExFilePickerResult;

import com.app.barcodeclient3.MainApplication;

public class AndroidReadExcelActivity extends AppCompatActivity {


    TextView countOfGoods;


    ImportGoodsListAdapter<Good> importGoodsListAdapter;
    ArrayList<Good> goodsList = new ArrayList<>();

    ProgressDialog progressDialog;

    ListView list;

    TextView countOfGoodsOnMobile;
    ArrayList<Good> goodsListOnMobile;

    CheckBox clearGoodsCheckBox;

    private static final int EX_FILE_PICKER_RESULT = 0;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  Rollbar.reportMessage("AndroidReadExcelActivity...", "debug");
        setContentView(R.layout.excel_example_activity);

        getSupportActionBar().setDisplayOptions(getSupportActionBar().getDisplayOptions() | androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);

        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        View abarView = getSupportActionBar().getCustomView();


        TextView titleBar = abarView.findViewById(R.id.abarTitle);
        titleBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        titleBar.setText(getString(R.string.import_from_excel));

        ImageView statusButton = abarView.findViewById(R.id.statusButton);
        statusButton.setVisibility(View.GONE);

        ImageView settingsButton = abarView.findViewById(R.id.settingsButton);
        settingsButton.setVisibility(View.INVISIBLE);

        clearGoodsCheckBox = findViewById(R.id.clearGoodsCheckBox);
        clearGoodsCheckBox.setChecked(true);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BACK ICON!!!!!!!!!!!!!!!!!
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setIcon(android.R.color.transparent);


        final Toolbar parent = (Toolbar) abarView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        list = findViewById(R.id.list);
      /*  list.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/


        countOfGoods =  findViewById(R.id.countOfGoods);
        countOfGoodsOnMobile = findViewById(R.id.countOfGoodsOnMobile);

        Button chooseFile =  findViewById(R.id.chooseFileBt);
        chooseFile.setOnClickListener(v -> showFileChooser2());

        Button clearGoodsBt =  findViewById(R.id.clearGoodsBt);
        clearGoodsBt.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(AndroidReadExcelActivity.this);
            builder.setTitle("Delete all goods?");


// Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainApplication.dbHelper.clear_AC_GOODS_CNT();
                    MainApplication.dbHelper.clearDic_Goods();
                    list.setAdapter(null);
                    list.refreshDrawableState();
                    if (goodsListOnMobile != null) goodsListOnMobile.clear();
                    countOfGoodsOnMobile.setText("0");
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();

        });
        h0.postDelayed(() -> {
            goodsListOnMobile = MainApplication.dbHelper.getGoodList();
            if (goodsListOnMobile != null) {
                int l = goodsListOnMobile.size();
                countOfGoodsOnMobile.setText("" + l);

                ImportGoodsListAdapter adapter = new ImportGoodsListAdapter<>(AndroidReadExcelActivity.this, R.layout.row_excel_good, goodsListOnMobile);
                list.setAdapter(adapter);
            }
        }, 300);


    }

    Handler h0 = new Handler(Looper.getMainLooper());
    Handler readExcelFileHandler = new Handler();

    public static final String my = "my";


    class ReadEXLfile extends Thread { ///!!!проверить
        String filename = "";

        public ReadEXLfile(String filename) {
            this.filename = filename;
        }


        public void run() {


            Log.d("my", "ReadEXLfile filename = " + filename);

            if (filename == null) {
                h0.post(() -> {
                    if (progressDialog != null) progressDialog.hide();
                    Log.d(my, "error open file!!! 1");
                    errOpenFileDialog(filename);
                });

                return;
            }
            Log.d("my", "AndroidReadExcelActivity success open file! = " + filename);


            if (!FileLoader.isExternalStorageAvailable() || FileLoader.isExternalStorageReadOnly()) {
                Log.w("FileUtils", "Storage not available or read only");
                Log.d(my, "Storage not available or read only");
                return;
            }

            if (!filename.contains(".")) return;
            int a = filename.lastIndexOf(".");
            if (a < 0) return;
            String postfix = filename.substring(a);
            Log.d(my, "postfix = " + postfix);
            if (postfix == null) return;
            if (postfix.length() == 0) return;

            if (postfix.equals(".xls")) {

                try {
                    Log.d("my", "myCell ++ xls");
                    // Creating Input Stream
                    // File file = FileLoader.getExcelFileFromSD(filename);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    File file = FileLoader.getExcelFileWithPathFromSD(filename);
                    FileInputStream myInput = new FileInputStream(file);
                    if (myInput == null) Log.d("my", "myInput is null");
                    else Log.d("my", "myInput ok!");

                    // Create a POIFSFileSystem object
                    POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

                    // Create a workbook using the File System
                    HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
                    Log.d("my", "getSheets =" + myWorkBook.getActiveSheetIndex());

                    // Get the first sheet from workbook
                    HSSFSheet mySheet = myWorkBook.getSheetAt(0);

                    /** We now need something to iterate through the cells.**/
                    Iterator<Row> rowIter = mySheet.rowIterator();

                    //int n_id

                    if(clearGoodsCheckBox.isChecked()) {

                        MainApplication.dbHelper.clear_AC_GOODS_CNT();
                        MainApplication.dbHelper.clearDic_Goods();
                        MainApplication.dbHelper.clearDic_Goods_GRP();
                    }


                    int iid = 0;
                    while (rowIter.hasNext()) {
                        HSSFRow myRow = (HSSFRow) rowIter.next();
                        int num = myRow.getRowNum();

                        int cnt = myRow.getLastCellNum();
                        Log.d("my", " num =" + num + " last = " + cnt);
                        String id = "";
                        String name = "";
                        String article = "";
                        String unit = "";
                        String barcode = "";
                        double price = 0;


                        Iterator<Cell> cellIter = myRow.cellIterator();
                        while (cellIter.hasNext()) {
                            HSSFCell myCell = (HSSFCell) cellIter.next();
                            int index = myCell.getColumnIndex();
                            //  Log.d("my", "myCell = " + myCell + " column = " + index);

                            switch (index) {
                                case 0:
                                    id = myCell.toString();
                                    break;
                                case 1:
                                    name = myCell.toString();
                                    break;
                                case 2:
                                    article = myCell.toString();
                                    break;
                                case 3:
                                    unit = myCell.toString();
                                    break;
                                case 4:

                                    barcode = myCell.toString();

                                    try {
                                        //double bc = Double.parseDouble(barcode);
                                        //int bb = (int) bc;
                                        long barc = Long.parseLong(barcode);
                                        barcode = Long.toString(barc);

                                    } catch (Exception e) {

                                    }
                                    break;

                                case 5:
                                    try {

                                        String priceStr = myCell.toString();
                                        Log.d(my, "price String ============== " + priceStr);
                                        price = Double.parseDouble(priceStr);
                                    } catch (Exception e) {
                                        Log.d("my", "err read Excel = " + e.toString());
                                    }
                            }
                        }

                        if (num > 0) {
                            Log.d("my", ">>>>>" + name + " ");
                            if (barcode.length() > 0 && article.length() == 0) {
                                article = barcode;
                            }

                            if (name.length() > 0 && article.length() > 0 && unit.length() > 0 && id.length() > 0) {


                                if (name.contains("'")) {
                                    name = name.replaceAll("'", "\''");
                                    Log.d("my", "replacer: " + name);
                                }


                                Good good = new Good(iid, 0, name, unit, article);
                                Log.d("my","*-*-*-**-******* barc3 = "+barcode);
                                ArrayList<String> barcs = new ArrayList<>();
                                barcs.add(barcode);
                                good.setBarcodes(barcs);
                                good.setOut_price(price);

                                Log.d("my", "***************good: " + good.toString());
                                goodsList.add(good);
                                iid = MainApplication.dbHelper.getMaxGoodId() + 1;
                                MainApplication.dbHelper.writeGood(iid, 0, name, article, unit, barcode, price);
                            }
                        }

                        iid++;

                    }
                    MainApplication.dbHelper.writeGoodGRP(0, 0, "Excel");


                    updateAdapterH.post(new Runnable() {
                        @Override
                        public void run() {

                            ArrayList<Good> totalList = new ArrayList<>();

                            totalList.addAll(goodsList);

                            int q=0;
                            String log ="";
                            for(Good good:goodsList)
                            {
                               String name = good.getName();
                               if(name.length()>10) name=name.substring(0,10);
                               log+=name;
                               log+=", ";
                            }



                            if (goodsListOnMobile != null) totalList.addAll(goodsListOnMobile);

                            importGoodsListAdapter = new ImportGoodsListAdapter<>(AndroidReadExcelActivity.this, R.layout.row_excel_good, goodsList);
                            list.setAdapter(importGoodsListAdapter);
                            countOfGoods.setText("" + goodsList.size());

                            countOfGoodsOnMobile.setText("" + totalList.size());


                            if (progressDialog != null) progressDialog.hide();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("my", "err open=" + e.toString());


                }
////////////////////////////////////////////////////////////////////////

            }
            if (postfix.equals(".xlsx")) {
                Log.d(my, "We have xlsx");
                parseXLSX(filename);
            }

            if (postfix.equals(".csv")) {
                Log.d("my", "import csv");

                readCsv(AndroidReadExcelActivity.this, filename);

                // String filename = "C:\Book.csv";


                System.out.println("Starting to parse CSV file using opencsv");
                parseUsingOpenCSV(filename);

            }


            if (!(filename.contains(".xls") || filename.contains(".csv"))) {
                h0.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(my, "File not contains 2 xls or csv " + filename);

                        if (progressDialog != null) progressDialog.hide();
                        errOpenFileDialog(filename);
                    }
                });

            }


            return;
        }
    }


    private void parseXLSX(String filename) {
       /* try {
            Log.d("my", "myCell ++ xls");
            // Creating Input Stream
            // File file = FileLoader.getExcelFileFromSD(filename);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            File file = FileLoader.getExcelFileWithPathFromSD(filename);
            // FileInputStream myInput = new FileInputStream(file);
            // if (myInput == null) Log.d("my", "myInput is null");
            // else Log.d("my", "myInput ok!");

            // Create a POIFSFileSystem object
            // POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System
            // HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            FileInputStream filestr = new FileInputStream(file);
            if (filestr == null) {
                Log.d("my", "myInput  xlsx is null");
                return;
            }
            XSSFWorkbook myWorkBook = new XSSFWorkbook(filestr);
            Log.d("my", "getSheets =" + myWorkBook.getActiveSheetIndex());

            // Get the first sheet from workbook
            // HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            XSSFSheet sheet = myWorkBook.getSheetAt(0);


            // Iterator<Row> rowIter = mySheet.rowIterator();
            Iterator<Row> rowIterator = sheet.iterator();

            //int n_id

            MainApplication.dbHelper.clear_AC_GOODS_CNT();
            MainApplication.dbHelper.clearDic_Goods();
            MainApplication.dbHelper.clearDic_Goods_GRP();


            int iid = 0;
            while (rowIterator.hasNext()) {
                // HSSFRow myRow = (HSSFRow) rowIterator.next();
                org.apache.poi.xssf.usermodel.XSSFRow myRow = (org.apache.poi.xssf.usermodel.XSSFRow) rowIterator.next();
                int num = myRow.getRowNum();

                int cnt = myRow.getLastCellNum();
                Log.d("my", " num =" + num + " last = " + cnt);
                String id = "";
                String name = "";
                String article = "";
                String unit = "";
                String barcode = "";
                double price = 0;


                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {
                    // HSSFCell myCell = (HSSFCell) cellIter.next();
                    org.apache.poi.xssf.usermodel.XSSFCell myCell = (org.apache.poi.xssf.usermodel.XSSFCell) cellIter.next();
                    int index = myCell.getColumnIndex();
                    //  Log.d("my", "myCell = " + myCell + " column = " + index);

                    switch (index) {
                        case 0:
                            id = myCell.toString();
                            break;
                        case 1:
                            name = myCell.toString();
                            break;
                        case 2:
                            article = myCell.toString();
                            break;
                        case 3:
                            unit = myCell.toString();
                            break;
                        case 4:

                            barcode = myCell.toString();
                            try {
                                double bc = Double.parseDouble(barcode);
                                int bb = (int) bc;
                                barcode = Integer.toString(bb);
                            } catch (Exception e) {
                            }
                            break;

                        case 5:
                            try {
                                Log.d(my, "price class = " + myCell.getClass().toString());
                                String priceStr = myCell.toString();
                                Log.d(my, "price String ============== " + priceStr);
                                price = Double.parseDouble(priceStr);
                            } catch (Exception e) {
                                Log.d("my", "err read Excel = " + e.toString());
                            }


                    }
                }

                if (num > 0) {
                    Log.d("my", ">>>>>" + name + " ");
                    if (barcode.length() > 0 && article.length() == 0) {
                        article = barcode;
                    }

                    if (name.length() > 0 && article.length() > 0 && unit.length() > 0 && id.length() > 0) {


                        if (name.contains("'")) {
                            name = name.replaceAll("'", "\''");
                            Log.d("my", "replacer: " + name);
                        }


                        Good good = new Good(iid, 0, name, unit, article);
                        ArrayList<String> barcs = new ArrayList<>();
                        barcs.add(barcode);
                        good.setBarcodes(barcs);
                        good.setOut_price(price);

                        Log.d("my", "***************good: " + good.toString());
                        goodsList.add(good);
                        iid = MainApplication.dbHelper.getMaxGoodId() + 1;
                        MainApplication.dbHelper.writeGood(iid, 0, name, article, unit, barcode, price);
                    }
                }

                iid++;

            }
            MainApplication.dbHelper.writeGoodGRP(0, 0, "Excel");


            updateAdapterH.post(new Runnable() {
                @Override
                public void run() {

                    ArrayList<Good> totalList = new ArrayList<>();

                    totalList.addAll(goodsList);

                    if (goodsListOnMobile != null) totalList.addAll(goodsListOnMobile);

                    importGoodsListAdapter = new ImportGoodsListAdapter<>(AndroidReadExcelActivity.this, R.layout.row_excel_good, goodsList);
                    list.setAdapter(importGoodsListAdapter);
                    countOfGoods.setText("" + goodsList.size());

                    countOfGoodsOnMobile.setText("" + totalList.size());

                    if (progressDialog != null) progressDialog.hide();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("my", "err open=" + e.toString());


        }*/
    }

    private void errOpenFileDialog(String filename) {

        Log.d(my, "File not found errOpenFileDialog " + filename);
        // Toast.makeText(AndroidReadExcelActivity.this, getString(R.string.cant_open_file), Toast.LENGTH_SHORT);
        AlertDialog.Builder builder = new AlertDialog.Builder(AndroidReadExcelActivity.this);
        builder.setMessage(getString(R.string.cant_open_file) + "  \n\r" + filename)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    Handler updateAdapterH = new Handler();


    private void parseUsingOpenCSV(String filename) {
        CSVReader reader;
        try {
            reader = new CSVReader(new FileReader(filename));
            String[] row;
            List<?> content = reader.readAll();

            int iid = 0;
            int first = 0;
            for (Object object : content) {
                row = (String[]) object;

                String id = "";
                String name = "";
                String article = "";
                String unit = "";
                String barcode = "";

                Log.d("my", ">>>>>>>>" + row.toString());
                for (int i = 0; i < row.length; i++) {
                    // display CSV values
                    System.out.println("Cell column index: " + i);
                    System.out.println("Cell Value: " + row[i]);
                    System.out.println("-------------");
                    Log.d("my", "import csv ok = ");
                    Log.d("my", " = " + row[i]);

                    switch (i) {
                        case 0:
                            id = row[i].toString();
                            Log.d("my", ">id" + id);
                            break;
                        case 1:
                            name = row[i].toString();
                            Log.d("my", ">name" + name);
                            break;
                        case 2:
                            article = row[i].toString();
                            Log.d("my", ">article" + article);
                            break;
                        case 3:
                            unit = row[i].toString();
                            Log.d("my", ">unit" + unit);
                            break;
                        case 4:

                            barcode = row[i].toString();
                            try {
                                double bc = Double.parseDouble(barcode);
                                int bb = (int) bc;
                                barcode = Integer.toString(bb);
                                Log.d("my", ">barcode" + barcode);
                            } catch (Exception e) {
                            }
                            break;
                    }
                }

                if (row.length > 0) {
                    Log.d("my", ">>>>>YYYYY" + name + " ");
                    if (barcode.length() > 0 && article.length() == 0) {
                        article = barcode;
                    }
                    Log.d("my", "ok 1");

                    if (first > 0)
                        if (name.length() > 0 && article.length() > 0 && unit.length() > 0 && id.length() > 0) {

                            Log.d("my", "ok 2");


                            if (name.contains("'")) {
                                name = name.replaceAll("'", "\''");
                                Log.d("my", "replacer: " + name);
                            }


                            Good good = new Good(iid, 0, name, unit, article);
                            ArrayList<String> barcs = new ArrayList<>();
                            barcs.add(barcode);
                            good.setBarcodes(barcs);

                            Log.d("my", "***************good: " + good.toString());
                            goodsList.add(good);
                            iid = MainApplication.dbHelper.getMaxGoodId() + 1;
                            MainApplication.dbHelper.writeGood(iid, 0, name, article, unit, barcode, 0);
                        }
                }
                first++;
            }

            try {
                MainApplication.dbHelper.writeGoodGRP(0, 0, "Excel");
            } catch (Exception e) {

            }

            importGoodsListAdapter = new ImportGoodsListAdapter<>(AndroidReadExcelActivity.this, R.layout.row_excel_good, goodsList);
            list.setAdapter(importGoodsListAdapter);
            countOfGoods.setText("" + goodsList.size());
            Log.d("my", " success import from CSV!!!");


        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            Log.d("my", "import csv err2 = " + e.toString());
            System.err.println(e.getMessage());
        }
    }


    public final List<String[]> readCsv(Context context, String CSV_PATH) {
        List<String[]> questionList = new ArrayList<String[]>();


        AssetManager assetManager = context.getAssets();
        try {
            InputStreamReader is = new InputStreamReader(assetManager
                    .open(CSV_PATH));
            Log.d("my", "import csv ok = ");

            BufferedReader reader = new BufferedReader(is);
            reader.readLine();
            String line;
            while ((line = reader.readLine()) != null) {

            }
        } catch (IOException e) {
            Log.d("my", "import csv err = " + e.toString());
            e.printStackTrace();
        }


        return questionList;
    }


    ///file chooser

    private static final int FILE_SELECT_CODE = 0;

    private void showFileChooser2()
    {
        ExFilePicker exFilePicker = new ExFilePicker();
        exFilePicker.start(this, EX_FILE_PICKER_RESULT);
    }

    /*private void showFileChooser() {


        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            Intent intent = new Intent();
           // intent.setType("*\/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);


            try {
                startActivityForResult(intent, FILE_SELECT_CODE);
            } catch (android.content.ActivityNotFoundException ex) {

                Toast.makeText(this, "Please install a File Manager.",
                        Toast.LENGTH_SHORT).show();
            }


        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*\/*");


            try {
                startActivityForResult(intent, FILE_SELECT_CODE);
            } catch (android.content.ActivityNotFoundException ex) {

                Toast.makeText(this, "Please install a File Manager.",
                        Toast.LENGTH_SHORT).show();
            }
        }


    }*/

    String chooseFilepath = "";

    Uri uri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == EX_FILE_PICKER_RESULT) {
            ExFilePickerResult result = ExFilePickerResult.getFromIntent(data);
            if (result != null && result.getCount() > 0) {
                // Here is object contains selected files names and path
                if(result!=null) {
                    Log.d("my", "result file = " + result.getPath());
                    Log.d("my", "result file = " + result.getNames());
                    String path = result.getPath()+result.getNames().get(0);
                    chooseFilepath = path;

                   /* if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        File file = new File(uri.getPath());//create path from uri
                        final String[] split = file.getPath().split(":");//split the path.
                        chooseFilepath = split[1];//assign it to a string(your choice).
                    }else{
                        try {
                            chooseFilepath= PathUtil.getPath(AndroidReadExcelActivity.this,uri);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }*/
                    Log.d("my", "File Path: " + chooseFilepath);
                    h0.postAtTime(() -> {
                        progressDialog = new ProgressDialog(AndroidReadExcelActivity.this);
                        progressDialog.setTitle("Waiting");
                        progressDialog.setMessage("Load goods...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        ReadEXLfile readEXLfile = new ReadEXLfile(chooseFilepath);
                        readEXLfile.start();

                    }, 300);
                }else{
                    Log.d("my","Result file is null");
                }
            }else{
            }
        }

       /* switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    uri = data.getData();
                    Log.d("my", "File Uri: " + uri.toString());
                    // Get the path
                    try {

                        chooseFilepath = null;//getPath(this, uri);


                        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                            File file = new File(uri.getPath());//create path from uri
                            final String[] split = file.getPath().split(":");//split the path.
                            chooseFilepath = split[1];//assign it to a string(your choice).
                        }else{
                            chooseFilepath= PathUtil.getPath(AndroidReadExcelActivity.this,uri);
                        }
                        Log.d("my", "File Path new : " + uri.getPath());
                        Log.d("my", "File Path: " + chooseFilepath);
                        // Get the file instance
                        // File file = new File(path);
                        // Initiate the upload


                        h0.postAtTime(() -> {
                            progressDialog = new ProgressDialog(AndroidReadExcelActivity.this);
                            progressDialog.setTitle("Waiting");
                            progressDialog.setMessage("Load goods...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            ReadEXLfile readEXLfile = new ReadEXLfile(chooseFilepath);
                            readEXLfile.start();

                        }, 500);

                    } catch (Exception e) {

                    }

                }
                break;
        }*/
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                finish();
                return true;
        }


        return super.onOptionsItemSelected(item);

    }


}
