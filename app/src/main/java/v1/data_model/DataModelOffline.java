package v1.data_model;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.app.barcodeclient3.MainApplication;
import com.app.barcodeclient3.R;

import java.util.ArrayList;
import java.util.Map;

import v1.data_model_offline_old.Good;
import v2.data.firebird.models.Inventory;

import static com.app.barcodeclient3.MainApplication.h;

/**
 * Created by MaestroVS on 10.01.2018.
 */

public class DataModelOffline implements DataModelInterface {

    private static  DataModelOffline ourInstance;

    public static DataModelOffline getInstance() {
        if(ourInstance==null) ourInstance = new DataModelOffline();
        return ourInstance;
    }

    @Override
    public void getInventoryList(Callback callback) {

        final Callback call1=callback;
        Log.d("my","getInventoryList offline...");

        Thread thread = new Thread(){
            @Override
            public void run() {
                ArrayList<Inventory> inventories = new ArrayList<>();

                Log.d("my","inv get is connect ok");
                ArrayList<v1.data_model_offline_old.Inventory> invlst = MainApplication.dbHelper.getInventoryList();
                //RequestInventoryList requestInventoryList = new RequestInventoryList();
                //ArrayList<Inventory> invlst = requestInventoryList.getInventoryList();

                Log.d("my","inv get inv lst ="+invlst);


                for(v1.data_model_offline_old.Inventory inv : invlst){
                    Inventory inventoryJSON = new Inventory((long)inv.getId(),
                            inv.getNum(),inv.getDatetime(),
                            (long)inv.getSubdivision().getId(),inv.getSubdivision().getName(),inv.getDocState());
                    inventories.add(inventoryJSON);
                }



                final Response response = new Response(inventories);
                Log.d("my","inv send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();

    }

    @Override
    public void getDatabaseVersion(Callback callback) {
        final Callback call1=callback;
        final Response response = new Response("offline");
        call1.sendResult(response);


    }

    @Override
    public void getGoodsList(Map<String, String> map, Callback callback) {

        final Callback call1=callback;
        Log.d("my","getGoodsList offline...");
        final Map<String, String> options = map;


        Thread thread = new Thread(){
            @Override
            public void run() {
                ArrayList<GoodJSON> goods = new ArrayList<>();
                ArrayList<Good> goodsList = new ArrayList<>();

                Log.d("my","inv get is connect ok");

                if(options.containsKey(INVENTORY_ID)) {
                    String hdId = options.get(INVENTORY_ID);
                    try{
                        int id = Integer.parseInt(hdId);
                        goodsList = MainApplication.dbHelper.getGoodList("","","",id);
                    }catch (Exception e){}

                }
                else {
                    goodsList = MainApplication.dbHelper.getGoodList();
                }


                Log.d("my","goods get  lst ="+goodsList);


                for(Good good : goodsList){
                    GoodJSON goodJSON = good.toGoodJSON();
                    goods.add(goodJSON);
                }



                final Response response = new Response(goods);
                Log.d("my","goods send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();

    }

    @Deprecated
    @Override
    public void getGoodsListAndSaveToSQLite(Map<String,String> options, Context context, Callback callback) {

    }

    @Override
    public void getGoodsGrpList(Callback callback) {

        final Callback call1 = callback;


        Thread thread = new Thread() {
            @Override
            public void run() {
                ArrayList<GoodGrpJSON> goodsGrpList = new ArrayList<>();

                GoodGrpJSON goodJSON = new GoodGrpJSON();// gson.fromJson(obj.toString(), GoodGrpJSON.class);
                goodJSON.setId(0);
                goodJSON.setParent_id(0);
                goodJSON.setName("rootrootroot");
                goodsGrpList.add(goodJSON);

                GoodGrpJSON goodJSON1 = new GoodGrpJSON();// gson.fromJson(obj.toString(), GoodGrpJSON.class);
                goodJSON.setId(1);
                goodJSON.setParent_id(0);//
                goodJSON.setName(MainApplication.getAppContext().getString(R.string.goods));
                goodsGrpList.add(goodJSON1);


                final Response response = new Response(goodsGrpList);
                Log.d("my", "goods send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);

                    }
                });

            }
        };
        thread.start();

    }

    public String createInventory(int id, String num, String datetime, String subdiv, int doc_type ){
       return MainApplication.dbHelper.insertInventoryDoc( num, datetime,subdiv , doc_type);
    }

    @Override
    public void sendEditInventoryDt(int invId, int goodId, float cnt,@NonNull Callback callback) {

    }

    @Deprecated
    @Override
    public void getGoodsCount(Callback callback) {

    }



    @Override
    public void sendEditInventoriesList(int invId, ArrayList<Good> goods, @NonNull Callback callback) {

    }
}
