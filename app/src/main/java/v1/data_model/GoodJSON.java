package v1.data_model;

/**
 * Created by MaestroVS on 21.12.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import v1.data_model_offline_old.Good;

public class GoodJSON  implements GoodIntf{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("grp_id")
    @Expose
    private Integer grp_id;

    @SerializedName("article")
    @Expose
    private String article;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("barcode")
    @Expose
    private String barcode;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("cnt_fact")
    @Expose
    private float cnt_fact;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getName() {
        return name;
    }

    private GoodGrpJSON parent;

    public GoodGrpJSON getParent() {
        return parent;
    }

    public void setParent(GoodGrpJSON parent) {
        this.parent = parent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getUnit() {
        return unit;
    }

    public float getPrice() {
        return price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getCnt_fact() {
        return cnt_fact;
    }

    public void setCnt_fact(float cnt_fact) {
        this.cnt_fact = cnt_fact;
    }

    /**
     *
     * @return convert to old Good type
     */
    public Good toGoodEssence()
    {
        Good good = new Good(id,0,name,unit,article);
        ArrayList<String> barcodes = new ArrayList<>();
        barcodes.add(barcode);
        good.setBarcodes(barcodes);
        good.setOut_price(price);
        good.setFcnt(cnt_fact);
        return good;
    }

    public Integer getGrp_id() {
        return grp_id;
    }

    public void setGrp_id(Integer grp_id) {
        this.grp_id = grp_id;
    }
}


