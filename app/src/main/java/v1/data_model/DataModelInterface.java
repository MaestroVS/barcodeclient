package v1.data_model;





import android.content.Context;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Map;

import v1.data_model_offline_old.Good;

/**
 * Created by MaestroVS on 19.12.2017.
 */

public interface DataModelInterface {

    String ID="id";
    String NAME="name";
    String ARTICLE="article";
    String BARCODE="barcode";
    String ALL="ALL";
    String GRP_ID="GRP_ID";
    String INVENTORY_ID="HD_ID";

    void getInventoryList(Callback callback);

    void getDatabaseVersion(Callback callback);

    /**
     *
     * @param options  Search by: 'name','article','barcode'
     * @param callback
     */
    void getGoodsList(Map<String, String> options, Callback callback);

    void getGoodsListAndSaveToSQLite(Map<String, String> options,Context context, Callback callback );

    void getGoodsGrpList(Callback callback);

    String createInventory(int id, String num, String datetime, String subdiv, int doc_type );

    void sendEditInventoryDt(int invId,int goodId,float cnt,@NonNull Callback callback);

     void getGoodsCount(Callback callback);



    void sendEditInventoriesList(int invId, ArrayList<Good> goods, @NonNull Callback callback);
}
