package v1.data_model;

/**
 * Created by MaestroVS on 06.02.2018.
 */

public interface GoodIntf {

    Integer getId();

    String getName();

    GoodGrpJSON getParent();

    void setParent(GoodGrpJSON parent);
}
