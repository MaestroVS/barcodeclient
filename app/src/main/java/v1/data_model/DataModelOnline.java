package v1.data_model;

import android.content.Context;

import androidx.annotation.NonNull;

import android.util.Log;

import com.app.barcodeclient3.MainApplication;
import com.app.barcodeclient3.R;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;


import v1.data_model_offline_old.Good;
import v2.data.firebird.models.Inventory;

import static v1.excel.AndroidReadExcelActivity.my;
import static com.app.barcodeclient3.MainApplication.executeProcedure;
import static com.app.barcodeclient3.MainApplication.executeStatement;
import static com.app.barcodeclient3.MainApplication.getAppContext;
import static com.app.barcodeclient3.MainApplication.h;
import static com.app.barcodeclient3.MainApplication.isConnect;

/**
 * Created by MaestroVS on 19.12.2017.
 */

public class DataModelOnline implements DataModelInterface {
    private static DataModelOnline ourInstance;

    public static DataModelOnline getInstance() {
        if (ourInstance == null) ourInstance = new DataModelOnline();
        return ourInstance;
    }

    @SerializedName("uniqid")
    @Expose
    String str;


    private DataModelOnline() {
    }

    @Override
    public void getInventoryList(@NonNull Callback callback) {

        final Callback call1 = callback;

        Thread thread = new Thread() {
            @Override
            public void run() {
                ArrayList<Inventory> inventories = new ArrayList<>();
                if (isConnect()) {
                    Log.d("my", "inv get is connect ok");
                    String statement = "select JB.ID, JB.NUM, JB.date_time, JB.DOC_STATE , "
                            + " (select DS.ID        from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_ID, "
                            + " (select DS.NAME         from DIC_SUBDIVISION DS   where DS.ID = JB.subdivision_id) as SUBDIVISION_NAME "
                            + " from jor_inventory_act JB "
                            + " 	where  JB.subdivision_id is not null and JB.doc_state = 0"
                            + " order by JB.DATE_TIME ";
                    JSONArray jsonArray = executeStatement(statement);
                    Log.d("my", "inv get stm =" + statement);
                    if (jsonArray != null) {
                        Log.d("my", "inv list not null size = " + jsonArray.length());
                        Log.d("my", "inv list = " + jsonArray.toString());

                        //[{"RESULT":"2.1.186.0"}]
                        if (jsonArray.length() > 0) {
                           /* try {
                                JSONObject obj = jsonArray.getJSONObject(0);
                                dbver = obj.getString("RESULT");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/

                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Gson gson = new Gson();
                                    Inventory inventory = gson.fromJson(obj.toString(), Inventory.class);
                                    if (inventory != null) {
                                        Log.d("my", "inventory ===+++= ok" + inventory.getSubdivisionName());
                                        inventories.add(inventory);
                                    } else Log.d("my", "inventory =---err");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("my", "inventory =---errtttt" + e.toString());
                                }
                            }

                        }
                    } else {
                        Log.d("my", "inv get is jsonArr null2");
                    }
                }
                final Response response = new Response(inventories);
                Log.d("my", "inv send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();


    }


    @Override
    public void getGoodsCount(@NonNull Callback callback) {

        final Callback call1 = callback;

        Thread thread = new Thread() {
            @Override
            public void run() {

                int count = 0;
                if (isConnect()) {
                    String statement = "select count(dg.id) from dic_goods dg";
                    JSONArray jsonArray = executeStatement(statement);
                    if (jsonArray != null) {

                        Log.d("my", "DB ver = " + jsonArray.toString());

                        //[{"RESULT":"2.1.186.0"}]
                        if (jsonArray.length() > 0) {
                            try {
                                JSONObject obj = jsonArray.getJSONObject(0);
                                Log.d(my, "getGoodsCount = " + obj.toString());
                                count = obj.getInt("count");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("my", "goods count= " + count);
                    }
                } else {
                    Log.d(my, "connect error");
                }
                final Response response = new Response(count);

                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });
            }
        };
        thread.start();
    }

    @Override
    public void getDatabaseVersion(@NonNull Callback callback) {

        final Callback call1 = callback;

        Thread thread = new Thread() {
            @Override
            public void run() {

                String dbver = null;
                if (isConnect()) {
                    String statement = "execute procedure sp_get_version";
                    JSONArray jsonArray = executeStatement(statement);
                    if (jsonArray != null) {

                        Log.d("my", "DB ver = " + jsonArray.toString());

                        //[{"RESULT":"2.1.186.0"}]
                        if (jsonArray.length() > 0) {
                            try {
                                JSONObject obj = jsonArray.getJSONObject(0);
                                dbver = obj.getString("result");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("my", "DB ver dbver= " + dbver);
                    }
                }
                final Response response = new Response(dbver);

                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });
            }
        };
        thread.start();
    }


    @Override
    public void getGoodsList(Map<String, String> map, @NonNull Callback callback) {


        final Callback call1 = callback;
        final Map<String, String> options = map;


        Thread thread = new Thread() {
            @Override
            public void run() {
                ArrayList<GoodJSON> goodsList = new ArrayList<>();
                if (isConnect()) {
                    Log.d("my", "goods get is connect ok");
                    String statement = getAppContext().getString(R.string.request_goods);

                    if (options.containsKey(INVENTORY_ID)) {
                        String hdId = options.get(INVENTORY_ID);
                        if (hdId == null) hdId = "null";
                        statement += ",       (select sum(jad.cnt_fact) from jor_inventory_act_dt jad\n" +
                                "        where jad.hd_id = " + hdId.trim() + " and jad.goods_id = DG.ID ) as CNT_FACT\n" +
                                "from DIC_GOODS DG ";
                    } else {
                        statement += ",       (select sum(jad.cnt_fact) from jor_inventory_act_dt jad\n" +
                                "        where jad.hd_id = null and jad.goods_id = DG.ID ) as CNT_FACT\n" +
                                "from DIC_GOODS DG ";
                    }


                    boolean or = false;
                    boolean where = false;
                    boolean and = false;

                    if (!options.containsKey(ALL)) {
                        if (options.containsKey(NAME) || options.containsKey(ARTICLE) || options.containsKey(BARCODE)) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);

                            if (options.containsKey(NAME)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String name = options.get(NAME).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_name);
                                statement += ("'" + name + "'");
                                or = true;
                                and = true;
                            }

                            if (options.containsKey(ARTICLE)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String article = options.get(ARTICLE).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_article);
                                statement += ("'" + article + "'");
                                or = true;
                                and = true;
                            }

                            if (options.containsKey(BARCODE)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String barcode = options.get(BARCODE).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_barcode);
                                statement += ("'" + barcode + "')");
                                or = true;
                                and = true;
                            }
                        }
                    }
                    if (options.containsKey(GRP_ID)) {
                        if (!where) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);
                        } else statement += getAppContext().getString(R.string.request_and);
                        statement += "  DG.GRP_ID=" + options.get(GRP_ID);
                    }

                    if (options.containsKey(ID)) {
                        if (!where) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);
                        } else statement += getAppContext().getString(R.string.request_and);
                        statement += "  DG.ID=" + options.get(ID);
                    }

                    if (!where) {
                        where = true;
                        statement += getAppContext().getString(R.string.request_where);
                    } else statement += getAppContext().getString(R.string.request_and);
                    statement += "  DG.is_service=0 order by DG.NAME"; //!!!!!!!!! проверка, что товар не является услугой

                    Log.d("getGoodsList", "goods get stm 1 =" + statement);
                    JSONArray jsonArray = executeStatement(statement);
                    Log.d("getGoodsList", "goods get stm 2=" + statement);
                    if (jsonArray != null) {
                        Log.d("my", "goods list not null size = " + jsonArray.length());
                        // Log.d("my", "goods list = " + jsonArray.toString());
                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {

                                Log.d("getGoodsList", "for i=" + i);
                                try {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Gson gson = new Gson();
                                    GoodJSON goodJSON = gson.fromJson(obj.toString(), GoodJSON.class);
                                    if (goodJSON != null) {
                                        // Log.d("my","goods ===+++= ok"+goodJSON.getName());
                                        goodsList.add(goodJSON);
                                    } else Log.d("my", "goods =---err");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("my", "goods =---errtttt" + e.toString());
                                }
                            }

                        }
                    } else {
                        Log.d("my", "goods get is jsonArr null");
                    }
                } else {
                    Log.d(my, "Can't connect to server");
                }
                final Response response = new Response(goodsList);
                Log.d("my", "goods send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();


    }


    /**
     * @param map
     * @param context
     * @param callback onError send error or progress in percent
     */
    public void getGoodsListAndSaveToSQLite(Map<String, String> map, Context context, @NonNull Callback callback) {


        final Callback call1 = callback;
        final Map<String, String> options = map;


        Thread thread = new Thread() {
            @Override
            public void run() {
                ArrayList<GoodJSON> goodsList = new ArrayList<>();
                if (isConnect()) {
                    Log.d("my", "goods get is connect ok");
                    String statement = getAppContext().getString(R.string.request_goods);
//(select sum(jad.cnt_fact) from jor_inventory_act_dt jad
//            where jad.hd_id = 10 and jad.goods_id = DG.ID) as CNT_FACT
                    if (options.containsKey(INVENTORY_ID)) {
                        String hdId = options.get(INVENTORY_ID);
                        if (hdId == null) hdId = "null";
                        statement += ",       (select sum(jad.cnt_fact) from jor_inventory_act_dt jad\n" +
                                "        where jad.hd_id = " + hdId.trim() + " and jad.goods_id = DG.ID ) as CNT_FACT\n" +
                                "from DIC_GOODS DG ";
                    } else {
                        statement += ",       (select sum(jad.cnt_fact) from jor_inventory_act_dt jad\n" +
                                "        where jad.hd_id = null and jad.goods_id = DG.ID ) as CNT_FACT\n" +
                                "from DIC_GOODS DG ";
                    }


                    boolean or = false;
                    boolean where = false;
                    boolean and = false;

                    if (!options.containsKey(ALL)) {
                        if (options.containsKey(NAME) || options.containsKey(ARTICLE) || options.containsKey(BARCODE)) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);

                            if (options.containsKey(NAME)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String name = options.get(NAME).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_name);
                                statement += ("'" + name + "'");
                                or = true;
                                and = true;
                            }

                            if (options.containsKey(ARTICLE)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String article = options.get(ARTICLE).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_article);
                                statement += ("'" + article + "'");
                                or = true;
                                and = true;
                            }

                            if (options.containsKey(BARCODE)) {
                                if (or) statement += getAppContext().getString(R.string.request_or);
                                String barcode = options.get(BARCODE).trim();
                                statement += getAppContext().getString(R.string.request_goods_by_barcode);
                                statement += ("'" + barcode + "')");
                                or = true;
                                and = true;
                            }
                        }
                    }
                    if (options.containsKey(GRP_ID)) {
                        if (!where) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);
                        } else statement += getAppContext().getString(R.string.request_and);
                        statement += "  DG.GRP_ID=" + options.get(GRP_ID);
                    }

                    if (options.containsKey(ID)) {
                        if (!where) {
                            where = true;
                            statement += getAppContext().getString(R.string.request_where);
                        } else statement += getAppContext().getString(R.string.request_and);
                        statement += "  DG.ID=" + options.get(ID);
                    }

                    if (!where) {
                        where = true;
                        statement += getAppContext().getString(R.string.request_where);
                    } else statement += getAppContext().getString(R.string.request_and);
                    statement += "  DG.is_service=0 order by DG.NAME"; //!!!!!!!!! проверка, что товар не является услугой

                    Log.d("getGoodsList", "goods get stm 1 =" + statement);
                    JSONArray jsonArray = executeStatement(statement);
                    Log.d("getGoodsList", "goods get stm 2=" + statement);
                    if (jsonArray != null) {
                        Log.d("my", "goods list not null size = " + jsonArray.length());
                        // Log.d("my", "goods list = " + jsonArray.toString());
                        if (jsonArray.length() > 0) {

                            int stoPrc = jsonArray.length();


                            int current_prc = -1;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                Log.d("getGoodsList", "for i=" + i);
                                try {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Gson gson = new Gson();
                                    GoodJSON good = gson.fromJson(obj.toString(), GoodJSON.class);
                                    if (good != null) {
                                        // Log.d("my","goods ===+++= ok"+goodJSON.getName());
                                        goodsList.add(good);

                                        int gid = good.getId();
                                        // int grp_id = 0;// good.getParent().getId();
                                        int grp_id = good.getGrp_id();
                                        String gname = good.getName();
                                        if (gname.contains("'")) gname = gname.replace("'", "''");
                                        String article = good.getArticle();
                                        if (article == null) article = "";
                                        if (article.contains("'"))
                                            article = article.replace("'", "''");
                                        String unit = good.getUnit();
                                        if (unit.contains("'")) unit = unit.replace("'", "''");
                                        String barcode = "";
                                        barcode = good.getBarcode();
                                        if (barcode == null) barcode = "";
                                        if (barcode.contains("'"))
                                            barcode = barcode.replace("'", "''");
                                        double out_price = good.getPrice();

                                        if (gname.contains("'")) gname = gname.replace("'", "''");

                                        // Log.d(my,"write good "+gname);


                                        MainApplication.dbHelper.writeGood(gid, grp_id, gname, article, unit, barcode, out_price);


                                        int x = i * 100 / stoPrc;

                                        if (x > current_prc) {
                                            current_prc = x;
                                            call1.onError("" + x);
                                        }

                                    } else Log.d("my", "goods =---err");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("my", "goods =---errtttt" + e.toString());
                                    call1.onError("error " + e.toString());
                                }

                            }

                        }
                    } else {
                        Log.d("my", "goods get is jsonArr null");
                    }
                } else {
                    Log.d(my, "Can't connect to server");
                }
                final Response response = new Response(goodsList);
                Log.d("my", "goods send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();


    }


    @Override
    public void getGoodsGrpList(Callback callback) {

        final Callback call1 = callback;


        Thread thread = new Thread() {
            @Override
            public void run() {
                ArrayList<GoodGrpJSON> goodsGrpList = new ArrayList<>();
                if (isConnect()) {
                    Log.d("my", "goods get is connect ok");
                    String statement = "select   dg.id,  dg.parent_id , dg.name from dic_goods_grp dg  order by dg.name";


                    JSONArray jsonArray = executeStatement(statement);
                    Log.d("my", "goods get stm =" + statement);
                    if (jsonArray != null) {
                        Log.d("my", "goods list not null size = " + jsonArray.length());
                        Log.d("my", "goods list = " + jsonArray.toString());
                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Gson gson = new Gson();
                                    GoodGrpJSON goodJSON = gson.fromJson(obj.toString(), GoodGrpJSON.class);
                                    if (goodJSON != null) {
                                        // Log.d("my","goods ===+++= ok"+goodJSON.getName());
                                        goodsGrpList.add(goodJSON);
                                    } else Log.d("my", "goods =---err");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("my", "goods =---errtttt" + e.toString());
                                }
                            }

                        }
                    } else {
                        Log.d("my", "goods get is jsonArr null");
                    }
                }
                final Response response = new Response(goodsGrpList);
                Log.d("my", "goods send resp...");
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });

            }
        };
        thread.start();


    }

    @Override
    @Deprecated
    public String createInventory(int id, String num, String datetime, String subdiv, int doc_type) {
        return null;
    }

    @Override
    public void sendEditInventoryDt(int invId, int goodId, float cnt, @NonNull Callback callback) {
        final Callback call1 = callback;
        final int invId1 = invId;
        final int goodId1 = goodId;
        final float cnt1 = cnt;

        Thread thread = new Thread() {
            @Override
            public void run() {

                int result1 = -1;
                if (isConnect()) {
                    String statement0 = getAppContext().getString(R.string.request_sys_connect);
                    int result0 = executeProcedure(statement0);
                    Log.d(my, "ready sys conn ==================== " + result0);

                    String statement = "execute procedure Z$INVENTORY_EDITOR_U( " + invId1 + "," + goodId1 + "," + cnt1 + ",0, null,null)";
                    if (MainApplication.isDB_3_0_68_0_plus) {
                        statement = "execute procedure Z$INVENTORY_EDITOR_U( " + invId1 + "," + goodId1 + "," + cnt1 + ",0, null,null, 1)";
                    }
                    result1 = executeProcedure(statement);

                    Log.d(my, "ready incert invdt ==================== " + result1);

                }
                final Response response = new Response(result1);

                h.post(new Runnable() {
                    @Override
                    public void run() {
                        call1.sendResult(response);
                    }
                });
            }
        };
        thread.start();


    }

    @Override
    public void sendEditInventoriesList(int invId, ArrayList<Good> goods, @NonNull Callback callback) {
        final Callback call1 = callback;
        final int invId1 = invId;
        final ArrayList<Good> accGoods = new ArrayList<>();
        accGoods.addAll(goods);


        Thread thread = new Thread() {
            @Override
            public void run() {

                int result1 = 0;
                if (isConnect()) {
                    String statement0 = getAppContext().getString(R.string.request_sys_connect);
                    int result0 = executeProcedure(statement0);
                    Log.d(my, "ready sys conn ==================== " + result0);

                    for (Good good : accGoods) {

                        String statement = "execute procedure Z$INVENTORY_EDITOR_U( " + invId1 + "," + good.getId() + "," + good.getFcnt() + ",0, null,null)";
                        result1 = executeProcedure(statement);
                        Log.d(my, "ready incert invdt ==================== " + result1);
                    }


                }
                final int result = result1;
                final Response response = new Response(result1);

                Log.d(my, "sendEditInventoriesList result = " + result1);


                h.post(new Runnable() {
                    @Override
                    public void run() {
                        if (result == -1) {
                            call1.onError("Error");
                        } else {
                            call1.sendResult(response);
                        }
                    }
                });
            }
        };
        thread.start();


    }


}
