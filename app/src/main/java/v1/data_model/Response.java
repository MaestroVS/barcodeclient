package v1.data_model;

/**
 * Created by MaestroVS on 19.12.2017.
 */

public class Response<T>  {

    private T body=null;

    public Response(T body){
        this.body=body;
    }


    public  T body() {
        return body;
    }
}
