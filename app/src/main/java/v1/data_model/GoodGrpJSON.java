package v1.data_model;

/**
 * Created by MaestroVS on 21.12.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GoodGrpJSON implements GoodIntf{

    @SerializedName("id")
    @Expose
    private Integer id=0;

    @SerializedName("parent_id")
    @Expose
    private int parent_id=0;

    @SerializedName("name")
    @Expose
    private String name="rootroot";


    public Integer getId() {
        return id;
    }

    public int getParentId() {
        return parent_id;
    }

    public String getName() {
        return name;
    }

    private boolean isParentDirectory=false;
    private boolean isRootDirectory=false;

    ArrayList<GoodGrpJSON> childGrpList = new ArrayList<>();

    ArrayList<GoodGrpJSON> brothersGrpList = new ArrayList<>();

    private GoodGrpJSON parent;

    public boolean isParentDirectory() {
        return isParentDirectory;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRootDirectory() {
        return isRootDirectory;
    }

    public void setRootDirectory(boolean rootDirectory) {
        isRootDirectory = rootDirectory;
    }

    public ArrayList<GoodGrpJSON> getChildGrpList() {
        return childGrpList;
    }

    public void addChildGrp(GoodGrpJSON grp)
    {
        childGrpList.add(grp);
        isParentDirectory=true;


    }

   /* public void addChildAndMeetWithBrothers(GoodGrpJSON child)
    {
       // Log.d(my,"addChildAndMeetWithBrothers");
        childGrpList.add(child);
        child.setParent(this); ///!!!!
        isParentDirectory=true;

        child.addBrother(child);
        if(childGrpList.size()==1){

        }else {
            child.addBrothersList(getChildGrpList());
           for(GoodGrpJSON otherChild:getChildGrpList())
           {
               otherChild.addBrother(child);
           }
        }


    }*/

   public void addChildren(GoodGrpJSON child)
   {
       isParentDirectory=true;
       childGrpList.add(child);
       child.setParent(this); ///!!!!
   }




    public void addChildGrpList(ArrayList<GoodGrpJSON> list)
    {
        childGrpList.addAll(list);
        isParentDirectory=true;
    }

    public void addBrother(GoodGrpJSON grp)
    {
        brothersGrpList.add(grp);
    }

    public void addBrothersList(ArrayList<GoodGrpJSON> list)
    {
        brothersGrpList.addAll(list);
    }

    public ArrayList<GoodGrpJSON> getBrothersList() {
        return brothersGrpList;
    }

    public GoodGrpJSON getParent() {
        return parent;
    }

    public void setParent(GoodGrpJSON parent) {
        this.parent = parent;
    }

    public String brothersToString(){
        if(brothersGrpList.size()==0) return "[]";
        String result="[";
        for(GoodGrpJSON bro: brothersGrpList)
        {
            result+=bro.getName();
            result+=",";
        }
        result=result.substring(0,result.length()-1);
        result+="]";
        return result;
    }

    public String childrenToString(){
        if(childGrpList.size()==0) return "[]";
        String result="[";
        for(GoodGrpJSON bro: childGrpList)
        {
            result+=bro.getName();
            result+=",";
        }
        result=result.substring(0,result.length()-1);
        result+="]";
        return result;
    }
}


