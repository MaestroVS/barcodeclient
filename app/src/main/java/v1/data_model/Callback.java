package v1.data_model;

/**
 * Created by MaestroVS on 19.12.2017.
 */

public interface Callback<T>  {

     boolean sendResult(Response<T> response);

     void onError(String error);


}
